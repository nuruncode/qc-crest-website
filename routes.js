module.exports = async () => {
  const contentful = require('contentful');

  const client = contentful.createClient({
    space: process.env.CF_SPACE_ID,
    accessToken: process.env.CF_DELIVERY_ACCESS_TOKEN,
    environment: process.env.CF_ENVIRONMENT
  });

  const pages = await client.getEntries({
    'content_type': 'page',
    'include': 10,
    'order': 'sys.updatedAt'
  });

  const products = await client.getEntries({
    'content_type': 'product',
    'include': 10
  });

  const collections = await client.getEntries({
    'content_type': 'collection',
    'include': 10
  });

  const articles = await client.getEntries({
    'content_type': 'article',
    'include': 10
  });

  let pagesRoutes = {};
  pages.items.forEach(page => {
    if (!page.fields.slug.fields.disabled) {
      let pagePath = page.fields.slug.fields.slug;
      pagesRoutes[`${pagePath}`] = { page: `${page.fields.pageMapping}` }
    }
  });

  products.items.forEach(product => {
    if (!product.fields.productOverview.fields.slug.fields.disabled) {
      let productPagePath = product.fields.productOverview.fields.slug.fields.slug;
      pagesRoutes[`${productPagePath}`] = { page : `${product.fields.productOverview.fields.pageMapping}` };
    }
  });

  collections.items.forEach(collection => {
    if (!collection.fields.slug.fields.disabled) {
      let collectionPagePath = collection.fields.slug.fields.slug;
      pagesRoutes[`${collectionPagePath}`] = { page : `${collection.fields.pageMapping}` }
    }
  });

  articles.items.forEach(article => {
    if (!article.fields.articleOverview.fields.slug.fields.disabled) {
      let articlePagePath = article.fields.articleOverview.fields.slug.fields.slug;
      pagesRoutes[`${articlePagePath}`] = { page : `${article.fields.articleOverview.fields.pageMapping}` };
    }
  });

  console.info(pagesRoutes);

  return {
    ...pagesRoutes
  };

};
