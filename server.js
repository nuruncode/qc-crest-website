// This file doesn't go through babel or webpack transformation.
// Make sure the syntax and sources this file requires are compatible with the current node version you are running
// See https://github.com/zeit/next.js/issues/1245 for discussions on Universal Webpack or universal Babel
const {createServer} = require('http');
const {parse} = require('url');
const next = require('next');
const path = require('path');
const fs = require('fs');
require("dotenv").config();
const getRoutes = require('./routes');
const dev = process.env.NODE_ENV !== 'production';
const port = process.env.PORT || 3000;
const app = next({dev});
const handle = app.getRequestHandler();
const routes = getRoutes();
const mimeTypes = {
  "html": "text/html",
  "jpeg": "image/jpeg",
  "jpg": "image/jpeg",
  "png": "image/png",
  "webp": "image/webp",
  "js": "text/javascript",
  "css": "text/css",
  "ico": "image/x-icon",
  "txt": "text/plain",
  "json": "application/json",
};

/**
 * Server Initialisation.
 */
app.prepare().then(() => {

  /**
   * Basic Request - Response Handling
   */
  createServer((req, res) => {
    const parsedUrl = parse(req.url, true);
    const {pathname, query} = parsedUrl;

    const serveStatic = (filePath, contentType) => {

      const filename = path.join(__dirname, filePath);

      fs.stat(filename, (err, stat) => {
        if (err) {
          res.writeHead(200, {'Content-Type': 'text/plain'});
          res.write('404 Not Found\n');
          res.end();
        }

        let mimeType = mimeTypes[path.extname(filename).split(".")[1]];
        res.writeHead(200, {'Content-Type': mimeType});

        let fileStream = fs.createReadStream(filename);
        fileStream.pipe(res);
      });
    };

    /**
     * Render the response
     */
    const route = routes[pathname];
    if (route) {
      return app.render(req, res, route.page, query)
    }

    switch (pathname) {
      // case '/sitemap.xml':
      //   serveStatic('/static/sitemap.xml', 'text/xml;charset=UTF-8');
      // case '/favicon.ico':
      //   serveStatic('/static/images/favicon.ico', 'image/x-icon');
      // case '/robots.txt':
      //   serveStatic('/config/robots/robots-preview.txt', 'text/plain;charset=UTF-8');
      default:
        handle(req, res, parsedUrl)
    }

  }).listen(port, err => {
    if (err) {
      throw err;
    }
    console.log('> Ready on http://localhost:' + port)
  })
});