import React, {useState, useCallback} from 'react';
import PropTypes from 'prop-types';
import Rotation from '../Rotation/Rotation';
import Heading from '../Heading/Heading';
import Image from '../Image/Image';
import Img from '../Image/Img/Img';
import ObLink from '../ObLink/ObLink';
import Icon from '../Icon/Icon';
import './ProductHighlights.scss';
import {getReviewAndRatingId} from "../UserQuoteBlock/UserQuoteBlock";
import RichText from '../RichText/RichText';

export default function ProductHighlights(props) {
    const {extraAttributes, isMobile, document} = props;
    const [selectedColorIndex, setSelectedColorIndex] = useState(0);
    const entity = extraAttributes.entity;
    const productOverview = entity.productOverview.fields;
    const productVariants = productOverview?.productVariants;
    const productHighlights = entity.productHighlights;
    const labels = document.fields;
    const pressQuote = productOverview.pressQuote.fields;
    const backgroundImage = document.fields.background;
    const startingAtButtonLabel = productOverview?.buyNowLabel?.fields.text;
    const anchorId = document.fields.anchorId;
    let reviewAndRatingId = getReviewAndRatingId(entity.productOverview);

    const renderHighlightsHeader = () => {
        return (
            <div className={`ob-product-highlights__header`}>
                <p className={`ob-product-highlights__surtitle`}>{productOverview.surtitle}</p>
                <Heading tag="h1" className={`ob-product-highlights__title`}>{productOverview.title}</Heading>
                <div data-bv-show="rating_summary" data-bv-product-id={reviewAndRatingId}></div>
                <div className={`ob-product-highlights__description`}>{productOverview.description}</div>
            </div>
        )
    }

    const renderColorButtons = () => {
        return (
            <div className={`ob-product-highlights__color-ctn`}>
                <p className={`ob-product-highlights__label`}>{labels.colorLabel}</p>
                <ul className={`ob-product-highlights__colors`}>
                    {productVariants.map((variant, index) => (
                        <li className="ob-product-highlights__selected-colors-item" key={index}>
                            <button
                            style={getColorStyle(variant.fields.color)}
                            className={`ob-product-highlights__color ${index === selectedColorIndex ? 'is-active' : ''}`}
                            onClick={() => setSelectedColorIndex(index)}>
                                <span className="visuallyhidden">{variant.fields.color.fields.name}</span>
                            </button>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }

    const [showMore, setShowMore] = useState(false);

    const highlightList = Object.keys(productHighlights).slice(0, 2).map(key => {
        return (
            <li key={key} className={`ob-product-highlights__highlights-list-item`}>
                <RichText document={productHighlights[key].fields.text} />
            </li>
        )}
    )

    const highlightListMore = Object.keys(productHighlights).slice(2).map(key => {
        return (
            <li key={key} className={`ob-product-highlights__highlights-list-item more ${showMore}`}>
                <RichText document={productHighlights[key].fields.text} />
            </li>
        )}
    )

    const handleShowMore = () => {
        setShowMore(!showMore);
    };

    const renderTextContent = () => {
        return (
            <div className={`ob-product-highlights__text-content`}>
                {!isMobile && renderHighlightsHeader()}
                {isMobile && renderColorButtons()}
                {pressQuote &&
                    <div className={`ob-product-highlights__quote`}>
                        <span className={`ob-product-highlights__quote-text`}>{pressQuote.quote}</span>
                        <Image image={pressQuote.pressLogo}/>
                    </div>
                }
                <div className={`ob-product-highlights__highlights-ctn`}>
                    <p className={`ob-product-highlights__label`}>{labels.highlightsLabel}</p>

                    { productHighlights && 
                        <ul className={`ob-product-highlights__highlights-list`}>
                            {highlightList}
                            { showMore &&
                                highlightListMore
                            }
                        </ul>
                    }
                    
                    { showMore ? (
                        <ObLink className="primaryGrey ob-product-highlights__see-more" tag='button' onClick={handleShowMore}>
                            <Icon name="minus6" roundedIcon="blue" viewBox={6}/>
                            SEE LESS
                            <span className="visuallyhidden"> highlights</span>
                        </ObLink>
                    ) : (
                        <ObLink className="primaryGrey ob-product-highlights__see-more" tag='button' onClick={handleShowMore}>
                            <Icon name="plus" roundedIcon="blue" viewBox={10}/>
                            SEE MORE
                            <span className="visuallyhidden"> highlights</span>
                        </ObLink>
                    )}
                </div>
                {!isMobile && renderColorButtons()}

                { startingAtButtonLabel &&
                  <button className={`ob-button`} onClick={event => props.onClickCallback(event)} data-skew={productOverview?.reviewAndRatingFamilyId}>{startingAtButtonLabel}</button>
                }
            </div>
        )
    }

    const render360Images = collection => {
        return (
            collection.map((image, index) => (
                <div key={`360-${index}`}>
                    <Img imageId={image.fields.assetId} alt={image.fields.alternateText} transformations={'w_700'} noLazyLoad={true} />
                </div>
            ))
        )
    }

    return (
        <div className={`ob-product-highlights`} id={anchorId}>
            <Image image={backgroundImage}>
                <div className={`ob-product-highlights__ctn`}>
                    {isMobile && renderHighlightsHeader()}
                    <ul className={`ob-product-highlights__img-ctn`}>
                        { productVariants ?
                        productVariants.map((variant, index) => (
                            <li className={`ob-product-highlights__img ${index === selectedColorIndex ? 'is-active' : ''}`} key={index}>
                                {variant.fields.backgroundsCollection ? (
                                    <div>
                                        <Rotation
                                            className={`ob-product-highlights__360-ctn`}
                                            tabIndex={0}
                                            autoPlay={25}
                                            playOnce={true}
                                            cycle={true} >
                                                {render360Images(variant.fields.backgroundsCollection)}
                                        </Rotation>
                                        <span className={`ob-product-highlights__360-icon`}>
                                            {/* Couldn't use the Icon component, because of the icon's unconventional viewbox */}
                                            <svg className={`ob-icon ob-icon--icon360`}
                                                 viewBox={`0 0 53 39`}
                                                 dangerouslySetInnerHTML={{ __html: `
                                                 <defs>
                                                    <path id="a" d="M0 .137h52.845V26H0z"/>
                                                    <path id="c" d="M.35.266h6.539V6.59H.35z"/>
                                                </defs>
                                                <g fill="none" fill-rule="evenodd">
                                                    <path d="M-5-12h63v63H-5z"/>
                                                    <path fill="#646464" d="M17.231 21.307l.111.586h.035l.118-.586 1.043-3.979h1.787l-2.157 6.34h-1.634l-2.157-6.34h1.788zM21.157 23.668h1.711v-6.34h-1.711v6.34zm0-7.857h1.711v-1.283h-1.711v1.283zM26.928 18.53a.892.892 0 00-.75.363c-.184.242-.297.56-.34.955l.018.03h2.097v-.153c0-.363-.085-.653-.254-.87-.17-.217-.427-.325-.77-.325m.151 5.262c-.914 0-1.644-.293-2.19-.88-.548-.585-.82-1.33-.82-2.232v-.234c0-.941.257-1.717.775-2.326.518-.61 1.212-.912 2.083-.908.855 0 1.52.257 1.992.773.473.516.71 1.213.71 2.092v.932h-3.786l-.012.035c.032.416.171.758.42 1.027.247.269.584.402 1.01.402.38 0 .693-.038.943-.114.25-.076.524-.196.821-.36l.463 1.057c-.258.212-.596.388-1.014.527a4.403 4.403 0 01-1.395.209M36.139 21.401h.035L37 17.329h1.606l-1.612 6.34h-1.429l-1.178-3.774h-.035l-1.178 3.774H31.75l-1.611-6.34h1.605l.838 4.06h.036l1.183-4.06h1.143zM20.241 4.826c0 1.448-.865 2.42-2.33 2.843 1.482.16 2.72 1.041 2.72 2.93 0 1.996-1.608 3.672-4.556 3.672-1.8 0-3.16-.67-4.096-1.8l1.5-1.465c.795.794 1.501 1.147 2.437 1.147 1.095 0 1.853-.636 1.853-1.73 0-1.254-.688-1.713-1.94-1.713h-.99l.3-1.924h.69c1.058 0 1.711-.565 1.711-1.554 0-.865-.617-1.377-1.589-1.377-.883 0-1.624.354-2.33 1.042l-1.377-1.5c1.112-1.06 2.436-1.607 4.025-1.607 2.577 0 3.972 1.36 3.972 3.036M27.953 9.929c0-1.536-.494-2.065-1.412-2.065-.636 0-1.236.335-1.748.935.035 2.489.46 3.478 1.66 3.478 1.006 0 1.5-.936 1.5-2.348m2.79-.035c0 2.435-1.66 4.378-4.326 4.378-3.16 0-4.52-2.33-4.52-5.755 0-4.043 1.96-6.727 5.191-6.727 1.13 0 2.03.335 2.72.794l-1.026 1.66c-.494-.283-1.006-.459-1.624-.459-1.342 0-2.207 1.165-2.365 3.266.741-.776 1.624-1.112 2.524-1.112 1.96 0 3.425 1.43 3.425 3.955M34.711 8.022c0 3.16.477 4.185 1.642 4.185 1.147 0 1.641-.954 1.641-4.185 0-3.248-.494-4.166-1.641-4.166-1.165 0-1.642 1.006-1.642 4.166m6.144 0c0 3.99-1.572 6.25-4.502 6.25-2.931 0-4.502-2.26-4.502-6.25 0-4.007 1.571-6.231 4.502-6.231 2.93 0 4.502 2.224 4.502 6.23"/><g transform="translate(0 13)"><mask id="b" fill="#fff"><use xlink:href="#a"/></mask><path fill="#646464" d="M36.715 14.74c6.448-1.688 10.217-4.569 10.217-7.916 0-1.377-.668-3.387-3.759-5.306V.137c6.085 2.442 9.672 5.957 9.672 9.548 0 3.433-3.23 6.778-8.882 9.22l-7.248-4.165zm-11.629 7.647l-.931-.066C10.385 21.367 0 15.934 0 9.686 0 6.453 2.866 3.297 7.923.9v1.948c-1.334 1.234-2.01 2.57-2.01 3.976 0 4.572 7.441 8.36 18.093 9.211l1.08.087v-3.704l11.82 6.791L25.086 26v-3.613z" mask="url(#b)"/>
                                                </g>
                                                    <g transform="translate(42)">
                                                        <mask id="d" fill="#fff"><use xlink:href="#c"/></mask>
                                                        <path fill="#646464" d="M2.288 3.446c0 .796.573 1.405 1.33 1.405.759 0 1.314-.61 1.314-1.423 0-.72-.463-1.422-1.313-1.422-.813 0-1.331.683-1.331 1.44m4.6-.04c0 1.85-1.526 3.184-3.29 3.184C1.707 6.59.352 5.191.352 3.47.33 1.685 1.79.265 3.62.265c1.936 0 3.269 1.442 3.269 3.141" mask="url(#d)"/>
                                                    </g>
                                                </g>
                                                 `}}
                                            />
                                        </span>
                                    </div>
                                ) : (
                                    <Image image={variant.fields.mainAsset} />
                                )}
                            </li>
                        )) : (
                            <li>
                                <Image image={productOverview.mainAsset} />
                            </li>
                        )}
                    </ul>
                    {!isMobile && renderTextContent()}
                </div>
            </Image>
            {isMobile && renderTextContent()}
        </div>
    )
}

// TODO MAKE THIS A HELPER METHOD WITH THE SERIES SELECTOR'S get
function getColorStyle(color) {
    const colorCode = color.fields.colorCode;
    const colorCodeTop = color.fields.colorCodeTop || colorCode;
    const colorCodeBottom = color.fields.colorCodeBottom || colorCode;
    const gradientStyle = color.fields.gradientStyle;

    switch (gradientStyle) {
    case 'linear':
        return {
            backgroundColor: colorCode,
            backgroundImage: `linear-gradient(to bottom, ${colorCodeTop} 0%, ${colorCodeBottom} 100%)`,
        };
    case 'highlightTop':
        return {
            backgroundColor: colorCode,
            backgroundImage: `radial-gradient(circle at center top, ${colorCodeTop} 0%, ${colorCodeBottom} 100%)`,
        };
    case 'highlightBottom':
        return {
            backgroundColor: colorCode,
            backgroundImage: `radial-gradient(circle at center bottom, ${colorCodeBottom} 0%, ${colorCodeTop} 100%)`,
        };
    default:
        return {
            backgroundColor: colorCode
        };
    }
}

ProductHighlights.propTypes = {
    extraAttributes: PropTypes.object,
    isMobile: PropTypes.bool,
    document: PropTypes.object,
    onClickCallback: PropTypes.func,
}
