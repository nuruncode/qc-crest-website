import React, {Component} from 'react'
import Block from '../../helpers/Block'

import {documentToHtmlString} from '@contentful/rich-text-html-renderer';
import PropTypes from "prop-types";

class RichText extends Component {

  constructor(props) {
    super(props);
    this.block = new Block(props);
  }

  render() {
    const document = this.block.getDocument();
    let className = this.props.className;
    let toConvert = document;
  
    if (this.block.getFields() && this.block.getFields().length > 0) {
      toConvert = this.block.getFieldValue('text');
      className = this.block.getFieldValue('classNames');
    }

    const html = documentToHtmlString(toConvert);

    return (
      <div className={className} dangerouslySetInnerHTML={{__html: html}} />
    );
  }
}

RichText.propTypes = {
  className: PropTypes.string,
  document: PropTypes.object,
};

export default RichText