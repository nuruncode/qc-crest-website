import React from 'react'
import RichText from "./RichText"

export default { title: 'Rich Text' };

const FAKE_RICH_TEXT = {
  content: [
    {
      content: [
        {
          data: {},
          marks: [
            {
              type: "bold"
            }
          ],
          value: "Some",
          nodeType: "text"
        },
        {
          data: {},
          marks: [],
          value: " rich text",
          nodeType: "text"
        }
      ],
      data: {},
      nodeType: "paragraph"
    }
  ],
  data: {},
  nodeType: "document"
}

const FAKE_DATA = {
  fields: {
    text: FAKE_RICH_TEXT,
    classNames: 'richtext'
  }
};

export const renderRichText = () => <RichText document={FAKE_DATA} />;
