import React, {Component} from 'react';
import throttle from 'lodash/throttle';
import SlideToggle from 'react-slide-toggle'
import Block from '../../helpers/Block'
import Icon from '../Icon/Icon'

import "./PageNavigation.scss"

class PageNavigation extends Component {

  constructor(props) {
    super(props);
    this.block = new Block(props);
    this.hasUnderline = this.block.getFieldValue('classNames').includes('page-navigation--underlined');

    // TODO: Change to actual content
    this.placeholderTitle = 'TITLE';

    this.state = {
      isMobile: false,
      isOpened: false,
      activeAnchor: ''
    };

    this.scrollTo = this.scrollTo.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResizeThrottled);
    this.handleResize();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResizeThrottled);
  }

  handleResize() {
    this.setState({
      isMobile: window.matchMedia("(max-width: 992px)").matches
    })
  }

  handleResizeThrottled = throttle(this.handleResize.bind(this), 100);

  buildAnchorForBlock(block, index) {
    const blockObj = new Block(block);
    const anchorId = blockObj.getAnchorId();
    const shortName = blockObj.getShortName();

    return (
      <li key={index} className={`ob-page-navigation__list-item`}>
        <a onClick={this.scrollTo}
          data-anchor-id={anchorId}
          className={`${this.state.activeAnchor === anchorId ? 'is-active' : ''} ${this.hasUnderline ? 'has-underline' : 'no-underline'} ob-page-navigation__list-link`}
          href={'#' + anchorId}
        >
          {shortName}
        </a>
      </li>
    )
  }

  scrollTo(event) {
    event.preventDefault();
    const currentTarget = event.currentTarget;
    const anchorId = currentTarget.dataset.anchorId
    const element = document.getElementById(anchorId);
    element.scrollIntoView({behavior: "smooth"});

    if (this.hasUnderline) {
      this.setState({'activeAnchor': anchorId});
    } else {
      this.setState({'activeAnchor': ''});
    }
  }

  render() {
    const className = 'ob-page-navigation ' + this.block.getFieldValue('classNames');

    return (this.state.isMobile ? (
      <SlideToggle collapsed={this.state.isMobile}
        onExpanding={() => {
          this.setState({isOpened: true})
        }}
        onCollapsing={() => {
          this.setState({isOpened: false})
        }}
      >
        {({ onToggle, setCollapsibleElement }) => (
          <div className={className}>
            <button className={`ob-page-navigation__title`} onClick={onToggle}>
              <span>{this.placeholderTitle}</span>
              <span className={`${this.state.isOpened ? 'is-open ' : ''}ob-page-navigation__icon`}>
                <Icon name='chevronDown' />
              </span>
            </button>

            <ul ref={setCollapsibleElement} className={`ob-page-navigation__list`}>
              {this.block.getFieldValue('blocks').map((block, index) => {
                  return this.buildAnchorForBlock(block, index);
              })}
            </ul>
          </div>
        )}
      </SlideToggle>
    ) : (
      <div className={className}>
        <ul className={`ob-page-navigation__list`}>
            {this.block.getFieldValue('blocks').map((block, index) => {
              return this.buildAnchorForBlock(block, index);
            })}
          </ul>
      </div>
    ));
  }
}

export default PageNavigation