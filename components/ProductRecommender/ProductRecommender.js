import React from 'react';
import PropTypes from 'prop-types';

import { ICloudinaryImage, IProductOverview } from '../../types';

import BodyText from '../BodyText/BodyText';
import Eyebrow from '../Eyebrow/Eyebrow';
import Heading from '../Heading/Heading';
import { ProductGrid } from '../ProductGrid/ProductGrid';

import './ProductRecommender.scss';

const IProductRecommenderProps = {
    contentType: PropTypes.oneOf(['productRecommenderBlock']),
    name: PropTypes.string,
    eyebrow: PropTypes.string,
    headline: PropTypes.string,
    bodyCopy: PropTypes.string,
    anchorId: PropTypes.string,
    switchTextAndProduct: PropTypes.bool,
    cta: PropTypes.string,
    ctaLink: PropTypes.string,
    singleColorBackground: PropTypes.string,
    backgroundAsset: ICloudinaryImage,
    mobileBackgroundAsset: ICloudinaryImage,
    productList: PropTypes.arrayOf(IProductOverview),
};

export default function ProductRecommenderBlock(props) {
    const fields = props.document?.fields;

    return fields ? ProductRecommender(fields) : null;
}

ProductRecommenderBlock.propTypes = {
    document: PropTypes.shape({
        fields: PropTypes.exact(IProductRecommenderProps),
    }),
};

export function ProductRecommender(props) {
    const { productList } = props;

    if (!productList) {
        return null;
    } else if (productList.length > 1) {
        return <MultipleProductRecommender {...props} />;
    } else {
        return <SingleProductRecommender {...props} />;
    }
}

ProductRecommender.propTypes = IProductRecommenderProps;

function SingleProductRecommender(props) {
    const { productList, anchorId, eyebrow, headline, bodyCopy, switchTextAndProduct } = props;

    return (
        <div className="ob-product-recommender show" id={anchorId}>
            <div className="ob-product-recommender__wrapper">
                <div className={`ob-product-recommender__side-by-side ${switchTextAndProduct ? 'ob-product-recommender__side-by-side--product-first' : ''}`}>
                    <div className="ob-product-recommender__side ob-product-recommender__side--text">
                        {!!eyebrow && (
                            <Eyebrow className="ob-product-recommender__side-eyebrow">
                                {eyebrow}
                            </Eyebrow>
                        )}

                        {!!headline && (
                            <Heading className="ob-product-recommender__side-heading" whiteText={false}>
                                {headline}
                            </Heading>
                        )}

                        {!!bodyCopy && (
                            <BodyText className="ob-product-recommender__side-body" whiteText={false}>
                                {bodyCopy}
                            </BodyText>
                        )}
                    </div>

                    <div className="ob-product-recommender__side ob-product-recommender__side--product">
                        <ProductGrid
                            products={productList}
                            itemsPerLine={1}
                            whiteText={false}
                            isCarrousel
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

SingleProductRecommender.propTypes = IProductRecommenderProps;

function MultipleProductRecommender(props) {
    const { productList, anchorId, eyebrow, headline, bodyCopy } = props;

    return (
        <div className="ob-product-recommender show" id={anchorId}>
            <div className="ob-product-recommender__wrapper">
                {!!eyebrow && (
                    <Eyebrow className="ob-product-recommender__eyebrow">
                        {eyebrow}
                    </Eyebrow>
                )}

                {!!headline && (
                    <Heading className="ob-product-recommender__heading" whiteText={false}>
                        {headline}
                    </Heading>
                )}

                {!!bodyCopy && (
                    <BodyText className="ob-product-recommender__body-text" whiteText={false}>
                        {bodyCopy}
                    </BodyText>
                )}

                {(Array.isArray(productList) && productList.length > 0) && (
                    <div className="ob-product-recommender__products">
                        <ProductGrid
                            products={productList}
                            itemsPerLine={productList.length > 3 ? productList.length : 3}
                            whiteText={false}
                            isCarrousel
                        />
                    </div>
                )}
            </div>
        </div>
    );
}

MultipleProductRecommender.propTypes = IProductRecommenderProps;
