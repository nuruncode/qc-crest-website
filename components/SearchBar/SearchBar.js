import React, { useState, useRef, useCallback} from 'react';
import './SearchBar.scss';
import PropTypes from 'prop-types';
import Icon from '../Icon/Icon';

const MOCK_AUTOCOMPLETE_SEARCH_RESULTS = [
    'Auto',
    'Automobile',
    'Autocar',
    'Autopatrouille',
    'Autoécole',
    'Automatique'
];

const getMockAutocompleteResults = (term) => {
    const resultsCount = Math.floor(Math.random() * (MOCK_AUTOCOMPLETE_SEARCH_RESULTS.length));
    return MOCK_AUTOCOMPLETE_SEARCH_RESULTS.slice(0, resultsCount + (term.length > 2 ? 0 : 1));
}

const MOCK_THROTTLE = 0.1;

const doMockAutocompleteSearch = (term, callback) => {

    let isCanceled = false

    const onCancel = () => {
       isCanceled = true;
    }

    const onTimeoutComplete = () => {
        if(!isCanceled) {
            callback(getMockAutocompleteResults(term))
        }
    }

    setTimeout(onTimeoutComplete, (Math.random() * MOCK_THROTTLE * 1000));

    return { cancel: onCancel };

}

export default function SearchBar({onSearch, placeholder}) {
    const [autoCompleteResults, setAutocompleteResults] = useState([]);
    const [isActive, setIsActive] = useState(false);
    const inputRef = useRef(null);
    const autoCompleteSearch = useRef();

    const handleReceiveAutoCompleteResult = useCallback(
        (results) => {
            autoCompleteSearch.current = undefined;
            setAutocompleteResults(results);
        },
        []
    );

    const closeAutocomplete = useCallback(
        () => {
            if (autoCompleteSearch.current) {
                autoCompleteSearch.current.cancel();
                autoCompleteSearch.current = undefined;
            }

            setIsActive(false);
            setAutocompleteResults([]);
        },
        []
    );

    const handleInput = useCallback(
        () => {
            setIsActive(true);

            const value = inputRef.current.value.trim();

            if (autoCompleteSearch.current) {
                autoCompleteSearch.current.cancel();
                autoCompleteSearch.current = undefined;
            }

            if (value === '') {
                setAutocompleteResults([]);
            } else {
                autoCompleteSearch.current = doMockAutocompleteSearch(value, handleReceiveAutoCompleteResult);
            }
        },
        []
    );

    const handleBlur = useCallback(
        () => {
            closeAutocomplete();
        },
        []
    );

    const handleAutocompleteResultClick = useCallback(
        (term) => {
            if (inputRef.current) {
                inputRef.current.blur();
                inputRef.current.value = term;
            }

            closeAutocomplete();
            onSearch(term);
        },
        [onSearch]
    );

    const handleMouseDown = useCallback(
        (event) => {
            event.preventDefault(); // keeps focus on input element
        },
        []
    );

    const handleSubmit = useCallback(
        (event) => {
            event.preventDefault();

            if (inputRef.current) {
                const term = inputRef.current.value.trim();

                inputRef.current.blur();
                closeAutocomplete();
                onSearch(term);
            }
        },
        [onSearch]
    );

    return (
        <form className="ob-search-bar" onSubmit={handleSubmit}>
            <input type="text" ref={inputRef} onInput={handleInput} placeholder={placeholder} onBlur={handleBlur} />

            <button type="submit" className="ob-search-bar-icon">
                <Icon name="search" />
            </button>

            {(isActive && autoCompleteResults.length > 0) && (
                <div role="list" onMouseDown={handleMouseDown} className="ob-search-bar-autocomplete">
                    <div className="ob-search-bar-autocomplete-title">
                        COMMON SEARCHES
                    </div>

                    <ul className="ob-search-bar-autocomplete-results">
                        {autoCompleteResults.map((autocompleteResult, i) => (
                            <li onClick={() => handleAutocompleteResultClick(autocompleteResult)} key={i}>{autocompleteResult}</li>
                        ))}
                    </ul>
                </div>
            )}
        </form>
    );
}

SearchBar.propTypes = {
    onSearch: PropTypes.func,
    placeholder: PropTypes.any,
}