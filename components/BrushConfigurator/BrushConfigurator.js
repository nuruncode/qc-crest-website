import C from 'classnames';
import throttle from 'lodash/throttle';
import React, { useCallback, useMemo, useRef, useState } from 'react';

import { uniqBy } from '../../helpers/Array';
import { useIsomorphicLayoutEffect } from '../../helpers/Hooks';

import Button from '../Button/Button';
import DotNav from '../DotNav/DotNav';
import Image from '../Image/Image';
import Label from '../Label/Label';
import { getColorStyle } from '../SeriesSelector/SeriesSelector'
import PropTypes from 'prop-types';

import '../Button/Button.scss'
import '../SeriesSelector/SeriesSelector.scss'
import './BrushConfigurator.scss';

export default function BrushConfigurator(props) {
    const {document: doc} = props;
    const collection = props.extraAttributes.entity;
    const seriesArray = collection.series;

    const scrollRef = useRef();

    const [selectedNeedsMap, setSelectedNeedsMap] = useState({});
    const [selectedFeaturesMap, setSelectedFeaturesMap] = useState({});
    const [currentMatchIndex, setCurrentMatchIndex] = useState(0);

    const titleLabel = doc.fields.title;
    const oralCareNeedsLabel = doc.fields.oralCareNeedsLabel;
    const selectFeaturesLabel = doc.fields.selectFeaturesLabel;
    const matchLabel = doc.fields.matchLabel;
    const matchDelimiterLabel = doc.fields.matchDelimiterLabel;

    const allProducts = useMemo(
        () => {
            const productArrays = seriesArray ? seriesArray.map(s => s.fields.products) : [];
            const products = [].concat(...productArrays);

            return uniqBy(products, p => p.fields.uniqueId);
        },
        [ seriesArray ]
    );

    const allNeeds = doc.fields.needs;
    const allFeatures = doc.fields.features;

    const selectedNeeds = useMemo(
        () => allNeeds.filter(need => selectedNeedsMap[ need.fields.name ]),
        [ allNeeds, selectedNeedsMap ]
    );

    const matchingProducts = useMemo(
        () => {
            const selectedNeedKeys = Object.keys(selectedNeedsMap);
            const selectedFeatureKeys = Object.keys(selectedFeaturesMap);

            if (selectedNeedKeys.length === 0 && selectedFeatureKeys.length === 0) {
                // no criteria selected, so there are no matching products to display
                return [];
            }

            // find all products that have every selected need and feature

            return allProducts.filter(product => {
                for (let i = 0; i < selectedNeedKeys.length; ++i) {
                    const selectedKey = selectedNeedKeys[i];

                    if (!product.fields.needs) {
                        return false;
                    }

                    if (!product.fields.needs.some(f => f.fields.name === selectedKey)) {
                        return false;
                    }
                }

                for (let i = 0; i < selectedFeatureKeys.length; ++i) {
                    const selectedKey = selectedFeatureKeys[i];

                    if (!product.fields.collectionFeatures) {
                        return false;
                    }

                    if (!product.fields.collectionFeatures.some(f => f.fields.name === selectedKey)) {
                        return false;
                    }
                }

                if(selectedFeatureKeys.length == 0 && selectedNeedKeys.length == 0) {
                    return false;
                } else {
                    return true;
                }
            });
        },
        [ allProducts, selectedNeedsMap, selectedFeaturesMap ]
    );

    const handleNeedToggle = useCallback(
        (event) => {
            const key = event.currentTarget.getAttribute('data-name');

            setSelectedNeedsMap(prevSelections => toggleSelection(prevSelections, key));

            if (scrollRef.current) {
                scrollRef.current.scrollLeft = 0;
            }
        },
        []
    );

    const handleFeatureToggle = useCallback(
        (event) => {
            const key = event.currentTarget.getAttribute('data-name');

            setSelectedFeaturesMap(prevSelections => toggleSelection(prevSelections, key));

            if (scrollRef.current) {
                scrollRef.current.scrollLeft = 0;
            }
        },
        []
    );

    const updateCurrentMatchIndex = useCallback(
        () => {
            if (scrollRef.current) {
                setCurrentMatchIndex(findCentermostChildNodeIndex(scrollRef.current));
            }
        },
        []
    );

    const handleScroll = useMemo(
        () => throttle(updateCurrentMatchIndex, 100),
        [ updateCurrentMatchIndex ]
    );

    const handleMatchNavClick = useCallback(
        (index) => {
            const items = scrollRef.current?.querySelectorAll('.ob-brush-configurator__matches-item');

            // Workaround Chrome bug where the horizontal scroll is aborted if
            // the vertical scroll is already at the target position
            window.scrollTo(window.scrollX, Math.round(window.scrollY) + 1);

            items?.[index]?.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'center' });
        },
        []
    );

    useIsomorphicLayoutEffect(
        () => {
            updateCurrentMatchIndex();

            return () => {
                handleScroll.cancel();
            };
        },
        [ updateCurrentMatchIndex, handleScroll ]
    );

    return (
        <div className="ob-brush-configurator">
            {!!titleLabel && (
                <div className="ob-brush-configurator__top">
                    <h1 className="ob-brush-configurator__title">
                        {titleLabel.fields.text}
                    </h1>
                </div>
            )}

            <div className="ob-brush-configurator__separator" />

            <div className="ob-brush-configurator__needs">
                {!!oralCareNeedsLabel && (
                    <div className="ob-brush-configurator__needs-top">
                        <h2 className="ob-brush-configurator__subtitle">
                            {oralCareNeedsLabel.fields.text}
                        </h2>
                    </div>
                )}

                <ul className="ob-brush-configurator__needs-list">
                    {!!allNeeds && allNeeds.map((need, i) => {
                        const active = selectedNeedsMap[need.fields.name];

                        return (
                            <li className="ob-brush-configurator__needs-item" key={i}>
                                <button
                                    className="ob-brush-configurator__toggle ob-outline-button"
                                    type="button"
                                    role="switch"
                                    data-name={need.fields.name}
                                    aria-checked={active ? 'true' : 'false'}
                                    onClick={handleNeedToggle}
                                >
                                    {need.fields.name}
                                </button>
                            </li>
                        );
                    })}
                </ul>
            </div>

            <div className="ob-brush-configurator__features">
                {!!selectFeaturesLabel && (
                    <div className="ob-brush-configurator__features-top">
                        <h2 className="ob-brush-configurator__subtitle">
                            {selectFeaturesLabel.fields.text}
                        </h2>
                    </div>
                )}

                <ul className="ob-brush-configurator__features-list">
                    {!!allFeatures && allFeatures.map((feature, i) => {
                        const active = selectedFeaturesMap[feature.fields.name];

                        return (
                            <li className="ob-brush-configurator__features-item" key={i}>
                                <button
                                    className="ob-brush-configurator__toggle ob-outline-button"
                                    type="button"
                                    role="switch"
                                    data-name={feature.fields.name}
                                    aria-checked={active ? 'true' : 'false'}
                                    onClick={handleFeatureToggle}
                                >
                                    <div className="ob-brush-configurator__toggle-inner">
                                        <span className={`ob-brush-configurator__toggle-icon ${feature.fields.logoId ? 'icon-' + feature.fields.logoId : ''}`} aria-hidden="true" />

                                        <span className="ob-brush-configurator__toggle-text">
                                            {feature.fields.title}
                                        </span>
                                    </div>
                                </button>
                            </li>
                        );
                    })}
                </ul>
            </div>

            {(matchingProducts.length > 0) && (
                <div className="ob-brush-configurator__matches">
                    {!!(matchLabel && matchDelimiterLabel && matchingProducts.length > 0) && (
                        <div className="ob-brush-configurator__matches-top">
                            <h2 className="ob-brush-configurator__subtitle">
                                <Label label={matchLabel} tokens={{currentIndex: currentMatchIndex + 1, totalResults: matchingProducts.length}}/>
                            </h2>
                        </div>
                    )}

                    <ul className="ob-brush-configurator__matches-list" ref={scrollRef} onScroll={handleScroll}>
                        {matchingProducts.map((product, i) => (
                            <li className="ob-brush-configurator__matches-item" key={product.uniqueId || i}>
                                <BrushConfiguratorProduct document={doc} product={product} needs={selectedNeeds} />
                            </li>
                        ))}
                    </ul>

                    {(matchingProducts.length > 1) && (
                        <DotNav className="ob-brush-configurator__matches-nav"
                            count={matchingProducts.length}
                            current={currentMatchIndex}
                            color="blue"
                            onClick={handleMatchNavClick}
                        />
                    )}
                </div>
            )}
        </div>
    );
}

BrushConfigurator.propTypes = {
    document: PropTypes.any,
    extraAttributes: PropTypes.any,
}

function BrushConfiguratorProduct(props) {
    const {document: doc, product, needs} = props;

    const brushSelectionLabel = doc.fields.brushSelectionLabel;
    const handleColorLabel = doc.fields.handleColorLabel;
    const inspiredLabel = doc.fields.inspiredLabel;

    const [selectedVariantIndex, setSelectedVariant] = useState(0);

    const productVariants = product.fields.productVariants;
    const productVariant = productVariants?.[selectedVariantIndex];

    const productImage = productVariant?.fields?.mainAsset || product.fields.mainAsset;

    const buyNowLabel = product.fields.buyNowLabel;

    const features = useMemo(
        () => {
            const a = [];

            if (needs && needs.length > 0) {
                const shortNames = needs.map(need => need.fields.shortName || need.fields.name);

                a.push({
                    fields: {
                        logoId: 'tooth',
                        description: <Label label={inspiredLabel} tokens={{needs:shortNames.join(', ')}}/>,
                    }
                });
            }

            if (product.fields.collectionFeatures) {
                a.push(...product.fields.collectionFeatures);
            }

            if (a.length > 6) {
                a.length = 6;
            }

            return a;
        },
        [ needs, product.fields.collectionFeatures ]
    );

    return (
        <div className="ob-brush-configurator-product">
            <div className="ob-brush-configurator-product__box">
                <div className="ob-brush-configurator-product__top">
                    <div className="ob-brush-configurator-product__pic">
                        {!!productImage && (
                            <Image className="ob-brush-configurator-product__pic-image"
                                image={productImage}
                            />
                        )}
                    </div>

                    <div className="ob-brush-configurator-product__info">
                        <div className="ob-brush-configurator-product__info-start">
                            <h2 className="ob-brush-configurator-product__info-name">
                                {product.fields.name}
                            </h2>

                            <p className="ob-brush-configurator-product__info-surtitle">
                                {product.fields.description}
                            </p>

                            {!!(productVariants && productVariants.length > 0) && (
                                <div className="ob-brush-configurator-product__colors">
                                    <h3 className="ob-brush-configurator-product__colors-subheading">
                                        {handleColorLabel.fields.text}
                                    </h3>

                                    <ul className="ob-brush-configurator-product__colors-list">
                                        {productVariants.map((variant, i) => {
                                            const color = variant.fields.color;

                                            return (
                                                <li className="ob-brush-configurator-product__colors-item" key={i}>
                                                    <button
                                                        type="button"
                                                        className={C('ob-series__color', {
                                                            ['ob-series__color--large-mobile']: productVariants.length <= 2,
                                                            ['ob-series__color--active']: i === selectedVariantIndex
                                                        })}
                                                        style={getColorStyle(color)}
                                                        onClick={() => setSelectedVariant(i)}
                                                    >
                                                        {color.fields.name}
                                                    </button>
                                                </li>
                                            );
                                        })}
                                    </ul>
                                </div>
                            )}
                        </div>

                        <div className="ob-brush-configurator-product__info-end">
                            <a className="ob-brush-configurator-product__link" href={product.fields.slug.fields.slug} 
                                dangerouslySetInnerHTML={{__html: product.fields.title}}>
                            </a>

                            {!!buyNowLabel && (
                                <div className="ob-brush-configurator-product__buy">
                                    <Button className="ob-brush-configurator-product__buy-btn" tag="a" href={product.fields.slug.fields.slug}>
                                        {buyNowLabel.fields.text}
                                    </Button>
                                </div>
                            )}
                        </div>
                    </div>
                </div>

                {(features.length > 0) && (
                    <div className="ob-brush-configurator-product__features">
                        {!!brushSelectionLabel && (
                            <h3 className="ob-brush-configurator-product__features-heading">
                                {brushSelectionLabel.fields.text}
                            </h3>
                        )}

                        <ul className="ob-brush-configurator-product__features-list">
                            {features.map((feature, i) => (
                                <li className="ob-brush-configurator-product__features-item" key={i}>
                                    <span className={`ob-brush-configurator-product__features-icon ${feature.fields.logoId ? 'icon-' + feature.fields.logoId : ''}`} aria-hidden="true" />

                                    <span className="ob-brush-configurator-product__features-text">
                                        {feature.fields.description}
                                    </span>
                                </li>
                            ))}
                        </ul>
                    </div>
                )}
            </div>
        </div>
    );
}

BrushConfiguratorProduct.propTypes = {
    document: PropTypes.any,
    product: PropTypes.any,
    needs: PropTypes.any,
}


function toggleSelection(prevSelections, key) {
    const selections = { ...prevSelections };

    if (selections[key]) {
        delete selections[key];
    }
    else {
        selections[key] = true;
    }

    return selections;
}

function findCentermostChildNodeIndex(parentNode) {
    const itemNodes = parentNode.childNodes;
    const originRect = parentNode.getBoundingClientRect();
    const originLeft = originRect.width * 0.5 + originRect.left;

    let currentIndex = 0;
    let currentDistance = Infinity;

    // find item closest to center of container
    for (let i = 0; i < itemNodes.length; ++i) {
        const itemRect = itemNodes[i].getBoundingClientRect();
        const itemLeft = itemRect.width * 0.5 + itemRect.left;
        const itemDistance = Math.abs(itemLeft - originLeft);

        if (itemDistance < currentDistance) {
            currentIndex = i;
            currentDistance = itemDistance;
        }
    }

    return currentIndex;
}