import React, {Component} from 'react';
import Block from '../../helpers/Block'
import HierarchyUtils from "../../helpers/HierarchyUtils";
import { calculateHeaderHeight } from "../../helpers/Utils"
import SlideToggle from "react-slide-toggle";
import throttle from 'lodash/throttle';
import Icon from "../Icon/Icon";
import "./MainMenu.scss";
import Image from "../Image/Image";

class MainMenu extends Component {

    _closedSubNavState = '';
    _desktopBreakpoint = '(max-width: 991px)';
    constructor(props) {
        super(props);
        this.block = new Block(props);
        this.insideMenuRef = React.createRef();
        this.subMenuRef = React.createRef();
        this.mobileMenuRef = React.createRef();
        this.openMobileMenuRef = React.createRef();
        this.innerMenuRef = React.createRef();
        this.primaryColor = '#0057B8';

        this.state = {
            isMainMenuOpen: false,
            currentSubNav: this._closedSubNavState,
            intervalId: 0,
            isMobile: true,
            lastScroll: 0
        };

        this.handleToggleSubNavigation = this.handleToggleSubNavigation.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.handleToggleMainMenu = this.handleToggleMainMenu.bind(this);
        this.handleMenuHeight = this.handleMenuHeight.bind(this);
        this.toggleScroll = this.toggleScroll.bind(this);
    }

    componentDidMount() {
        window.addEventListener("mousedown", this.handleClickOutside);
        window.addEventListener("resize", this.handleResizeThrottled);
        window.addEventListener("scroll", this.handleScrollThrottled);
        this.handleScroll();
        this.handleResize();
    }
    
    componentWillUnmount() {
        window.removeEventListener("mousedown", this.handleClickOutside);
        window.removeEventListener("resize", this.handleResizeThrottled);
        window.removeEventListener("scroll", this.handleScrollThrottled);
    }

    handleResize() {
        if (this.openMobileMenuRef?.current && this.state.isMainMenuOpen) {
            this.openMobileMenuRef.current.onToggle();
        }

        this.setState({
            isMobile: window.matchMedia(this._desktopBreakpoint).matches,
            currentSubNav: this._closedSubNavState,
            isMainMenuOpen: false,
        },() => {
            this.toggleScroll();
            calculateHeaderHeight();
        });
    }

    handleScroll() {
        if (!document.body.classList.contains('noScroll')) {
            this.setState({
                lastScroll: window.pageYOffset
            })
        }
    }

    toggleScroll() {
        if (this.state.isMainMenuOpen || this.state.currentSubNav !== this._closedSubNavState) {
            document.body.classList.add('noScroll');
            document.body.style.top = `-${this.state.lastScroll}px`;
            document.body.style.height = `-${this.state.lastScroll}px`;
        } else {
            document.body.classList.remove('noScroll');
            window.scrollTo(0, this.state.lastScroll);
        }
    }

    handleResizeThrottled = throttle(this.handleResize.bind(this), 100);

    handleScrollThrottled = throttle(this.handleScroll.bind(this), 100);

    handleClickOutside(event) {
        if (this.insideMenuRef.current) {
            if (!this.insideMenuRef.current.contains(event.target)) {
                this.setState({currentSubNav: this._closedSubNavState});
            }
        }
    }

    handleToggleSubNavigation(event) {
        const targetElemId = event.currentTarget.dataset.childId;

        if (targetElemId) {
            if (this.state.currentSubNav === targetElemId) {
                this.setState({currentSubNav: this._closedSubNavState}, this.toggleScroll);
            } else {
                this.setState({currentSubNav: targetElemId}, () => {
                    this.toggleScroll();
                    const subNav = this.subMenuRef.current;
                    if (this.mobileMenuRef?.current) {
                        const announcementBar = document.querySelector('.ob-announcement-bar');
                        if (announcementBar && announcementBar.style.display !== 'none') {
                            subNav.style.height = `calc(${window.innerHeight}px - ${subNav.offsetTop + announcementBar.offsetHeight}px)`;
                        } else {
                            subNav.style.height = `calc(${window.innerHeight}px - ${subNav.offsetTop}px)`;
                        }
                    } else {
                        const header = subNav.offsetParent.offsetParent;
                        subNav.style.height = `calc(${window.innerHeight}px - ${header.offsetHeight}px)`;
                    }
                });
            }
        } else {
            this.setState({currentSubNav: this._closedSubNavState}, this.toggleScroll);
        }
    }

    handleToggleMainMenu() {
        this.setState({
            isMainMenuOpen: !this.state.isMainMenuOpen,
            currentSubNav: this._closedSubNavState,
        }, this.toggleScroll);
    }

    handleMenuHeight() {
        const innerMenu = this.innerMenuRef.current.querySelector('.ob-menu-collapsible');
        const announcementBar = document.querySelector('.ob-announcement-bar');

        if (announcementBar && announcementBar.style.display !== 'none') {
            innerMenu.style.height = `calc(${window.innerHeight}px - ${innerMenu.offsetTop + announcementBar.offsetHeight}px)`;
        } else {
            innerMenu.style.height = `calc(${window.innerHeight}px - ${innerMenu.offsetTop}px)`;
        }
    }

    renderCloseSubNavBtn() {
        return (
            <li className={`ob-main-menu__subNav--close-btn`}>
                <button onClick={ event => { this.handleToggleSubNavigation(event) } }>
                    <Icon name="close" size={3} color={this.primaryColor} />
                    <span className={`visuallyhidden`}>Close</span>
                </button>
            </li>
        )
    }

    renderHierarchyTitle(hierarchy, title, callback = null) {
        const level = HierarchyUtils.getLevel(hierarchy);
        const childId = HierarchyUtils.getHierarchyId(hierarchy);
        const type = hierarchy.fields.type;

        if (level === 'child'
            && type !== 'product-by-need'
            && type !== 'product-by-type'
            && type !== 'why-oral-b'
            && type !== 'conditions'
            && type !== 'life-stages'  ) {
            return (
                <button className={`ob-main-menu__subNav--btn link-button${this.state.currentSubNav === childId ? ' is-active' : ''}`}
                        data-child-id={childId}
                        onClick={ (event) => { this.handleToggleSubNavigation(event) } }
                >
                    {this.state.isMobile && hierarchy.fields.mainAsset ? (
                        <span className={`ob-main-menu__subNav--btn-ctn`}>
                            <span className={`ob-main-menu__subNav--btn-img`}>
                                <Image image={hierarchy.fields.mainAsset} />
                            </span>
                            <span>
                                <span className={`ob-main-menu__subNav--btn-title`} dangerouslySetInnerHTML={{__html: title}}></span>
                                <span className={`ob-main-menu__subNav--btn-icon`}>
                                    <Icon name="chevronRight" size="1.3" color={this.primaryColor} />
                                </span>
                            </span>
                        </span>
                    ) : (
                        <span>
                            <span className={`ob-main-menu__subNav--btn-title`} dangerouslySetInnerHTML={{__html: title}}></span>
                            <span className={`ob-main-menu__subNav--btn-icon`}>
                                {this.state.isMobile ? (
                                    <Icon name="wideChevronDown" size="0.8" rotate={-90} color="#3D3D41" />
                                    ) : (
                                        <Icon name="chevronDown" size="1.3" rotate={90} color={this.primaryColor} />
                                    )}
                            </span>
                        </span>
                    )
                    }
                </button>
            );
        } else {
            return (
                <div>
                    <p className={`ob-sub-navigation__title ob-sub-navigation__title--desktop`} dangerouslySetInnerHTML={{__html: title}}></p>
                    <button onClick={callback} className={`ob-sub-navigation__title ob-sub-navigation__title--mobile`}>
                        <span dangerouslySetInnerHTML={{__html: title}}></span>
                        <span>
                            <Icon name="plus" viewBox={10} size="1" color="#000000"  />
                        </span>
                    </button>
                </div>
            );
        }
    }

    renderHierarchy(hierarchy) {
        const destinations = hierarchy.fields.destinations;
        const title = hierarchy.fields.title;
        const hierarchyId = HierarchyUtils.getHierarchyId(hierarchy);

        const isSubNav = hierarchy.fields.type === 'product-by-need' || hierarchy.fields.type === 'product-by-type' || hierarchy.fields.type === 'why-oral-b' || hierarchy.fields.type === 'conditions' || hierarchy.fields.type === 'life-stages';
        const isMainMenu = hierarchyId === 'Hierarchy---Main-Menu';
        const isSubNavCtn = !isMainMenu && !isSubNav;

        let itemClassNames = !isSubNav ? 'ob-main-menu__list--item' : 'ob-sub-navigation--item';
        let mainMenuClassNames = isMainMenu ? 'ob-main-menu' : '';
        let subNavigationListClassNames = isSubNav ? 'ob-sub-navigation__list' : '';
        let iconPosition = null;
        const mainMenuListClassNames = isSubNavCtn ? 'ob-main-menu__subNav--ctn' : '';
        const listClassNames = `ob-main-menu__list ${mainMenuListClassNames}`;
        const onlyShowOnClassNames = hierarchy.fields.onlyShowOn ? `ob-main-menu__list--${hierarchy.fields.onlyShowOn}` : '';
        if (isSubNavCtn) {
            itemClassNames = ' ob-sub-navigation';
            iconPosition = 'after';
        }

        const renderMobileHierarchy = () => {
            return (
                <div className={`ob-main-menu--mobile`}>
                    <SlideToggle
                        duration={500}
                        collapsed={isMainMenu && !this.state.isMainMenuOpen}
                        ref={this.openMobileMenuRef}
                        range
                        onExpanding={() => {
                            this.handleToggleMainMenu();
                        }}
                        onCollapsing={() => {
                            this.handleToggleMainMenu();
                        }}
                        onExpanded={() => {
                            this.handleMenuHeight();
                        }}
                        onCollapsed={() => {
                            calculateHeaderHeight();
                        }}> 
                        {({ onToggle, setCollapsibleElement, range }) => (
                            <div ref={this.innerMenuRef} className={`${mainMenuClassNames} ${onlyShowOnClassNames} ${subNavigationListClassNames}`}>
                                {this.state.isMainMenuOpen && isMainMenu && this.state.currentSubNav !== this._closedSubNavState &&
                                <button className={`ob-main-menu__return-btn`} onClick={ event => { this.handleToggleSubNavigation(event) } }>
                                    <Icon name="arrowLeft" size="2" />
                                    <span className={`visuallyhidden`}>Return to main menu</span>
                                </button>
                                }
                                {isMainMenu &&
                                <div className={`ob-main-menu__logo ob-main-menu__logo--mobile`}>
                                    {HierarchyUtils.renderDestination(hierarchy.fields.destinations[0],() => this.renderHierarchy(hierarchy.fields.destinations[0]))}
                                </div>
                                }
                                {isMainMenu &&
                                    <div className={`ob-main-menu__open-ctn`}>
                                        <button onClick={onToggle}>
                                            {this.state.isMainMenuOpen ? (
                                            <span>
                                                <Icon name="close" size="2" color={this.primaryColor}/>
                                                <span className={`visuallyhidden`}>Close Menu</span>
                                            </span>
                                            ) : (
                                            <span>
                                                <Icon name="hamburger" size="2" color={this.primaryColor}/>
                                                <span className={`visuallyhidden`}>Open Menu</span>
                                            </span>
                                        )}
                                        </button>
                                    </div>
                                }
                                {title && !destinations && this.renderHierarchyTitle(hierarchy, title)}
                                {destinations && !isSubNav &&
                                <div ref={setCollapsibleElement} className={`ob-menu-collapsible`}>
                                    {title && this.renderHierarchyTitle(hierarchy, title)}
                                    <div ref={isMainMenu && this.mobileMenuRef}>
                                        <ul ref={isSubNavCtn && this.state.currentSubNav === hierarchyId && this.subMenuRef}
                                            id={hierarchyId}
                                            className={`${listClassNames}${this.state.currentSubNav === hierarchyId ? ' is-active' : ''}`}
                                            style={range ? { opacity: Math.max(0, range) } : { opacity: 1 }}>
                                            {
                                                renderDestination('chevronRight', iconPosition)
                                            }
                                        </ul>
                                    </div>
                                </div>
                                }
                                {destinations && isSubNav &&
                                <SlideToggle
                                    duration={500}
                                    collapsed={hierarchyId !== 'Hierarchy---More-Products---By-Type'}>
                                    {({ onToggle, setCollapsibleElement, range }) => (
                                        <div>
                                            {title && this.renderHierarchyTitle(hierarchy, title, onToggle)}
                                            <ul className={`ob-sub-navigation__list`} ref={setCollapsibleElement} style={range ? { opacity: Math.max(0, range) } : { opacity: 1 }}>
                                                {
                                                    renderDestination('chevronRight')
                                                }
                                            </ul>
                                        </div>
                                    )}
                                </SlideToggle>
                                }
                            </div>
                        )}
                    </SlideToggle>
                </div>
            );
        };

        const renderDesktopHierarchy = () => {
            return (
                <div className={`${mainMenuClassNames} ${onlyShowOnClassNames} ${subNavigationListClassNames}`}>
                    {title && !destinations && this.renderHierarchyTitle(hierarchy, title)}
                    {destinations &&
                    <div className={isMainMenu ? 'ob-main-menu__ctn' : ''}>
                        {isMainMenu && destinations &&
                            <div className={`ob-main-menu__logo ob-main-menu__logo--desktop`}>
                                {HierarchyUtils.renderDestination(hierarchy.fields.destinations[0],() => this.renderHierarchy(hierarchy.fields.destinations[0]))}
                            </div>
                        }
                        {title && this.renderHierarchyTitle(hierarchy, title)}
                        <ul ref={isSubNavCtn && this.state.currentSubNav === hierarchyId && this.subMenuRef}
                            id={hierarchyId}
                            className={`${listClassNames}${this.state.currentSubNav === hierarchyId ? ' is-active' : ''}`}>
                            { destinations.map((dest, index) => (
                                <li className={`${itemClassNames} ${!dest.fields.image && !isMainMenu ? 'no-img' : ''}`} key={index} data-is-logo={isMainMenu && index === 0} data-is-language-selector={dest.fields.contentType === 'languageSelectionBlock'}>
                                    {
                                        dest.fields.image && !isMainMenu ?
                                            HierarchyUtils.renderDestination(dest, () => this.renderHierarchy(dest), {name: 'ovalArrowRight', position : 'before'}) :
                                            HierarchyUtils.renderDestination(dest, () => this.renderHierarchy(dest))
                                    }
                                </li>
                            )) }
                            {isSubNavCtn && this.state.currentSubNav === hierarchyId &&
                                this.renderCloseSubNavBtn()
                            }
                        </ul>
                    </div>
                    }
                    {isMainMenu &&
                    <div>
                        <div className={`ob-main-menu__search--ctn`}>
                            <button data-child-id="ob-main-menu__search--subNav" onClick={ (event) => { this.handleToggleSubNavigation(event) } }>
                                <Icon name="search" size='1.6' color={this.primaryColor} />
                                <span className={`visuallyhidden`}>Search</span>
                            </button>
                            <div className={`ob-main-menu__subNav--ctn${this.state.currentSubNav === 'ob-main-menu__search--subNav' ? ' is-active' : ''}`}
                                 id="ob-main-menu__search--subNav">
                                <div>
                                    <h2>Search</h2>
                                    <input type="text" />
                                </div>
                            </div>
                        </div>
                    </div>
                    }
                </div>
            )
        };

        const renderDestination = (iconName = '', iconPosition = '') => {
            const icon = {
                name: iconName,
                position: iconPosition,
            };

            return destinations.map((dest, index) => (
                <li className={`${itemClassNames} ${!dest.fields.image && !dest.fields.mainAsset ? 'no-img' : ''}`} key={index} data-is-logo={isMainMenu && index === 0}>
                    {HierarchyUtils.renderDestination(dest, () => this.renderHierarchy(dest), icon)}
                </li>
            ))
        };

        return this.state.isMobile ? renderMobileHierarchy() : renderDesktopHierarchy();
    }

    render() {
        const className = `main-menu ${this.block.getFieldValue('classNames') !== undefined ? this.block.getFieldValue('classNames') : ''}`;
        const hierarchyDoc = this.block.getFieldValue('hierarchy');
        const hierarchyImage = hierarchyDoc.fields.mainAsset;

        return (
            <nav aria-label='Main menu' ref={this.insideMenuRef} className={`${className} ${this.state.isMainMenuOpen ? 'is-active' : ''}`}>
                {this.renderHierarchy(hierarchyDoc)}
                <Image image={hierarchyImage}></Image>
            </nav>
        )
    }
}

export default MainMenu