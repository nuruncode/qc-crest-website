import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import LazyLoad from 'react-lazyload';
import {Parallax} from 'react-scroll-parallax'
import throttle from 'lodash/throttle';

import ImageFactory from '../../../adapters/cloudinary/ImageFactory';
import { useIsomorphicLayoutEffect } from '../../../helpers/Hooks';
import { roundUpTheSize } from '../BackgroundImage/BackgroundImage.helpers'
import { withController } from 'react-scroll-parallax';

const Img = (props) => {

    const {
        imageId,
        elemId,
        alt,
        transformations,
        customClassName,
        alignmentClassName,
        format,
        noLazyLoad,
        resetImage
    } = props;

    const nodeRef = useRef(null);
    const nodeImg = useRef(null);
    const [width, setWidth] = useState(0);
    const [resetImageAsset] = useState(false);

    let classname= 'ob-mainAsset-wrapper';
    if (alignmentClassName) classname += ` ${alignmentClassName}`;
    if (customClassName) classname += ` ${customClassName}`;

    function updateWidth() {
        if (nodeRef.current) {
            setWidth(roundUpTheSize(nodeRef.current.getBoundingClientRect().width));
        }
    }

    useEffect(
        () => {
            const handleResize = throttle(updateWidth, 100);

            window.addEventListener('resize', handleResize);

            return () => {
                window.removeEventListener('resize', handleResize);
                handleResize.cancel();
            };
        },
        []
    );

    useEffect(
        () => {
            if(props.resetImage && nodeImg.current) {
                let imgSrc = nodeImg.current.src;
                nodeImg.current.src = "";
                setTimeout(() => {
                    nodeImg.current.src = imgSrc;
                }, 0);
            }
        },
        [props.resetImage]
    );

    useIsomorphicLayoutEffect(
        updateWidth,
        []
    );

    const url = ImageFactory.buildImageUrlByWidth(imageId, (width !== 0) ? width : 'auto', transformations, format);

    const handleLoad = () => {
        // updates cached values after image dimensions have loaded
        props.parallaxController.update();
    };

    return (
        <div
            className={classname}
            ref={nodeRef}
            style={{
                marginBottom: props.offsetBottom,
                marginTop: props.offsetTop,
                marginLeft: props.offsetLeft,
                marginRight: props.offsetRight,
                transform: props.scale ? `scale(${props.scale})`: undefined,
            }}
        >
            {  !noLazyLoad ? (
                    <LazyLoad offset={500}>
                        {  props.hasAssetParallax ? (
                            <Parallax
                                className="img-parallax"
                                x={props.assetX} y={props.assetY}>
                                <img ref={nodeImg} className="ob-mainAsset-wrapper-img" src={url} id={elemId} alt={alt || ""} onLoad={handleLoad}/>
                            </Parallax>
                        ) : (
                            <img ref={nodeImg} className="ob-mainAsset-wrapper-img" src={url} id={elemId} alt={alt || ""} />
                        )}
                    </LazyLoad>
                ) :
                props.hasAssetParallax ? (
                    <Parallax
                        className="img-parallax"
                        x={props.assetX} y={props.assetY}>
                        <img ref={nodeImg} src={url} alt={alt || ""} id={elemId} onLoad={handleLoad} />
                    </Parallax>
                ) : (
                    <img ref={nodeImg} src={url} alt={alt || ""} id={elemId} />
                )
            }
        </div>
    )
}

Img.defaultProps = {
    transformations: 'c_limit',
    hasAssetParallax: false,
    resetImage: false
}

Img.propTypes = {
    imageId: PropTypes.string,
    elemId: PropTypes.string,
    alt: PropTypes.string,
    scale: PropTypes.string,
    offsetBottom: PropTypes.string,
    offsetTop: PropTypes.string,
    offsetLeft: PropTypes.string,
    offsetRight: PropTypes.string,
    transformations: PropTypes.string,
    className: PropTypes.string,
    customClassName: PropTypes.string,
    alignmentClassName: PropTypes.string,
    format: PropTypes.string,
    noLazyLoad: PropTypes.bool,
    resetImage: PropTypes.bool
}

export default withController(Img);
