import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import LazyLoad from 'react-lazyload';
import throttle from 'lodash/throttle';

import { useIsomorphicLayoutEffect } from '../../../helpers/Hooks';
import { getBackgroundImage, roundUpTheSize } from './BackgroundImage.helpers'

import './BackgroundImage.scss';

export default function BackgroundImage(props) {
    const {backgroundOffsetTop: marginTop, backgroundPosition, children = null, customStyles = null} = props;
    const paddingTop = `${ 100 / props.widthToHeightRatio }%`;

    const nodeRef = useRef(null);
    const placeholderRef = useRef(null);
    const [width, setWidth] = useState(0);

    function updateWidth() {
        const node = nodeRef.current || placeholderRef.current;

        if (node) {
            setWidth(roundUpTheSize(node.getBoundingClientRect().width));
        }
    }

    useEffect(
        () => {
            const handleResize = throttle(updateWidth, 100);

            window.addEventListener('resize', handleResize);

            return () => {
                window.removeEventListener('resize', handleResize);
                handleResize.cancel();
            };
        },
        []
    );

    useIsomorphicLayoutEffect(
        updateWidth,
        []
    );

    return (
        <LazyLoad offset={500}
            placeholder={
                <div
                    className={`ob-background-wrapper ${props.className ? props.className : ''}`}
                    ref={placeholderRef}
                    style={{
                        marginTop: marginTop, paddingTop, backgroundPosition
                    }}
                />
            }
        >
            <div
                className={`ob-background-wrapper ${props.className ? props.className : ''}`}
                ref={nodeRef}
                style={{
                    marginTop, paddingTop, backgroundPosition,
                    backgroundImage: getBackgroundImage(props),
                    ...customStyles
                }}
            >
                {children}
            </div>
        </LazyLoad>
    );
}

BackgroundImage.defaultProps = {
    transformations: 'c_limit',
    fragments: [{ minWidth: 0, size: 1 }],
    widthToHeightRatio: 1
}

BackgroundImage.propTypes = {
    imageId: PropTypes.string,
    transformations: PropTypes.string,
    fragments: PropTypes.array,
    widthToHeightRatio: PropTypes.number,
    children: PropTypes.any
}

