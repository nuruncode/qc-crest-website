import ImageFactory from '../../../adapters/cloudinary/ImageFactory';

export const roundUpTheSize = (width) => {
    return Math.floor(width / 100) * 100 + 100;
}

export const getBackgroundImage = ({ imageId, width, transformations, format }) => {
    if (imageId) {
        return `url(${ImageFactory.buildImageUrlByWidth(imageId, width, transformations, format)})`
    }
}
