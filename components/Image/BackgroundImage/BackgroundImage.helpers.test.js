import {
    roundUpTheSize,
    getBackgroundImage
} from './BackgroundImage.helpers'

describe('Helpers for styled components', () => {

    beforeEach(() => {
        process.env = Object.assign(process.env, { CLOUDINARY_URL : 'https://res.cloudinary.com/pgone' })
    })

    describe('Round up to the size function', () => {
        it('should return 400 for input 301', function () {
            const width = 301

            expect(roundUpTheSize(width))
                .toEqual(400)
        })
    })

    describe('Get Background Image function', () => {
        it('should return background image property with image of size rounded up to 100px', function () {
            const imageId = 'test'
            const width = 400

            expect(getBackgroundImage({imageId, width}))
                .toEqual('url(https://res.cloudinary.com/pgone/image/upload/w_400,c_limit,q_auto,f_auto/test)')
        })
    })
})

