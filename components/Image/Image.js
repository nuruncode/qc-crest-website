import React, { Fragment } from 'react'
import PropTypes from "prop-types";
import BackgroundImage from './BackgroundImage/BackgroundImage';
import Img from './Img/Img';

export default function Image(props) {

    const { children = null, image, scale, customStyles = null, elemId = null} = props;

    if (image) {
        const assetId = image.fields.assetId;
        const alt = image.fields.alternateText;
        const rendition = image.fields.imageRendition;
        const aspectRatio = rendition?.fields.aspectRatio;
        const transformations = rendition?.fields.transformations;
        const customClassName = image.fields.classNames;
        const format = image.fields.forcedFormat || 'auto';
        const alignmentClassName = props.className;
        const resetImage = props.resetImage;

        return (children || props.forceBackground ?
                <BackgroundImage
                    {...props}
                    imageId={assetId}
                    alt={alt}
                    widthToHeightRatio={aspectRatio}
                    transformations={transformations}
                    customClassName={customClassName}
                    backgroundPosition={props.backgroundPosition}
                    backgroundOffsetTop={props.backgroundOffsetTop}
                    alignmentClassName={alignmentClassName}
                    format={format}
                    assetX={props.assetX}
                    assetY={props.assetY}
                    customStyles={customStyles}
                    hasAssetParallax={props.hasAssetParallax}>
                    {children}
                </BackgroundImage>
                :
                <Img
                    {...props}
                    scale={scale}
                    imageId={assetId}
                    elemId={elemId}
                    alt={alt}
                    widthToHeightRatio={aspectRatio}
                    transformations={transformations}
                    offsetTop={props.offsetTop}
                    offsetRight={props.offsetRight}
                    offsetBottom={props.offsetBottom}
                    offsetLeft={props.offsetLeft}
                    customClassName={customClassName}
                    alignmentClassName={alignmentClassName}
                    format={format}
                    resetImage={resetImage}
                />
        );
    } else {
        return (
            <Fragment>
                {children}
            </Fragment>
        )
    }
}

Image.propTypes = {
    offsetTop: PropTypes.string,
    offsetRight: PropTypes.string,
    offsetLeft: PropTypes.string,
    offsetBottom: PropTypes.string,
    classname: PropTypes.string,
    children: PropTypes.any,
    image: PropTypes.any
};
