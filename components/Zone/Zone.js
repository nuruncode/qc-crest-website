import PropTypes from 'prop-types';
import React from 'react';

import Block from '../../helpers/Block';
import Image from '../Image/Image';

export default function Zone(props) {
    const block = new Block(props);
    const extraAttributes = props.extraAttributes || {};

    const className = 'zone ' + block.getFieldValue('classNames');
    const role = block.getFieldValue('role');
    const backgroundImage = block.getFieldValue('backgroundImage');

    const children = block.renderChildBlocks({ extraAttributes }, props.isMobile, props.isTablet, props.isTiny, props.onClickCallback, props.customEvent);

    return (backgroundImage ? (
        <Image image={backgroundImage}>
            {children}
        </Image>
    ) : (
        <div className={className} role={role}>
            {children}
        </div>
    ));
}

Zone.propTypes = {
    className: PropTypes.string,
    backgroundImage: PropTypes.object,
    isMobile: PropTypes.bool,
    isTablet: PropTypes.bool,
    isTiny: PropTypes.bool,
    extraAttributes: PropTypes.object,
    onClickCallback:  PropTypes.func,
    customEvent: PropTypes.object
};
