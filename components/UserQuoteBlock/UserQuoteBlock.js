import React, {useEffect, useRef} from 'react';
import Block from '../../helpers/Block';
import Image from '../Image/Image';
import Heading from '../Heading/Heading'
import Icon from '../Icon/Icon';
import ObLink from '../ObLink/ObLink';
import SlideToggle from 'react-slide-toggle';
import {StarRating} from '../StarRating/StarRating'
import PropTypes from 'prop-types'
import './UserQuoteBlock.scss'

export default function UserQuote(props) {
    const {extraAttributes, isMobile} = props;

    return (extraAttributes && extraAttributes.entity && extraAttributes.entity.featuredQuote) ?
        renderUserQuoteBlock(extraAttributes.entity, props, isMobile) :
        '';
}

function renderUserQuoteBlock(entity, props) {
    const product = entity;
    const productOverview = new Block(entity.productOverview);
    const propsBlock = new Block(props);
    const userQuote = new Block(entity.featuredQuote);

    const classNames = propsBlock.getFieldValue('classNames');
    const title = propsBlock.getFieldValue('title');
    const anchorId = propsBlock.getAnchorId();
    const mainAsset = productOverview.getFieldValue('mainAsset');
    const productName = productOverview.getFieldValue('name');

    const authorName = userQuote.getFieldValue('authorName');
    const quote = userQuote.getFieldValue('quote');
    const starRating = userQuote.getFieldValue('starRating');

    // const [reviewCollapsed, setReviewCollapsed] = useState(props.isMobile ? true : false);

    // useEffect(() => {
    //     setReviewCollapsed(props.isMobile ? true : false)
    // }, [ props.isMobile])

    let reviewAndRatingId = getReviewAndRatingId(product.productOverview);

    const reviewClickHandler = event => {
        event.preventDefault();
        const warButon = document.querySelector('.bv-write-review ');
        if (warButon) {
            // Trigger bazaarvoice write a review
            warButon.click();
        }
    }

    let bazaarVoiceRender;
    if (reviewAndRatingId) {
        bazaarVoiceRender = (
            <>
                <div data-bv-show="reviews" data-bv-product-id={reviewAndRatingId}></div>

                <div data-bv-show="questions" data-bv-product-id={reviewAndRatingId}></div>
            </>);
    }


    const reviewRef = useRef();
    useEffect(() => {
        if (props.isMobile == false && reviewRef.current.state.toggleState == 'COLLAPSED') {
            reviewRef.current.onToggle();
        } else if (props.isMobile == true && reviewRef.current.state.toggleState == 'EXPANDED' ) {
            reviewRef.current.onToggle();
        }
    }, [props.isMobile])

    return (

        <div className={`ob-user-quote ${classNames}`} id={anchorId} >
            <div className='ob-user-quote_wrapper'>
                <Heading className='ob-user-quote_heading' tag='h2'>
                    {title}
                </Heading>

                { reviewAndRatingId &&
                    <div className='ob-war-summary'>
                        <div data-bv-show="rating_summary" data-bv-product-id={reviewAndRatingId}></div>

                        <ObLink className="primaryGrey ob-user-quote_war-btn" icon="chevronRight" tag='button' onClick={reviewClickHandler}>
                            <Icon name="chevronRight" roundedIcon="blue"/>
                            WRITE A REVIEW
                            <span className="visuallyhidden">This action will open a modal dialog.</span>
                        </ObLink>
                    </div>
                }

                <div className='ob-user-quote_flex-container'>
                    <div className='ob-user-quote_flex-container-col'>
                        <div className='ob-user-quote_star-rating'>
                            <StarRating ratingNumber={starRating} />
                        </div>

                        <div className='ob-user-quote_quote'>
                            {quote}
                        </div>

                        <div className='ob-user-quote_author-name'>{authorName}</div>
                    </div>
                    <div className='ob-user-quote_flex-container-col'>
                        <Image image={mainAsset} className='ob-user-quote_image'></Image>
                    </div>

                </div>


                <SlideToggle ref={reviewRef} collapsed>
                    {({ onToggle, setCollapsibleElement, toggleState }) => (
                        <>
                            {toggleState === "COLLAPSED" || toggleState === "COLLAPSING" ? (
                                    <ObLink className="primaryGrey ob-user-quote_expand-review-btn" icon="chevronRight" tag='button' onClick={onToggle}>
                                        <Icon name="chevronRight" roundedIcon="blue"/>
                                        SEE MORE REVIEWS
                                        <span className="visuallyhidden">. This action will show the reviews.</span>
                                    </ObLink>
                                ) : (
                                    <ObLink className="primaryGrey ob-user-quote_expand-review-btn" icon="chevronRight" tag='button' onClick={onToggle}>
                                        <Icon name="chevronRight" roundedIcon="blue"/>
                                        SEE LESS REVIEWS
                                        <span className="visuallyhidden">. This action will hide the reviews.</span>
                                    </ObLink>
                                )}

                            <div itemScope itemType="https://schema.org/Product" ref={setCollapsibleElement} className='ob-user-quote_collapse-review'>
                                <h1 className='visuallyhidden' itemProp="name">{productName}</h1>
                                <div>{bazaarVoiceRender}</div>
                            </div>
                        </>
                    )}
                </SlideToggle>
            </div>
        </div>

    )
}

export function getReviewAndRatingId(productOverview) {
    let reviewAndRatingId;
    if(productOverview) {
        let productVariants = productOverview.fields.productVariants;
        if(productVariants) {
            for (let i = 0; i < productVariants.length; i++) {
                let productVariant = productVariants[i];
                if (productVariant.fields.isBazaarVoiceIdentifier) {
                    reviewAndRatingId = productVariant.fields.sku;
                }
            }
        }
    }

    return reviewAndRatingId;
}

renderUserQuoteBlock.propTypes = {
    isMobile: PropTypes.bool,
}
