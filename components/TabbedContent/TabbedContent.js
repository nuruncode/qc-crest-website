import React, {Component} from 'react';
import PropTypes from "prop-types";
import throttle from 'lodash/throttle';
import Block from '../../helpers/Block';
import Icon from '../Icon/Icon'
import './TabbedContent.scss';

class TabbedContent extends Component {

  constructor(props) {
    super(props);
    this.block = new Block(props);

    this.state = {
      currentTab : '',
      isMobile : false
    }

    this.clickTab = this.clickTab.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResizeThrottled);
    this.handleResize();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResizeThrottled);
  }

  handleResize() {
    this.setState({
      isMobile: window.matchMedia("(max-width: 992px)").matches
    })
  }

  handleResizeThrottled = throttle(this.handleResize.bind(this), 100);

  renderTab(block, index, renderTabContent = null) {
    const blockObj = new Block(block);
    const tabId = this.getTabId(blockObj);
    blockObj.setFieldValue('anchorName', tabId);

    const label = blockObj.getShortName();

    return (
      <div key={tabId + index} className={`${this.state.currentTab === tabId ? 'is-active ' : ''}ob-tabbed-content__tab-list-item`}>
        <button data-tab-id={tabId} onClick={this.clickTab}>
          {label}
          <span className={`ob-tabbed-content__tab-icon`}><Icon name="chevronDown" /></span>
        </button>

        {renderTabContent && renderTabContent()}
      </div>
    );
  }

  renderTabContent(block, index) {
    const blockObj = new Block(block);
    const tabId = this.getTabId(blockObj);
    blockObj.setFieldValue('anchorName', tabId);
    if (index === 0 && this.state.currentTab === '') {
      this.setState({currentTab: tabId});
    }

    return (
      <div key={tabId + index} id={tabId} className={`${this.state.currentTab === tabId ? 'is-active ' : ''}ob-tabbed-content__content-list-item`}>
        {blockObj.renderBlockFromDocument()}
      </div>
    );
  }

  getTabId(blockObj) {
    return 'tab-' + blockObj.getAnchorId();
  }

  clickTab(event) {
    event.preventDefault();
    this.setState({currentTab: event.currentTarget.dataset.tabId})
  }

  render() {
    let className = 'ob-tabbed-content ' + this.props.className;
    return (this.state.isMobile ? (
      <div className={className}>
        <div className={`ob-tabbed-content__tab-list`}>
          {
            this.block.getFieldValue('blocks').map((block, index) => {
              return this.renderTab(block, index, () => this.renderTabContent(block, index));
            })
          }
        </div>
      </div>
    ) : (
      <div className={className}>
        <div className={`ob-tabbed-content__tab-list`}>
          {
            this.block.getFieldValue('blocks').map((block, index) => {
              return this.renderTab(block, index);
            })
          }
        </div>
        <ul className={`ob-tabbed-content__content-list`}>
          {
            this.block.getFieldValue('blocks').map((block, index) => {
              return this.renderTabContent(block, index);
            })
          }
        </ul>
      </div>
    ));
  }
}

TabbedContent.propTypes = {
    className: PropTypes.string,
};

export default TabbedContent