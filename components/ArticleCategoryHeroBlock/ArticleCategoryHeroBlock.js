import React from 'react';

import Block from '../../helpers/Block';

import Icon from '../Icon/Icon';
import Image from '../Image/Image';
import Heading from '../Heading/Heading';

import './ArticleCategoryHeroBlock.scss';

export default function ArticleCategoryHeroBlock(props) {
    const block = new Block(props);

    const className = block.getFieldValue('classNames');
    const anchorId = block.getAnchorId();

    const title = block.getFieldValue('title');
    const titleLevel = block.getFieldValue('titleLevel');
    const body = block.getFieldValue('body');
    const backgroundAsset = block.getFieldValue('backgroundAsset');
    const ctas = block.getFieldValue('ctas');

    return (
        <div className={`ob-article-category-hero ${className}`} id={anchorId}>
            <Image className="ob-article-category-hero-background" image={backgroundAsset}>
                <div className="ob-article-category-hero-wrapper">
                    <div className="ob-article-category-hero-content">
                        <Heading className="ob-article-category-hero-heading" tag={`h${titleLevel}`}>
                            {title}
                        </Heading>

                        <p className="ob-article-category-hero-text">
                            {body}
                        </p>

                        <ul className="ob-article-category-hero-cta-list">
                            {ctas.map((cta, ctaIndex) => (
                                <li className="ob-article-category-hero-cta-item" key={ctaIndex}>
                                    <a className="ob-article-category-hero-cta-link ob-link primaryGrey" href={cta.fields.url}>
                                        <Icon className="ob-article-category-hero-cta-icon" name="chevronRight" roundedIcon="blue" size={1.4} />

                                        {cta.fields.title}
                                    </a>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </Image>
        </div>
    )
}
