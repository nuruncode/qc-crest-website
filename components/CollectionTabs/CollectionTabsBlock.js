import React, { useEffect, useRef } from 'react';
import Block from '../../helpers/Block';
import Eyebrow from "../Eyebrow/Eyebrow";
import './CollectionTabsBlock.scss';
import PropTypes from 'prop-types';
import UnderlineTabsNavigation from '../UnderlineTabsNavigation/UnderlineTabsNavigation';
import {getSelectedIndex, mapLinks} from './helpers';
import { scrollIntoView } from '../../helpers/Scroll';

export default function CollectionTabsBlock(props) {
    const {extraAttributes, document} = props;

    const block = new Block(props);
    
    const extraClassNames = document?.fields?.classNames;
    const surtitle = document?.fields?.surtitle;
    const links = document?.fields?.links;
    const anchorId = block.getAnchorId();
    const currentURL = extraAttributes?.slug;
    const tabsRef = useRef();

    const classNames = ['ob-collection-tabs'];
    if (extraClassNames) {
        classNames.push(extraClassNames);
    }

    useEffect(() => {
        if (window?.location?.hash === `#${anchorId}` && tabsRef?.current) {
            let timeout = null;
            window.setTimeout(() => {
                timeout = scrollIntoView(tabsRef.current, {behavior: 'smooth'});
            }, 250);

            return () => {
                clearTimeout(timeout);
            }
        }
    }, []);



    const tabs = mapLinks(links);
    const selectedIndex = getSelectedIndex(links, currentURL);

    return (
        <div className={classNames.join(' ')} id={anchorId} ref={tabsRef}>
            <Eyebrow className="ob-collection-tabs__eyebrow">{surtitle}</Eyebrow>
            <div className="ob-collection-tabs-block__tabs-wrapper">
                <UnderlineTabsNavigation tabs={tabs} selectedIndex={selectedIndex}/>
            </div>
        </div>
    )
}

CollectionTabsBlock.propTypes = {
    extraAttributes: PropTypes.any,
    document: PropTypes.any,
}