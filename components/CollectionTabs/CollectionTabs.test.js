import React from 'react';
import CollectionTabsBlock from "./CollectionTabsBlock";
import CollectionTabsDropdownBlock from "./CollectionTabsDropdownBlock";
import toJson from 'enzyme-to-json';

function createMockCollectionTabsBlockProps(props) {
    return {
        document: {
            fields: {
                anchorId: "anchor",
                contentType: "collectionTabsBlock",
                links: [
                    {
                        fields: {
                            contentType: "link",
                            name: "name",
                            title: "title",
                            type: "internal",
                            url: "url"
                        }
                    }
                ],
                name: "name"
            },
        },
        extraAttributes: {
            fields: {
                slug: "url"
            }
        },
        ...props,
    };
}

describe('Collection tabs block rendering', () => {
    it('should render collection tabs block with the parameters supplied', () => {
        const props = createMockCollectionTabsBlockProps();
        const wrapper = mount(<CollectionTabsBlock {...props}/>);
        expect(toJson(wrapper)).toMatchSnapshot()
    });
});

describe('Collection tabs dropdown block rendering', () => {
    it('should render content block with the parameters supplied', () => {
        const props = createMockCollectionTabsBlockProps();
        const wrapper = mount(<CollectionTabsDropdownBlock {...props}/>);
        expect(toJson(wrapper)).toMatchSnapshot()
    });
});