import React from 'react';
import PropTypes from 'prop-types';
import Block from '../../helpers/Block';
import Eyebrow from '../Eyebrow/Eyebrow';
import Button from '../Button/Button';
import './SpotlightStickyBar.scss'
import ObLink from "../ObLink/ObLink";
import Image from "../Image/Image";

export default function SpotlightStickyBar(props) {
    const block = new Block(props);

    const scrollToPage =  block.getFieldValue('scrollTo');
    const callToActionLink =  block.getFieldValue('callToActionLink');
    const logoLink =  block.getFieldValue('logoLink');
    const logo =  block.getFieldValue('logo');

    const onClick = (event) => {
        props.onClickCallback(event);
    }

    return (
        <div className='ob-SpotlightStickyBar' id={block.getFieldValue('name')}>
            <div className='ob-SpotlightStickyBar__wrapper'>
                <ObLink href={logoLink} className='ob-SpotlightStickyBar-link'>
                    <Image image={logo}></Image>
                </ObLink>
                <div className="ob-SpotlightStickyBar__eyebrow-container">
                    <Eyebrow whiteText={true}>{block.getFieldValue('title')}</Eyebrow>
                </div>
                {
                    scrollToPage &&
                    <Button invertedTheme size='medium' dataCustom={scrollToPage}
                            onClick={event => {onClick(event)}}>
                        {block.getFieldValue('callToActionLabel')}
                    </Button>
                }
                {
                    scrollToPage == null && callToActionLink &&
                    <Button invertedTheme size='medium' tag='a' href={callToActionLink}>
                        {block.getFieldValue('CallToActionLabel')}
                    </Button>
                }
            </div>
        </div>
    )
}
SpotlightStickyBar.propTypes = {
    onClickCallback: PropTypes.func
};
