import React from 'react';
import Rotation from './Rotation';

function createMockRotationProps(props) {
    return {
        className: '',
        cycle: true,
        vertical: false,
        reverse: false,
        autoPlay: 25,
        children: [<img src="https://via.placeholder.com/150x700" alt="" key={0} />,<img src="https://via.placeholder.com/150x700" alt="" key={1} />, <img src="https://via.placeholder.com/150x700" alt="" key={2} />],
        tabIndex: 0,
        pauseOnHover: false,
        playOnce: true,
        ...props
    };
}

describe('<Rotation /> rendering', () => {
    it('should render the Rotation component with the parameters supplied', () => {
        const props = createMockRotationProps();
        const wrapper = render(<Rotation {...props} />);
        expect(wrapper).toMatchSnapshot()
    });
});