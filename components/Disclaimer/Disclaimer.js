import React from 'react'
import PropTypes from 'prop-types'
import './Disclaimer.scss'

function Disclaimer({children, tag: Tag, id, className, styles, whiteText}) {

    if (!children) {
        return null
    }

    className = className ? `ob-disclaimer ${className}` : `ob-disclaimer`;

    return (
        <Tag id={id} className={className} data-white-text={whiteText} style={styles}>
            {children}
        </Tag>
    )
}

Disclaimer.propTypes = {
    /**
    * The html to be structured
    */
    children: PropTypes.any,

    /**
    * The tag to be used for the containing element
    */
    tag: PropTypes.string,

    /**
    * The ID attribute be added to the element
    */
    id: PropTypes.string,

    /**
    * The Class attribute be added to the element
    */
    className: PropTypes.string,

    /**
     * Add custom styles to the disclaimer
     */
    styles: PropTypes.object,

    /**
    * Set white text if needed
    */
    whiteText: PropTypes.bool,
}

Disclaimer.defaultProps = {
    tag: 'p',
    className: '',
    styles: {},
    whiteText: false
  }

export default Disclaimer

