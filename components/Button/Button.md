### Examples

**Oral-B Default Button**

```
<Button>MEET THE IO MAGNETIC 9</Button>
```

**Oral-B White Default Button**

```
<Button whiteTheme>MEET THE IO MAGNETIC 9</Button>
```

**Oral-B Grey Default Button**

```
<Button greyTheme>MEET THE IO MAGNETIC 9</Button>
```

**Oral-B Inverted Button**

Border and color set to white, and transparent background

```
<div style={{ backgroundColor: '#3D3D41' }}>
<Button invertedTheme>MEET THE IO MAGNETIC 9</Button>
</div>
```

**Oral-B Blue Large Button**

```
<Button size='large'>MEET THE IO MAGNETIC 9</Button>
```

**Oral-B White Large Button**

```
<Button size='large' whiteTheme>MEET THE IO MAGNETIC 9</Button>
```

**Oral-B Grey Large Button**

```
<Button size='large' greyTheme>MEET THE IO MAGNETIC 9</Button>
```

**Oral-B White Large Button with custom styles**

```
<Button styles={{fontSize: '10px',backgroundColor: '#ddd'}} size='large' whiteButton='true' withBorder>MEET THE IO MAGNETIC 9</Button>
```

**Oral-B Button becomes a link**

```
<Button tag="a" href="www.google.com" size='large' whiteButton='true' withBorder>MEET THE IO MAGNETIC 9</Button>
```

