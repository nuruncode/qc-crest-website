import React from 'react'
import PropTypes from 'prop-types'
import "./Button.scss"

/** The oralB button */

const Button = ({
    children, 
    tag: Tag, 
    whiteTheme, 
    size, 
    className, 
    greyTheme, 
    invertedTheme, 
    borderTheme,
    styles, 
    href,
    target,
    role,
    ariaLabel,
    type,
    dataCustom,
    onClick}) => {

    if (!children) {
        return null
    }

    className = className ? `ob-button ${className}` : `ob-button`;

    return (
        <Tag
            className={className}
            data-size={size}
            data-white-theme={whiteTheme}
            data-grey-theme={greyTheme}
            data-inverted-theme={invertedTheme}
            data-border-theme={borderTheme}
            data-custom={dataCustom}
            type={Tag === 'button' && type ? type : undefined}
            href={['a'].indexOf(Tag) > -1 && href ? href : undefined}
            target={['a', 'form'].indexOf(Tag) > -1 ? target : undefined}
            formTarget={Tag === 'button' ? target : undefined}
            role={role}
            aria-label={ariaLabel}
            style={styles}
            onClick={onClick}
        >
            <span dangerouslySetInnerHTML={{__html: children}}></span>
        </Tag>
    )
}

Button.propTypes = {
    /** Children is the text button */
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.array
    ]),

    /** Becomes white background button 
     * with primary blue text if set to true*/
    whiteTheme: PropTypes.bool,

    /** Transparent with border and 
     * color white if set to true*/
    invertedTheme: PropTypes.bool,
    
    /** Color and Border grey if set to true */
    greyTheme: PropTypes.bool,

    /** Increase the height with the value: 'large'*/
    size: PropTypes.string,

    /** Custom class. */
    className: PropTypes.string,

    /** Aria-label is mandatory if not children in the button */
    ariaLabel: PropTypes.string,

    /** Custom styles to be applied to the button */
    styles: PropTypes.object,
    
    /**
    * The tag or component to be used e.g. button, a, Link
    */
    tag: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,
        PropTypes.func
    ]),

    /**
    * The target for the button
    */
    target: PropTypes.oneOf(['_self', '_blank', '_parent', '_top']),


    /** You can set an Href. */
    href: PropTypes.string,

    /**
     * The type the button
     */
    type: PropTypes.string,

    role: PropTypes.string,

    borderTheme: PropTypes.bool,

    dataCustom: PropTypes.string,

    onClick: PropTypes.any,

    productVariantColor: PropTypes.any

};

Button.defaultProps = {
    tag: 'button',
    styles: {}
}

export default Button;


