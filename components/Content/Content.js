import './Content.scss'

import React, { useEffect, useState, useRef, useLayoutEffect, useCallback } from 'react';
import { getConfigurationBooleanValue, getConfigurationValue } from "../../helpers/ContentBlockConfiguration";

import Block from '../../helpers/Block';
import BodyText from '../BodyText/BodyText'
import Button from '../Button/Button'
import Disclaimer from '../Disclaimer/Disclaimer'
import Eyebrow from '../Eyebrow/Eyebrow'
import Heading from '../Heading/Heading'
import Icon from '../Icon/Icon'
import Image from '../Image/Image';
import ObLink from '../ObLink/ObLink'
import { Parallax } from 'react-scroll-parallax';
import PropTypes from 'prop-types'
import { Waypoint } from 'react-waypoint';
import VideoPlayer from '../VideoPlayer/VideoPlayer';

function VideoBackgroundWrapper({backgroundAsset, children}) {
    
    const videoPlayerRef = useRef(null);
    const [playing, setIsPlaying] = useState(false);

    const handlePlaying = useCallback(() => {
        setIsPlaying(true);
    }, []);

    const handleStopped = useCallback(() => {
        setIsPlaying(false);
    }, []);

    const handlePlayClick = useCallback(() => {

        const videoRef = videoPlayerRef.current?.getVideoRef();

        if(videoRef && videoRef instanceof HTMLVideoElement) {

            if(videoRef.paused || videoRef.ended) {
                videoRef.play()
            } else {
                videoRef.pause();
            }
        }
    }, []);

    useLayoutEffect(() => {
        const videoRef = videoPlayerRef.current?.getVideoRef();

        if(videoRef && videoRef instanceof HTMLVideoElement) {

            (videoRef.paused || videoRef.ended ? handleStopped: handlePlaying)();
            videoRef.addEventListener('playing', handlePlaying);
            videoRef.addEventListener('play', handlePlaying);
            videoRef.addEventListener('pause', handleStopped);
            videoRef.addEventListener('ended', handleStopped);

            return () => {
                videoRef.removeEventListener('playing', handlePlaying);
                videoRef.removeEventListener('play', handlePlaying);
                videoRef.removeEventListener('pause', handleStopped);
                videoRef.removeEventListener('ended', handleStopped);
            }
        }
    }, [handlePlaying, handleStopped]);

    return (
        <div className="ob-contentBlock-video-wrapper">
            <div className="ob-contentBlock-video">
                <VideoPlayer ref={videoPlayerRef} video={backgroundAsset.fields} />
            </div>
            <div className="ob-contentBlock-video-controls-wrapper">
                <button onClick={handlePlayClick} className="ob-contentBlock-video-controls">
                    {playing ? "Pause": "Play"}
                </button>
            </div>

            <div className="ob-contentBlock-video-content">
                {children}
            </div>
        </div>
    )
}

VideoBackgroundWrapper.propTypes = {
    backgroundAsset: PropTypes.any,
    children: PropTypes.any,
}

VideoBackgroundWrapper.defaultProps = {
    backgroundAsset: undefined,
    children: null,
}

function BackgroundWrapper({
    backgroundAsset, 
    backgroundPosition, 
    sectionOffsetTop, 
    children
}) {
    switch(backgroundAsset?.fields?.contentType) {
        case "cloudinaryVideo":
            return (
                <VideoBackgroundWrapper backgroundAsset={backgroundAsset}>
                    {children}
                </VideoBackgroundWrapper>
            );
        default:
            return (
                <Image image={backgroundAsset}
                    backgroundPosition={backgroundPosition}
                    backgroundOffsetTop={sectionOffsetTop}>
                    {children}
                </Image>
            );
    }
}

BackgroundWrapper.propTypes = {
    backgroundAsset: PropTypes.any,
    backgroundPosition: PropTypes.any,
    sectionOffsetTop: PropTypes.any,
    children: PropTypes.any,
}

BackgroundWrapper.defaultProps = {
    backgroundAsset: undefined,
    backgroundPosition: 0,
    sectionOffsetTop: 0,
    children: null,
}

/**
 *
 * Supported Classes:
 *  By default the background is white and the text color is primary-grey.
 *  The CTA is a Button by default
 *
 *  - background-primary-grey : change the background to primary grey
 *  - background-secondary-grey : change the background to secondary grey
 *  - background-secondary-off-white : change the background to secondary-off-white
 *  - background-isolate-grey : change the background to isolate grey
 *  - background-primary-blue : change the background to primary-blue
 *  - background-secondary-blue : change the background to secondary-blue
 *
 */

export default function ContentBlock(props) {

    const {isTiny, isMobile} = props;

    const valueOf = (name) => {
        let value = undefined;
        if (name && block && block.getFieldValue(name) !== undefined) {
            value = block.getFieldValue(name);
        }
        return value;
    }

    const generateContentblockState = () => {
        return {
            availableConfigurations: valueOf('devicesConfigurations').map(configuration => configuration.fields.deviceType),
            textAlign: configurationValue('textAlign', isMobile, isTiny, ''),
            textContainerWidth: configurationValue('textContainerWidth', isMobile, isTiny, ''),
            titleWidth: configurationValue('titleWidth', isMobile, isTiny, ''),
            descriptionWidth: configurationValue('descriptionWidth', isMobile, isTiny, ''),
            textContainerHorizontalAlignment: configurationValue('textContainerHorizontalAlignment', isMobile, isTiny, ''),
            textContainerVerticalAlignment: configurationValue('textContainerVerticalAlignment', isMobile, isTiny, ''),
            textContainerOffset: configurationValue('textContainerOffset', isMobile, isTiny, ''),
            textContainerMarginRight: configurationValue('textContainerMarginRight', isMobile, isTiny, ''),
            textContainerMarginLeft: configurationValue('textContainerMarginLeft', isMobile, isTiny, ''),
            mainAsset: configurationValue('mainAsset', isMobile, isTiny,),
            mainAssetMaxWidth: configurationValue('mainAssetMaxWidth', isMobile, isTiny, ''),
            mainAssetMaxHeight: configurationValue('mainAssetMaxHeight', isMobile, isTiny, ''),
            mainAssetHeight: configurationValue('mainAssetHeight', isMobile, isTiny, ''),
            mainAssetOffsetTop: configurationValue('mainAssetMarginTop', isMobile, isTiny, ''),
            mainAssetOffsetRight: configurationValue('mainAssetMarginRight', isMobile, isTiny, ''),
            mainAssetOffsetBottom: configurationValue('mainAssetOffsetBottom', isMobile, isTiny, ''),
            mainAssetOffsetLeft: configurationValue('mainAssetOffsetLeft', isMobile, isTiny, ''),
            mainAssetScale: configurationValue('mainAssetScale', isMobile, isTiny, ''),
            backgroundAsset: configurationValue('backgroundAsset', isMobile, isTiny),
            mainAssetHorizontalAlignment: configurationValue('mainAssetHorizontalAlignment', isMobile, isTiny),
            mainAssetVerticalAlignment: configurationValue('mainAssetVerticalAlignment', isMobile, isTiny),
            backgroundPosition: configurationValue('backgroundPosition', isMobile, isTiny, ''),
            sectionOffsetTop: configurationValue('sectionOffsetTop', isMobile, isTiny, ''),
            mainAssetParallax: configurationValue('mainAssetParallax', isMobile, isTiny, ''),
            mainAssetParallaxSetting: configurationValue('mainAssetParallaxSetting', isMobile, isTiny, ''),
            textParallax: configurationValue('textParallax', isMobile, isTiny, ''),
            textParallaxSetting: configurationValue('textParallaxSetting', isMobile, isTiny, ''),
            textParallaxConfiguration: configurationValue('textParallaxConfiguration', isMobile, isTiny, ''),
            assetParallaxConfiguration: configurationValue('assetParallaxConfiguration', isMobile, isTiny, ''),
            textShowAnimation: configurationValue('textShowAnimation', isMobile, isTiny, ''),
            textFadeIn: configurationBooleanValue('textFadeIn', isMobile, isTiny),
            textColor: configurationBooleanValue('textColor', isMobile, isTiny),
        };
    }

    const configurationValue = (fieldName, isMobile, isTiny, defaultValue = false) => {
        return getConfigurationValue(availableConfigurations,
            valueOf('devicesConfigurations'),
            fieldName,
            isMobile,
            isTiny,
            defaultValue);
    }


    const configurationBooleanValue = (fieldName, isMobile, isTiny, defaultValue = false) => {
        return getConfigurationBooleanValue(availableConfigurations,
            valueOf('devicesConfigurations'),
            fieldName,
            isMobile,
            isTiny,
            defaultValue);
    }

    let [show, setShow] = useState('')

    const _handleWaypointEnter = () => {
        // Trigger by waypoint enter
        showContentElement();
    }

    const showContentElement = () => {
        // Animation on the text container on the "onEnter" event
        setShow('show');
    }

    const getTitleFontSize = () => {
        // By default the Title font size is medium.
        let titleFontSize = '';

        switch (valueOf('titleFontSize')) {
            case 'small':
                titleFontSize = 'ob-display-3-xl';
                break;
            case 'medium':
                titleFontSize = 'ob-display-2-xl';
                break;
            case 'large':
                titleFontSize = 'ob-display-1-xl';
                break;
            default:
                titleFontSize = 'ob-display-2-xl';
        }

        return titleFontSize;
    }

    const getParallaxConfigurationValue = (hasParallax, parallaxConfigurationAttribute, axis) => {
        let value = [0, 0];

        const srcAttr = axis + 'Src';
        const destAttr = axis + 'Dest';

        if (hasParallax) {
            if (blockState[parallaxConfigurationAttribute].fields[srcAttr] && blockState[parallaxConfigurationAttribute].fields[destAttr]) {
                value = [blockState[parallaxConfigurationAttribute].fields[srcAttr], blockState[parallaxConfigurationAttribute].fields[destAttr]];
            }
        }
        return value;
    }

    const block = new Block(props);
    const availableConfigurations = valueOf('devicesConfigurations').map(configuration => configuration.fields.deviceType);
    const className = valueOf('classNames');
    const isALink = valueOf('callToActionIsALink');
    const title = valueOf('title');
    const titleLevel = valueOf('titleLevel');
    const blueEyebrow = valueOf('blueEyebrow');
    const titleFontSize = getTitleFontSize()
    const disclaimer = valueOf('disclaimer');
    const textsImage = valueOf('textsImage');
    const anchorId = block.getAnchorId();
    const [blockState, setblockState] = useState(generateContentblockState());


    // mainAssetOffsetY/mainAssetOffsetX props give Bottom/Left percentage
    const backgroundColor = valueOf('backgroundColor');

    const textContainerBackgroundColor = valueOf('textContainerBackgroundColor');
    const textContainerHasPadding = valueOf('textContainerHasPadding');

    useEffect(() => {
        if (blockState.textFadeIn == false) {
            //If text fadeIn at false show the element at pageLoad
            showContentElement();
        }

        if (block.props.document.fields.devicesConfigurations) {
            setblockState(generateContentblockState());
        }
    }, [isMobile, isTiny]);

    const hasTextParallax = (blockState.textParallaxConfiguration) ? true : false;

    const textX = getParallaxConfigurationValue(hasTextParallax, 'textParallaxConfiguration', 'x');
    const textY = getParallaxConfigurationValue(hasTextParallax, 'textParallaxConfiguration', 'y');

    const hasAssetParallax = (blockState.assetParallaxConfiguration) ? true : false;

    const assetX = getParallaxConfigurationValue(hasAssetParallax, 'assetParallaxConfiguration', 'x');
    const assetY = getParallaxConfigurationValue(hasAssetParallax, 'assetParallaxConfiguration', 'y');

    const isWhiteText = (blockState ?.textColor) === 'White' ? true : false;

    return (
        <section
            className={
                `ob-contentBlock content ${className ? className : ''} ${blockState.textShowAnimation} ${blockState.backgroundAsset ? '' : 'no-background'}`
            }
            id={anchorId}
            style={{
                backgroundColor: backgroundColor
            }}>

            <BackgroundWrapper backgroundAsset={blockState.backgroundAsset}
                sectionOffsetTop={blockState.sectionOffsetTop}
                backgroundPosition={blockState.backgroundPosition}
            >
                <div className={`ob-contentBlock-wrapper ${show}`}>
                    {blockState.textFadeIn !== false &&
                        <Waypoint onEnter={_handleWaypointEnter} />
                    }

                    <div className={`ob-contentBlock-textContent
                            ${blockState.textContainerHorizontalAlignment && `horizontal-${blockState.textContainerHorizontalAlignment}`}
                            ${blockState.textContainerVerticalAlignment && `vertical-${blockState.textContainerVerticalAlignment}`}
                            `}>

                        <div className={`ob-contentBlock-textContent-inner${textContainerHasPadding ? ' hasPadding' : ''} `}
                            style={{
                                marginTop: blockState.textContainerOffset,
                                marginLeft: blockState.textContainerMarginLeft,
                                marginRight: blockState.textContainerMarginRight,
                                backgroundColor: textContainerBackgroundColor,
                                textAlign: blockState.textAlign,
                                width: blockState.textContainerWidth
                            }}>

                            <Parallax className="ob-contentBlock-textContent-parallax" x={textX} y={textY} disabled={!hasTextParallax}>

                                {textsImage &&
                                    <div className={`ob-contentBlock-text-image`}>
                                        <Image image={textsImage} />
                                    </div>
                                }

                                <Eyebrow whiteText={isWhiteText} blueEyebrow={blueEyebrow}>{valueOf('surtitle')}</Eyebrow>

                                <Heading
                                    className={titleFontSize}
                                    whiteText={isWhiteText}
                                    tag={`h${titleLevel}`}
                                    styles={{ width: blockState.titleWidth }}>
                                    {title}
                                </Heading>

                                <BodyText
                                    whiteText={isWhiteText}
                                    styles={{ width: blockState.descriptionWidth }}>
                                    {valueOf('description')}
                                </BodyText>

                                {isALink ? (
                                    <ObLink href={`${valueOf('callToActionLink')}`}
                                        className={isWhiteText ? 'white' : 'primaryGrey'}
                                        icon="chevronRight">
                                        <Icon name="chevronRight" roundedIcon={isWhiteText ? 'white' : 'blue'} />
                                        {valueOf('callToActionLabel')}
                                    </ObLink>
                                ) : (
                                        <Button whiteTheme={isWhiteText}
                                            tag="a"
                                            href={valueOf('callToActionLink')}>
                                            {valueOf('callToActionLabel')}
                                        </Button>
                                    )}

                                <Disclaimer whiteText={isWhiteText}>{disclaimer}</Disclaimer>

                                {blockState.textFadeIn !== false &&
                                    <Waypoint onEnter={_handleWaypointEnter} />
                                }

                            </Parallax>
                        </div>

                    </div>

                    {blockState.mainAsset &&
                        <div className={`ob-contentBlock-mainAsset-container`} style={{
                            width: blockState.mainAssetMaxWidth,
                            height: blockState.mainAssetHeight,
                            maxHeight: blockState.mainAssetMaxHeight,
                            marginRight: blockState.mainAssetOffsetRight,
                            marginLeft: blockState.mainAssetOffsetLeft,
                            marginTop: blockState.mainAssetOffsetTop,
                            marginBottom: blockState.mainAssetOffsetBottom,
                        }}>
                            <Image
                                scale={blockState.mainAssetScale}
                                image={blockState.mainAsset}
                                assetX={assetX}
                                assetY={assetY}
                                hasAssetParallax={hasAssetParallax}
                                className={`
                                    ${blockState.mainAssetHorizontalAlignment && `horizontal-${blockState.mainAssetHorizontalAlignment}`}
                                    ${blockState.mainAssetVerticalAlignment && `vertical-${blockState.mainAssetVerticalAlignment}`}
                            `} />
                        </div>
                    }
                </div>
            </BackgroundWrapper>
        </section>
    )
}

ContentBlock.propTypes = {
    isMobile: PropTypes.bool,
    isTiny: PropTypes.bool
}
