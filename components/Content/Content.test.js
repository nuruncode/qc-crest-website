import React from 'react';
import ContentBlock from "./Content";
import toJson from 'enzyme-to-json';

function createMockContentProps(props) {
    return {
        document: {
            fields: {
                anchorId: "anchor",
                callToActionLabel: "ctaLabel",
                callToActionLink: "ctaLink",
                classNames: "class",
                contentType: "contentBlock",
                description: "description",
                devicesConfigurations: [{
                    fields:{
                        backgroundAsset: {
                            fields: {
                                assetId: "assetId",
                                contentType: "cloudinaryImage",
                                imageRendition: {
                                    fields: {
                                        aspectRatio: 1,
                                        contentType: "imageRendition",
                                        name: "name",
                                        transformations: "test"
                                    }
                                },
                                name: "name"
                            }
                        },
                        contentType: "contentBlockConfiguration",
                        deviceType: "desktop",
                        name: "Content Block Config 1 - desktop",
                        textAlign: "center",
                        textColor: "White",
                        textContainerHorizontalAlignment: "center",
                        textContainerOffset: "8%",
                        textContainerVerticalAlignment: "top",
                        textContainerWidth: "50%",
                        textFadeIn: true,
                        textShowAnimation: "bottom-transition"
                    }
                }],
                name: "name",
                shortName: "shortName",
                title: "title",
                titleFontSize: "large",
                titleLevel: "1"
            },
        },
        isMobile: false,
        isTablet: false,
        isTiny: false,
        ...props,
    };
}

describe('Content block rendering', () => {
    it('should render content block with the parameters supplied', () => {
        const props = createMockContentProps();
        const wrapper = mount(<ContentBlock {...props}/>);
        expect(toJson(wrapper)).toMatchSnapshot()
    });
});