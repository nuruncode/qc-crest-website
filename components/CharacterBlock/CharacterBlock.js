import React, { useState } from 'react'
import { Waypoint } from 'react-waypoint';
import Block from '../../helpers/Block';
import Image from '../Image/Image';
import Eyebrow from '../Eyebrow/Eyebrow';
import Heading from '../Heading/Heading';
import BodyText from '../BodyText/BodyText';
import './CharacterBlock.scss'
import { useMatchMedia } from '../../helpers/Hooks'

export default function CharacterBlock(props) {

    const block = new Block(props);
    const title = block.getFieldValue('title');
    const titleLevel = block.getFieldValue('titleLevel');
    const eyebrow = block.getFieldValue('eyebrow');
    const description = block.getFieldValue('description');
    const backgroundImage = block.getFieldValue('backgroundImage');
    const mobileBackgroundImage = block.getFieldValue('mobileBackgroundImage');
    const characterLogo = block.getFieldValue('characterLogo');
    const isCustomMobile = useMatchMedia('(max-width: 375px)', null);

    const anchorId = block.getFieldValue('anchorId');
    const customClassname = block.getFieldValue('classNames');
    const classname = customClassname ? `ob-CharacterBlock ${customClassname}` : `ob-CharacterBlock`;

    let [show, setShow] = useState('')

    const _handleWaypointEnter = () => {
        setShow('show');
    }

    return (
        <div id={anchorId} className={`${classname} ${show}`}>
            <Waypoint onEnter={_handleWaypointEnter} />
            <Image image={isCustomMobile ? mobileBackgroundImage : backgroundImage}>
                <div className="ob-CharacterBlock__wrapper">
                    <div className="ob-CharacterBlock__content">
                        <Image image={characterLogo} className={`ob-CharacterBlock__content-logo`}/>
                        <Eyebrow className='ob-CharacterBlock__content-eyebrow'>{eyebrow}</Eyebrow>
                        <Heading className='ob-display-1-xl ob-CharacterBlock__content-heading' tag={`h${titleLevel}`}>
                            {title}
                        </Heading>
                        <BodyText>{description}</BodyText>
                    </div>
                </div>
            </Image>
        </div>
    )
}