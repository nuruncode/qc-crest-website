import React, { useCallback, useEffect, useRef, useState } from 'react';
import SlideToggle from 'react-slide-toggle';

import { getAnchorIdFromEntity, stringifyAnchor, useAnchor } from '../../helpers/Anchor';
import { toggleScroll } from "../../helpers/Utils";
import Block from '../../helpers/Block';

import Icon from '../Icon/Icon';
import './LanguageSelection.scss';

const primaryColor = '#0057B8';

export default function LanguageSelection(props) {
    const block = new Block(props);

    const anchor = useAnchor();
    const overlayToggleRef = useRef();
    const languageSelectorRef = useRef();
    const linksRef = useRef();

    const [isLanguageSelectorOpen, setLanguageSelectorOpen] = useState(false);
    const [lastScroll, setLastScroll] = useState(0);

    const handleExpanding = useCallback(
        () => { setLanguageSelectorOpen(true); },
        []
    );

    const handleCollapsing = useCallback(
        () => { setLanguageSelectorOpen(false); },
        []
    );

    const handleClickOutside = useCallback(
        (event) => {
            if (overlayToggleRef.current && languageSelectorRef.current && !languageSelectorRef.current.contains(event.target)) {
                overlayToggleRef.current.onToggle();
            }
        },
        []
    );

    const handleScroll = useCallback(
        () => {
            if (!document.body.classList.contains('noScroll')) {
                setLastScroll(window.pageYOffset);
            }
        }
    )

    useEffect(
        () => {
            if (isLanguageSelectorOpen) {
                window.addEventListener('mousedown', handleClickOutside);
                toggleScroll(isLanguageSelectorOpen, lastScroll);
                
                return () => {
                    window.removeEventListener('mousedown', handleClickOutside);
                }
            } else {
                toggleScroll(isLanguageSelectorOpen, lastScroll);
            }
        },
        [isLanguageSelectorOpen]
    );

    useEffect(
        () => {
            window.addEventListener('scroll', handleScroll);
            return () => {
                window.removeEventListener('scroll', handleScroll);
            }
        },
        [lastScroll]
    );

    const scrollTo = (event) => {
        event.preventDefault();

        if (languageSelectorRef.current) {
            const currentTarget = event.currentTarget;
            const anchorId = currentTarget.dataset.anchorId
            const element = languageSelectorRef.current.querySelector('#' + anchorId);
            element.scrollIntoView({behavior: 'smooth'});
        }
    };

    const calculateHeight = () => {
        const announcementBar = document.querySelector('.ob-announcement-bar');
        const overlay = languageSelectorRef.current.querySelector('.ob-language-overlay');
        const header = document.querySelector('.zone-header');

        if (props.isMobile) {
            if (announcementBar && announcementBar.style.display !== 'none') {
                linksRef.current.style.height = `calc(${window.innerHeight}px - ${linksRef.current.offsetTop + announcementBar.offsetHeight}px)`
                overlay.style.height = `calc(${window.innerHeight}px - ${overlay.offsetTop + announcementBar.offsetHeight}px)`
            } else {
                linksRef.current.style.height = `calc(${window.innerHeight}px - ${linksRef.current.offsetTop}px)`
                overlay.style.height = `calc(${window.innerHeight}px - ${overlay.offsetTop}px)`
            }
        } else {
            if (header) {
                overlay.style.height = `calc(${window.innerHeight}px - ${overlay.offsetTop}px)`;
                linksRef.current.style.height = `${window.innerHeight - (linksRef.current.offsetTop + header.offsetHeight)}px`
            }
        }
    };

    // This is the label displayed and also serves as the CTA to trigger the language menu
    const currentLanguage = block.getFieldValue('currentLanguage');
    const title = block.getFieldValue('title');
    const regions = block.getFieldValue('regions');
    // [TODO] add BE copy for jumpToCopy and closeButtonCopy
    const jumpToCopy = 'Jump to';
    const closeButtonCopy = 'Close language selection';

    return (
        <SlideToggle
            ref={overlayToggleRef}
            duration={500}
            collapsed
            onExpanding={handleExpanding}
            onCollapsing={handleCollapsing}
            onExpanded={calculateHeight}
        >
            {({ onToggle, setCollapsibleElement }) => (
                <div className="ob-language-selector" ref={languageSelectorRef}>
                    <button className="ob-language-selector-toggle" onClick={() => {
                        onToggle();
                        calculateHeight();
                    }}>
                        {currentLanguage}
                        <Icon className="ob-language-selector-toggle-icon" name="wideChevronDown" size="0.8" />
                    </button>

                    <div className="language-overlay ob-language-overlay"
                         ref={setCollapsibleElement}
                         style={{display: 'none'}}
                     >
                        <div className="ob-language-overlay-inner">
                            <div className="ob-language-overlay-header">
                                <div className="ob-language-overlay-title">{title}</div>
                            </div>

                            <div className="ob-language-overlay-tabs">
                                <div className="ob-language-overlay-tabs-desktop">
                                    <ul className="mini-nav-regions ob-language-overlay-tabs">
                                        {regions.map((region, index) => (
                                            <li key={`region-nav-${index}`}>
                                                <a
                                                    href={stringifyAnchor({
                                                        ...anchor,
                                                        region: getAnchorIdFromEntity(region),
                                                    })}
                                                    data-anchor-id={getAnchorIdFromEntity(region)}
                                                    onClick={scrollTo}
                                                >
                                                    {region.fields.name}
                                                </a>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            </div>

                            <div className="ob-language-overlay-links" ref={linksRef}>
                                <div className="ob-language-overlay-tabs">
                                    <SlideToggle duration={500} collapsed>
                                        {({ onToggle, setCollapsibleElement }) => (
                                            <div className="ob-language-overlay-tabs-mobile">
                                                <button className="ob-language-overlay-tabs-toggle" onClick={onToggle}>
                                                    {jumpToCopy}
                                                </button>

                                                <div ref={setCollapsibleElement}
                                                    className="ob-language-overlay-tabs-mobile-inner"
                                                    style={{display: 'none'}}>
                                                    <ul className="mini-nav-regions">
                                                        {regions.map((region, index) => (
                                                            <li key={'region-nav-' + index}>
                                                                <a
                                                                    href={stringifyAnchor({
                                                                        ...anchor,
                                                                        region: getAnchorIdFromEntity(region),
                                                                    })}
                                                                    data-anchor-id={getAnchorIdFromEntity(region)}
                                                                    onClick={scrollTo}
                                                                >
                                                                    {region.fields.name}
                                                                </a>
                                                            </li>
                                                        ))}
                                                    </ul>
                                                </div>
                                            </div>
                                        )}
                                    </SlideToggle>
                                </div>
                                <ul className="market-selection">
                                    {regions.map((region, regionIndex) => (
                                        <li key={'region-' + regionIndex}>
                                            <div className="market-selection-title" id={getAnchorIdFromEntity(region)}>
                                                {region.fields.name}
                                            </div>

                                            <ul>
                                                {region.fields.markets.map((market, marketIndex) => (
                                                    <li key={'market-' + marketIndex}>
                                                        <a href={market.fields.link}>
                                                            {market.fields.nativeLanguageLabel}
                                                        </a>
                                                    </li>
                                                ))}
                                            </ul>
                                        </li>
                                    ))}
                                </ul>
                            </div>

                            <button className="ob-language-overlay-close-btn" onClick={onToggle} aria-label={closeButtonCopy}>
                                <Icon name="close" size="2.4" color={primaryColor} />
                            </button>
                        </div>
                    </div>
                </div>
            )}
        </SlideToggle>
    );
}
