import React from 'react';
import SpotlightPreorderBlock from './SpotlightPreorderBlock';

function createMockSpotlightPreorderProps(props) {
    return {
        isMobile : false,
        document: {
            body: 'body',
            title: 'title',
            anchorId: 'anchor id',
            classNames: 'class name',
            backgroundAsset: {},
            availableProducts: '',
            preOrderButtonLabel: 'label',
            preOrderCallToAction: "call to action",
            mobileBackgroundAsset: {}
        },
        index: 0,
        ...props
    };
}

describe('<SpotlightPreorderBlock /> rendering', () => {
    it('should render the Spotlight Preorder with the parameters supplied', () => {
        const props = createMockSpotlightPreorderProps();
        const wrapper = shallow(<SpotlightPreorderBlock {...props} />);
        expect(wrapper).toMatchSnapshot()
    });
});