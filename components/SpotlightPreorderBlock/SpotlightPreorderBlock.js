import React from 'react';
import Image from '../Image/Image';
import Button from '../Button/Button';
import Block from '../../helpers/Block';
import Heading from '../Heading/Heading';
import { Transition } from 'react-transition-group';
import OnePageScrollContext from '../OnePageScroll/OnePageScrollContext';
import PropTypes from 'prop-types';

import './SpotlightPreorderBlock.scss';

export default function SpotlightPreorder(props) {
    const {document, isMobile, index} = props;
    const block = new Block(document);

    const body = block.getFieldValue('body');
    const title = block.getFieldValue('title');
    const anchorId = block.getFieldValue('anchorId');
    const classNames = block.getFieldValue('classNames');
    const backgroundAsset = block.getFieldValue('backgroundAsset');
    const preOrderButtonLabel = block.getFieldValue('preOrderButtonLabel');
    const preOrderCallToAction = block.getFieldValue('preOrderCallToAction');
    const mobileBackgroundAsset = block.getFieldValue('mobileBackgroundAsset');

    const isActiveBlock = (indexArg) => {
        return index === indexArg;
    };

    return (
        <Transition in={isActiveBlock()} timeout={600}>
            {() => (
                <OnePageScrollContext.Consumer>
                    { context =>
                        <div className={`ob-spotlightContentBlock ob-spotlight-preorder ${classNames}`} id={anchorId} data-isactiveblock={isActiveBlock(context.currentIndex)} >
                            {isMobile && mobileBackgroundAsset && 
                                <Image image={mobileBackgroundAsset} />
                            }
                            <Image image={backgroundAsset && !isMobile ? backgroundAsset : undefined} customStyles={{backgroundSize: 'cover', backgroundPositionY: '-15px', backgroundRepeat: 'no-repeat'}}>
                                <div className={`ob-spotlight-preorder__wrapper`}>
                                    <Heading className={`ob-spotlight-preorder__title`}>{title}</Heading>
                                    <p className={`ob-spotlight-preorder__body`}>{body}</p>
                                    <Button tag="a" href={preOrderCallToAction}>{preOrderButtonLabel.fields.text}</Button>
                                </div>
                            </Image>
                        </div>
                    }
                </OnePageScrollContext.Consumer>)
            }
        </Transition>
    );
}

SpotlightPreorder.propTypes = {
    document: PropTypes.any,
    isMobile: PropTypes.any,
    index: PropTypes.any,
}