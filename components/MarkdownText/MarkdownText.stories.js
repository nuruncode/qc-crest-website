import React from 'react'
import MarkdownText from "./MarkdownText"

export default {
  title: 'Mark Down Text',
  component: MarkdownText
};

const FAKE_MARKDOWN_TEXT = "The __bold__ element of that story is not ~~Strikethrough~~";

const FAKE_DATA = {
  fields: {
    text: FAKE_MARKDOWN_TEXT,
    classNames: 'markdown'
  }
};
export const renderMarkdownText = () => <MarkdownText document={FAKE_DATA}/>;
