import React from 'react';
import ReactMarkdown from 'react-markdown';
import Block from '../../helpers/Block'
import PropTypes from "prop-types";
import htmlParser from 'react-markdown/plugins/html-parser';

const astPlugins = [
    htmlParser({
        isValidNode: node => node.type !== 'script',
        processingInstructions: [/* ... */]
    }),
];

export function Markdown({ source }) {
    return (
        <ReactMarkdown
          source={source}
          escapeHtml={false}
          astPlugins={astPlugins}
        />
    );
}

Markdown.propTypes = {
    source: PropTypes.any,
}

export default function MarkdownText(props) {
    const block = new Block(props);
    const document = block.getDocument();
    const Tag = props.isSpan ? 'span' : 'div';

    return ((document?.fields) ?
        <Tag className={block.getFieldValue('classNames')}>
           <ReactMarkdown source={block.getFieldValue('text')} escapeHtml={false} astPlugins={astPlugins} />
        </Tag>
    :
        <Tag className={props.className}>
           <ReactMarkdown source={props.value} escapeHtml={false} astPlugins={astPlugins} />
        </Tag>
    );
}

MarkdownText.propTypes = {
    value: PropTypes.string,
    isSpan: PropTypes.bool,
    className: PropTypes.string,
};
