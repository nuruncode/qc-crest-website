import PropTypes from 'prop-types';
import React from 'react';

export default Label;

export function Label({ label, tokens }) {
    if (!label) {
        return '';
    } else if (tokens) {
        return replaceTokensFromString(label.fields.text, tokens);
    } else {
        return label.fields.text;
    }
}

Label.propTypes = {
    label: PropTypes.object,
    tokens: PropTypes.object,
};

export function LabelHTML({ label, tokens, ...props }) {
    if (!label) {
        return (
            <span {...props} />
        );
    } else if (tokens) {
        return (
            <span dangerouslySetInnerHTML={{ __html: replaceTokensFromString(label.fields.text, tokens) }} {...props} />
        );
    } else {
        return (
            <span dangerouslySetInnerHTML={{ __html: label.fields.text }} {...props} />
        );
    }
}

LabelHTML.propTypes = {
    label: PropTypes.object,
    tokens: PropTypes.object,
};

export function replaceTokensFromLabel(label, tokens) {
    return replaceTokensFromString(label.fields.text, tokens);
}

export function replaceTokensFromString(s, tokens) {
    for (let tokenKey in tokens) {
        s = s.replace(new RegExp('{' + tokenKey + '}', 'g'), tokens[tokenKey]);
    }

    return s;
}