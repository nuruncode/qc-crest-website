import React from 'react';
import Label from './Label';

export default { title: 'Label' };

const label = { fields : { text : 'this is the label for this story'}};
const labelWithToken = { fields : { text : 'this is the "{tokenToReplace}" for this story'}};
const tokens =  {tokenToReplace: "replaced label"};

export const renderLabel = () => <Label label={label}/>;

export const renderLabelWithToken = () => <Label label={labelWithToken} tokens={tokens} />;

