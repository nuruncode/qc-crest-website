import React from 'react';
import Block from '../../helpers/Block';
import { focusOnElement } from '../../helpers/Utils';

export default function SkipNavigationBlock(props) {
    const block = new Block(props);

    const label = block.getFieldValue('label');
    const targetID = block.getFieldValue('targetID');

    const goToContent = (event) => {
        if (targetID !== '' && document.getElementById(targetID) !== null) {
            return;
        }

        event.preventDefault();
        const mainContent = document.querySelector('[role="main"]');
        focusOnElement(mainContent);
    }

    return (
        <a onClick={(event) => goToContent(event)} className={`visually-hidden ob-skip-navigation`} href={`#${targetID}`}>
            {label.fields.text}
        </a>
    )
}