### Examples

**Display 1 - XL**

Whith the class 'ob-display-1-xl' 

```
<Heading className='ob-display-1-xl'>Display 1 XL</Heading>
```

**Display 1 - XS**

Whith the class 'ob-display-1-xs' 

```
<Heading className='ob-display-1-xs'>Display 1 XS</Heading>
```

**Display 2 - XL**

Whith the class 'ob-display-2-xl' 

```
<Heading className='ob-display-2-xl'>Display 2 XL</Heading>
```

**Display 2 - XS**

Whith the class 'ob-display-2-xs' 

```
<Heading className='ob-display-2-xs'>Display 2 Xs</Heading>
```

**Display 3 - XL**

Whith the class 'ob-display-3-xl' 

```
<Heading className='ob-display-3-xl'>Display 3 XL</Heading>
```

**Display 3 - XS**

Whith the class 'ob-display-3-xs' 

```
<Heading className='ob-display-3-xs'>Display 3 XS</Heading>
```

**Display 1 - XL - Inverted(White text)**

Whith the class 'ob-display-1-xl' and whiteText=true

```
<div style={{ backgroundColor: '#3D3D41' }}>
    <Heading className='ob-display-1-xs' whiteText='true'>Display 1 XS</Heading>
</div>
```

