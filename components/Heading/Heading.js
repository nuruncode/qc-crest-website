import React from 'react'
import PropTypes from 'prop-types'
import './Heading.scss'

function Heading({children, tag: Tag, id, className, styles, whiteText}) {

    if (!children) {
        return null
    }

    className = className ? `ob-heading ${className}` : `ob-heading`;

    return (
        <Tag id={id} className={className} data-white-text={whiteText} style={styles}>
            <span dangerouslySetInnerHTML={{__html: children}}></span>
        </Tag>
    )
}

Heading.propTypes = {
    /**
    * The html to be structured
    */
    children: PropTypes.any,

    /**
    * The tag to be used for the containing element
    */
    tag: PropTypes.string,

    /**
    * The ID attribute be added to the element
    */
    id: PropTypes.string,

    /**
    * The Class attribute be added to the element
    */
    className: PropTypes.string,

    /**
    * Set white text if needed
    */
    whiteText: PropTypes.bool,

    /**
     * Add custom styles to the heading
     */
    styles: PropTypes.object
}

Heading.defaultProps = {
    tag: 'h2',
    className: '',
    styles: {},
    whiteText: false
  }

export default Heading

