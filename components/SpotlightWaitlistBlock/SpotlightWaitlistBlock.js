import React, {useState, useEffect, useRef} from 'react';
import Modal from 'react-modal';
import { Transition } from 'react-transition-group';
import OnePageScrollContext from '../OnePageScroll/OnePageScrollContext';
import Block from '../../helpers/Block';

import Heading from '../Heading/Heading'
import Image from '../Image/Image';
import Icon from '../Icon/Icon';

import './SpotlightWaitlistBlock.scss';

import getConfig from "next/config";
const {publicRuntimeConfig} = getConfig();

export default function SpotlightWaitlist(props) {
    const {document, isMobile, index} = props;
    return (document) ?
        renderSpotlightWaitlistBlock(document, isMobile, index) :
        '';
}

/*** TODOS:
 * Add Proper handling when an error occurs once we have the url for the form (~lines 82-97)
 ***/

function renderSpotlightWaitlistBlock(entity, isMobile, index) {
    const block = new Block(entity);

    const [erroneousInputs, setErroneousInputs] = useState([]);
    const [serverSideErrorMessage, setServerSideErrorMessage] = useState(false);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const modalRef = useRef(null);
    const customStyles = {
        overlay: {
            backgroundColor: 'none'
        },
        content : {
            position            : 'relative',
            border              : 'none',
            top                 : '0',
            left                : '0',
            right               : 'auto',
            bottom              : 'auto',
            padding             : '0',
            marginRight         : '0',
            height              : '100%',
            boxShadow           : 'rgba(0, 0, 0, 0.5) 0 2px 4px',
            background          : 'rgba(255, 255, 255, 0.5)'
        }
    }

    useEffect(
        () => {
            Modal.setAppElement('.js-modal-container');
        },[]
    );

    const closeModal = () => {
        setIsModalOpen(false);
    };
    
    const handleSubmit = event => {
        event.preventDefault();
        const form = event.target;
        const data = new FormData(form);
        const inputs = form.querySelectorAll('input');
        let errors = [];

        inputs.forEach(input => {
            input.classList.remove('--error');
            if (input.hasAttribute('required')) {
                if (input.value.length === 0) {
                    errors.push({id: input.getAttribute('id')});
                    input.classList.add('--error');
                } else if (input.hasAttribute('pattern')) {
                    if (checkPattern(input)) {
                        errors.push({id: input.getAttribute('id')});
                        input.classList.add('--error');
                    }
                }
            } else if (input.value.length > 0 && input.hasAttribute('pattern')) {
                if (checkPattern(input)) {
                    errors.push({id: input.getAttribute('id')});
                    input.classList.add('--error');
                }
            }
        });

        if (errors.length === 0) {
            const registrationUrl = getRegistrationUrl();
            const requestHeader = getRegistrationHeader();
            const requestBody = formatBody(data.get('firstname'), data.get('email'));

            fetch(registrationUrl, {
                method: 'POST',
                headers: requestHeader,
                body: JSON.stringify(requestBody)
            })
            .then((response, e) => {
                // 201 for created
                if (response.status === 201) {
                    setErroneousInputs([]);
                    setIsModalOpen(true);
                } else {
                    return response.json();
                }
            })
            .then(object => {
                // print server side error message
                if(object && object.error && object.error.message) {
                    setServerSideErrorMessage(object.error.message);
                } else {
                    setServerSideErrorMessage(false);
                }
            })
            .catch(error => console.error('An error occured', error));
        } else {
            setErroneousInputs([...errors]);
        }
    };

    const checkPattern = input => {
        return input.value.search(new RegExp(input.getAttribute('pattern'))) < 0;
    }

    const title = block.getFieldValue('title');
    const body = block.getFieldValue('body');
    const ctaLabel = block.getFieldValue('callToActionLabel');
    const legalText = block.getFieldValue('legalText');
    const formLabels = block.getFieldValue('formLabels');
    const classNames = block.getFieldValue('classNames');
    const anchorId = block.getFieldValue('anchorId');
    const backgroundAsset = block.getFieldValue('backgroundAsset');
    const mobileBackgroundAsset = block.getFieldValue('mobileBackgroundAsset');
    const thankYouTitleLabel = block.getFieldValue('thankYouTitleLabel').fields.text;
    const thankYouBackgroundAsset = block.getFieldValue('thankYouBackgroundAsset');
    const thankYouDescriptionLabel = block.getFieldValue('thankYouDescriptionLabel').fields.text;
    const duration = 600;

    const isActiveBlock = (indexArg) => {
        return index === indexArg;
    };

    return (
        <Transition in={isActiveBlock()} timeout={duration}>
            {() => (
                <OnePageScrollContext.Consumer>
                    { context =>
                        <div className={`ob-spotlightContentBlock ob-spotlight-waitlist js-modal-container ${classNames}`} id={anchorId} data-isactiveblock={isActiveBlock(context.currentIndex)} >
                            { isMobile &&
                                <div className={`ob-spotlight-waitlist__mobile-img`}>
                                    <Image image={mobileBackgroundAsset ? mobileBackgroundAsset : undefined} />
                                </div>
                            }
                            <Image image={backgroundAsset && !isMobile ? backgroundAsset : undefined}  customStyles={{backgroundSize: 'cover', backgroundPositionY: '-15px', backgroundRepeat: 'no-repeat'}}>
                                <div className={`ob-spotlight-waitlist__wrapper`}>
                                    <Heading className={`ob-spotlight-waitlist__title`}>{title}</Heading>
                                    <div className={`ob-spotlight-waitlist__body`}>{body}</div>

                                    {   serverSideErrorMessage &&
                                        <div className={`ob-spotlight-waitlist__form-error`}>{serverSideErrorMessage}</div>
                                    }

                                    <form className={`ob-spotlight-waitlist__form`} onSubmit={handleSubmit} noValidate>
                                        <ul className={`ob-spotlight-waitlist__form-list`}>
                                            {
                                                formLabels.map((formLabel, formLabelIndex) => (
                                                    <li className={`ob-spotlight-waitlist__form-list-item`} key={'formLabel-' + formLabelIndex}>
                                                        <label htmlFor={formLabel.fields.id} className={`ob-spotlight-waitlist__form-label`}>{formLabel.fields.label.fields.text}</label>
                                                        <input
                                                            type={formLabel.fields.type}
                                                            pattern={formLabel?.fields?.validation?.fields?.pattern}
                                                            required={formLabel?.fields?.validation?.fields?.required}
                                                            id={formLabel.fields.id}
                                                            name={formLabel.fields.id}
                                                            className={`ob-spotlight-waitlist__form-input`} />
                                                            
                                                            { erroneousInputs.length > 0 &&
                                                                erroneousInputs.map((error, index) => {
                                                                    return error.id === formLabel.fields.id && formLabel?.fields?.validation?.fields?.errorMessage &&
                                                                        <p key={index} className={`ob-spotlight-waitlist__form-error`}>
                                                                            {formLabel.fields.validation.fields.errorMessage}
                                                                        </p>
                                                                })
                                                            }
                                                    </li>
                                                ))
                                            }
                                        </ul>
                                        <button type="submit" className={`ob-button`}>{ctaLabel.fields.text}</button>
                                    </form>
                                    <div className={`ob-spotlight-waitlist__legal-text`}>{legalText}</div>
                                </div>
                            </Image>
                            <Modal
                                isOpen={isModalOpen}
                                onRequestClose={closeModal}
                                closeTimeoutMS={250}
                                style={customStyles}
                                ref={modalRef}
                            >
                                <div className={`ob-spotlight-waitlist__modal-container`}>
                                    {!isMobile &&
                                        <div className={`ob-spotlight-waitlist__modal-content`}>
                                            <Image image={thankYouBackgroundAsset} />
                                        </div>
                                    }
                                    <div className={`ob-spotlight-waitlist__modal-text-container ob-spotlight-waitlist__modal-content`}>
                                        <Heading>{thankYouTitleLabel}</Heading>
                                        <p className={`ob-spotlight-waitlist__modal-text-content`}>{thankYouDescriptionLabel}</p>
                                    </div>
                                    {isMobile &&
                                        <div>
                                            <Image image={thankYouBackgroundAsset} />
                                        </div>
                                    }
                                    <button onClick={closeModal} className={`ob-spotlight-waitlist__close-modal`}>
                                        <Icon name="close" size="2.4" />
                                    </button>
                                </div>
                            </Modal>
                        </div>
                    }
                </OnePageScrollContext.Consumer>)}
        </Transition>
    )
}

function getRegistrationUrl() {
    return publicRuntimeConfig.GCS_URl + '/registration';
}

function getRegistrationHeader() {
    let registrationHeader = new Headers();

    registrationHeader.append('Content-Type', 'application/json');
    registrationHeader.append('access_token', publicRuntimeConfig.GCS_ACCESS_TOKEN);

    return registrationHeader;
}

function formatBody(firstname, email) {
    let body = {};
    body.campaign = {
        'campaignId': publicRuntimeConfig.GCS_IO_CAMPAIGN_ID,
        'locale': publicRuntimeConfig.MARKET
    };
    body.consumer = {'firstName': firstname, 'emailAddress': email};
    body.address = [];
    body.order = {'optIn': true};

    return body;
}