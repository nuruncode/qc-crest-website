import React, {Component} from 'react';
import Block from '../../helpers/Block';
import throttle from 'lodash/throttle';
import Image from '../Image/Image';
import LazyLoad from 'react-lazyload';
import {Waypoint} from 'react-waypoint';
import VideoPlayer from '../VideoPlayer/VideoPlayer';
import Modal from 'react-modal';
import Icon from '../Icon/Icon';
import Heading from '../Heading/Heading'
import BodyText from '../BodyText/BodyText'
import "./ContentVideo.scss"

/**
 * TODOs
 *  - Proper style for video modal
 *  - Proper interaction for video transcript (currently in visuallyhidden container)
 *  - add back-end copy for playVideoCopy and closeVideoCopy (for accessibility purposes)
 */

/**
 *
 * Supported Classes:
 *  By default the background is white and the text color is primary-grey.
 *  - white-text : change the text color to white
 *  - align-center : force a text-align: center without impacting the text container alignment
 *
 *  - background-primary-grey : change the background to primary grey
 *  - background-secondary-grey : change the background to secondary grey
 *  - background-secondary-off-white : change the background to secondary-off-white
 *  - background-isolate-grey : change the background to isolate grey
 *  - background-primary-blue : change the background to primary-blue
 *  - background-secondary-blue : change the background to secondary-blue
 *
 */

const customStyles = {
    overlay: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)'
    },
    content : {
        border              : 'none',
        top                 : '50%',
        left                : '50%',
        right               : 'auto',
        bottom              : 'auto',
        padding             : '0',
        marginRight         : '-50%',
        transform           : 'translate(-50%, -50%)',
        maxWidth            : '123.6rem',
        width               : '90%'
    }
};

const desktopBreakpointQuery = '(min-width: 768px)';

class ContentVideo extends Component {

    constructor(props) {
        super(props);
        this.block = new Block(props);
        this.videoBlockRef = React.createRef();
        this.modalRef = React.createRef();

        this.state = {
            modalIsOpen: false,
            isMobile: true,
        };

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.trapFocusInModal = this.trapFocusInModal.bind(this);
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    trapFocusInModal(event) {
        // The modal traps focus by default, however 
        // the Cloudinary video player has many hidden "focusable" elements.
        // The modal was trying to focus on elements that were hidden in the DOM, 
        // so it focused on the rest of the page instead.

        const element = event.currentTarget;
        const focusableElements = element.querySelectorAll('.vjs-control-bar button:not([disabled]), .vjs-control-bar a[href]:not([disabled]), .ob-modal-close');
        const firstFocusableElement = focusableElements[0];
        const lastFocusableElement = focusableElements[focusableElements.length - 1];
        const KEYCODE_TAB = 9;
        
        const isTabPressed = (event.key === 'Tab' || event.keyCode === KEYCODE_TAB);
        
        if (!isTabPressed) { 
            return; 
        }
        
        if ( event.shiftKey ) /* shift + tab */ {
            if (document.activeElement === firstFocusableElement) {
                lastFocusableElement.focus();
                event.preventDefault();
            }
        } else /* tab */ {
            if (document.activeElement === lastFocusableElement) {
                firstFocusableElement.focus();
                event.preventDefault();
            }
        }
    }

    valueOf(name) {
        return this.block.getFieldValue(name);
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResizeThrottled);
        this.handleResize();
        Modal.setAppElement('.js-modal-container');
        if (this.modalRef && this.modalRef.current) {
            this.modalRef.current.node.addEventListener('keydown', this.trapFocusInModal);
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResizeThrottled);
        if (this.modalRef && this.modalRef.current) {
            this.modalRef.current.node.removeEventListener('keydown', this.trapFocusInModal);
        }
    }

    handleResize() {
        this.setState({
            isMobile: ! window.matchMedia(desktopBreakpointQuery).matches
        });
    }

    handleResizeThrottled = throttle(this.handleResize.bind(this), 100);

    _handleWaypointEnter = () => {
        // Animation on the text container on the "onEnter" event
        // Trigger by waypoint
        const node = this.videoBlockRef.current;
        node.classList.add('show');
    }

    renderVideo = (video) => {

        if(video) {
            return <VideoPlayer video={video} />
        } else {
            return '';
        }
    };

    render() {
        // Can have the class white-text for color white,
        // or align-center for centering the text (Left align by default)
        // Can have the class button-right or button-top for play button position

        const mobileLayout = this.valueOf('mobileLayout');
        const desktopLayout = this.valueOf('desktopLayout');
        let className = 'ob-contentBlock ob-videoBlock js-modal-container';

        if (this.valueOf('classNames')) {
            className += this.valueOf('classNames');
        }

        if (this.state.isMobile) {
            className += ' layout-' + mobileLayout;
        } else {
            className += ' layout-' + desktopLayout;
        }

        const isWhiteText = className.includes('white-text');
        const mobileBkgImage = mobileLayout.includes('background-image');
        const desktopBkgImage = desktopLayout.includes('background-image');
        const imageIsBackground = (this.state.isMobile && mobileBkgImage) || (!this.state.isMobile && desktopBkgImage);

        const title = this.valueOf('title');
        const titleLevel = this.valueOf('titleLevel');
        const anchorId = this.block.getAnchorId();
        const video = this.block.getFields().video.fields;
        const keyframeImage = video.keyframe;
        const textContainerMarginBottom = this.valueOf('textContainerMarginBottom');
        const textContainerMarginTop = this.valueOf('textContainerMarginTop');
        const playIconColor = this.valueOf('playIconColor');
        const backgroundColor = this.valueOf('backgroundColor');
        const playVideoCopy = 'Play video';
        const closeVideoCopy = 'Close video';

        const getPlayIconSize = () => {
            let playIconSize = '6';

            if (!this.state.isMobile) {
                playIconSize = '10';
            }
            return playIconSize;
        };

        return (
            <section className={className} id={anchorId}
                 style={{
                     backgroundColor: backgroundColor
                 }}>
                {imageIsBackground ? (
                    <LazyLoad offset={200}>
                        <Image image = {keyframeImage}>
                            <div className="ob-videoContent-wrapper">
                                <Waypoint onEnter={this._handleWaypointEnter} />
                                <div className='ob-videoContent-textContainer'
                                     ref={this.videoBlockRef}
                                     style={{
                                         marginBottom: textContainerMarginBottom,
                                         marginTop: textContainerMarginTop
                                     }}>
                                    <div className='ob-videoContent-text'>

                                        <Heading whiteText={isWhiteText} tag={`h${titleLevel}`} className="ob-display-2-xl">{title}</Heading>

                                        <BodyText whiteText={isWhiteText}>{this.valueOf('description')}</BodyText>
                                    </div>

                                    <div className='ob-videoContent-button-container'>
                                        <button onClick={this.openModal}
                                                className='ob-videoContent-button'
                                                aria-label={playVideoCopy}>
                                            <Icon name="play" color={playIconColor} viewBox={31.4} size={getPlayIconSize()} />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </Image>
                    </LazyLoad>
                ) : (
                    <LazyLoad offset={200}>
                        <div className="ob-videoContent-wrapper">
                            <Waypoint onEnter={this._handleWaypointEnter} />
                            <div className='ob-videoContent-textContainer'
                                 ref={this.videoBlockRef}>
                                <div className='ob-videoContent-text'>
                                    <Heading whiteText={isWhiteText} tag={`h${titleLevel}`} className="ob-display-2-xl">{title}</Heading>

                                    <BodyText whiteText={isWhiteText}>{this.valueOf('description')}</BodyText>
                                </div>
                            </div>
                        </div>

                        <div className='ob-videoContent-image'>
                            <Image image = {keyframeImage}></Image>

                            <button onClick={this.openModal}
                                    className='ob-videoContent-button'
                                    aria-label={playVideoCopy}>
                                <Icon name="play" viewBox={31.4} color={playIconColor} size={getPlayIconSize()} />
                            </button>
                        </div>
                    </LazyLoad>
                )}

                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    closeTimeoutMS={250}
                    style={customStyles}
                    contentLabel={this.valueOf('title')}
                    ref={this.modalRef}
                >
                    <button className="ob-modal-close"
                            onClick={this.closeModal}
                            aria-label={closeVideoCopy}>
                        <Icon name="close" size="2.4" color="#FFFFFF" />
                    </button>
                    {this.renderVideo(video) }
                </Modal>
            </section>
        )
    }
}

export default ContentVideo