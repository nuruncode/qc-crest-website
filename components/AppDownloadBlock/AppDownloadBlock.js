import './AppDownloadBlock.scss';

import Block from '../../helpers/Block';
import Eyebrow from '../Eyebrow/Eyebrow';
import Heading from '../Heading/Heading';
import Image from '../Image/Image';
import ImageFactory from '../../adapters/cloudinary/ImageFactory';
import React from 'react';

export default function AppDownloadBlock(props) {
    const block = new Block(props);

    const className = block.getFieldValue('classNames');
    const anchorId = block.getAnchorId();

    const title = block.getFieldValue('title');
    const eyebrow = block.getFieldValue('eyebrow');
    const backgroundAsset = block.getFieldValue('backgroundAsset');
    const mobileBackgroundAsset = block.getFieldValue('mobileBackgroundAsset');
    const links = block.getFieldValue('downloadLinks');

    const mobileRatio = mobileBackgroundAsset?.fields?.imageRendition?.fields?.aspectRatio;
    const desktopRatio = backgroundAsset?.fields?.imageRendition?.fields?.aspectRatio;

    const storeAssets = links?.map(link => {
        const image = link?.fields?.image;
        const alt = image?.fields?.alternateText;
        const assetId = image?.fields?.assetId;

        return {
            url: link?.fields?.url,
            alt,
            image : assetId && {
                mobile: ImageFactory.buildImageUrlByHeight(assetId, '50'),
                desktop: ImageFactory.buildImageUrlByHeight(assetId, '65'),
            },
        }
    });

    return (
        <div className={`ob-app-download-wrapper-1 ${className}`} id={anchorId}>
            <div className="ob-app-download-wrapper-2">
                <div className="ob-app-download-wrapper-3">
                    <div className="ob-app-download">
                        <div className="ob-app-download-mobile-background">
                            <Image forceBackground image={mobileBackgroundAsset} />
                        </div>
                        <div className="ob-app-download-desktop-background">
                            <Image forceBackground image={backgroundAsset} />
                        </div>
                        <div className="ob-app-download-content-wrapper">
                            <div className="ob-app-download-content">
                                <Eyebrow className="ob-app-download-eyebrow">{eyebrow}</Eyebrow>
                                <Heading className="ob-app-download-heading">{title}</Heading>
                                <div className="ob-app-download-stores">
                                {storeAssets?.map((asset, i) => asset.image && (
                                    <a key={i} className="ob-app-download-store" href={asset.url}>
                                        <img data-format="mobile" alt={asset.alt} src={asset.image.mobile} />
                                        <img data-format="desktop" alt={asset.alt} src={asset.image.desktop} />
                                    </a>
                                ))}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="ob-app-download-ratios">
                        {!!mobileRatio && <div data-format="mobile" className="ob-app-download-ratio" style={{paddingBottom: `${(100 / mobileRatio).toFixed(2)}%`}}/>}
                        {!!desktopRatio && <div data-format="desktop" className="ob-app-download-ratio" style={{paddingBottom: `${(100 / desktopRatio).toFixed(2)}%`}}/>}
                    </div>
                </div>
            </div>
        </div>
    )
}