import './Eyebrow.scss'

import PropTypes from 'prop-types'
import React from 'react'

function Eyebrow({children, tag: Tag, id, className, styles, whiteText, blueEyebrow}) {

    if (!children) {
        return null
    }

    className = className ? `ob-eyebrow ${className}` : `ob-eyebrow`;

    return (
        <Tag id={id} 
            className={className} 
            data-white-text={whiteText} 
            data-blue-text={blueEyebrow}
            style={styles}>

            {children}
        </Tag>
    )
}

Eyebrow.propTypes = {
    /**
    * The html to be structured
    */
    children: PropTypes.any,

    /**
    * The tag to be used for the containing element
    */
    tag: PropTypes.string,

    /**
    * The ID attribute be added to the element
    */
    id: PropTypes.string,

    /**
    * The Class attribute be added to the element
    */
    className: PropTypes.string,

    /**
     * Add custom styles to the eyebrow
     */
    styles: PropTypes.object,

    /**
    * Set white text if needed
    */
    whiteText: PropTypes.bool,

    blueEyebrow: PropTypes.any,
}

Eyebrow.defaultProps = {
    tag: 'p',
    className: '',
    styles: {},
    whiteText: false
  }

export default Eyebrow

