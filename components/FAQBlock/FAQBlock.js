import React, { useCallback, useState, useRef, useEffect } from 'react';
import { Markdown } from '../MarkdownText/MarkdownText';
import Icon from '../Icon/Icon';
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';
import './FAQBlock.scss';
import '../Button/Button.scss'
import SearchBar from '../SearchBar/SearchBar';

function ToggleButton({text, isActive, onClick}) {
    return (
        <button
            className="ob-outline-button"
            type="button"
            role="switch"
            aria-checked={isActive ? 'true' : 'false'}
            onClick={onClick}
        >
            {text}
        </button>
    );
}

ToggleButton.propTypes = {
    text: PropTypes.any,
    isActive: PropTypes.any,
    onClick: PropTypes.any, 
}

function FAQQA({qa, documentKey, activeItem, onClickCallback}) {

    const [isOpen, setIsOpen] = useState(false);
    const [answerHeight, setAnswerHeight] = useState(0);
    const answerRef = useRef(null);

    const onClickHandler = useCallback(() => {
        onClickCallback(documentKey === activeItem ? false : documentKey);
    }, [])

    const calculateHeight = useCallback(
        () => {
            if (answerRef.current) {
                setAnswerHeight(answerRef.current.scrollHeight);
            }
        },
        [],
    );

    useEffect(() => {
        setIsOpen(documentKey === activeItem)
    },[activeItem])

    return (
        <CSSTransition in={isOpen} timeout={800} onEntering={calculateHeight} onExit={calculateHeight}>
            <div className={'ob-faq__qa'}>
                <button onClick={onClickHandler} className="ob-faq__question">
                    <div className="ob-faq__question-toggle-wrapper">
                        <div className="ob-faq__question-toggle">
                            {isOpen ? <Icon name="minus6" viewBox={6} />
                                : <Icon name="plus" viewBox={10}/>}
                        </div>
                    </div>
                    <div className="ob-faq__question-text">
                        {qa.question}
                    </div>
                </button>
                <div ref={answerRef} className="ob-faq__answer" style={{height: answerHeight}}>
                    {!!qa.answer && <div className="ob-faq__answer-content">
                        <Markdown source={qa.answer}/>
                    </div>}
                </div>
            </div>
        </CSSTransition>
    );
}

FAQQA.propTypes = {
    qa: PropTypes.any,
    documentKey: PropTypes.any,
    activeItem: PropTypes.any,
    onClickCallback: PropTypes.any,
}

FAQQA.defaultProps = {
    qa: {}
}

function FAQ(props) {
    const {extraAttributes} = props;

    const popularTopicsText = "Popular topics others look for".toUpperCase();
    const results = extraAttributes?.results?.map(result => result?.fields) || [];
    const categories = [...new Set(results.map(result => result.categories))];

    const [filteredResults, setFilteredResults] = useState(results);
    const [selectedCategories, setSelectedCategories] = useState([]);

    const filterResults = () => {
        let matchingResults = results.filter(result => {
            for (let i = 0; i < selectedCategories.length; ++i) {
                const selectedCategory = selectedCategories[i];

                if (result.categories.some(category => category === selectedCategory)) {
                    return true;
                }
            }

            if(selectedCategories.length == 0) {
                return true;
            } else {
                return false;
            }
        });

        setFilteredResults(matchingResults);
    };

    const handleCategorySelection = (category, selectedCategories, setSelectedCategories, isActive, setIsActive) => {
        setIsActive(prevState => !prevState);
        const currentCategory = category[0];
        let isSelected = isActive;

        if(isSelected === false) {
            selectedCategories.push(currentCategory);
            setSelectedCategories([...selectedCategories]);
        } else {
            selectedCategories.splice(selectedCategories.indexOf(currentCategory), 1);
            setSelectedCategories([...selectedCategories]);
        }

        filterResults();
    };


    const searchResultsTitle= "We found 6 results for Warranty";

    const handleSearch = useCallback(
        () => {
        },
        []
    );

    const [activeItem, setIsActiveItem] = useState(false);

    const onClickCallback = useCallback((id) => {
        setIsActiveItem(prevState => prevState === id ? false : id);
    }, []);

    return (
        <div className="ob-faq">
            <div className="ob-faq__search">
                <div className="ob-faq__search-title">
                    {"Cest Support".toUpperCase()}
                </div>

                <SearchBar onSearch={handleSearch} />
            </div>

            <div className="ob-faq__popular-topics">
                <div className="ob-faq__popular-topics-title">
                    {popularTopicsText}
                </div>

                <div className="ob-faq__popular-topics-container">
                    {categories.map((category, categoryIndex) => {
                        const [isActive, setIsActive] = useState(false);

                        return (<div className="ob-faq__popular-topics__toggle-wrapper" key={categoryIndex}>
                            <ToggleButton key={categoryIndex} text={category}
                                          isActive={isActive}
                                          onClick={() => handleCategorySelection(category, selectedCategories, setSelectedCategories, isActive, setIsActive)}
                            />
                        </div>)
                    })}
                </div>
            </div>

            <div className="ob-faq__search-results">
                <div className="ob-faq__search-results-title">
                    {searchResultsTitle}
                </div>

                <div className="ob-faq__qas">
                    {filteredResults.map((qa, i) => {
                        return (
                            <FAQQA
                                key={i}
                                qa={qa}
                                documentKey={i}
                                activeItem={activeItem}
                                onClickCallback={onClickCallback}/>
                        );
                    })}
                </div>
            </div>
        </div>
    )
}

FAQ.propTypes = {
    extraAttributes: PropTypes.any,
}

FAQ.defaultProps = {
    extraAttributes: {},
    document: {},
};

export default FAQ;