import React, {Component} from 'react';
import Block from '../../helpers/Block';
import SlideToggle from 'react-slide-toggle';
import Icon from "../Icon/Icon";
import "./AnnouncementBar.scss"
import {isLater} from '../../helpers/functions'
import {calculateHeaderHeight} from '../../helpers/Utils'


class Content extends Component {

    constructor(props) {
        super(props);
        this.block = new Block(props);
        this.announcementBar = React.createRef();
        this.disableDays = this.block.getFieldValue('disableDays');
    }

    valueOf(name) {
        return this.block.getFieldValue(name);
    }

    componentDidMount() {
        //Display announcementBar is there isn't localStorage or Storage date < Now
        const storage = localStorage.getItem('AnnouncementBarDisableDate')
        if (!storage || (storage && isLater(storage))) {
            this.announcementBar.current.onToggle();
        } else {
            document.querySelector('.ob-announcement-bar').remove();
        }
    }

    render() {
        let className = 'ob-announcement-bar ' + this.block.getFieldValue('classNames');
        // [TODO] add BE copy for closeButtonCopy
        const closeButtonCopy = 'Close announcement';

        const reduceLayoutPadding = () => {
            const utilityHeader = document.querySelector('.ob-utilityBar');
            const mainMenu = document.querySelector('.main-menu');
            const height = utilityHeader.clientHeight + mainMenu.clientHeight;
            const layout = document.querySelector('.layout:not(.ob-spotlight-layout)');
            layout.style.paddingTop = `${height}px`;

            const productSubNav = document.querySelector('.ob-product-sub-nav.is-sticky');
            if (productSubNav) {
                productSubNav.style.top = `${height}px`;
            }
        }

        const handleClose = () => {
            if (this.disableDays) {
                var today = new Date();
                var disableDays = new Date();
                disableDays.setDate(today.getDate()+this.disableDays);
                localStorage.setItem('AnnouncementBarDisableDate', disableDays);
            }
        }

        return (
            <SlideToggle
                ref={this.announcementBar}
                collapsed
                duration={500} 
                onCollapsing={reduceLayoutPadding}
                onExpanded={calculateHeaderHeight}
                onCollapsed={handleClose}>
                {({ onToggle, setCollapsibleElement }) => (
                    <div className={className}
                         style={{display: 'none'}}
                         ref={setCollapsibleElement}>
                        <div className='ob-announcement-bar-inner'>
                            <div className='description'>
                                {this.valueOf('description')}
                            </div>

                            <a className='ob-announcement-bar-cta'href={this.valueOf('callToActionLink')}>
                                {this.valueOf('callToActionLabel')}
                            </a>

                            <button className='ob-announcement-bar-close-btn'
                                    onClick={onToggle} aria-label={closeButtonCopy}>
                                <Icon name="close" size="1.2" color={'#FFFFFF'} />
                            </button>
                        </div>
                    </div>
                )}
            </SlideToggle>
        )
    }
}

export default Content