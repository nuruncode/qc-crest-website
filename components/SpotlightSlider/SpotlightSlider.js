import Block from '../../helpers/Block';
import OnePageScroll from '../OnePageScroll/OnePageScroll';
import PropTypes from "prop-types";
import React from 'react';
import "./spotlight-custom.scss";

export default function SpotlightSlider(props) {

    const block = new Block(props);
    /*eslint-disable */
    let _pageScroller = null;
    /*eslint-enable */
    let extraAttributes = props.extraAttributes || {};
    let className = 'scroller-container ' + block.getFieldValue('classNames');
    return (
        <div className={className}>
            <OnePageScroll ref={c => _pageScroller = c} customEvent={props.customEvent} isMobile={props.isMobile}>
                {block.renderChildBlocks({extraAttributes: extraAttributes}, props.isMobile)}
            </OnePageScroll>
        </div>
    )
}

SpotlightSlider.propTypes = {
    className: PropTypes.string,
    extraAttributes: PropTypes.any,
    isMobile: PropTypes.any,
    customEvent: PropTypes.any,
};