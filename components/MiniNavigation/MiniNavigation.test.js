import React from 'react';
import MiniNavigation from "./MiniNavigation";

function createMockMiniNavigationBlockProps(props) {
    return {
        document: {
            fields: {
                backgroundGrey: false,
                contentType: "miniNavigationBlock",
                hierarchy: {
                    fields: {
                        contentType: "hierarchy",
                        destinations: [
                            {
                                fields: {
                                    contentType: "link",
                                    name: "name",
                                    title: "title",
                                    type: "internal",
                                    url: "url"
                                }
                            }
                        ],
                        name: "name"
                    }
                },
                name: "name",
                placeholderTitle: "placeholderTitle"
            }
        },
        isMobile: null,
        isTablet: false,
        isTiny: false,
        ...props,
    };
}

window.matchMedia = jest.fn().mockImplementation(query => {
    return {
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(),
        removeListener: jest.fn(),
    };
});

describe('Mini navigation block rendering', () => {
    it('should render mini navigation block with the parameters supplied', () => {
        const props = createMockMiniNavigationBlockProps();
        const wrapper = mount(<MiniNavigation {...props}/>);
        expect(wrapper).toMatchSnapshot()
    });
});