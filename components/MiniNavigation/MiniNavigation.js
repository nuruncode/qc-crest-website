import React, { Component, Fragment } from 'react';
import throttle from 'lodash/throttle';
import SlideToggle from 'react-slide-toggle';
import Block from '../../helpers/Block';
import { scrollIntoView } from '../../helpers/Scroll';
import HierarchyUtils from '../../helpers/HierarchyUtils';
import Icon from '../Icon/Icon';
import './MiniNavigation.scss';

class MiniNavigation extends Component {

    constructor(props) {
        super(props);
        this.block = new Block(props);

        this.placeholderTitle = this.block.props.document.fields.placeholderTitle;
        this.renderHierarchy = this.renderHierarchy.bind(this);

        this.state = {
            isMobile: false,
            isOpened: false,
        }
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResizeThrottled);
        this.handleResize();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResizeThrottled);
    }

    handleResize() {
        this.setState({
            isMobile: window.matchMedia("(max-width: 992px)").matches
        })
    }

    handleResizeThrottled = throttle(this.handleResize.bind(this), 100);

    renderHierarchy(onClickHandler) {
        const hierarchy = this.block.getFieldValue('hierarchy');
        const destinations = hierarchy.fields.destinations;
        return (
            <Fragment>
                {destinations &&
                    <ul className={`ob-mini-nav__list`}>
                        {destinations.map((destination, index) => (
                            <li
                                className={`ob-mini-nav__list-item`}
                                key={index}
                                data-index={index}>
                                {HierarchyUtils.renderDestination(destination, null, null, onClickHandler)}
                            </li>
                        ))
                        }
                    </ul>
                }
            </Fragment>
        )
    }

    render() {
        const className = this.block.getFieldValue('classNames');
        const greyBackground = this.block.getFieldValue('backgroundGrey') ? 'grey-background' : '';

        const onClickHandler = (event) => {
            event.preventDefault();
            const currentTarget = event.currentTarget;
            if (currentTarget) {
                const anchorId = currentTarget.getAttribute("href");
                const element = document.querySelector(anchorId);
                scrollIntoView(element, { behavior: 'smooth' });
            }
        };

        return (
            <div className={`ob-mini-nav ${className ? className : ''} ${greyBackground ? greyBackground : ''}`}>
                {this.state.isMobile ? (
                    <SlideToggle
                        collapsed
                        duration={500}
                        onExpanding={() => {
                            this.setState({ isOpened: true })
                        }}
                        onCollapsing={() => {
                            this.setState({ isOpened: false })
                        }}>
                        {({ onToggle, setCollapsibleElement }) => (
                            <Fragment>
                                <button className={`ob-mini-nav__title`} onClick={onToggle}>
                                    <span>{this.placeholderTitle}</span>
                                    <span className={`${this.state.isOpened ? 'is-open ' : ''}ob-mini-nav__icon`}>
                                        <Icon name='chevronDown' />
                                    </span>
                                </button>

                                <div ref={setCollapsibleElement}>
                                    {this.renderHierarchy(onClickHandler)}
                                </div>
                            </Fragment>
                        )}
                    </SlideToggle>
                ) : (
                        <Fragment>
                            {this.renderHierarchy(onClickHandler)}
                        </Fragment>
                    )}
            </div>
        )
    }
}

export default MiniNavigation