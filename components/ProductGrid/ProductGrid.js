import './ProductGrid.scss';

import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import {chunk, flatten, minimum} from '../../helpers/functions'
import { Waypoint } from 'react-waypoint';
import Button from '../Button/Button';
import C from 'classnames';
import { CSSTransition } from 'react-transition-group';
import DotNav from '../DotNav/DotNav';
import Image from '../Image/Image';
import PropTypes from 'prop-types';
import SeeMoreButton from '../SeeMoreButton/SeeMoreButton';

const DEFAULT_IMAGE_RATIO = 1;

function makeLines(products, itemsPerLine) {
    return chunk(products, itemsPerLine)
    .map(chunk => {
        const aspectRatios = chunk
            .map(item => item ? 
                item?.fields?.mainAsset?.fields?.imageRendition?.fields?.aspectRatio: 
                DEFAULT_IMAGE_RATIO);

        const minAspectRatio = minimum(aspectRatios)

        return {aspectRatio: minAspectRatio, items: chunk};
    }).map(line => {
        // Temporary while aspect Ratios are too extreme
        const aspectRatio = Math.max(line.aspectRatio, 0.4);
        return {...line, aspectRatio};
    });
}

function ProductOverviewLoaded({
    whiteText, 
    aspectRatio, 
    imageDelay, 
    contentDelay, 
    description, 
    title, 
    slug, 
    buyNowText,
    image,
}) { 
    return (
        <div className={C('ob-product-grid__product', {'white-text': whiteText})}>
            {!!aspectRatio && <div className="ob-product-grid__product-image-wrapper-1">
                <div className="ob-product-grid__product-image-wrapper-2">
                    <div className="ob-product-grid__product-image-wrapper-3">
                        <div className="ob-product-grid__product-image-wrapper-4" style={{paddingBottom: `${100 / aspectRatio}%`}}>
                            <div className="ob-product-grid__product-image" {...(imageDelay && {style: {animationDelay: imageDelay + 's'}})}>
                                {!!image && <Image image={image}/>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>}

            <div className="ob-product-grid__product-content-wrapper-1">
                <div className="ob-product-grid__product-content-wrapper-2">
                    <div className="ob-product-grid__product-content" {...(contentDelay && {style: {animationDelay: contentDelay + 's'}})}>
                        <div className="ob-product-grid__product-upper-content-wrapper">
                            <div className={`ob-product-grid__product-upper-content`}>
                                {description}
                            </div>
                        </div>
                        <div className="ob-product-grid__product-lower-content-wrapper">
                            <div className="ob-product-grid__product-lower-content">
                                {!!title && <a href={slug} 
                                    className="ob-product-grid__product-link" 
                                    dangerouslySetInnerHTML={{__html: title}}/>}
                                {!!slug && <Button tag="a"
                                    href={slug}
                                    invertedTheme={!!whiteText}
                                    greyTheme={true}>
                                    {buyNowText}
                                </Button>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

ProductOverviewLoaded.propTypes = {
    whiteText: PropTypes.bool,
    aspectRatio: PropTypes.number,
    imageDelay: PropTypes.number,
    contentDelay: PropTypes.number,
    description: PropTypes.string,
    title: PropTypes.string,
    slug: PropTypes.string,
    buyNowText: PropTypes.string,
    image: PropTypes.object,
}

function ProductOverviewLoading({aspectRatio}) {
    return (
        <div className="ob-product-grid__product ob-product-grid__product--placeholder">
            {!!aspectRatio && <div className="ob-product-grid__product-image-wrapper-1">
                <div className="ob-product-grid__product-image-wrapper-2">
                    <div className="ob-product-grid__product-image-wrapper-3">
                        <div className="ob-product-grid__product-image-wrapper-4" style={{paddingBottom: `${100 / aspectRatio}%`}}>
                            <div className="ob-product-grid__product-image-placeholder" />
                        </div>
                    </div>
                </div>
            </div>}

            <div className="ob-product-grid__product-content-wrapper-1">
                <div className="ob-product-grid__product-content-wrapper-2">
                    <div className="ob-product-grid__product-content" style={{opacity: 1}}>
                        <div className="ob-product-grid__product-upper-content-wrapper">
                            <div className="ob-product-grid__product-upper-content">
                                <span className="ob-product-grid__product-upper-content-placeholder" />
                            </div>
                        </div>
                        <div className="ob-product-grid__product-lower-content-wrapper">
                            <div className="ob-product-grid__product-lower-content">
                                <div className="ob-product-grid__product-link">
                                    <span className="ob-product-grid__product-link-placeholder"></span>
                                </div>
                                <span className="ob-product-grid__button-placeholder"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

ProductOverviewLoading.propTypes = {
    aspectRatio: PropTypes.number,
}

function ProductOverview({
    product, 
    whiteText, 
    aspectRatio, 
    index, 
    hasTransistion
}) {

    if(product) {
        const slug = product?.fields?.slug?.fields?.slug;
        const title  = product?.fields?.title;
        const description = product?.fields?.description;
        const image = product?.fields?.mainAsset;
        
        const imageDelay = hasTransistion ? index * 0.25 : undefined;
        const contentDelay = hasTransistion ? imageDelay + 0.25 : undefined;

        const buyNowText = product?.fields?.buyNowLabel?.fields?.text;

        return <ProductOverviewLoaded 
            whiteText={whiteText} 
            aspectRatio={aspectRatio} 
            slug={slug}
            title={title}
            description={description}
            image={image}
            imageDelay={imageDelay}
            contentDelay={contentDelay}
            buyNowText={buyNowText}/>
            
    } else {
        return <ProductOverviewLoading
            whiteText={whiteText} 
            aspectRatio={aspectRatio}/>
    }
}

ProductOverview.propTypes = {
    product: PropTypes.any,
    whiteText: PropTypes.any,
    aspectRatio: PropTypes.any,
    productId: PropTypes.any,
}

export function ProductGrid({ 
    products,
    seeMoreProductsText,
    seeLessProductsText,
    itemsPerLine, 
    whiteText, 
    isCarrousel,
    isMobile}) {
    const [currentPage, setCurrentPage] = useState(0);
    const [isViewedMore, setIsViewedMore] = useState(false);
    const [navHeight, setNavHeight] = useState(0);
    const collapsibleRef = useRef(null);
    const productGridRef = useRef(null);
    const [isShown, setIsShown] = useState(false);

    const onScroll = useCallback(
        (event) => {
            const currentRatio = event.target.scrollLeft / window.innerWidth;
            const min = Math.floor(currentRatio);
    
            let currentPosition = currentRatio < (min + 0.5) ? min : min + 1;
            currentPosition = currentPosition = Math.max(0, currentPosition);
            currentPosition = Math.min(products.length - 1, currentPosition);

            setCurrentPage(currentPosition);
        },
        [],
    );

    const onViewMore = useCallback(
        () => {
            setIsViewedMore(prevState => !prevState);
        },
        [],
    );

    const calculateHeight = useCallback(
        () => {
            if (collapsibleRef.current) {
                setNavHeight(collapsibleRef.current.scrollHeight);
            }
        },
        [],
    );

    const handleDotClick = useCallback((i) => {
        if(productGridRef.current) {
            const nextY = productGridRef.current.clientWidth * i;
            if(productGridRef.current.scrollTo) {
                productGridRef.current.scrollTo({ left: nextY, behavior: 'smooth'});
            } else {
                productGridRef.current.scrollLeft = nextY;
            }
        }
    }, []);

    const handleWaypoint = useCallback(() => {
        setIsShown(true);
    }, []); 

    const fullScreenWidth = useMemo(() => `${(100 / itemsPerLine).toFixed(2)}%`, [itemsPerLine]);
    const lines = useMemo(() => makeLines(products, itemsPerLine), [products, itemsPerLine]);
    const firstLine = useMemo(() => lines.length > 0 && lines[0], [lines]);
    const remainingLines = useMemo(() => lines.length > 1 ? lines.slice(1) : undefined, [lines]);
    const remainingCount = useMemo(() => remainingLines?.reduce((previous, current) => previous += current.items.length, 0), [remainingLines]);

    const makeLine = useCallback((line, title, hasTransistion = false) => line.items.map((item, i) => (
        <div key={`${title}_${i}`} style={{width: fullScreenWidth}}
            className="ob-product-grid-product-wrapper">
                <ProductOverview product={item} 
                    whiteText={whiteText} 
                    aspectRatio={line.aspectRatio}
                    index={i}
                    hasTransistion={hasTransistion}/>
        </div>
    )), [fullScreenWidth, whiteText]);

    useEffect(() => {
        if(productGridRef.current) {
            productGridRef.current.scrollLeft = 0;
            setCurrentPage(0);
        }
    }, [isMobile]);


    return (
        <div className="ob-product-grid__wrapper">
            <CSSTransition in={isViewedMore} timeout={800} onEntering={calculateHeight} onExit={calculateHeight}>
                <div onScroll={onScroll} ref={productGridRef} className={C('ob-product-grid', 
                    {'ob-product-grid--carrousel': isCarrousel},
                    {'ob-product-grid--shown': isShown})}>
                    {products.map((_, i) => <div key={i} className="ob-product-grid-snap-hack" style={{left: (i * 100) + '%'}}/>)}
                    
                    {/* Arbitrary numbers */}
                    {!!firstLine && <Waypoint onEnter={handleWaypoint} topOffset={"500px"} bottomOffset={"300px"}>
                        <div className="ob-product-grid-first-line" style={{width: firstLine.items.length * 100 + '%'}}>
                            {makeLine(firstLine, "firstLine", true)}
                        </div>
                    </Waypoint>}
                    
                    {!!remainingLines && 
                    <div className="ob-product-grid-collapsible-wrapper" style={{ height: navHeight, width: remainingCount * 100 + '%'}}>
                        <div className="ob-product-grid-collapsible" ref={collapsibleRef}>
                            {flatten(
                                remainingLines.map(
                                    (line, i) => makeLine(line, `remainingLine_${i}`)
                                    )
                                )}
                        </div>
                    </div>}
                    {!!remainingLines && !!seeMoreProductsText && 
                    <div className="ob-product-grid__see-more-wrapper">
                        <SeeMoreButton whiteText={whiteText}
                            onClick={onViewMore} 
                            isOpen={isViewedMore} 
                            seeMoreText={seeMoreProductsText}
                            seeLessText={seeLessProductsText}/>
                    </div>}
                </div>
            </CSSTransition>
            {products.length > 1 && 
                <DotNav className="ob-product-grid__progress-bar" 
                    onClick={handleDotClick} 
                    count={products.length} 
                    current={currentPage} />}
        </div>
    );
}

ProductGrid.defaultProps = {
    itemsPerLine: 3,
    products: [],
    isCarrousel: false,
    seeMoreProductsText: "See more",
    seeLessProductsText: "See less",
}

ProductGrid.propTypes = {
    itemsPerLine: PropTypes.number,
    children: PropTypes.any,
    seeMoreProductsText: PropTypes.string,
    seeLessProductsText: PropTypes.string,
    products: PropTypes.any,
    whiteText: PropTypes.any,
    isMobile: PropTypes.bool,
    isCarrousel: PropTypes.bool,
}
