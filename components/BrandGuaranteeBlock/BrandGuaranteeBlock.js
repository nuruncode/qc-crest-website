import React from 'react';
import Block from '../../helpers/Block';
import "./BrandGuaranteeBlock.scss";
import Icon from '../Icon/Icon';
import ObLink from "../ObLink/ObLink";

export default function BrandGuarantee(props) {
    const {extraAttributes, isMobile} = props;

    return (extraAttributes && extraAttributes.entity && extraAttributes.entity.brandGuarantee) ?
        renderBrandGuaranteeBlock(extraAttributes.entity.brandGuarantee, isMobile) :
        '';
}

function renderBrandGuaranteeBlock(entity) {
    const block = new Block(entity);

    const value = name => {
        return block.getFieldValue(name);
    }

    let classNames = 'brand-guarantee ' + block.getFieldValue('classNames');

    const claims = value('claims');
    const ctaLabel = value('callToActionLabel');
    const ctaLink = value('callToActionLink');
    const anchorId = value('anchorId');

    return (
        <div className={`ob-${classNames}`} id={anchorId} >
            {
                claims.map((claim, claimIndex) => (
                    <div className={`ob-brand-guarantee__claim`} key={'claim-' + claimIndex}>
                        <div className={`ob-brand-guarantee__title`}>{claim.fields.title}</div>
                        <div className={`ob-brand-guarantee__statistic`}>{claim.fields.statistic}</div>
                    </div>
                ))
            }
            {
                ctaLink && ctaLabel &&
                    <div className={`ob-brand-guarantee__cta-link`}>
                        <ObLink href={ctaLink}
                                className={'white'}>
                            {ctaLabel}
                            <Icon size={0.7} color={'black'} name='chevronRight' />
                        </ObLink>
                    </div>
            }


        </div>
    )
}