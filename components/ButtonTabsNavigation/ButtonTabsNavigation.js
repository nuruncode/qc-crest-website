import React, { useCallback, useState, useRef, useMemo } from 'react';
import C from 'classnames';
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';
import './ButtonTabsNavigation.scss';
import Icon from "../Icon/Icon";

export default function ButtonTabsNavigation({tabs, selectedIndex}) {

    const [navCollapsed, setNavCollapsed] = useState(true);
    const [navHeight, setNavHeight] = useState(0);
    const navList = useRef(null);

    const handleNavToggle = useCallback(
        () => { setNavCollapsed(prevState => !prevState); },
        [],
    );

    const calculateHeight = useCallback(
        () => {
            if (navList.current) {
                setNavHeight(navList.current.scrollHeight);
            }

        },
        [],
    );

    const selectedItem = useMemo(() => tabs[selectedIndex], [tabs]);

    return (
        <CSSTransition in={!navCollapsed} timeout={800} onEntering={calculateHeight} onExit={calculateHeight}>
            <nav className="ob-button-tabs-navigation">
                <button className="ob-button-tabs-navigation-toggle" type="button" aria-hidden="true" onClick={handleNavToggle}>
                    <div className="ob-button-tabs-navigation-link-name">
                        {selectedItem?.text}
                    </div>

                    <Icon className="ob-button-tabs-navigation-chevron" name="chevronDown" size={1.6} />
                </button>

                <div className="ob-button-tabs-navigation-collapsible" style={{ height: navHeight }}>
                    <ul className="ob-button-tabs-navigation-list" ref={navList}>
                        {tabs.map((tab, i) => (
                            <li key={i}
                                className={C('ob-button-tabs-navigation-item', {
                                    ['ob-button-tabs-navigation-item--active']: i === selectedIndex,
                                })}
                            >
                                <a className="ob-button-tabs-navigation-link"
                                    href={tab?.link}
                                >
                                    <div className="ob-button-tabs-navigation-link-name">
                                        {tab?.text}
                                    </div>
                                </a>
                            </li>
                        ))}
                    </ul>
                </div>
            </nav>
        </CSSTransition>
    );
}

ButtonTabsNavigation.propTypes = {
    tabs: PropTypes.any,
    selectedIndex: PropTypes.any,
}

ButtonTabsNavigation.defaultProps = {
    tabs: [],
}

ButtonTabsNavigation.propTypes = {
    tabs: PropTypes.any,
    selectedIndex: PropTypes.any,
}
