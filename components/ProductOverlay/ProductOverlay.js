import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import throttle from 'lodash/throttle';
import Block from "../../helpers/Block";
import { toggleScroll } from "../../helpers/Utils";
import Icon from "../Icon/Icon";
import './ProductOverlay.scss'

export default function ProductOverlay(props) {
    const { customEvent, onClickCallback, states, children } = props;
    const block = new Block(props);
    const [isOverlayOpen, setIsOverlayOpen] = useState(states.isOverlayOpen);
    const [lastButton, setLastButton] = useState(null);
    const [lastScroll, setLastScroll] = useState(0);
    const anchorId = block.getAnchorId();
    const productOverlayRef = useRef();
    let classNames = block.getFieldValue('classNames') ? block.getFieldValue('classNames') : '';
    classNames += `${anchorId}`;

    useEffect(() => {
        setLastScroll(window.pageYOffset);
    }, []);

    useEffect(() => {
        if (!customEvent?.event) {
            return;
        }
        if (customEvent.event instanceof HTMLElement || customEvent.event instanceof Node) {
            const skew = customEvent?.event?.dataset?.skew ? customEvent.event.dataset.skew : '';
            if (skew.includes(anchorId)) {
                setLastButton(customEvent.event);
                setIsOverlayOpen(true);
            } else {
                setIsOverlayOpen(false);
            }
        }
    }, [customEvent]);

    useEffect(() => {
        if (productOverlayRef?.current) {
            toggleOverlay();
            productOverlayRef.current.addEventListener('keydown', trapFocusInOverlay);
            return () => {
                productOverlayRef.current.removeEventListener('keydown', trapFocusInOverlay)
            }
        }
    },[isOverlayOpen]);

    useEffect(() => {
        const handleScroll = throttle(() => {
            if (!document.body.classList.contains('noScroll')) {
                setLastScroll(window.pageYOffset);
            }
        }, 100);
        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('scroll', handleScroll);
        }
    },[lastScroll]);

    const toggleOverlay = () => {
        const productOverlayElem = productOverlayRef.current;

        if (isOverlayOpen) {
            productOverlayElem.classList.add('is-active');
            productOverlayElem.focus();
        } else {
            productOverlayElem.classList.remove('is-active');
            if (lastButton) {
                lastButton.focus();
            }
        }
        toggleScroll(isOverlayOpen, lastScroll);
    };

    const trapFocusInOverlay = event => {
        if (!isOverlayOpen) {
            return;
        }
        const element = event.currentTarget;
        const focusableElements = element.querySelectorAll('button:not([disabled]), a[href]:not([disabled])');
        const firstFocusableElement = focusableElements[0];
        const lastFocusableElement = focusableElements[focusableElements.length - 1];
        const KEYCODE_TAB = 9;

        const isTabPressed = (event.key === 'Tab' || event.keyCode === KEYCODE_TAB);

        if (!isTabPressed) {
            return;
        }

        if ( event.shiftKey ) /* shift + tab */ {
            if (document.activeElement === firstFocusableElement) {
                lastFocusableElement.focus();
                event.preventDefault();
            }
        } else /* tab */ {
            if (document.activeElement === lastFocusableElement) {
                firstFocusableElement.focus();
                event.preventDefault();
            }
        }
    };

    return (
        <div ref={productOverlayRef} className={`ob-product-overlay ${classNames}`}>
            <div className={`ob-product-overlay__wrapper`}>
                <button className={`ob-product-overlay__close-btn`} onClick={event => onClickCallback(event)}>
                    <Icon name="close" size={2} />
                </button>
                <div id="ob-product-overlay__content">
                    { children }
                </div>
            </div>
        </div>
    )
}

ProductOverlay.defaultProps = {
    states: {isOverlayOpen: false}
};

ProductOverlay.propTypes = {
    customEvent: PropTypes.object,
    onClickCallback: PropTypes.func,
    states: PropTypes.object,
    children: PropTypes.any
};