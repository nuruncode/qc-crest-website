import React from 'react';
import ProductOverlay from './ProductOverlay';

function createMockProductOverlayProps(props) {
    return {
        
        document: {
            fields: {
                anchorId: "overlay-10101010101",
                contentType: "productOverlay",
                name: "name",
            }
        },
        isMobile: false,
        ...props,
    };
}

describe('Product Overlay Block rendering', () => {
    it('should render product overlay block with the parameters supplied', () => {
        const props = createMockProductOverlayProps();
        const wrapper = mount(<ProductOverlay {...props}/>);
        expect(wrapper).toMatchSnapshot()
    });
});