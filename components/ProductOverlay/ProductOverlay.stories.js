import React from 'react';
import ProductOverlay from "./ProductOverlay";

export default { title: 'Product Overlay' };

let customEvent = {
    event: <button className="ob-button" data-skew="10101010101">Buy now - From £150</button>
}

const blockProps = {
    className: 'is-active',
    document: {
        fields: {
            anchorId: "overlay-10101010101",
            contentType: "productOverlay",
            name: "Product Overlay Test",
        }
    }
};

const states = {
    isOverlayOpen: true
}

const onClickCallback = (event) => {
    if (event) {
        customEvent = {
            event: event.target
        }
    }
}

export const renderProductOverlay = () => <ProductOverlay {...blockProps} states={states} onClickCallback={onClickCallback} customEvent={customEvent} />;

