import React, {Component} from 'react';
import Block from '../../helpers/Block';
import PropTypes from "prop-types";
import Heading from "../Heading/Heading";

import './SideBySideContainer.scss';

class SideBySideContainer extends Component {

  constructor(props) {
    super(props);
    this.block = new Block(props);
  }

  renderTile(block, index) {
    const blockObj = new Block(block);
    const tileId = this.getTileId(blockObj);
    blockObj.setFieldValue('anchorId', tileId);

    const sideIndicator = (index%2 == 0)? 'left' : 'right';

    return (
      <div key={tileId} className={`ob-side-by-side__column ob-side-by-side__${sideIndicator}-side`}>
        {blockObj.renderBlockFromDocument()}
      </div>
    );
  }

  getTileId(blockObj) {
    return 'side-by-side-' + blockObj.getAnchorId();
  }

  render() {
    let className = 'ob-side-by-side ' + this.props.className;

    return (
      <div className={className}>
        <div className={`ob-side-by-side__title-wrapper`}>
          <Heading tag="h2">{this.block.getFieldValue('title')}</Heading>
          <p className={`ob-side-by-side__subtitle`}>{this.block.getFieldValue('description')}</p>
        </div>
        <div className={`ob-side-by-side__wrapper`}>
          {this.block.getFieldValue('blocks').map((block, index) => {
            return this.renderTile(block, index);
          })}
        </div>
      </div>
    );
  }
}

SideBySideContainer.propTypes = {
    className: PropTypes.string,
};

export default SideBySideContainer