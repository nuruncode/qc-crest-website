import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../VideoPlayer/VideoPlayer.scss';
import VideoPlayerFactory from "../../helpers/VideoPlayerFactory";
import OnePageScrollContext from '../OnePageScroll/OnePageScrollContext';

class SpotlightVideoPlayer extends Component {

    constructor(props) {
        super(props);
        this.videoRef = React.createRef();
        this.video = props.video;
        this.playVideo = props.playVideo;
        this.currentIndex = props.currentIndex;
        this.nextFrameOnVideoEnd = props.nextFrameOnVideoEnd;
    }

    componentDidMount() {
        if (this.videoIsDefined()) {
            let videoPlayerFactory = new VideoPlayerFactory(this.videoRef, this.video);
            const cloudinaryVideoPlayer = videoPlayerFactory.create();
            cloudinaryVideoPlayer.on('ended', () => {
                // Force show arrow video ended
                this.props.onePageScrollContext.showArrow();
            });
        }
        this._ismounted = true;
    }

    componentDidUpdate() {
        if (!this._ismounted) {
            return;
        }

        if (this.props.playVideo) {
            if (!this.video.loop && this.props.onePageScrollContext.resetVideo) {
                this.resetVideo();
            }
            this.playDomVideo();
        } else {
            this.pauseVideo();
        }

        if(!this.props.onePageScrollContext.resetVideo) {
            this.props.onePageScrollContext.setVideoResetState();
        }
    }

    resetVideo = () => {
        document.getElementById(`video-player-${this.props.anchorId}_html5_api`).currentTime = 0;
    }

    playDomVideo = () => {
        document.getElementById(`video-player-${this.props.anchorId}_html5_api`).play();
    }

    pauseVideo = () => {
        document.getElementById(`video-player-${this.props.anchorId}_html5_api`).pause();
    }

    componentWillUnmount() {
        this._ismounted = false;
    }

    shouldComponentUpdate(nextProps) {
        if (this.props.playVideo !== nextProps.playVideo) {
            return true;
        }
        return false;
    }

    videoIsDefined() {
        return (this.video && this.video.assetId);
    }

    renderTranscript() {
        return <div className="video-player-transcript visuallyhidden">{this.video.transcript}</div>
    }

    render() {
        return (
            <div className="ob-video-player">
                <OnePageScrollContext.Consumer>
                    {context => (
                        <div>
                            <video
                                muted
                                onEnded={() => context.videoEnded(this.nextFrameOnVideoEnd)}
                                id={`video-player-${this.props.anchorId}`}
                                ref={this.videoRef} />
                            {this.renderTranscript()}
                        </div>
                    )}
                </OnePageScrollContext.Consumer>
            </div>
        )
    }
}

SpotlightVideoPlayer.propTypes = {
    video: PropTypes.object,
    playVideo: PropTypes.any,
    currentIndex: PropTypes.any,
    onePageScrollContext: PropTypes.any,
    nextFrameOnVideoEnd: PropTypes.any,
    anchorId: PropTypes.any,
};

export default SpotlightVideoPlayer