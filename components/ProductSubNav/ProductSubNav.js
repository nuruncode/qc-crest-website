import React, {useEffect, useRef, useState} from "react";
import SlideToggle from "react-slide-toggle";
import Block from "../../helpers/Block";
import { smoothVerticalScroll } from '../../helpers/Utils';
import Component from "../../helpers/entity/Component"
import Heading from '../Heading/Heading';
import Button from '../Button/Button';
import Icon from '../Icon/Icon';
import PropTypes from 'prop-types';
import './ProductSubNav.scss';
import throttle from "lodash/throttle";

export default function ProductSubNav(props) {
    const {extraAttributes} = props;

    const block = new Block(props);
    const product = new Component(extraAttributes.entity);
    const className = 'ob-product-sub-nav ' + block.getFieldValue('classNames');
    const productSubNavRef = useRef(null);

    const [showNav, setShowNav] = useState(true);
    const [scrollPos, setScrollPos] = useState(0);

    const updateNavState = () => {
        const header = document.querySelector('.zone-header');
        const productSubNav = productSubNavRef.current;
        const firstBlock = document.querySelector('.ob-product-sub-nav.is-sticky + div');
        const layout = document.querySelector('.layout');

        if (!productSubNav && !firstBlock && !header && !layout) {
            return;
        }
        const tempPos = window.pageYOffset;
        const isVisible = scrollPos > tempPos;
        setShowNav(isVisible);
        setScrollPos(tempPos);
        updateStyles('scroll');
    };

    const updateStyles = updateEvent => {
        const header = document.querySelector('.zone-header');
        const productSubNav = productSubNavRef.current;
        const firstBlock = document.querySelector('.ob-product-sub-nav.is-sticky + div');
        const layout = document.querySelector('.layout');

        if (!productSubNav && !firstBlock && !header && !layout) {
            return;
        }

        if (updateEvent === 'scroll') {
            header.classList.add(`visible-${showNav}`);
            header.classList.remove(`visible-${!showNav}`);
            productSubNav.classList.add(`is-top-${!showNav}`);
            productSubNav.classList.remove(`is-top-${showNav}`);

            // This prevents a page glitch and a huge white space at the top of the page
            header.style.top = showNav ? '0' : `-${header.offsetHeight}px`;
            productSubNav.style.top = showNav ? `${header.offsetHeight}px` : '0';
            firstBlock.style.paddingTop = scrollPos <= (productSubNav.offsetHeight) && showNav ? `${productSubNav.offsetHeight}px` : '0';
            layout.style.paddingTop = scrollPos <= (productSubNav.offsetHeight) && showNav ? `${header.offsetHeight}px` : '0';
        } else if (updateEvent === 'anchorClick') {
            if (header.classList.contains('visible-true')) {
                layout.style.paddingTop = `${header.offsetHeight}px`;
            }
            firstBlock.style.paddingTop = `${productSubNav.offsetHeight}px`;
        }
    };

    useEffect(() => {
        setScrollPos(window.pageYOffset);
    },[]);

    useEffect(
        () => {
            if (className.includes('scrolling-header')) {
                const handleScroll = throttle(updateNavState, 100);
                window.addEventListener('wheel', handleScroll);
                window.addEventListener('touchmove', handleScroll);
                return () => {
                    window.removeEventListener('wheel', handleScroll);
                    window.removeEventListener('touchmove', handleScroll);
                }
            }
        },
        [scrollPos]
    );

    const scrollTo = event => {
        event.preventDefault();
        const currentTarget = event.currentTarget;
        const anchorId = currentTarget.dataset.anchorId;
        const element = document.querySelector(anchorId);
        if (!element) {
            return;
        }
        updateStyles('anchorClick');
        smoothVerticalScroll(element, 'top');
    };

    const  renderLink = (product, link, index, productSubNavRef) => {
        return (linkNeedToDisplay(product, link)) ?
            (<li key={index}>
                <a onClick={event => scrollTo(event, productSubNavRef)} data-anchor-id={link.url} href={link.url}>{link.title}</a>
            </li>) : '';
    }

    const linkNeedToDisplay = (product, link) => {
        switch (link.url) {
            case '#highlights' :
                return (typeof product.highlights !== 'undefined');
            case '#features' :
                return (typeof product.featuresTab !== 'undefined');
            case '#in-the-box' :
                return (typeof product.inTheBox !== 'undefined');
            case '#gallery' :
                return (typeof product.gallery !== 'undefined');
            case '#reviews' :
                return (typeof product.reviews !== 'undefined');
            default :
                return false;
        }
    }

    return (
        <div className={className} ref={productSubNavRef}>
            {/* START MOBILE LAYOUT */}
            <div className={`ob-product-sub-nav__container ob-product-sub-nav__container--mobile`}>
                <SlideToggle collapsed duration={500}>
                    {({ onToggle, setCollapsibleElement, range, toggleState }) => {
                        const styles = range ? { opacity: Math.max(0, range) } : { opacity: 1 };
                        return (
                            <div className={`ob-product-sub-nav__top`}>
                                <h2 className={`ob-product-sub-nav__title`}>
                                    <button onClick={() => {
                                        onToggle();
                                        updateNavState();
                                    }}>
                                        <span dangerouslySetInnerHTML={{__html: product.props.productOverview.fields.title}}></span>        
                                        {toggleState === "COLLAPSED" || toggleState === "COLLAPSING" ? (
                                            <Icon name="chevronDown" color="#3D3D41" />
                                            ) : (
                                            <Icon name="chevronTop" color="#3D3D41" />
                                            )}
                                    </button>
                                </h2>
                                <Button borderTheme size="medium">Buy now</Button>

                                <ul ref={setCollapsibleElement} className={`ob-page-navigation__list`} style={styles}>
                                    {block.getFieldValue('links').map((link, index) => {
                                        return renderLink(product.props, link.fields, index, productSubNavRef)
                                    })}
                                </ul>
                            </div>
                        )
                    }}
                </SlideToggle>
            </div>
            {/* END MOBILE LAYOUT */}
            
            {/* START DESKTOP LAYOUT */}
            <div className={`ob-product-sub-nav__container ob-product-sub-nav__container--desktop`}>
                <div className={`ob-product-sub-nav__top`}>
                    <Heading className={`ob-product-sub-nav__title`} tag="h2">{product.props.productOverview.fields.title}</Heading>
                    <ul className={`ob-page-navigation__list`}>
                        {block.getFieldValue('links').map((link, index) => {
                            return renderLink(product.props, link.fields, index, productSubNavRef)
                        })}
                    </ul>
                </div>
                <Button borderTheme size="small">Buy now - From £150</Button>
            </div>
            {/* END DESKTOP LAYOUT */}
        </div>
    );
}

ProductSubNav.propTypes = {
    extraAttributes: PropTypes.any
};