import React, { useState, useCallback, useRef } from 'react';
import Block from '../../helpers/Block';
import Heading from "../Heading/Heading";
import Image from "../Image/Image";
import VideoPlayer from '../VideoPlayer/VideoPlayer';
import Icon from "../Icon/Icon";
import ObLink from "../ObLink/ObLink";
import "./GalleryBlock.scss";
import { CSSTransition } from 'react-transition-group';

export default function Gallery(props) {
    const { extraAttributes } = props;

    return (extraAttributes && extraAttributes.entity && extraAttributes.entity.gallery) ?
        renderGalleryBlock(extraAttributes.entity, props) :
        '';
}

function getRoundedWidth(width) {
    let widthOnly = width.replace('%', '');
    let widthValue = widthOnly;

    return widthValue;
}

function defineGallery(galleryItems, showMoreItems) {
    if (galleryItems && galleryItems.length > 1) {
        return (
            <div className={'ob-gallery__masonry ' + (showMoreItems ? '' : 'gallery-hidden')}>
                {
                    galleryItems.slice(1).map((product, productIndex) => (
                        <div key={'product-' + productIndex} className={'grid-item'} style={{ width: getRoundedWidth(product.fields.width) + '%' }}>
                            {product.fields.mainAsset.fields.contentType === 'cloudinaryImage' ? (
                                <Image noLazyLoad={true} image={product.fields.mainAsset} />
                            ) : (
                                    <VideoPlayer video={product.fields.mainAsset.fields} />
                                )}
                        </div>
                    ))
                }
            </div>
        )
    }

    else {
        return;
    }
}

function renderGalleryBlock(entity, props) {

    const block = new Block(entity);
    const galleryBlock = new Block(props);

    let [showMoreItems, setShowMoreItems] = useState(false);
    const collapsibleRef = useRef(null);
    const [collapsibleHeight, setCollapsibleHeight] = useState(0);

    const customClassNames = block.getFieldValue('classNames');
    const className = 'gallery ' + (customClassNames ? customClassNames : '');
    const titleValue = galleryBlock.getFieldValue('title');
    const isWhiteText = className.includes("white-text");
    const anchorId = galleryBlock.getFieldValue('anchorId');
    const seeMoreButtonLabel = galleryBlock.getFieldValue('seeMoreButtonLabel')
    const seeLessButtonLabel = galleryBlock.getFieldValue('seeLessButtonLabel')
    const [buttonLabel, setButtonLabel] = useState(seeMoreButtonLabel);

    let galleryItemsJson = [];
    if (entity.gallery) {
        galleryItemsJson = entity.gallery;
    }

    let galleryItems = defineGallery(galleryItemsJson, showMoreItems);
    let mainGalleryItem = null;
    if (galleryItemsJson && galleryItemsJson.length > 0) {
        mainGalleryItem = galleryItemsJson[0].fields.mainAsset;
    }

    const _handleClickShowItems = useCallback(() => {
        setShowMoreItems(!showMoreItems);
        if (showMoreItems == false) {
            setButtonLabel(seeLessButtonLabel);
        } else {
            setButtonLabel(seeMoreButtonLabel);
        }
    }, [showMoreItems]);

    const _calculateHeight = useCallback(
        () => {
            if (collapsibleRef.current) {
                setCollapsibleHeight(collapsibleRef.current.scrollHeight);
            }
        },
        [],
    );

    return (
        <div className={`ob-gallery ${className}`} id={anchorId} >

            {mainGalleryItem.fields.contentType === 'cloudinaryImage' ? (
                <Image image={mainGalleryItem}>
                    <div className={'ob-gallery__wrapper'}>
                        <Heading whiteText={isWhiteText} tag={`h1`} >{titleValue}</Heading>
                    </div>

                    {/* See more button */}
                    { showMoreItems ? (
                        <ObLink onClick={_handleClickShowItems} className="ob-gallery__button primaryGrey see-less-button" tag='button'>
                            <Icon name="minus6" roundedIcon="blue" viewBox={6} />
                            {buttonLabel}
                        </ObLink>
                    ) : (
                        <ObLink onClick={_handleClickShowItems} className="ob-gallery__button primaryGrey see-more-button" tag='button'>
                            <Icon name="plus" roundedIcon="blue" viewBox={10} />
                            {buttonLabel}
                        </ObLink>
                        )
                    }
                </Image>
            ) : (
                    <div className={'ob-gallery__wrapper--video'}>
                        <Heading whiteText={isWhiteText} tag={`h1`} >{titleValue}</Heading>
                        <VideoPlayer video={mainGalleryItem.fields} />
                    </div>
                )
            }
            
            <CSSTransition in={showMoreItems} timeout={800} onEntering={_calculateHeight} onExit={_calculateHeight}>
                <div className={`ob-gallery__container`}>
                    <div className={`ob-gallery__container-collapsible-wrapper`} style={{ height: collapsibleHeight }}>
                        <div ref={collapsibleRef} className="ob-gallery__container-collapsible">
                            {galleryItems}
                        </div>
                    </div>
                </div>
            </CSSTransition>
        </div>
    )
}