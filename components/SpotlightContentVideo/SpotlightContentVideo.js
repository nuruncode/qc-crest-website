import React, { useState, useEffect, useContext } from 'react';
import Block from '../../helpers/Block';
import Heading from '../Heading/Heading';
import BodyText from '../BodyText/BodyText';
import SpotlightVideoPlayer from '../SpotlightVideoPlayer/SpotlightVideoPlayer';
import './SpotlightContentVideo.scss';
import PropTypes from 'prop-types'
import OnePageScrollContext from '../OnePageScroll/OnePageScrollContext';
import Image from '../Image/Image';

export default function SpotlightContentVideo(props) {

    const valueOf = (name) => {
        let value = undefined;
        if (name && block && block.getFieldValue(name) !== undefined) {
            value = block.getFieldValue(name);
        }
        return value;
    };

    /*eslint-disable */    
    const generateContentBlockState = () => {
        let isMobile = props.isMobile;
    };
    /*eslint-enable */

    const isActiveBlock = (index) => {
        return props.index === index;
    };

    const block = new Block(props);
    const textFadeIn = valueOf('textFadeIn');
    const introText = valueOf('introText');
    const introLogo = valueOf('introLogo');
    const title = valueOf('title');
    const titleLevel = valueOf('titleLevel');
    const nextFrameOnVideoEnd = valueOf('nextFrameOnVideoEnd');
    const introDurationSecond = valueOf('introDurationSecond');
    const anchorId = block.getAnchorId();
    const playButtonColor = valueOf('playButtonColor') || 'white';

    /*eslint-disable */
    let [blockState, setBlockState] = useState(generateContentBlockState());
    /*eslint-enable */

    let className = 'content ' + block.getFieldValue('classNames');
    const isWhiteText = className.includes("white-text");
    let [show, setShow] = useState('');
    let [removeIntro, setRemoveIntro] = useState('');

    const video = block.getFields().video.fields;

    const renderVideo = (video, allowVideoPlay, index) => {
        if (video) {
            return <SpotlightVideoPlayer
                playVideo={isActiveBlock(index) && allowVideoPlay}
                onePageScrollContext={onePageScrollContext}
                nextFrameOnVideoEnd={nextFrameOnVideoEnd}
                anchorId={anchorId}
                video={video} />
        } else {
            return '';
        }
    };

    const textContainerHasPadding = valueOf('textContainerHasPadding');

    const onePageScrollContext = useContext(OnePageScrollContext);

    useEffect(() => {
        if (props.index == 0) {
            onePageScrollContext.hideArrow();
        } else {
            onePageScrollContext.showArrow();
        }
        if (textFadeIn == false) {
            //If text fadeIn at false show the element at pageLoad
            // showContentElement();
        }
        if (props.isMobile) {
            setBlockState(generateContentBlockState());
        }
    }, [props.isMobile, textFadeIn]);

    useEffect(() => {
        if (introDurationSecond) {
            setTimeout(function () {
                // Remove intro after 4s
                setRemoveIntro('hide');
            }, (introDurationSecond*1000));
        }
    }, []);

    return (
        <OnePageScrollContext.Consumer>
            {context => (
                <section className={`ob-spotlightContentBlock ob-spotlightVideo ${className}`}
                    id={anchorId}
                    data-isactiveblock={isActiveBlock(context.currentIndex)}
                    style={{
                        pointerEvents: isActiveBlock(context.currentIndex) ? 'auto' : 'none'
                    }}>

                    <div className={`ob-spotlightContentBlock-wrapper ${show}`}>

                        { introText && introLogo && 
                            <div className={`ob-spotlightVideo__intro ${removeIntro}`}>
                                <Image image={introLogo} className='ob-spotlightVideo__intro-image'/>
                                <BodyText whiteText={isWhiteText}>{introText}</BodyText>
                            </div>
                        }

                        <div className={`ob-spotlightContentBlock-textContent
                                        ${textContainerHasPadding ? ' hasPadding' : ''}`}>

                            <Heading whiteText={isWhiteText} tag={`h${titleLevel}`} className="ob-display-2-xl">{title}</Heading>

                            <BodyText whiteText={isWhiteText}>{valueOf('description')}</BodyText>
                        </div>
                    </div>

                    <div className="ob-spotlightVideo-container">
                        {renderVideo(video, context.allowVideoPlay, context.currentIndex)}

                        <button onClick={() => context.toggleVideoPlay()}
                            style={{
                                color: [playButtonColor === 'white' ? '#ffffff' : '#0a2c54'],
                                position: 'fixed',
                                bottom: '1rem',
                                left: '1rem'
                            }}>Play/Pause video</button>

                        <button onClick={() => context.forceVideoReset()}
                                style={{
                                    color: [playButtonColor === 'white' ? '#ffffff' : '#0a2c54'],
                                    position: 'fixed',
                                    bottom: '3rem',
                                    left: '1rem'
                                }}>Reset video</button>
                    </div>
                </section>
            )}
        </OnePageScrollContext.Consumer>
    )
}

SpotlightContentVideo.propTypes = {
    isMobile: PropTypes.any,
    index: PropTypes.any,
}

SpotlightContentVideo.defaultProps = {
    isMobile: PropTypes.bool,
    index: PropTypes.number
};