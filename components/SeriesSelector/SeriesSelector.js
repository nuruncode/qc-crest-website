import C from 'classnames';
import PropTypes from 'prop-types';
import React, { useCallback, useMemo, useRef, useState } from 'react';
import { CSSTransition } from 'react-transition-group';

import ImageFactory from '../../adapters/cloudinary/ImageFactory';
import { findIndexByAnchorId, getAnchorIdFromEntity, stringifyAnchor, useAnchor } from '../../helpers/Anchor';

import Button from '../Button/Button';
import Icon from '../Icon/Icon';
import Image from '../Image/Image';
import MarkdownText from '../MarkdownText/MarkdownText';
import { LabelHTML } from '../Label/Label';

import './SeriesSelector.scss';
import {getReviewAndRatingId} from "../UserQuoteBlock/UserQuoteBlock";
import {getBazaarVoiceMasterProductId} from "../../pages/product";

export default function Series(props) {
    const {document: doc} = props;
    const collection = props.extraAttributes.entity;

    const anchor = useAnchor();
    const selectedSeriesId = anchor.series;

    let seriesArray = collection.series;

    const selectedSeriesIndex = useMemo(
        () => findIndexByAnchorId(seriesArray, selectedSeriesId) || 0,
        [selectedSeriesId, seriesArray],
    );
    const selectedSeries = seriesArray?.[selectedSeriesIndex];
    const selectedSeriesProducts = selectedSeries?.fields?.products;

    return (
        <div className="ob-series">
            <SeriesSelector
                anchor={anchor}
                document={doc}
                collection={collection}
                seriesArray={seriesArray}
                selectedSeries={selectedSeries}
                selectedSeriesIndex={selectedSeriesIndex}
            />

            {!!(selectedSeriesProducts && selectedSeriesProducts.length > 1) && (
                <SeriesComparison
                    document={doc}
                    products={selectedSeriesProducts}
                    selectedSeries={selectedSeries}
                />
            )}
        </div>
    );
}

Series.propTypes = {
    document: PropTypes.object,
    extraAttributes: PropTypes.object,
}

function SeriesSelector(props) {
    const {document: doc, anchor, collection} = props;
    const {seriesArray, selectedSeries, selectedSeriesIndex} = props;

    const selectedVariantId = anchor.variant;

    const blockSubtitleLabel = doc.fields.blockSubtitleLabel;
    const handleColorLabel = doc.fields.handleColorLabel;
    const optionsLabel = doc.fields.optionsLabel;

    const featuredProduct = selectedSeries?.fields?.featuredProduct;
    const featuredProductVariants = featuredProduct?.fields?.productVariants;
    const selectedVariantIndex = useMemo(
        () => findIndexByAnchorId(featuredProductVariants, selectedVariantId) || 0,
        [selectedVariantId, featuredProductVariants],
    );
    const selectedVariant = featuredProductVariants?.[selectedVariantIndex];

    const selectedSeriesBackground = selectedSeries?.fields?.backgroundAsset;
    const selectedSeriesOptions = selectedSeries?.fields?.options;
    const selectedSeriesProducts = selectedSeries?.fields?.products;

    const selectedSeriesImage = selectedVariant?.fields?.seriesAsset || selectedVariant?.fields?.mainAsset || selectedSeries?.fields?.mainAsset;

    const ctaLink = selectedSeries?.fields?.ctaLink;
    const ctaLabel = selectedSeries?.fields?.ctaLabel;

    const style = {
        backgroundColor: selectedSeries?.fields?.backgroundColor,
        backgroundImage: selectedSeriesBackground && `url(${ImageFactory.buildImageUrlByWidth(selectedSeriesBackground.fields.assetId)})`,
    };

    // Master Product id containing the reviews of all the product in his family
    const reviewAndRatingMasterProductId = getBazaarVoiceMasterProductId(featuredProduct?.fields?.reviewAndRatingFamilyId);


    return (
        <div className="ob-series-selector" style={style}>
            <div className="ob-series-selector__top">
                <h1 className="ob-series-selector__collection">
                    {collection.shortTitle}
                </h1>
            </div>

            <div className="ob-series-selector__series">
                {!!blockSubtitleLabel && (
                    <h2 className="ob-series-selector__subtitle">
                        {blockSubtitleLabel.fields.text}
                    </h2>
                )}

                <SeriesTabs
                    items={seriesArray}
                    selectedItem={selectedSeries}
                    selectedIndex={selectedSeriesIndex}
                />
            </div>

            {!!selectedSeries && (
                <div className="ob-series-selector__selected">
                    <div className="ob-series-selector__selected-main">
                        <div className="ob-series-selector__selected-pic">
                            {!!selectedSeriesImage && (
                                <div className="ob-series-selector__selected-pic-wrap" style={{ maxHeight: selectedSeries.fields.assetHeight ? `${selectedSeries.fields.assetHeight}px` : undefined }}>
                                    <Image className="ob-series-selector__selected-pic-image"
                                        image={selectedSeriesImage}
                                    />
                                </div>
                            )}
                        </div>

                        <div className="ob-series-selector__selected-gap" />

                        <div className="ob-series-selector__selected-info">

                            <h2 className="ob-series-selector__selected-name" dangerouslySetInnerHTML={{__html: selectedSeries.fields.title}}>
                            </h2>

                            <div data-bv-show="rating_summary"
                                 data-bv-product-id={reviewAndRatingMasterProductId}></div>

                            <MarkdownText className="ob-series-selector__selected-description"
                                value={selectedSeries.fields.description}
                            />

                            {!!(selectedSeries.fields.highlights && selectedSeries.fields.highlights.length > 0) && (
                                <ul className="ob-series-selector__selected-highlights">
                                    {selectedSeries.fields.highlights.map((serieHightlight, i) => (
                                        <li className="ob-series-selector__selected-highlights-item" key={i}>
                                            {!!serieHightlight.fields.iconName && (
                                                <span className={`ob-series-selector__selected-highlights-icon ${serieHightlight.fields.iconName}`} />
                                            )}

                                            <span className="ob-series-selector__selected-highlights-text">
                                                {serieHightlight.fields.highlight}
                                            </span>
                                        </li>
                                    ))}
                                </ul>
                            )}

                            {!!(selectedSeries.fields.experienceLink && selectedSeries.fields.experienceLinkText) && (
                                <p className="ob-series-selector__selected-experience">
                                    <a className="ob-series-selector__selected-experience-link" href={selectedSeries.fields.experienceLink}>
                                        {selectedSeries.fields.experienceLinkText.fields.text}
                                    </a>
                                </p>
                            )}

                            {!!(featuredProductVariants && featuredProductVariants.length > 0) && (
                                <div className="ob-series-selector__selected-colors">
                                    {!!handleColorLabel && (
                                        <h3 className="ob-series-selector__selected-subheading">
                                            {handleColorLabel.fields.text}
                                        </h3>
                                    )}

                                    <ul className="ob-series-selector__selected-colors-list">
                                        {featuredProductVariants.map((variant, i) => {
                                            const {color} = variant.fields;

                                            return (
                                                <li className="ob-series-selector__selected-colors-item" key={i}>
                                                    <a
                                                        className={C('ob-series__color', {
                                                            ['ob-series__color--large-mobile']: featuredProductVariants.length <= 2,
                                                            ['ob-series__color--active']: i === selectedVariantIndex
                                                        })}
                                                        href={stringifyAnchor({
                                                            series: getAnchorIdFromEntity(selectedSeries),
                                                            variant: getAnchorIdFromEntity(variant),
                                                        })}
                                                        style={getColorStyle(color)}
                                                    >
                                                        {color.fields.name}
                                                    </a>
                                                </li>
                                            );
                                        })}
                                    </ul>
                                </div>
                            )}

                            {!!(selectedSeriesOptions) && (
                                <div className="ob-series-selector__selected-options">
                                    {!!optionsLabel && (
                                        <h3 className="ob-series-selector__selected-subheading ob-series-selector__selected-subheading--options">
                                            {optionsLabel.fields.text}
                                        </h3>
                                    )}

                                    <MarkdownText className="ob-series-selector__selected-options-text"
                                        value={selectedSeriesOptions}
                                    />
                                </div>
                            )}

                            {!!ctaLink && ctaLabel && (
                                <BuySeries
                                    className="ob-series-selector__selected-info-buy"
                                    label={ctaLabel.fields.text}
                                    href={ctaLink}
                                    product={featuredProduct}
                                />
                            )}
                        </div>
                    </div>

                    {!!ctaLink && ctaLabel && (
                        <BuySeries
                            className="ob-series-selector__selected-buy"
                            label={ctaLabel.fields.text}
                            href={ctaLink}
                            product={featuredProduct}
                        />
                    )}
                </div>
            )}
        </div>
    );
}

SeriesSelector.propTypes = {
    document: PropTypes.object,
    anchor: PropTypes.any,
    collection: PropTypes.any,
    seriesArray: PropTypes.any,
    selectedSeries: PropTypes.any,
    selectedSeriesIndex: PropTypes.any,
}

function SeriesTabs({ items, selectedItem, selectedIndex }) {
    const [navCollapsed, setNavCollapsed] = useState(true);
    const [navHeight, setNavHeight] = useState(0);
    const navList = useRef(null);

    const handleNavToggle = useCallback(
        () => { setNavCollapsed(prevState => !prevState); },
        [],
    );

    const calculateHeight = useCallback(
        () => {
            if (navList.current) {
                setNavHeight(navList.current.scrollHeight);
            }
        },
        [],
    );

    return (
        <CSSTransition in={!navCollapsed} timeout={800} onEntering={calculateHeight} onExit={calculateHeight}>
            <nav className="ob-series-selector__nav">
                <button className="ob-series-selector__nav-toggle" type="button" aria-hidden="true" onClick={handleNavToggle}>
                    <div className="ob-series-selector__nav-link-name"
                        dangerouslySetInnerHTML={{__html: selectedItem.fields.shortTitle}}>
                    </div>

                    {!!selectedItem.fields.startingAtLabel && (
                        <div className="ob-series-selector__nav-link-price">
                            {selectedItem.fields.startingAtLabel.fields.text}
                        </div>
                    )}

                    <Icon className="ob-series-selector__nav-chevron" name="chevronDown" size={1.6} />
                </button>

                <div className="ob-series-selector__nav-collapsible" style={{ height: navHeight }}>
                    <ul className="ob-series-selector__nav-list" ref={navList}>
                        {items.map((item, i) => (
                            <li key={i}
                                className={C('ob-series-selector__nav-item', {
                                    ['ob-series-selector__nav-item--active']: i === selectedIndex
                                })}
                            >
                                <a className="ob-series-selector__nav-link"
                                    href={stringifyAnchor({ series: getAnchorIdFromEntity(item) })}
                                >
                                    <div className="ob-series-selector__nav-link-name"
                                        dangerouslySetInnerHTML={{__html: item.fields.shortTitle}}>
                                    </div>

                                    {!!item.fields.startingAtLabel && (
                                        <div className="ob-series-selector__nav-link-price">
                                            {item.fields.startingAtLabel.fields.text}
                                        </div>
                                    )}
                                </a>
                            </li>
                        ))}
                    </ul>
                </div>
            </nav>
        </CSSTransition>
    );
}

SeriesTabs.propTypes = {
    items: PropTypes.any,
    selectedItem: PropTypes.any,
    selectedIndex: PropTypes.any,
}

function BuySeries({className, label, href, product}) {
    return (
        <div className={className}>
            {!!product && (
                <h3 className="ob-series-selector__selected-buy-heading">
                    <a className="ob-series-selector__selected-buy-heading-link" href={product.fields.slug.fields.slug} dangerouslySetInnerHTML={{__html: product.fields.title}}>
                    </a>
                </h3>
            )}

            <Button className="ob-series-selector__selected-buy-btn" tag="a" href={href}>
                {label}
            </Button>
        </div>
    );
}

BuySeries.propTypes = {
    className: PropTypes.any,
    label: PropTypes.any,
    href: PropTypes.any,
    product: PropTypes.any,
}

function SeriesComparison(props) {
    const {document: doc, products, selectedSeries} = props;

    return (
        <div className="ob-series-comparison">
            <div className="ob-series-comparison__sizer">
                <h2 className="ob-series-comparison__title">
                    <LabelHTML label={doc.fields.compareLabel} tokens={{seriesName: selectedSeries.fields.shortTitle}} />
                </h2>

                <ul className="ob-series-comparison__list">
                    {products.map((product, i) => (
                        <li key={i} className="ob-series-comparison__item">
                            <SeriesComparisonItem document={doc} product={product} />
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
}

SeriesComparison.propTypes = {
    document: PropTypes.any,
    products: PropTypes.any,
}

function SeriesComparisonItem(props) {
    const {document: doc, product} = props;

    const handleColorLabel = doc.fields.handleColorLabel;

    const [selectedVariantIndex, setSelectedVariant] = useState(0);

    const productOptions = product.fields.options;
    const productVariants = product.fields.productVariants;
    const productVariant = productVariants[selectedVariantIndex];

    const productImage = productVariant?.fields?.mainAsset || product.fields.mainAsset;

    const ctaLink = product.fields.ctaLink || product.fields.slug?.fields?.slug;
    const buyNowLabel = product.fields.buyNowLabel;

    let reviewAndRatingId = getReviewAndRatingId(product);

    return (
        <div className="ob-series-comparison__product">
            <div className="ob-series-comparison__product-pic">
                {!!productImage && (
                    <div className="ob-series-comparison__product-pic-wrap">
                        <Image className="ob-series-comparison__product-pic-image"
                            image={productImage}
                        />
                    </div>
                )}
            </div>

            <div className="ob-series-comparison__product-info">
                <h2 className="ob-series-comparison__product-name">
                    <a className="ob-series-comparison__product-name-link ob-link" href={product.fields.slug.fields.slug} dangerouslySetInnerHTML={{__html: product.fields.title}}>
                    </a>
                </h2>


                {!!(productVariants && productVariants.length > 0) && (
                    <div className="ob-series-comparison__product-colors">
                        <div itemScope itemType="http://schema.org/Product">
                        <meta itemProp={product.fields.title} content="ProductName" />           
                            <div data-bv-show="rating_summary"
                                 data-bv-product-id={reviewAndRatingId}></div>
                        </div>

                        {!!(ctaLink && buyNowLabel) && (
                            <Button className="ob-series-comparison__product-buy-btn" tag="a" href={ctaLink}>
                                {buyNowLabel.fields.text}
                            </Button>
                        )}

                        <h3 className="ob-series-comparison__product-subheading">
                            {handleColorLabel.fields.text}
                        </h3>

                        <ul className="ob-series-comparison__product-colors-list">
                            {productVariants.map((variant, i) => {
                                const color = variant.fields.color;

                                return (
                                    <li className="ob-series-comparison__product-colors-item" key={i}>
                                        <button
                                            type="button"
                                            className={C('ob-series__color ob-series__color--large-mobile', {
                                                ['ob-series__color--active']: i === selectedVariantIndex
                                            })}
                                            style={getColorStyle(color)}
                                            onClick={() => setSelectedVariant(i)}
                                        >
                                            {color.fields.name}
                                        </button>
                                    </li>
                                );
                            })}
                        </ul>
                    </div>
                )}

                {!!productOptions && (
                    <MarkdownText className="ob-series-comparison__product-options-text"
                        value={productOptions}
                    />
                )}
            </div>
        </div>
    );
}

SeriesComparisonItem.propTypes = {
    document: PropTypes.any,
    product: PropTypes.any,
}

export function getColorStyle(color) {
    const colorCode = color.fields.colorCode;
    const colorCodeTop = color.fields.colorCodeTop || colorCode;
    const colorCodeBottom = color.fields.colorCodeBottom || colorCode;
    const gradientStyle = color.fields.gradientStyle;

    switch (gradientStyle) {
    case 'linear':
        return {
            backgroundColor: colorCode,
            backgroundImage: `linear-gradient(to bottom, ${colorCodeTop} 0%, ${colorCodeBottom} 100%)`,
        };
    case 'highlightTop':
        return {
            backgroundColor: colorCode,
            backgroundImage: `radial-gradient(circle at center top, ${colorCodeTop} 0%, ${colorCodeBottom} 100%)`,
        };
    case 'highlightBottom':
        return {
            backgroundColor: colorCode,
            backgroundImage: `radial-gradient(circle at center bottom, ${colorCodeBottom} 0%, ${colorCodeTop} 100%)`,
        };
    default:
        return {
            backgroundColor: colorCode
        };
    }
}
