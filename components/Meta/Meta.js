import React from 'react';

import ImageFactory from '../../adapters/cloudinary/ImageFactory';
import {validURL} from '../../helpers/Utils';

import PropTypes from 'prop-types'

import Head from 'next/head';
import getConfig from "next/config";
const {publicRuntimeConfig} = getConfig();

export default function Meta(props) {
    const { metadata, pagePath } = props;

    const title = metadata.metaTitle;
    const description = metadata.metaDescription;
    const keywords = metadata.metaKeywords;

    const noindex = metadata.noIndex;
    const nofollow = metadata.noFollow;

    const openGraphPageTitle = metadata.openGraphPageTitle;
    const openGraphDescription = metadata.openGraphDescription;

    let metaRobots = [];
    metaRobots.push(noindex ? "noindex" : "index");
    metaRobots.push(nofollow ? "nofollow" : "follow");

    let openGraphImageUrl = null;

    if (metadata.openGraphImage && metadata.openGraphImage.fields) {
        const ogImageAssetId = metadata.openGraphImage.fields.assetId;
        openGraphImageUrl = ImageFactory.buildImageUrl(ogImageAssetId);
    }

    let canonical = generateCanonical(metadata, pagePath);

    return (
        <Head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <title>{title}</title>
            <link rel="manifest" href="/static/manifest.webmanifest"/>
            <meta charSet="UTF-8"/>
            <meta name="description" content={description}/>
            <meta name="robots" content={metaRobots}/>
            <meta name="theme-color" content="#0057B8"/>

            {keywords &&
                <meta name="keywords" content={keywords}/>}

            {openGraphPageTitle &&
                <meta property="og:title" content={openGraphPageTitle}/>}

            {openGraphDescription &&
                <meta property="og:description" content={openGraphDescription}/>}

            {openGraphImageUrl &&
                <meta property="og:image" content={openGraphImageUrl}/>}

            {canonical &&
                <meta property="og:url" content={canonical}/>}

            {canonical &&
                <link rel="canonical" href={canonical} />}
        </Head>
    );
}

Meta.propTypes = {
    metadata: PropTypes.object,
    pagePath: PropTypes.string,
};

function generateCanonical(metadata, pagePath) {
    const baseURl = publicRuntimeConfig.BASE_URL;
    let canonical = metadata.canonicalUrl;
    if(canonical == null) {
        canonical = pagePath;
    }

    if(!validURL(canonical)) {
        canonical = "https://" + baseURl + canonical;
    }

    return canonical;
}
