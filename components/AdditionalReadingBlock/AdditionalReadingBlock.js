import React from 'react';

import PropTypes from 'prop-types';
import Block from '../../helpers/Block';
import Heading from '../Heading/Heading'

export default function AdditionalReadingBlock(props) {
    const {extraAttributes, isMobile} = props;
    const block = new Block(props);

    const relatedArticles = props.extraAttributes.entity.relatedArticles;
    const title = block.getFieldValue('title');

    const className = 'additional-reading ' + block.getFieldValue('classNames');
    const anchorId = block.getAnchorId();

    return (
        <div className={`ob-additional-reading ${className}`} id={anchorId}>
            <Heading>{title}</Heading>
            {relatedArticles.map((article, i) => {
                return (<div key={i}>{article.fields.title}</div>)
            })}
        </div>
    )
}

AdditionalReadingBlock.propTypes ={
    extraAttributes: PropTypes.object,
    isMobile: PropTypes.bool,
}
