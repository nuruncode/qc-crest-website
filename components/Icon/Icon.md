### Examples

** Available Icons**

Chevron right
```
<Icon name="chevronRight"></Icon>
```

Chevron down
```
<Icon name="chevronDown"></Icon>
```

Chevron left
```
<Icon name="chevronLeft"></Icon>
```

Chevron top
```
<Icon name="chevronTop"></Icon>
```

Wide Chevron down
```
<Icon name="wideChevronDown"></Icon>
```

Hamburger
```
<Icon name="hamburger"></Icon>
```

Close
```
<Icon name="close"></Icon>
```

Search
```
<Icon name="search"></Icon>
```

Arrow left
```
<Icon name="arrowLeft"></Icon>
```

Arrow Right
```
<Icon name="arrowRight"></Icon>
```

Oval Arrow right
```
<Icon name="ovalArrowRight"></Icon>
```

Oval Arrow right blue
```
<Icon name="chevronRight" roundedIcon="blue"></Icon>
```

Oval Arrow right white
```
<div style={{ backgroundColor: '#3D3D41' }}>
<Icon name="chevronRight" roundedIcon="white"></Icon>
</div>
```
