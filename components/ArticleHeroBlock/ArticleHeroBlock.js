import React from 'react';
import Block from '../../helpers/Block';
import Image from '../Image/Image';
import Heading from '../Heading/Heading';

import './ArticleHeroBlock.scss';
import ObLink from "../ObLink/ObLink";

export default function ArticleHeroBlock(props) {
    const block = new Block(props);
    const {extraAttributes, isMobile} = props;

    const className = block.getFieldValue('classNames');
    const anchorId = block.getAnchorId();

    const title = extraAttributes.entity.articleOverview.fields.title;
    const eyebrow = extraAttributes.entity.category.fields.title;
    const slug = extraAttributes.entity.category.fields.slug.fields.slug;
    const readingTimeLabel = "Read " + extraAttributes.entity.articleOverview.fields.readTime + " minutes";
    const body = extraAttributes.entity.articleOverview.fields.description;
    const backgroundAsset = extraAttributes.entity.backgroundAsset;

    return (
        <div className={`ob-article-hero ${className || ''}`} id={anchorId}>
            <Image className="ob-article-hero-background" image={backgroundAsset}>
                <div className="ob-article-hero-wrapper">
                    <div className="ob-article-hero-content">
                        <ObLink href={`${slug}`}>
                            {eyebrow}
                        </ObLink>

                        <Heading className="ob-article-hero-heading" tag="h1">
                            {title}
                        </Heading>

                        <p className="ob-article-hero-text">
                            {body}
                        </p>
                        { readingTimeLabel &&
                            <p className="ob-article-hero-time">
                                {readingTimeLabel}
                            </p>
                        }
                    </div>
                </div>
            </Image>
        </div>
    )
}