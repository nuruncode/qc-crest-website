import React, { useState, useEffect, useMemo } from 'react';
import Block from '../../helpers/Block';
import Image from '../Image/Image';
import { Waypoint } from 'react-waypoint';
import Eyebrow from '../Eyebrow/Eyebrow'
import Heading from '../Heading/Heading'
import Button from '../Button/Button'
import BodyText from '../BodyText/BodyText'
import Icon from '../Icon/Icon'
import ObLink from '../ObLink/ObLink'
import PropTypes from 'prop-types'
import './SpotlightContentBlock.scss'
import OnePageScrollContext from '../OnePageScroll/OnePageScrollContext';
import { getConfigurationValue } from "../../helpers/ContentBlockConfiguration";
import { Transition } from 'react-transition-group';

/**
 *
 * Supported Classes:
 *  By default the background is white and the text color is primary-grey.
 *  The CTA is a Button by default
 *  - white-text : change the text color to white
 *  - align-center : force a text-align: center without impacting the text container alignment
 *
 *  - background-primary-grey : change the background to primary grey
 *  - background-secondary-grey : change the background to secondary grey
 *  - background-secondary-off-white : change the background to secondary-off-white
 *  - background-isolate-grey : change the background to isolate grey
 *  - background-primary-blue : change the background to primary-blue
 *  - background-secondary-blue : change the background to secondary-blue
 *
 *  - hasLink : Use ObLink component for the CTA instead ob Button
 *
 */

export default function SpotlightContentBlock(props) {

    const duration = 600;
    
    const valueOf = (name) => {
        let value = undefined;
        if (name && block && block.getFieldValue(name) !== undefined) {
            value = block.getFieldValue(name);
        }
        return value;
    }

    const generateContentBlockState = () => {
        let isMobile = props.isMobile;
        return {
            availableConfigurations: valueOf('devicesConfigurations').map(configuration => configuration.fields.deviceType),
            textAlign: configurationValue('textAlign', isMobile, ''),
            textContainerHorizontalAlignment: configurationValue('textAlignment', isMobile, ''),
            textContainerVerticalAlignment: configurationValue('textContainerVerticalAlignment', isMobile, ''),
            textContainerOffset: configurationValue('textContainerOffset', isMobile, ''),
            textContainerMarginRight: configurationValue('textContainerMarginRight', isMobile, ''),
            textContainerMarginLeft: configurationValue('textContainerMarginLeft', isMobile, ''),
            mainAsset: configurationValue('mainAsset', isMobile),
            mainAssetHorizontalAlignment: configurationValue('mainAssetHorizontalAlignment', isMobile, ''),
            mainAssetMaxWidth: configurationValue('mainAssetMaxWidth', isMobile, ''),
            mainAssetOffsetTop: configurationValue('mainAssetMarginTop', isMobile, ''),
            mainAssetOffsetRight: configurationValue('mainAssetMarginRight', isMobile, ''),
            mainAssetOffsetBottom: configurationValue('mainAssetOffsetBottom', isMobile, ''),
            mainAssetOffsetLeft: configurationValue('mainAssetOffsetLeft', isMobile, ''),
            secondaryAsset: configurationValue('secondaryAsset', isMobile, ''),
            additionalAssetList: configurationValue('additionalAssetList', isMobile, ''),
            backgroundAsset: configurationValue('backgroundAsset', isMobile),
            mainAssetScale: configurationValue('mainAssetScale', isMobile, '')
        };
    }

    const configurationValue = (fieldName, isMobile, defaultValue = false) => {
        return getConfigurationValue(availableConfigurations,
            valueOf('devicesConfigurations'),
            fieldName,
            isMobile,
            props.isTiny,
            defaultValue);
    };

    let [show, setShow] = useState('')

    const _handleWaypointEnter = () => {
        // Trigger by waypoint enter
        showContentElement();
    };

    const showContentElement = () => {
        // Animation on the text container on the "onEnter" event
        setShow('show');
    };

    const imageRender = (resetImage) => {
        return (
            <Image offsetBottom={blockState.mainAssetOffsetBottom}
                   offsetTop={blockState.mainAssetOffsetTop}
                   offsetLeft={blockState.mainAssetOffsetLeft}
                   offsetRight={blockState.mainAssetOffsetRight}
                   scale={blockState.mainAssetScale}
                   image={blockState.mainAsset}
                   resetImage={resetImage}
                   className={blockState.mainAssetHorizontalAlignment && `horizontal-${blockState.mainAssetHorizontalAlignment}`} />
        )
    };

    const secondaryImageRender = (resetImage) => {
        return (
            <Image offsetBottom={blockState.mainAssetOffsetBottom}
                   offsetTop={blockState.mainAssetOffsetTop}
                   offsetLeft={blockState.mainAssetOffsetLeft}
                   offsetRight={blockState.mainAssetOffsetRight}
                   scale={blockState.mainAssetScale}
                   image={blockState.secondaryAsset}
                   resetImage={resetImage}
                   className={blockState.mainAssetHorizontalAlignment && `horizontal-${blockState.mainAssetHorizontalAlignment}`} />
        )
    };

    const additionalAssetRender = (image, key) => {
        return (
            <Image key={key}
                   offsetBottom={blockState.mainAssetOffsetBottom}
                   offsetTop={blockState.mainAssetOffsetTop}
                   offsetLeft={blockState.mainAssetOffsetLeft}
                   offsetRight={blockState.mainAssetOffsetRight}
                   scale={blockState.mainAssetScale}
                   image={image}
                   className={blockState.mainAssetHorizontalAlignment && `horizontal-${blockState.mainAssetHorizontalAlignment}`} />
        )
    };

    const isActiveBlock = (index) => {
        return props.index === index;
    };

    const block = new Block(props);
    const textFadeIn = valueOf('textFadeIn');
    const refreshImageOnFrameChange = valueOf('refreshImageOnFrameChange') || false;
    const availableConfigurations = valueOf('devicesConfigurations').map(configuration => configuration.fields.deviceType);
    
    let blockState  = useMemo(() => generateContentBlockState(), [props.isMobile]);

    let className = 'content ' + block.getFieldValue('classNames');
    const isWhiteText = className.includes("white-text");
    const hasLink = className.includes("hasLink");

    const title = valueOf('title');
    const titleLevel = valueOf('titleLevel');
    let textImage = valueOf('textImage');
    const textImageMobile = valueOf('textImageMobile');
    const textImagePosition = valueOf('textImagePosition') || 'top';
    const anchorId = block.getAnchorId();
    const textContainerBackgroundColor = valueOf('textContainerBackgroundColor');
    const textContainerHasPadding = valueOf('textContainerHasPadding');

    if(props.isMobile && textImageMobile) {
        textImage = textImageMobile;
    }

    useEffect(() => {
        if (textFadeIn == false) {
            //If text fadeIn at false show the element at pageLoad
            showContentElement();
        }
    }, []);

    return (
        <Transition in={isActiveBlock()} timeout={duration}>
            {() => (
                <OnePageScrollContext.Consumer>
                    {context => (
                        <section className={`ob-spotlightContentBlock ${className}`} id={anchorId} data-isactiveblock={isActiveBlock(context.currentIndex)}>
                            {textFadeIn !== false &&
                                <Waypoint onEnter={_handleWaypointEnter} />
                            }

                            <Image image={blockState.backgroundAsset !== false ? blockState.backgroundAsset : undefined} className={`ob-spotlightContentBlock-background`} forceBackground/>

                            <div className={`ob-spotlightContentBlock-wrapper ${blockState.textContainerVerticalAlignment && `vertical-${blockState.textContainerVerticalAlignment}`}
                        ${show}`}>
                                <div className={`ob-spotlightContentBlock-textContent
                                    ${textContainerHasPadding ? ' hasPadding' : ''}
                                    ${blockState.textContainerHorizontalAlignment && `horizontal-${blockState.textContainerHorizontalAlignment}`}`}
                                    style={{
                                        marginTop: blockState.textContainerOffset,
                                        marginLeft: blockState.textContainerMarginLeft,
                                        backgroundColor: textContainerBackgroundColor,
                                        textAlign: blockState.textAlign
                                    }}>

                                    <Eyebrow whiteText={isWhiteText}>{valueOf('surtitle')}</Eyebrow>

                                    { (textImage && textImagePosition == 'top') &&
                                        <Image noLazyLoad image={textImage} />
                                    }

                                    <Heading whiteText={isWhiteText} tag={`h${titleLevel}`} className="ob-display-2-xl">{title}</Heading>

                                    <BodyText whiteText={isWhiteText}>{valueOf('textContent')}</BodyText>

                                    { (textImage && textImagePosition == 'bottom') &&
                                        <Image noLazyLoad image={textImage} />
                                    }

                                    { hasLink ? (
                                        <ObLink href={`${valueOf('callToActionLink')}`}
                                            className={isWhiteText ? 'white' : ''}
                                            icon="chevronRight">
                                            <Icon name="chevronRight" roundedIcon="blue" />
                                            {valueOf('callToActionLabel')}
                                        </ObLink>
                                    ) : (
                                        <Button whiteTheme={isWhiteText}
                                            tag="a"
                                            href={valueOf('callToActionLink')}>
                                            {valueOf('callToActionLabel')}
                                        </Button>
                                    )}

                                    { textFadeIn &&
                                        <Waypoint onEnter={this._handleWaypointEnter} />
                                    }

                                </div>

                                { blockState.mainAsset &&
                                    <div className={`ob-spotlightContentBlock-mainAsset-container`}>
                                        {imageRender(isActiveBlock(context.currentIndex) && refreshImageOnFrameChange)}
                                    </div>
                                }

                                { blockState.secondaryAsset &&
                                    <div className={`ob-spotlightContentBlock-secondaryAsset-container`}>
                                        {secondaryImageRender(isActiveBlock(context.currentIndex) && refreshImageOnFrameChange)}
                                    </div>
                                }

                                { blockState.additionalAssetList &&
                                    <div className={`ob-spotlightContentBlock-additionalAssetList-container`}>
                                        { blockState.additionalAssetList.map((image, key) =>
                                            additionalAssetRender(image, key)
                                        )}
                                    </div>
                                }
                            </div>
                        </section>
                    )}
                </OnePageScrollContext.Consumer>
            )}
        </Transition>
    )
}

SpotlightContentBlock.propTypes = {
    isMobile: PropTypes.bool,
    isTiny: PropTypes.bool,
    index: PropTypes.any,
}