import React from 'react';
import VideoPlayer from './VideoPlayer';
import toJson from 'enzyme-to-json';

function createMockVideoProps(props) {
    return {
        name: 'video name',
        assetId: 'mocked/assetId',
        autoplay: false,
        transcript: 'transcript example',
        ...props,
    };
}

describe('<Video player /> rendering', () => {
    it('should render the video player with the parameters supplied', () => {
        const props = createMockVideoProps();
        const wrapper = render(<VideoPlayer video={props}/>);
        expect(toJson(wrapper)).toMatchSnapshot()
    });
});