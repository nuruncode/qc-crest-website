import React, { Component } from 'react';
import PropTypes from 'prop-types';
import VideoPlayerFactory from "../../helpers/VideoPlayerFactory";
import './VideoPlayer.scss';

class VideoPlayer extends Component {

    constructor(props){
        super(props);
        this.videoRef = React.createRef();
        this.video = props.video;
    }

    getVideoRef() {
        return this.videoRef?.current;
    }
    
    componentDidMount() {
        if(this.videoIsDefined()) {
            const videoPlayerFactory = new VideoPlayerFactory(this.videoRef, this.video);
            videoPlayerFactory.create();
        }
    }

    videoIsDefined() {
        return (this.video && this.video.assetId);
    }

    renderTranscript() {
        return <div className="video-player-transcript visuallyhidden">{this.video.transcript}</div>
    }

    render() {
        return (
            <div className="ob-video-player">
                {this.renderTranscript()}
                <video
                    muted
                    id='video-player'
                    ref={this.videoRef}
                />
            </div>
        )
    }
}

VideoPlayer.propTypes = {
    video: PropTypes.object,
};

export default VideoPlayer