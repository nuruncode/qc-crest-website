import './ComparisonChartBlock.scss'

import React, { Fragment, useCallback, useRef } from 'react';

import ImageFactory from '../../adapters/cloudinary/ImageFactory';
import PropTypes from 'prop-types';
import times from 'lodash/times';
import { Markdown } from '../MarkdownText/MarkdownText';

const MINIMUM_WIDTH_MOBILE_VW = 40;
const PADDING_HORIZONTAL_MOBILE_PX = 39;

function Row({children, role}) {
    return (
        <div role={role} className="ob-comparison-chart-row">{children}</div>
    );
}

Row.defaultProps = {
    role: 'row',
}

Row.propTypes = {
    children: PropTypes.any,
    role: PropTypes.any,
}

function Cell({className, children, role}) {
    return (
        <div className={"ob-comparison-chart-cell " + className} role={role}>{children}</div>
    );
}

Cell.propTypes = {
    className: PropTypes.string,
    children: PropTypes.any,
    role: PropTypes.string
}

function CellHtml({className, children, role}) {
    return (
        <div className={"ob-comparison-chart-cell " + className} role={role}>{children}</div>
    );
}

CellHtml.propTypes = {
    className: PropTypes.string,
    children: PropTypes.any,
    role: PropTypes.string,
}

export default function ComparisonChart({ extraAttributes, className }) {
    const entity = extraAttributes?.entity;

    const leftFadeRef = useRef(null);
    const rightFadeRef = useRef(null);

    const onChartScroll = useCallback((event) => {
        let scrollRatio = event.target.scrollLeft / (event.target.scrollWidth - event.target.offsetWidth);
        scrollRatio = Math.max(0, scrollRatio);
        scrollRatio = Math.min(1, scrollRatio);

        if(leftFadeRef.current) {
            leftFadeRef.current.style.opacity = scrollRatio;
        }

        if(rightFadeRef.current) {
            rightFadeRef.current.style.opacity = 1 - scrollRatio;
        }
    }, []);

    const series = entity?.series;
    const title = entity?.comparisonChart?.fields?.title;
    const classNames = ["ob-comparison-chart"];

    if(className) {
        classNames.push(className);
    }

    const priceRangeLabels = series?.map(x => x?.fields?.priceRangeLabel?.fields?.text);
    const comparisonPoints = series?.map(x => x?.fields?.comparisonPoints?.map(comparisonPoint => comparisonPoint?.fields?.text) || []);
    const longestComparision = comparisonPoints?.reduce((p, c) => c.length > p ? c.length : p, 0);

    return (
        <div className={classNames.join(' ')}>
            <h1 className="ob-comparison-chart-title">{title}</h1>
            <div className="ob-comparison-chart-wrapper-1">
                {Array.isArray(series) && <div onScroll={onChartScroll} className="ob-comparison-chart-wrapper-2">
                    {series.map((_, i) => <div key={i} className="ob-comparison-chart-snap-hack" style={{left: `${i * MINIMUM_WIDTH_MOBILE_VW}vw`}}/>)}
                    <div className="ob-comparison-chart-table"
                        role="table"
                        style={{width: `calc(${MINIMUM_WIDTH_MOBILE_VW * series.length}vw + ${2* PADDING_HORIZONTAL_MOBILE_PX}px)`}}>
                        <Row role="rowheader">
                            {series.map((currentSeries, i) => {
                                const productVariants = currentSeries?.fields?.featuredProduct?.fields?.productVariants;
                                const shortTitle = currentSeries?.fields?.shortTitle;
                                
                                let altText = undefined;
                                let imageSrc = undefined;

                                if(Array.isArray(productVariants) && productVariants.length > 0) {
                                    const asset = productVariants[0]?.fields?.mainAsset;
                                    altText = asset?.fields?.alternateText;
                                    const assetId = asset?.fields?.assetId;
                                    imageSrc = assetId &&  ImageFactory.buildImageUrlByHeight(assetId, '220');
                                }

                                return (
                                    <CellHtml key={i} className="ob-comparison-chart-header" role="columnheader">
                                        <Fragment>
                                            <div className="ob-comparison-chart-image">
                                                <img style={{height: '220px'}} alt={altText} src={imageSrc} />
                                            </div>
                                            <span className="ob-comparison-chart-short-title" dangerouslySetInnerHTML={{__html: shortTitle}}></span>
                                        </Fragment>
                                    </CellHtml>
                                )
                            })}
                        </Row>
                        <Row>
                            {priceRangeLabels.map((priceRangeLabel, i) => 
                                <Cell key={i} className="ob-comparison-chart-price-range" role="cell">{priceRangeLabel}</Cell>)}
                        </Row>
                        {times(longestComparision, i =>
                            <Row key={`feature-row__${i}`}>

                                {comparisonPoints.map((comparisonPoint, j) =>
                                    <Cell key={j} className="ob-comparison-chart-feature-cell" role="cell">
                                        {!!comparisonPoint[i] &&<Markdown source={comparisonPoint[i]} />}
                                    </Cell>)}
                            </Row>
                        )}
                    </div>
                </div>}
                <div className="ob-comparison-chart-fade-left" ref={leftFadeRef}/>
                <div className="ob-comparison-chart-fade-right" ref={rightFadeRef}/>
            </div>
        </div>
    )
}

ComparisonChart.propTypes = {
    extraAttributes: PropTypes.any,
    className: PropTypes.any,
}

ComparisonChart.defaultProps = {
    extraAttributes: {},
}