import './ProductGridBlock.scss';

import React from 'react';

import Block from '../../helpers/Block';
import BodyText from '../BodyText/BodyText';
import Eyebrow from '../Eyebrow/Eyebrow';
import Heading from '../Heading/Heading';
import { ProductGrid } from '../ProductGrid/ProductGrid';

export default function ProductGridBlock(props) {
    const {extraAttributes, isMobile, document} = props;

    const relatedProducts = extraAttributes?.entity?.relatedProducts;
    const products = relatedProducts || document.fields.productList || [];

    return (products) ?
        renderProductGridBlock(products, props, isMobile, !!relatedProducts) :
        '';
}

function renderProductGridBlock(products, props, isMobile, isCarrousel) {
    const propsBlock = new Block(props);

    const title = propsBlock.getFieldValue('title');
    const titleLevel = propsBlock.getFieldValue('titleLevel') || '1';
    const blockClassNames = propsBlock.getFieldValue('classNames');
    const anchorId = propsBlock.getFieldValue('anchorId');
    const eyebrow = propsBlock.getFieldValue('eyebrow');
    const body = propsBlock.getFieldValue('body');
    const seeMoreLabel = propsBlock.getFieldValue('seeMoreLabel')?.fields?.text;
    const seeLessLabel = propsBlock.getFieldValue('seeLessLabel')?.fields?.text;
    const itemsPerLine = propsBlock.getFieldValue('maxNumberInlineItems');
    let whiteText = false;

    const classNames = ['ob-product-grid-block'];

    if(blockClassNames) {
        classNames.push(blockClassNames);
    }

    if (blockClassNames && blockClassNames.indexOf("white-text") != -1) {
        whiteText = true;
    }

    const radialBackground = propsBlock.getFieldValue('radialBackground');

    const style = {
        bg: {
            background: propsBlock.getFieldValue('background')
        }
    }

    const radialStyle = {
        bg: {
            background: radialBackground
        }
    }    

    return (
        <div className={classNames.join(' ')} id={anchorId} style={style.bg ? style.bg : null}>
            {!!radialBackground && <span className="radial-background"  style={radialStyle.bg ? radialStyle.bg : null}></span>}
            <div className="ob-product-grid-block-wrapper">
                {!!eyebrow && <Eyebrow className="ob-product-grid-block__eyebrow">{eyebrow}</Eyebrow>}
                {!!title && <Heading className="ob-product-grid-block__heading" tag={`h${titleLevel}`} whiteText={whiteText}>{title}</Heading>}
                {!!body && <BodyText className="ob-product-grid-block__body-text" whiteText={whiteText}>{body}</BodyText>}
                {Array.isArray(products) && products.length > 0 &&
                <div className="ob-product-grid-block__product-grid-wrapper">
                    <ProductGrid isMobile={isMobile}
                        whiteText={whiteText}
                        itemsPerLine={itemsPerLine}
                        seeMoreProductsText={seeMoreLabel}
                        seeLessProductsText={seeLessLabel}
                        isCarrousel={isCarrousel}
                        products={products} />
                </div>}
            </div>
        </div>
    )
}