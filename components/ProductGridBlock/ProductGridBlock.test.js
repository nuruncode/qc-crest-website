import React from 'react';
import ProductGridBlock from "./ProductGridBlock";

function createMockProductGridProps(props) {
    return {
        document: {
            anchorId: 'anchor id',
            classNames: 'class name',
            background: 'background',
            body: 'body',
            contentType: 'productGridBlock',
            maxNumberInlineItems: '3',
            name: 'name',
            productList: [],
            seeMoreLabel: {},
            title: 'title',
            titleLevel: '1'
        },
        isMobile: false,
        ...props,
    };
}

describe('Product Grid Block rendering', () => {
    it('should render product grid block with the parameters supplied', () => {
        const props = createMockProductGridProps();
        const wrapper = mount(<ProductGridBlock {...props}/>);
        expect(wrapper).toMatchSnapshot()
    });
});