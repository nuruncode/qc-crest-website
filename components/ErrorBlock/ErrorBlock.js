import React, { useCallback } from 'react';
import Image from '../Image/Image'
import Eyebrow from '../Eyebrow/Eyebrow';
import PropTypes from 'prop-types';

import SearchBar from '../SearchBar/SearchBar';

import './ErrorBlock.scss';
import '../Button/Button.scss';
import Heading from '../Heading/Heading';

function Shortcut({title, link}) {
    return (
        <>
            {!!title && !!link && <a href={link}
                className="ob-outline-button ob-error-block-shortcut"
            >
                {title}
            </a>}
        </>
    );
}

export default function ErrorBlock({document}) {

    const eyebrow = document?.fields?.eyebrow?.fields?.text;
    const heading = document?.fields?.heading?.fields?.text;
    const body = document?.fields?.body;
    const searchTitle = document?.fields?.searchTitle?.fields?.text;
    const searchPlaceholder = document?.fields?.searchPlaceholder?.fields?.text;
    const shortcutsTitle = document?.fields?.shortcutsTitle?.fields?.text;

    const desktopBackground = document?.fields?.backgroundAsset;
    const desktopAspectRatio = desktopBackground?.fields?.imageRendition?.fields?.aspectRatio;

    const mobileBackground = document?.fields?.mobileBackgroundAsset;
    // const mobileAspectRatio = mobileBackground?.fields?.imageRendition?.fields?.aspectRatio;

    const shortcuts = document?.fields?.shortcuts?.map(shortcut => 
            ({
                title: shortcut?.fields?.title, 
                link: shortcut?.fields?.pagePath?.fields?.slug
            })
        );

    const _handleSearch = useCallback(() => {
        //Do nothing for now
    });

    return (
        <div className="ob-error-block-wrapper-1">
            <div className="ob-error-block-wrapper-2">
                <div className="ob-error-block-ratios">
                    {!!desktopAspectRatio && <div className="ob-error-block-aspect-ratio" data-format="desktop" style={{width: '100%', paddingBottom: `${100 / desktopAspectRatio}%`}}></div>}
                </div>
                <div className="ob-error-block">
                    {!!desktopBackground && <div data-format="desktop" className="ob-error-block-background">
                        <Image forceBackground image={desktopBackground}/>
                    </div>}
                    {!!mobileBackground && <div data-format="mobile" className="ob-error-block-background">
                        <Image forceBackground image={mobileBackground}/>
                    </div>}
                    <div className="ob-error-block-content-wrapper-1">
                        <div className="ob-error-block-content-wrapper-2">
                            <div className="ob-error-block-content-wrapper-3">
                                <div className="ob-error-block-content-wrapper-4">
                                    <div className="ob-error-block-content">
                                        {!!eyebrow && <Eyebrow className="ob-error-block-eyebrow">{eyebrow}</Eyebrow>}
                                        {!!heading && <Heading className="ob-error-block-heading">{heading}</Heading>}
                                        {!!body && <div className="ob-error-block-body">{body}</div>}
                                        <div className="ob-error-block-search">
                                            {!!searchTitle && <Eyebrow className="ob-error-block-search-title">{searchTitle}</Eyebrow>}
                                            <div className="ob-error-block-search-bar">
                                                <SearchBar onSearch={_handleSearch} placeholder={searchPlaceholder} />
                                            </div>
                                        </div>
                                        {Array.isArray(shortcuts) && shortcuts.length > 0 && <div className="ob-error-block-shortcuts-wrapper">
                                            {!!shortcutsTitle && <div className="ob-error-block-shortcuts-title">
                                                {shortcutsTitle}
                                            </div>}
                                            <div className="ob-error-block-shortcuts">
                                                {shortcuts.map((shortcut, i) => <Shortcut key={i} {...shortcut}/>)}
                                            </div>
                                        </div>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

ErrorBlock.propTypes = {
    document: PropTypes.object,
}