import React from 'react';
import Promo from './Promo';

function createMockPromoProps(props) {
    return {
        document: {
            anchorId: 'anchor id',
            classNames: 'class name',
        },
        extraAttributes: {
            entity: {
                fields: {
                    promo: {
                        subtitle: 'suptitle',
                        description: "description",
                        retailerText: "retailer text",
                        backgroundAsset: {},
                        titleLevel: "2",
                        title: "title",
                        retailerLogo: {},
                        callToActionLink: "call to action",
                        callToActionLabel: "call to action label"
                    }
                }
            }
        },
        isMobile: false,
        ...props,
    };
}

describe('Promo Block rendering', () => {
    it('should render promo with the parameters supplied', () => {
        const props = createMockPromoProps();
        const wrapper = mount(<Promo {...props}/>);
        expect(wrapper).toMatchSnapshot()
    });
});