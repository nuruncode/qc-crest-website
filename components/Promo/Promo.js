import React from 'react';
import PropTypes from 'prop-types';

import Block from '../../helpers/Block';
import Image from '../Image/Image';
import Heading from '../Heading/Heading'
import Button from '../Button/Button'
import BodyText from '../BodyText/BodyText'
import Component from "../../helpers/entity/Component";

import './Promo.scss';

export default function Promo(props) {
    const {extraAttributes, isMobile} = props;
    const entityPromo = extraAttributes?.entity?.promo;

    if (!entityPromo) {
        return null;
    }

    const block = new Block(props);
    const promo = new Component(entityPromo);

    const className = 'promo ' + block.getFieldValue('classNames');
    const isWhiteText = className.includes("white-text");
    const anchorId = block.getAnchorId();

    const subtitle = promo.getFieldValue('subtitle');
    const desctiption = promo.getFieldValue('description');
    const retailerText = promo.getFieldValue('retailerText');
    const backgroundAsset = isMobile ? promo.getFieldValue('mobileBackgroundAsset') : promo.getFieldValue('backgroundAsset');
    const titleLevel = promo.getFieldValue('titleLevel');
    const title = promo.getFieldValue('title');
    const retailerLogo = promo.getFieldValue('retailerLogo');
    const callToActionLink = promo.getFieldValue('callToActionLink');
    const callToActionLabel = promo.getFieldValue('callToActionLabel');

    return (
        <div className={`ob-promo ${className}`} id={anchorId} >
            <Image image={backgroundAsset}>
                <div className="ob-promo__wrapper">
                    <div className="ob-promo__content">
                        <div className={`ob-promo__top`}>
                            <Heading className="ob-promo__title" tag={`h${titleLevel}`} whiteText={isWhiteText}>
                                {title}
                            </Heading>

                            <p className="ob-promo__subtitle">
                                {subtitle}
                            </p>

                            <BodyText>
                                {desctiption}
                            </BodyText>
                        </div>

                        <div className="ob-promo__retailer-wrapper">
                            <Image className="ob-promo__retailer-logo" image={retailerLogo} />

                            <p className="ob-promo__retailer-text">
                                {retailerText}
                            </p>

                            <Button
                                className="ob-promo__retailer-button"
                                tag="a"
                                size="large"
                                href={callToActionLink}
                                whiteTheme
                            >
                                {callToActionLabel}
                            </Button>
                        </div>
                    </div>
                </div>
            </Image>
        </div>
    )
}

Promo.propTypes = {
    extraAttributes: PropTypes.any,
    isMobile: PropTypes.any,
}
