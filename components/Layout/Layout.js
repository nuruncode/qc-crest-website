import React, {useState, useEffect} from 'react';
import throttle from 'lodash/throttle';
import PropTypes from 'prop-types'
import {ParallaxProvider} from 'react-scroll-parallax';

import Block from '../../helpers/Block'
import { useMatchMedia } from '../../helpers/Hooks'

import '../../static/styles/global.scss'

export default function Layout(props) {
  const block = new Block(props);
  const extraAttributes = props.extraAttributes || {};

  const isTiny = useMatchMedia('(max-width: 375px)', null);
  const isMobile = useMatchMedia('(max-width: 767px)', null);
  const isTablet= useMatchMedia('(max-width: 1023px)', null);

  const [customEvent, setCustomEvent] = useState({
    event: {}
  });

  const onClickCallback = (event) => {
    if (event) {
      setCustomEvent({
        event: event.target
      });
    }
  }

  return (
    <div className={'layout ' + block.getFieldValue('classNames')}>
      <ParallaxProvider>
          {block.renderChildBlocks({extraAttributes}, isMobile, isTablet, isTiny, onClickCallback, customEvent)}
      </ParallaxProvider>
    </div>
  );
}

Layout.propTypes = {
  extraAttributes: PropTypes.object
}
