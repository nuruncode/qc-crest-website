import React, { useRef, useEffect, useCallback } from 'react';
import C from 'classnames';
import PropTypes from 'prop-types';

import { useIsomorphicLayoutEffect } from '../../helpers/Hooks';
import { scrollIntoView } from '../../helpers/Scroll';

import './UnderlineTabsNavigation.scss';

export default function UnderlineTabsNavigation({tabs, selectedIndex, isMobile}) {
    const tabsRef = useRef(null);

    const runScrollTo = useCallback((element, options) => {
        if(tabsRef.current) {
            const elementRect = element.getBoundingClientRect();
            const elementWidth = elementRect.right - elementRect.left;
            const windowWidth = window.innerWidth;
            const leftMargin = (windowWidth - elementWidth) / 2;
            const dy =  elementRect.left - leftMargin;
            const nextY = tabsRef.current.scrollLeft + dy;
            if(tabsRef.current.scrollTo) {
                tabsRef.current.scrollTo({ left: nextY, ...options});
            } else {
                tabsRef.current.scrollLeft = nextY;
            }
        }
    }, []);

    const onResize = useCallback(() => {
        if(tabsRef.current) {
            const activeTabQuery = tabsRef.current.querySelector('.ob-underline-tabs-navigation-tab--active');
            if(activeTabQuery) {
                runScrollTo(activeTabQuery);
            }
        }
    }, []);

    const scrollToContent = (event, link) => {
        if (link.charAt(0) !== '#') {
            return;
        }
        event.preventDefault();
        const element  = document.querySelector(link);
        if (element) {
            scrollIntoView(element, {behavior: 'smooth'});
        }
    };

    useEffect(() => {
        window.addEventListener('resize', onResize);
        return () => window.removeEventListener('resize', onResize)
    }, []);

    useIsomorphicLayoutEffect(() => {
        onResize();
    }, [tabsRef.current]);

    useEffect(() => {
        if(tabsRef.current && !isMobile) {
            tabsRef.current.scrollLeft = 0;
        }
    }, [isMobile])

    return (
        <div ref={tabsRef} className="ob-underline-tabs-navigation">
            {tabs.map((tab, i) => (
                <a key={i}
                    href={tab?.link}
                    onClick={event => scrollToContent(event, tab?.link)}
                    className={C('ob-underline-tabs-navigation-tab', {
                    'ob-underline-tabs-navigation-tab--active': i === selectedIndex, })}
                >
                    {tab?.text}
                </a>
            ))}
        </div>
    )
}

UnderlineTabsNavigation.propTypes = {
    tabs: PropTypes.any,
    selectedIndex: PropTypes.any,
    isMobile: PropTypes.bool
}