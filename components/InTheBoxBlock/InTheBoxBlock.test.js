import React from 'react';
import InTheBoxBlock from "./InTheBoxBlock";

function createMockInTheBoxProps(props) {
    return {
        document: {
            anchorId: 'anchor id',
            classNames: 'class name',
        },
        extraAttributes: {
            entity: {
                fields: {
                    inTheBox: {
                        body: 'body',
                        contentType: 'inTheBox',
                        inTheBoxImage: {},
                        items: 'items',
                        mobileInTheBoxImage: {},
                        name: 'name',
                        title: 'title',
                        titleLevel: '1'
                    }
                }
            }
        },
        isMobile: false,
        ...props,
    };
}

describe('In The Box Block rendering', () => {
    it('should render in the box block with the parameters supplied', () => {
        const props = createMockInTheBoxProps();
        const wrapper = mount(<InTheBoxBlock {...props}/>);
        expect(wrapper).toMatchSnapshot()
    });
});