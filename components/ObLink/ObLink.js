import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'

import './ObLink.scss'

function ObLink({children, tag: Tag, href, id, className, styles, role, onClick}) {

    if (!children) {
        return null
    }

    return (
        <>
            { href && Tag == 'a' ? (
                <Link href={href}>
                    <Tag className={`ob-link ${className ? className : ''}`} id={id} style={styles} role={role} onClick={onClick}>
                        {children}
                    </Tag>
                </Link>
            ) : (
                <Tag className={`ob-link ${className ? className : ''}`} id={id} style={styles} role={role} onClick={onClick}>
                    {children}
                </Tag>
            )}

        </>
    )
}

ObLink.propTypes = {
    /**
    * The tag or component to be used e.g. button, a, Link
    */
    tag: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,
        PropTypes.func  
    ]),

    /** Children is the text button */
    children: PropTypes.any,

    /** Link */
    href: PropTypes.string,

    /** Custom class. */
    className: PropTypes.string,

    /** Custom ID. */
    id: PropTypes.string,

    /** Custom styles to be applied to the link */
    styles: PropTypes.object,

    icon: PropTypes.string,

    role:  PropTypes.string,

    onClick: PropTypes.any,
}

ObLink.defaultProps = {
    tag: 'a',
    styles: {}
}


export default ObLink

