### Examples

**Oral-B Default Link / Anchor**

Use Link router from next.

```
<ObLink href="#">TEXT LINK HERE</ObLink>
```
```jsx
import Icon from '../Icon/Icon.js';
<ObLink href="#" className="primaryGrey" icon="chevronRight">
    <Icon name="chevronRight" roundedIcon="blue"/>
    TEXT LINK HERE
</ObLink>
```
```
import Icon from '../Icon/Icon.js';
<div style={{ backgroundColor: '#3D3D41' }}>
<ObLink href="#" className="white" icon="chevronRight" roundedIcon="blue">
    <Icon name="chevronRight" roundedIcon="white"/>
    READ MORE ABOUT iO IN THE PRESS
</ObLink>
</div>
```


**Oral-B Button with link style and '+' icon**
```
import Icon from '../Icon/Icon.js';
<ObLink onClick={_handleClickShowItems} className="ob-gallery__button primaryGrey see-more-button" tag='button'>
    <Icon name="plus" roundedIcon="blue" viewBox={10} />
    {buttonLabel}
</ObLink>
```


**Oral-B Button with link style and '-' icon**
```
<ObLink onClick={_handleClickShowItems} className="ob-gallery__button primaryGrey see-more-button" tag='button'>
    <Icon name="plus" roundedIcon="blue" viewBox={10} />
    {buttonLabel}
</ObLink>
```