import "./OnePageScroll.scss";

import Icon from '../Icon/Icon';
import OnePageScrollContext from './OnePageScrollContext';
import ProgressBar from '../ProgressBar/ProgressBar'
import PropTypes from "prop-types";
import React from "react";

const previousTouchMove = Symbol();
const scrolling = Symbol();
const wheelScroll = Symbol();
const touchMove = Symbol();
const keyPress = Symbol();
const onWindowResized = Symbol();
const addNextComponent = Symbol();
const scrollWindowUp = Symbol();
const scrollWindowDown = Symbol();
const setRenderComponents = Symbol();
const _isMounted = Symbol();

const _isBodyScrollEnabled = Symbol();
const disableScroll = Symbol();
const enableScroll = Symbol();

const ANIMATION_TIMER = 200;
const KEY_UP = 33;
const KEY_DOWN = 34;
const DISABLED_CLASS_NAME = "scroll--disabled";
const CONTAINER_CLASS_NAME = "one-page-scroll-wrapper";

const nextFrameCopy = 'Continue reading';

export default class ReactPageScroller extends React.Component {
    static propTypes = {
        animationFrames: PropTypes.number,
        animationTimer: PropTypes.number,
        transitionTimingFunction: PropTypes.string,
        pageOnChange: PropTypes.func,
        scrollUnavailable: PropTypes.func,
        containerHeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        containerWidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        blockScrollUp: PropTypes.bool,
        blockScrollDown: PropTypes.bool,
        children: PropTypes.any,
        customEvent: PropTypes.object,
        isMobile:  PropTypes.bool,
    };

    static defaultProps = {
        animationFrames: 0,
        animationTimer: 1250,
        transitionTimingFunction: "ease-in-out",
        containerHeight: "100%",
        containerWidth: "100%",
        blockScrollUp: false,
        blockScrollDown: false
    };

    constructor(props) {
        super(props);
        this.state = {
            componentIndex: 0,
            componentsToRenderLength: 0,
            allowVideoPlay: true,
            scrollBackwards: false,
            showArrow: false,
            resetVideo: true,
            componentsTotalLength: 0
        };
        this[previousTouchMove] = null;
        this[scrolling] = false;
        this[_isMounted] = false;
        this[_isBodyScrollEnabled] = true;
    }

    componentDidUpdate(prevProps) {
        // only update chart if the data has changed
        if (prevProps.customEvent !== this.props.customEvent) {
            // const frameNumber = this.props.customEvent.event.currentTarget.dataset.custom;
            if (this.props.customEvent.event) {
                const pageNumber = this.props.customEvent.event.dataset.custom;
                this.goToPage(parseInt(pageNumber))
            }
        }

        if (this.getChildren().length - 1 === this.state.componentIndex) {
            const footer = document.querySelector('.ob-footer');
            const activeBlock = document.querySelector('[data-isactiveblock="true"]');
            activeBlock.appendChild(footer);
        }
      }

    componentDidMount = () => {
        this[_isMounted] = true;
        let renderChildren = this.getChildren();
        
        window.addEventListener('resize', this[onWindowResized]);

        document.ontouchmove = (event) => {
            event.preventDefault();
        };

        this._pageContainer.addEventListener("touchmove", this[touchMove], {passive: true});
        this._pageContainer.addEventListener("keydown", this[keyPress]);

        let componentsToRenderLength = 0;

        if (renderChildren[this.state.componentIndex] != null) {
            componentsToRenderLength++;
        } else {
            componentsToRenderLength++;
        }

        this[addNextComponent](componentsToRenderLength);

        document.documentElement.classList.add(CONTAINER_CLASS_NAME);

        if (renderChildren) {
            this.setState({componentsTotalLength: renderChildren.length});
        }
    };

    componentWillUnmount = () => {
        this[_isMounted] = false;

        window.removeEventListener('resize', this[onWindowResized]);

        document.ontouchmove = () => { return true; };

        this._pageContainer.removeEventListener("touchmove", this[touchMove]);
        this._pageContainer.removeEventListener("keydown", this[keyPress]);

        document.documentElement.classList.remove(CONTAINER_CLASS_NAME);
    };

    goToPage = (number) => {
        const { pageOnChange } = this.props;
        const { componentIndex, componentsToRenderLength } = this.state;
        const children = this.getChildren();

        let newComponentsToRenderLength = componentsToRenderLength;

        if (componentIndex !== number) {
            if (!this[scrolling]) {

                this[scrolling] = true;
                if (pageOnChange) {
                    pageOnChange(number + 1);
                }
                
                if (this["container_" + (number + 2)] == null && children[number + 1] != null) {
                    newComponentsToRenderLength = number + 2;
                } else if(this["container_" + (number + 1)] == null && children[number] != null) {
                    newComponentsToRenderLength = number + 1;
                }

                setTimeout(() => {
                    this.setState({ componentIndex: number, componentsToRenderLength: newComponentsToRenderLength }, () => {
                        this[scrolling] = false;
                        this[previousTouchMove] = null;
                    });
                }, this.props.animationTimer + ANIMATION_TIMER)

            } else if (!this[scrolling] && children[number] == null) {
                for (let i = componentsToRenderLength; i <= number; i++) {
                    newComponentsToRenderLength++;
                }

                if (children[number + 1] != null) {
                    newComponentsToRenderLength++
                }

                this[scrolling] = true;
                this.setState({
                    componentsToRenderLength: newComponentsToRenderLength
                }, () => {
                    if (pageOnChange) {
                        pageOnChange(number + 1);
                    }

                    setTimeout(() => {
                        this.setState({ componentIndex: number }, () => {
                            this[scrolling] = false;
                            this[previousTouchMove] = null;
                        });
                    }, this.props.animationTimer + ANIMATION_TIMER)
                });
            }
        }
    };

    getChildren = () => {
        let renderChildren = this.props.children;

        if(renderChildren.props.children) {
            renderChildren = renderChildren.props.children;
        }

        return renderChildren;
    };

    checkArrow = () => {
        if(!this.state.showArrow) {
            this.setState({
                showArrow: true
            });
        }
    };

    hideArrow = () => {
        if(this.state.showArrow) {
            this.setState({
                showArrow: false
            });
        }
    };

    focusOnCurrent = () => {
        if (this["container_" + (this.state.componentIndex)] != null) {
            this["container_" + (this.state.componentIndex)].focus();
        }
    };

    render() {
        const {containerHeight, containerWidth } = this.props;

        return (
            <OnePageScrollContext.Provider
                value={{
                    toggleVideoPlay: (resetVideo) => {
                        this.setState({
                            allowVideoPlay: !this.state.allowVideoPlay,
                            resetVideo: false
                        });
                    },
                    setVideoResetState: () => {
                        this.setState({
                            resetVideo: true
                        });
                    },

                    forceVideoReset: () => {
                        this.setState({
                            allowVideoPlay: false,
                            resetVideo: false
                        });

                        setTimeout(() => {
                            this.setState({
                                allowVideoPlay: true,
                                resetVideo: true
                            });
                        }, 10);
                    },
                    resetVideo: this.state.resetVideo,
                    allowVideoPlay: this.state.allowVideoPlay,
                    currentIndex: this.state.componentIndex,
                    hideArrow: () => {
                        this.hideArrow();
                    },
                    videoEnded: (goToNextSlide) => {
                        if(goToNextSlide) {
                            this[scrollWindowDown]();
                        }
                    },
                    showArrow: () => {
                        this.checkArrow();
                    },
                }}>
                <ProgressBar 
                    currentIndex={this.state.componentIndex} 
                    componentsTotalLength={this.state.componentsTotalLength}/>

                <div ref={c => this._pageContainer = c}
                     className={`scroller-frame scroller-frame-${this.state.componentIndex} ${this.state.scrollBackwards ? 'scroll-backwards' : ''}`}
                     onWheel={this[wheelScroll]}
                     style={{
                         height: containerHeight,
                         width: containerWidth,
                         position: "relative"
                     }}
                     tabIndex={0}>
                    {this[setRenderComponents]()}
                </div>
            </OnePageScrollContext.Provider>
        )
    }

    [disableScroll] = () => {
        if (this[_isBodyScrollEnabled]) {
            this[_isBodyScrollEnabled] = false;
            window.scrollTo({
                left: 0,
                top: 0,
                behavior: 'smooth'
            });
            document.body.classList.add(DISABLED_CLASS_NAME);
            document.documentElement.classList.add(DISABLED_CLASS_NAME);
        }
    };

    [enableScroll] = () => {
        if (!this[_isBodyScrollEnabled]) {
            this[_isBodyScrollEnabled] = true;
            document.body.classList.remove(DISABLED_CLASS_NAME);
            document.documentElement.classList.remove(DISABLED_CLASS_NAME);
        }
    };

    [wheelScroll] = (event) => {
        if (event.deltaY < 0) {
            this[scrollWindowUp]();
        } else {
            this[scrollWindowDown]();
        }
    };

    [touchMove] = (event) => {
        if (this[previousTouchMove] !== null) {
            if (event.touches[0].clientY > this[previousTouchMove]) {
                this[scrollWindowUp]();
            } else {
                this[scrollWindowDown]();
            }
        } else {
            this[previousTouchMove] = event.touches[0].clientY;
        }
    };

    [keyPress] = (event) => {
        if (event.keyCode === KEY_UP) {
            this[scrollWindowUp]();
        }
        if (event.keyCode === KEY_DOWN) {
            this[scrollWindowDown]();
        }
    };

    [onWindowResized] = () => {
        this.forceUpdate();
    };

    [addNextComponent] = (componentsToRenderOnMountLength) => {
        let componentsToRenderLength = 0;
        let renderChildren = this.getChildren();

        if (componentsToRenderOnMountLength != null) {
            componentsToRenderLength = componentsToRenderOnMountLength;
        }

        componentsToRenderLength = Math.max(componentsToRenderLength, this.state.componentsToRenderLength);

        if (componentsToRenderLength <= this.state.componentIndex + 1) {
            if (renderChildren[this.state.componentIndex + 1] != null) {
                componentsToRenderLength++;
            }
        }

        this.setState({
            componentsToRenderLength
        });
    };

    [setRenderComponents] = () => {
        const newComponentsToRender = [];

        let renderChildren = this.props.children.props.children;

        for (let i = 0; i < this.state.componentsToRenderLength; i++) {
            if (renderChildren[i] != null) {
                newComponentsToRender.push(
                    <div className={`${i > this.state.componentIndex ? 'pre-step' : 'step-' + (this.state.componentIndex - i + 1)} scroller-content scroller-content-` + i}
                         key={i} ref={c => this["container_" + i] = c}
                         tabIndex={-1}
                         aria-hidden={i > this.state.componentIndex}
                         style={{ height: "100%", width: "100%" }}>
                        {renderChildren[i]}
                        {(this.state.componentIndex === i && i !== (this.state.componentsTotalLength - 1)) &&
                            <button className={`ob-one-page-scroll-arrow ${this.state.showArrow ? 'show-arrow' : ''}`}
                                    aria-label={nextFrameCopy}
                                    onClick={() => this[scrollWindowDown]()}>
                                <Icon name="thinChevronDown" color={'#ffffff'} size={ this.props.isMobile ? '4' : '5.8'} />
                            </button>
                        }
                    </div>
                );
            } else {
                break;
            }
        }

        return newComponentsToRender;
    };

    [scrollWindowUp] = () => {
        if (!this[scrolling] && !this.props.blockScrollUp) {

            if (this["container_" + (this.state.componentIndex - 1)] != null) {
                // This will make sure that the user will be able to scroll back up on the last screen, 
                // but allows a scrolling buffer so that the window doesn't scroll back up instantly.
                const activeBlock = document.querySelector('[data-isactiveblock="true"]');
                if (activeBlock.scrollTop > 0) {
                    return;
                }
                
                this[disableScroll]();
                this[scrolling] = true;

                if (this.props.pageOnChange) {
                    this.props.pageOnChange(this.state.componentIndex);
                }

                setTimeout(() => {
                    this[_isMounted] && this.setState((prevState) => ({
                        componentIndex: prevState.componentIndex - 1,
                        scrollBackwards: true
                    }));

                    this.checkArrow();
                    this.focusOnCurrent();
                });

                setTimeout(() => {
                    this[scrolling] = false;
                    this[previousTouchMove] = null;
                }, this.props.animationTimer + ANIMATION_TIMER);
            } else {
                this[enableScroll]();
                if (this.props.scrollUnavailable) {
                    this.props.scrollUnavailable();
                }
            }
        }
    };

    [scrollWindowDown] = () => {
        if (!this[scrolling] && !this.props.blockScrollDown) {

            if (this["container_" + (this.state.componentIndex + 1)] != null) {
                this[disableScroll]();
                this[scrolling] = true;

                if (this.props.pageOnChange) {
                    this.props.pageOnChange(this.state.componentIndex + 2);
                }

                setTimeout(() => {
                    this[_isMounted] && this.setState((prevState) => ({
                        componentIndex: prevState.componentIndex + 1,
                        scrollBackwards: false
                    }));
                    this[addNextComponent]();
                    this.checkArrow();
                    this.focusOnCurrent();
                });

                setTimeout(() => {
                    this[scrolling] = false;
                    this[previousTouchMove] = null;
                }, this.props.animationTimer + ANIMATION_TIMER);

            } else {
                this[enableScroll]();
                if (this.props.scrollUnavailable) {
                    this.props.scrollUnavailable();
                }
                if (this.getChildren().length - 1 === this.state.componentIndex) {
                    const activeBlock = document.querySelector('[data-isactiveblock="true"]');
                    activeBlock.style.overflowY = 'auto';
                }
            }
        }
    };
}
