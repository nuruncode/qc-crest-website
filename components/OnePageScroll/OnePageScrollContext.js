import React from 'react';

const OnePageScrollContext = React.createContext();

export default OnePageScrollContext;