import React, {Component} from 'react';
import SlideToggle from "react-slide-toggle";
import throttle from 'lodash/throttle';
import Block from '../../helpers/Block'
import HierarchyUtils from "../../helpers/HierarchyUtils";
import Icon from "../Icon/Icon";
import Image from "../Image/Image";
import Eyebrow from "../Eyebrow/Eyebrow";
import "./Footer.scss"

class Footer extends Component {

    _desktopBreakpoint = 992;
    constructor(props) {
        super(props);
        this.block = new Block(props);

        this.state = {
            width: 0
        };
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResizeThrottled);
        this.handleResize();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResizeThrottled);
    }

    handleResize() {
        this.setState({
            width: window.innerWidth
        })
    }

    handleResizeThrottled = throttle(this.handleResize.bind(this), 100);

    renderHierarchy(hierarchy) {
        const destinations = hierarchy.fields.destinations;
        let title = hierarchy.fields.title;
        let isMainFooter = HierarchyUtils.getHierarchyId(hierarchy) === "Hierarchy---Footer";
        const isCollapsible = destinations && destinations.length > 1;

        const setClassNames = dest => {
            let className = dest.fields.className ? dest.fields.className : ''
            if (dest.fields.onlyShowOn) {
                className += ` ob-footer__onlyShowOn--${dest.fields.onlyShowOn}`
            }
            if (dest.fields.contentType === 'hierarchy') {
                return 'ob-footer__list ' + className;
            } else if (isMainFooter) {
                return 'ob-footer__item ' + className;
            }
            return className;
        };

        const renderFooter = (callback = null, setCollapsibleElement = null, range = null, toggleState = null) => {
            const styles = range ? { opacity: Math.max(0, range) } : { opacity: 1 };
            return (
                <nav>
                    {title && this.renderHierarchyTitle(hierarchy, title, callback, isCollapsible, toggleState)}
                    {destinations && HierarchyUtils.getHierarchyId(hierarchy) !== 'Hierarchy---Footer---Utility' &&
                        <ul id={HierarchyUtils.getHierarchyId(hierarchy)} ref={HierarchyUtils.getHierarchyId(hierarchy) !== 'Hierarchy---Footer---Top' && setCollapsibleElement}>
                        {
                            destinations.map((dest, index) => (
                                <li key={index} className={setClassNames(dest)} style={styles}>
                                    {
                                        dest?.fields?.image !== undefined ? (
                                            <a href={dest.fields.url}>
                                                <Image noLazyLoad={true} image={dest.fields.image} />
                                                <span>{dest.fields.title}</span>
                                            </a>
                                        ) : (
                                            HierarchyUtils.renderDestination(dest, () => this.renderHierarchy(dest))
                                        )
                                    }
                                </li>
                            ))
                        }
                        </ul>
                    }
                    {destinations && HierarchyUtils.getHierarchyId(hierarchy) === 'Hierarchy---Footer---Utility' &&
                        <ul id={HierarchyUtils.getHierarchyId(hierarchy)}>
                        {
                            destinations.map((dest, index) => (
                                <li key={index} className={setClassNames(dest)} style={styles}>
                                    { HierarchyUtils.getHierarchyId(dest) === 'Link---Footer---AdChoice' &&
                                        <a id="_bapw-link" href="#" rel="noreferrer" target="_blank" className={`ob-footer__adChoice`}>
                                            <Image noLazyLoad={true} elemId="_bapw-icon" image={dest.fields.image} />
                                            <span>{dest.fields.title}</span>
                                        </a>
                                    }
                                    { HierarchyUtils.getHierarchyId(dest) === 'Link---Footer---CCPA' &&
                                        <span dangerouslySetInnerHTML={{__html: `
                                            <a href="javascript:void(0)" onclick="Optanon.ToggleInfoDisplay();">${dest.fields.title}</a>
                                        `,}}>
                                        </span>
                                    }
                                    { HierarchyUtils.getHierarchyId(dest) !== 'Link---Footer---AdChoice' && HierarchyUtils.getHierarchyId(dest) !== 'Link---Footer---CCPA' && (
                                        dest?.fields?.image !== undefined ? (
                                            <a href={dest.fields.url}>
                                                <Image noLazyLoad={true} image={dest.fields.image} />
                                                <span>{dest.fields.title}</span>
                                            </a>
                                        ) : (
                                            HierarchyUtils.renderDestination(dest, () => this.renderHierarchy(dest))
                                        )
                                    )
                                    }
                                </li>
                            ))
                        }
                        <li dangerouslySetInnerHTML={{__html: this.block.getFieldValue('copyright')}}></li>
                        </ul>
                    }
                </nav>
            )
        };

        const renderCollapsibleFooter = () => {
            return (
                <SlideToggle
                    duration={500}
                    collapsed={!isMainFooter}>
                    {({ onToggle, setCollapsibleElement, range, toggleState }) => (
                        renderFooter(onToggle, setCollapsibleElement, range, toggleState)
                    )}
                </SlideToggle>
            )
        }

        return this.state.width <= this._desktopBreakpoint && isCollapsible ? renderCollapsibleFooter() : renderFooter();
    }

    renderHierarchyTitle(hierarchy, title, callback = null, isCollapsible = false, toggleState = null) {

        return (
            <button onClick={callback} type="button" className="ob-footer__title-ctn">
                <Eyebrow tag="h2" className="ob-footer__title">
                    <span>{title}</span>
                    {isCollapsible &&<span className={`ob-footer__title-icon ${toggleState === "COLLAPSED" || toggleState === "COLLAPSING" ? '' : 'ob-footer__title-icon--reversed'}`}><Icon name="fullChevronDown" viewBox={12} size={1.3} /></span>}
                </Eyebrow>
            </button>
        );
    }

    render() {
        const hierarchyDoc = this.block.getFieldValue('hierarchy');
        return (
            <div className='ob-footer'>
                {this.renderHierarchy(hierarchyDoc)}
            </div>
        )
    }
}

export default Footer