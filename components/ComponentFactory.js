import RichText from './RichText/RichText';
import MarkdownText from './MarkdownText/MarkdownText';
import Zone from './Zone/Zone';
import Layout from './Layout/Layout';
import Content from './Content/Content';
import ContentVideo from './ContentVideo/ContentVideo.js';
import MainMenu from './MainMenu/MainMenu';
import Footer from './Footer/Footer';
import LanguageSelection from './LanguageSelection/LanguageSelection';
import PageNavigation from './PageNavigation/PageNavigation';
import TabbedContent from './TabbedContent/TabbedContent';
import AnnouncementBar from './AnnouncementBar/AnnouncementBar';
import SideBySideContainer from './SideBySideContainer/SideBySideContainer';
import SeriesSelector from './SeriesSelector/SeriesSelector';
import MiniNavigation from './MiniNavigation/MiniNavigation';
import PromoBlock from './Promo/Promo';
import SpotlightSlider from './SpotlightSlider/SpotlightSlider';
import SpotlightContentVideo from './SpotlightContentVideo/SpotlightContentVideo';
import SpotlightContentBlock from './SpotlightContentBlock/SpotlightContentBlock';
import SpotlightStickyBar from './SpotlightStickyBar/SpotlightStickyBar';
import InTheBoxBlock from './InTheBoxBlock/InTheBoxBlock';
import ProductGridBlock from './ProductGridBlock/ProductGridBlock';
import CollectionTabsBlock from './CollectionTabs/CollectionTabsBlock';
import ProductHighlights from './ProductHighlights/ProductHighlights';
import FeaturesTabBlock from './FeaturesTabBlock/FeaturesTabBlock';
import BrandGuaranteeBlock from "./BrandGuaranteeBlock/BrandGuaranteeBlock";
import GalleryBlock from "./GalleryBlock/GalleryBlock";
import FAQBlock from "./FAQBlock/FAQBlock";
import BrushConfigurator from "./BrushConfigurator/BrushConfigurator";
import ComparisonChartBlock from "./ComparisonChartBlock/ComparisonChartBlock";
import SpotlightWaitlistBlock from "./SpotlightWaitlistBlock/SpotlightWaitlistBlock";
import SpotlightPreorderBlock from "./SpotlightPreorderBlock/SpotlightPreorderBlock";
import CollectionTabsDropdown from "./CollectionTabs/CollectionTabsDropdownBlock";
import ProductSubNav from './ProductSubNav/ProductSubNav';
import CharacterBlock from './CharacterBlock/CharacterBlock';
import ProductRecommenderBlock from './ProductRecommender/ProductRecommender';
import ShowcaseBlock from './ShowcaseBlock/ShowcaseBlock';
import ArticleHeroBlock from "./ArticleHeroBlock/ArticleHeroBlock";
import SkipNavigationBlock from './SkipNavigationBlock/SkipNavigationBlock';
import AppDownloadBlock from './AppDownloadBlock/AppDownloadBlock';
import ArticleCategoryHeroBlock from "./ArticleCategoryHeroBlock/ArticleCategoryHeroBlock";
import RecentlyViewedProductsBlock from "./RecentlyViewedProductsBlock/RecentlyViewedProductsBlock";
import AdditionalReadingBlock from "./AdditionalReadingBlock/AdditionalReadingBlock";
import UserQuoteBlock from "Component/UserQuoteBlock/UserQuoteBlock";
import ProductOverlay from "./ProductOverlay/ProductOverlay";
import ErrorBlock from 'Component/ErrorBlock/ErrorBlock';

class ComponentFactory {

  constructor() {
    this.modules = {};
    this.addModule('Zone', Zone);
    this.addModule('Layout', Layout);
    this.addModule('ContentBlock', Content);
    this.addModule('contentVideo', ContentVideo);
    this.addModule('richTextBlock', RichText);
    this.addModule('markdownTextBlock', MarkdownText);
    this.addModule('mainMenuBlock', MainMenu);
    this.addModule('footerBlock', Footer);
    this.addModule('languageSelectionBlock', LanguageSelection);
    this.addModule('pageNavigationBlock', PageNavigation);
    this.addModule('tabbedContentBlock', TabbedContent);
    this.addModule('announcementBarBlock', AnnouncementBar);
    this.addModule('sideBySideContainerBlock', SideBySideContainer);
    this.addModule('seriesSelectorBlock', SeriesSelector);
    this.addModule('miniNavigationBlock', MiniNavigation);
    this.addModule('promoBlock', PromoBlock);
    this.addModule('spotlightSlider', SpotlightSlider);
    this.addModule('spotlightContentVideo', SpotlightContentVideo);
    this.addModule('spotlightContentBlock', SpotlightContentBlock);
    this.addModule('spotlightStickyBarBlock', SpotlightStickyBar);
    this.addModule('inTheBoxBlock', InTheBoxBlock);
    this.addModule('productGridBlock', ProductGridBlock);
    this.addModule('collectionTabsBlock', CollectionTabsBlock);
    this.addModule('productHighlightBlock', ProductHighlights);
    this.addModule('featuresTabBlock', FeaturesTabBlock)
    this.addModule('brandGuaranteeBlock', BrandGuaranteeBlock);
    this.addModule('galleryBlock', GalleryBlock);
    this.addModule('faqBlock', FAQBlock);
    this.addModule('brushConfiguratorBlock', BrushConfigurator);
    this.addModule('comparisonChartBlock', ComparisonChartBlock);
    this.addModule('userQuoteBlock', UserQuoteBlock);
    this.addModule('spotlightWaitlistBlock', SpotlightWaitlistBlock);
    this.addModule('spotlightPreOrderBlock', SpotlightPreorderBlock);
    this.addModule('collectionSegmentTabsDropdownBlock', CollectionTabsDropdown);
    this.addModule('productSubNav', ProductSubNav);
    this.addModule('characterBlock', CharacterBlock);
    this.addModule('productRecommenderBlock', ProductRecommenderBlock);
    this.addModule('showcaseBlock', ShowcaseBlock);
    this.addModule('articleHeroBlock', ArticleHeroBlock);
    this.addModule('skipNavigationBlock', SkipNavigationBlock);
    this.addModule('appDownloadBlock', AppDownloadBlock);
    this.addModule('articleCategoryHeroBlock', ArticleCategoryHeroBlock);
    this.addModule('recentlyViewedProductsBlock', RecentlyViewedProductsBlock);
    this.addModule('additionalReadingBlock', AdditionalReadingBlock);
    this.addModule('productOverlay', ProductOverlay);
    this.addModule('errorBlock', ErrorBlock);
  }

  addModule(moduleName, moduleClass) {
    this.modules[moduleName.toUpperCase()] = moduleClass;
  }

  getModule(moduleClass) {
    const moduleName = moduleClass.toUpperCase();
    return this.modules[moduleName];
  }
}

export default new ComponentFactory();
