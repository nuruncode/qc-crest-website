import React from 'react';
import Block from '../../helpers/Block';
import Image from '../Image/Image';
import Heading from '../Heading/Heading';
import './ShowcaseBlock.scss';

export default function ShowcaseBlock(props) {
    const block = new Block(props);

    const className = block.getFieldValue('classNames');
    const anchorId = block.getAnchorId();

    const title = block.getFieldValue('title');
    const description = block.getFieldValue('description');
    const sideImage = block.getFieldValue('sideImage');

    return (
        <div className={`ob-showcase ${className}`} id={anchorId} >
            <Image image = {sideImage}></Image>
            <div className={`ob-showcase__content`}>
                <Heading tag="h1" className={`ob-showcase__title`}>{title}</Heading>
                <p className={`ob-showcase__description`}>{description}</p>
            </div>
        </div>
    )
}