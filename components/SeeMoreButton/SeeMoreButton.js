import './SeeMoreButton.scss'
import C from 'classnames';
import Icon from "../Icon/Icon";
import PropTypes from 'prop-types';
import React from 'react';

export default function SeeMoreButton({isOpen, seeMoreText, seeLessText, onClick, whiteText}) {

    return (
        <div className={
            C('ob-see-more-button"',
            {'ob-see-more-button--white': whiteText})
        }>
            <Icon className="ob-see-more-button__icon" roundedIcon="blue" name={isOpen ? 'chevronLeft' : 'chevronRight'}/>
            <button onClick={onClick} type={'button'} className={'ob-see-more-button__text'}>{isOpen ? seeLessText : seeMoreText}</button>
        </div>
    )
}

SeeMoreButton.propTypes = {
    isOpen: PropTypes.bool,
    seeMoreText: PropTypes.string,
    seeLessText: PropTypes.string,
    onClick: PropTypes.func,
    whiteText: PropTypes.bool,
}