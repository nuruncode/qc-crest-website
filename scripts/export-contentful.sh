#!/bin/bash

#space-id
# -ss | --source-space-id
# -ds | --destination-space-id
#environment-id
# -se | --source-environment-id
# -de | --destination-environment-id
#management-token
# -smt | --source-management-token
# -dmt | --destination-management-token

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

function usage
{
  echo ""
  echo "   usage : export-contentful.sh [-ss source-space-id] [-se source-environment-id] [-smt source-management-token]"
  echo ""
  echo "          -ss, --source-space-id"
  echo "                 source Contentful space id"
  echo "          -se, --source-environment-id"
  echo "                 source Contentful environment id (default: master)"
  echo "          -smt, --source-management-token"
  echo "                 source Contentful management token"
  echo "          -h, --help"
  echo "                 This help"
  echo ""
  echo ""
}

while [ "$1" != "" ]
do
  case $1 in
     -ss | --source-space-id )
        shift
        SOURCE_SPACE_ID=$1
        ;;
     -se | --source-environment-id )
        shift
        SOURCE_ENVIRONMENT_ID=$1
        ;;
     -smt | --source-management-token )
        shift
        SOURCE_MANAGEMENT_TOKEN=$1
        ;;
     -h | --help )
        usage
        exit
        ;;
     * )
        usage
        exit
        ;;
  esac
  shift
done

#
#  Validations
#
if [[ "${SOURCE_SPACE_ID}" == "" ]]; then
   usage
   exit 1
fi

if [[ "${SOURCE_ENVIRONMENT_ID}" == "" ]]; then
   SOURCE_ENVIRONMENT_ID='master'
fi

if [[ "${SOURCE_MANAGEMENT_TOKEN}" == "" ]]; then
   usage
   exit 1
fi

echo ""
echo "Exporting backup from ${SOURCE_ENVIRONMENT_ID}"
echo ""

EXPORT_DIRECTORY="${DIR}/backup"
EXPORT_FILE="contentful-export.${SOURCE_SPACE_ID}.${SOURCE_ENVIRONMENT_ID}.`date +%Y%m%d%H%M%S`.json"
CLI_PATH="${DIR}/../node_modules/.bin/"

if [ ! -d "${EXPORT_DIRECTORY}" ]; then
   mkdir -p "${EXPORT_DIRECTORY}"
fi

"${CLI_PATH}"/contentful space export --space-id ${SOURCE_SPACE_ID} --environment-id ${SOURCE_ENVIRONMENT_ID} --management-Token ${SOURCE_MANAGEMENT_TOKEN} --export-dir ${EXPORT_DIRECTORY} --content-file ${EXPORT_FILE}
