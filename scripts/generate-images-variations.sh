#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

function usage
{
  echo ""
  echo "   usage : generate-image-variations.sh [-f filename]"
  echo ""
  echo "          -f, --filename"
  echo "                 filename that you want generate variations"
  echo "          -h, --help"
  echo "                 This help"
  echo ""
  echo ""
}

while [ "$1" != "" ]
do
  case $1 in
     -f | --filename )
        shift
        FILENAME=$1
        ;;
     -h | --help )
        usage
        exit
        ;;
     * )
        usage
        exit
        ;;
  esac
  shift
done

#
#  Validations
#
if [[ "${FILENAME}" == "" ]]; then
   usage
   exit 1
fi

if ! [ -x "$(command -v pngquant)" ]; then
  echo 'Error: pngquant is not installed.' >&2
  echo 'run : brew install pngquant' >&2
  exit 1
fi

if ! [ -x "$(command -v cwebp)" ]; then
  echo 'Error: webp is not installed.' >&2
  echo 'run : brew install webp' >&2
  exit 1
fi

if ! [ -x "$(command -v convert)" ]; then
  echo 'Error: convert is not installed.' >&2
  exit 1
fi

FILENAME_EXTENSION="${FILENAME##*.}"
FILENAME_WITHOUT_EXTENSION=$(basename "${FILENAME%.*}")

GENERATED_DIRECTORY="generated"
GENERATED_TEMP_DIRECTORY="generated/temp"

RETINA_SIZE=100%
DESKTOP_SIZE=50%
MOBILE_RETINA_SIZE=70%
MOBILE_SIZE=35%

RETINA_SUFFIX='@2x.png'
DESKTOP_SUFFIX='.png'
MOBILE_RETINA_SUFFIX='-mobile@2x.png'
MOBILE_SUFFIX='-mobile.png'

if [ ! -d "${GENERATED_DIRECTORY}" ]; then
   mkdir -p "${GENERATED_DIRECTORY}"
fi

rm -rf ${GENERATED_DIRECTORY}/*

if [ ! -d "${GENERATED_TEMP_DIRECTORY}" ]; then
   mkdir -p "${GENERATED_TEMP_DIRECTORY}"
fi

echo ""
echo "Generating file variation ..."
echo ""
echo "generated files :"
echo ""

## GENRATE RETINA DESKTOP
convert -compress lossless -resize ${RETINA_SIZE} ${FILENAME} ${GENERATED_TEMP_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${RETINA_SUFFIX}
pngquant --speed 1 --output ${GENERATED_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${RETINA_SUFFIX} -- "${GENERATED_TEMP_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${RETINA_SUFFIX}"
echo "${GENERATED_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${RETINA_SUFFIX}"

## GENERATE DESKTOP
convert -compress None -antialias -resize ${DESKTOP_SIZE} ${FILENAME} ${GENERATED_TEMP_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${DESKTOP_SUFFIX}
pngquant --speed 1 --output ${GENERATED_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${DESKTOP_SUFFIX} -- "${GENERATED_TEMP_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${DESKTOP_SUFFIX}"
echo "${GENERATED_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${DESKTOP_SUFFIX}"

## GENRATE RETINA MOBILE
convert -compress lossless -resize ${MOBILE_RETINA_SIZE} ${FILENAME} ${GENERATED_TEMP_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${MOBILE_RETINA_SUFFIX}
pngquant --speed 1 --output ${GENERATED_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${MOBILE_RETINA_SUFFIX} -- "${GENERATED_TEMP_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${MOBILE_RETINA_SUFFIX}"
echo "${GENERATED_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${MOBILE_RETINA_SUFFIX}"

## GENERATE MOBILE
pngquant --speed 1 --output ${GENERATED_TEMP_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${MOBILE_SUFFIX} -- "${FILENAME}"
convert -compress None -antialias -resize ${MOBILE_SIZE} ${GENERATED_TEMP_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${MOBILE_SUFFIX} ${GENERATED_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${MOBILE_SUFFIX}
#pngquant --speed 1 --output ${GENERATED_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${MOBILE_SUFFIX} -- "${GENERATED_TEMP_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${MOBILE_SUFFIX}"
echo "${GENERATED_DIRECTORY}/${FILENAME_WITHOUT_EXTENSION}${MOBILE_SUFFIX}"

#rm -rf ${GENERATED_TEMP_DIRECTORY}
