How to restore :

- If not already installed, add contentful-cli module :
    ${HOME_REPO} : ./yarn add contentful-cli
- Download and Unzip backup file from the build artifact in the scripts/backup/ directory
- Execute restore command with parameters
    cd scripts
    ./restore-contentful.sh -ds mdzpln36fbpp -de develop -dmt CFPAT-d3e463dd9380921024acd2697b6c1e806601131b75c3d1e5a0100c9f8e389f4e -ef contentful-export.mdzpln36fbpp.develop.20190215171743.json

parameters :
space-id
 -ds | --destination-space-id
environment-id
 -de | --destination-environment-id
management-token
 -dmt | --destination-management-token
export-file
 -ef | --export-file

