#!/bin/bash

#space-id
# -ss | --source-space-id
# -ds | --destination-space-id
#environment-id
# -se | --source-environment-id
# -de | --destination-environment-id
#management-token
# -smt | --source-management-token
# -dmt | --destination-management-token

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

function usage
{
  echo ""
  echo "   usage : restore-contentful.sh [-ds destination-space-id] [-de destination-space-id] [-dmt destination-management-token] [-ef export-file]"
  echo ""
  echo "          -ds, --destination-space-id"
  echo "                 destination Contentful space id"
  echo "          -de, --destination-environment-id"
  echo "                 destination Contentful environment id (default: master)"
  echo "          -dmt, --destination-management-token"
  echo "                 destination Contentful management token"
  echo "          -ef, --export-file"
  echo "                 contentful export file name in the script/backup directory"
  echo "          -h, --help"
  echo "                 This help"
  echo ""
  echo ""
}

while [ "$1" != "" ]
do
  case $1 in
     -ds | --destination-space-id )
        shift
        DESTINATION_SPACE_ID=$1
        ;;
     -de | --destination-environment-id )
        shift
        DESTINATION_ENVIRONMENT_ID=$1
        ;;
     -dmt | --destination-management-token )
        shift
        DESTINATION_MANAGEMENT_TOKEN=$1
        ;;
     -ef | --export-file )
        shift
        EXPORT_FILE=$1
        ;;
     -h | --help )
        usage
        exit
        ;;
     * )
        usage
        exit
        ;;
  esac
  shift
done

#
#  Validations
#
if [[ "${DESTINATION_SPACE_ID}" == "" ]]; then
   usage
   exit 1
fi

if [[ "${DESTINATION_ENVIRONMENT_ID}" == "" ]]; then
   DESTINATION_ENVIRONMENT_ID='master'
fi

if [[ "${DESTINATION_MANAGEMENT_TOKEN}" == "" ]]; then
   usage
   exit 1
fi

EXPORT_DIRECTORY="${DIR}/backup"
CLI_PATH="${DIR}/../node_modules/.bin/"

if [ ! -f "${EXPORT_DIRECTORY}/${EXPORT_FILE}" ]; then
   echo ""
   echo "-- > Export file does not exist"
   usage
   exit 1
fi
 
echo ""
echo "Importing backup into ${DESTINATION_ENVIRONMENT_ID} environment"
echo ""

"${CLI_PATH}"/contentful space import --space-id ${DESTINATION_SPACE_ID} --environment-id ${DESTINATION_ENVIRONMENT_ID} --management-Token ${DESTINATION_MANAGEMENT_TOKEN} --content-file ${EXPORT_DIRECTORY}/${EXPORT_FILE}


