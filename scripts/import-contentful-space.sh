#!/bin/bash

#space-id
# -ss | --source-space-id
# -ds | --destination-space-id
#environment-id
# -se | --source-environment-id
# -de | --destination-environment-id
#management-token
# -smt | --source-management-token
# -dmt | --destination-management-token

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

function usage
{
  echo ""
  echo "   usage : import-contenuful-space.sh [-ss source-space-id] [-se]"
  echo ""
  echo "          -ss, --source-space-id"
  echo "                 source Contentful space id"
  echo "          -se, --source-environment-id"
  echo "                 source Contentful environment id (default: master)"
  echo "          -smt, --source-management-token"
  echo "                 source Contentful management token"
  echo "          -ds, --destination-space-id"
  echo "                 destination Contentful space id"
  echo "          -de, --destination-environment-id"
  echo "                 destination Contentful environment id (default: master)"
  echo "          -dmt, --destination-management-token"
  echo "                 destination Contentful management token"
  echo "          -h, --help"
  echo "                 This help"
  echo ""
  echo ""
}

while [ "$1" != "" ]
do
  case $1 in
     -ss | --source-space-id )
        shift
        SOURCE_SPACE_ID=$1
        ;;
     -se | --source-environment-id )
        shift
        SOURCE_ENVIRONMENT_ID=$1
        ;;
     -smt | --source-management-token )
        shift
        SOURCE_MANAGEMENT_TOKEN=$1
        ;;
     -ds | --destination-space-id )
        shift
        DESTINATION_SPACE_ID=$1
        ;;
     -de | --destination-environment-id )
        shift
        DESTINATION_ENVIRONMENT_ID=$1
        ;;
     -dmt | --destination-management-token )
        shift
        DESTINATION_MANAGEMENT_TOKEN=$1
        ;;
     -h | --help )
        usage
        exit
        ;;
     * )
        usage
        exit
        ;;
  esac
  shift
done

#
#  Validations
#
if [[ "${SOURCE_SPACE_ID}" == "" ]]; then
   usage
   exit 1
fi

if [[ "${SOURCE_ENVIRONMENT_ID}" == "" ]]; then
   SOURCE_ENVIRONMENT_ID='master'
fi

if [[ "${SOURCE_MANAGEMENT_TOKEN}" == "" ]]; then
   usage
   exit 1
fi

if [[ "${DESTINATION_SPACE_ID}" == "" ]]; then
   usage
   exit 1
fi

if [[ "${DESTINATION_ENVIRONMENT_ID}" == "" ]]; then
   DESTINATION_ENVIRONMENT_ID='master'
fi

if [[ "${DESTINATION_MANAGEMENT_TOKEN}" == "" ]]; then
   usage
   exit 1
fi

echo ""
echo "Installing contentful client ..."
echo ""

`${DIR}/../yarn` global add contentful-cli

echo ""
echo "Exporting backup from source ..."
echo ""

EXPORT_DIRECTORY="${DIR}/backup"
EXPORT_FILENAME="contentful_space_export_${SOURCE_SPACE_ID}_${SOURCE_ENVIRONMENT_ID}.json"

mkdir -p ${EXPORT_DIRECTORY}

contentful space export --space-id ${SOURCE_SPACE_ID} --environment-id ${SOURCE_ENVIRONMENT_ID} --management-Token ${SOURCE_MANAGEMENT_TOKEN} --export-dir ${EXPORT_DIRECTORY} --content-file ${EXPORT_FILENAME}

echo ""
echo "Importing backup into destination ..."
echo ""

contentful space import --space-id ${DESTINATION_SPACE_ID} --environment-id ${DESTINATION_ENVIRONMENT_ID} --management-Token ${DESTINATION_MANAGEMENT_TOKEN} --content-file ${EXPORT_DIRECTORY}/${EXPORT_FILENAME}

rm -f ${EXPORT_DIRECTORY}/${EXPORT_FILENAME}
