import Document, {Head, Main, NextScript} from 'next/document'
import getConfig from "next/config";
import React from "react"
import contentfulClient from 'Model/ContentfulClient';

const {publicRuntimeConfig} = getConfig();

export default class BaseDocument extends Document {

  static async getInitialProps({renderPage}) {
    const page = renderPage(App => {
      const Page = (props) => <App {...props} />
      return Page;
    });

    // define language code
    //const space = await contentfulClient.getClient().getSpace();
    const client = await contentfulClient.getClient();
    const locales = await client.getLocales();
    const defaultLocale = locales.items.find(locale => locale.default);
    const languageCode = defaultLocale.code.substring(0, 2);
    const countryCode = defaultLocale.code.slice(-2);

    // return styles collected
    return {...page, languageCode: languageCode, countryCode: countryCode}
  }

  render() {
    return (
      <html lang={this.props.languageCode}>

      <Head>
        <meta name="ps-key" content="UNIQUE KEY SPECIFIC TO CLIENT/BRAND" />
        <meta name="ps-country" content={this.props.countryCode}/>
        <meta name="ps-language" content={this.props.languageCode}/>
        <script src="//cdn.pricespider.com/1/lib/ps-widget.js" async>
        </script>
        <link rel="preconnect" href="https://www.googletagmanager.com" />
        <script defer type="text/javascript" dangerouslySetInnerHTML={{__html: `
        var PGdataLayer = {
                            'GTM': {
                              'SiteCountry':'${this.props.countryCode}',
                              'GoogleAnalyticsSiteSpeedSampleRate':'regular',
                              'GoogleAnalyticsAllowLinker':'false',
                              'GoogleAnalyticsOptimizeContainerID':'GTM-MRLZ5XZ',
                              'GoogleReCaptcha':'false',
                              'SiteHost': '',
                              'SiteTechnicalAgency': 'Nurun',
                              'SiteLocalContainer': '',
                              'GoogleAnalyticsLocal': '',
                              'ConsentOverlay': 'OneTrust',
                              'ConsentOverlayID': 'consent overlayID goes here',
                              'SitePrivacyProtection': 'CCPA',
                            }
                          };
        `}}>
        </script>
        <script defer dangerouslySetInnerHTML={{__html: `
        (
                function(w,d,s,l,i) {
                  w[l]=w[l]||[];w[l].push({
                    'gtm.start' : new Date().getTime(),
                    event : 'gtm.js'
                  });
                  var f = d.getElementsByTagName(s)[0],
                  j=d.createElement(s),
                  dl = l!='dataLayer'?'&l='+l:'';

                  j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;
                  f.parentNode.insertBefore(j,f);
                }
            )(window,document,'script','dataLayer','GTM-N94XXFB');
        `,}}>
        </script>
        <link rel="preconnect" href="https://api.bazaarvoice.com" />
        <link rel="preconnect" href="https://d.agkn.com" />
        <link rel="preload" href="/static/fonts/Neutraface2Text-Book.woff" as="font" crossOrigin="anonymous" />
        <link rel="preload" href="/static/fonts/Neutraface2Text-Book.woff2" as="font" crossOrigin="anonymous" />
        <link rel="shortcut icon" href="/static/images/OralB_Favicon.png" type="image/x-icon" />
        <link rel="icon" href="/static/images/OralB_Favicon.png" type="image/x-icon" />
        <link href="/static/styles/vendor/cloudinary-video-player/dist/cld-video-player.min.css" rel="stylesheet" />
        <link href="/static/styles/font-face.css" rel="preload stylesheet" as="style" />
      </Head>
      <body className="ob-body">
      <noscript dangerouslySetInnerHTML={{__html: `
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N94XXFB" height="0" width="0" style="display:none;visibility:hidden"></iframe>
      `}}>
      </noscript>

      <Main />
        <script type="text/javascript" defer dangerouslySetInnerHTML={{
          __html: `
            (function() {
            var ev = document.createElement('script');
            ev.type = 'text/javascript';
            ev.async = true;
            ev.setAttribute('data-ev-tag-pid', ${publicRuntimeConfig.DATA_EV_TAG_PID});
            ev.setAttribute('data-ev-tag-ocid', ${publicRuntimeConfig.DATA_EV_TAG_OCID});
            ev.src = ('https:' == document.location.protocol? 'https://' : 'http://') + 'c.betrad.com/pub/tag.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ev, s);
          })();`,
        }}>
        </script>
        <NextScript />
      </body>
      </html>
    )
  }
}
