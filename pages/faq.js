import "@babel/polyfill";
import React, {Fragment} from 'react'

import FAQDao from 'Model/dao/FAQDao';
import Router from 'next/router';
import Head from 'next/head';
import Block from '../helpers/Block';
import PageDao from "../model/dao/PageDao";
import PropTypes from 'prop-types';
import Meta from '../components/Meta/Meta';

export default class FAQ extends React.Component {

    componentDidMount() {
        Router.beforePopState(({as}) => {
            location.href = as;
        });
    }

    static async getInitialProps({ asPath }) {
        const baseProps = await this.initPage(asPath);
        return {
            ...baseProps
        }
    }

    static async initPage(slug) {
        let faqs = undefined;
        let page = undefined;

        // Errors pages doesn't have a page entry in Contentful CMS
        if (slug) {
            page = await PageDao.getActivePageBySlug(slug);
            faqs = await FAQDao.getAllFAQs();
        }

        return {'page' : page, 'faqs': faqs };
    }

    constructor(props) {
        super(props);
    }

    /**
     * Rendering of the page. This always render the page using a common layout.
     *
     * Override the render method to do otherwise.
     *
     * @returns {*}
     */
    render() {
        let page = this.getPage();
        let pagePath = page.slug;

        return (
            <Fragment>
                <Meta metadata={this.getMetadata()} pagePath={pagePath} />
                <div className="ob-wrapper">
                    {this.renderBody()}
                </div>
            </Fragment>
        )
    }

    getPage() {
        return this.props.page.fields;
    }

    getFAQ() {
        return this.props.faqs;
    }

    getMetadata() {
        return this.props.page.fields.metadata.fields;
    }

    /**
     * By default, the page will render the modules of the Product.
     * Can be overridden on the child class to do specific rendering.
     * @returns {*|void}
     */
    renderBody() {
        return new Block(this.props.page).renderChildBlocks({
            extraAttributes : {
                entity: this.getPage(),
                results: this.getFAQ()
            }
        });
    }
}

FAQ.propTypes = {
    page: PropTypes.any,
    faqs: PropTypes.any,
}
