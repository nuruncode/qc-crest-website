import '@babel/polyfill';
import 'es6-promise/auto';

import App from 'next/app';

import '../static/styles/normalize.scss';
import '../static/styles/global.scss';
import '../static/styles/icomoon.scss';
import '../static/styles/header.scss';
import '../static/styles/modal.scss';
import '../static/styles/one-page-scroll.scss';
import '../static/styles/bazaarvoice.scss';

export default App;
