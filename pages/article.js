import "@babel/polyfill";
import React, {Fragment} from 'react'
import PropTypes from 'prop-types'

import articleDao from 'Model/dao/ArticleDao';
import Router from 'next/router';
import Head from 'next/head';
import Block from '../helpers/Block';
import Meta from '../components/Meta/Meta';

class Article extends React.Component {

    componentDidMount() {
        Router.beforePopState(({as}) => {
            location.href = as;
        });
    }

    static async getInitialProps({ asPath }) {
        const baseProps = await this.initPage(asPath);
        return {
            ...baseProps
        }
    }

    static async initPage(slug) {
        let article = undefined;

        // Errors pages doesn't have a page entry in Contentful CMS
        if (slug) {
            article = await articleDao.getActiveArticleBySlug(slug);
        }

        return {'article' : article};
    }

    constructor(props) {
        super(props);
    }

    /**
     * Rendering of the page. This always render the page using a common layout.
     *
     * Override the render method to do otherwise.
     *
     * @returns {*}
     */
    render() {
        let article = this.getArticle();
        let pagePath = article.articleOverview.fields.slug.fields.slug;

        return (
            <Fragment>
                <Meta metadata={this.getMetadata()} pagePath={pagePath}/>
                <div className="ob-wrapper">
                    {this.renderBody()}
                </div>
            </Fragment>
        )
    }

    getArticle() {
        return this.props.article.fields;
    }

    getMetadata() {
        return this.props.article.fields.metadata.fields
    }

    /**
     * By default, the page will render the modules of the Product.
     * Can be overridden on the child class to do specific rendering.
     * @returns {*|void}
     */
    renderBody() {
        return new Block(this.props.article).renderChildBlocks({ extraAttributes : {entity: this.getArticle()}});
    }
}

Article.propTypes = {
    article: PropTypes.object
}

export default Article
