import "@babel/polyfill";
import React, {Fragment} from 'react'
import PropTypes from 'prop-types'

import productDao from 'Model/dao/ProductDao';
import Router from 'next/router';
import Head from 'next/head';
import Block from '../helpers/Block';

import Meta from '../components/Meta/Meta';

import getConfig from "next/config";
import ImageFactory from "../adapters/cloudinary/ImageFactory";

const {publicRuntimeConfig} = getConfig();

import { setViewed } from '../persistence/recentlyViewed'

const brandName = "crest"

class Product extends React.Component {

    componentDidMount() {
        const slug = this.props?.slug;

        if(slug) {
            setViewed(slug);
        }

        Router.beforePopState(({as}) => {
            location.href = as;
        });
    }

    static async getInitialProps({ asPath }) {
        const baseProps = await this.initPage(asPath);
        return {
            ...baseProps
        }
    }

    static async initPage(slug) {
        let product = undefined;

        // Errors pages doesn't have a page entry in Contentful CMS

        if (slug) {
            product = await productDao.getActiveProductBySlug(slug);
        }

        return {'product' : product, 'slug' : slug};
    }

    constructor(props) {
        super(props);
    }

    /**
     * Rendering of the page. This always render the page using a common layout.
     *
     * Override the render method to do otherwise.
     *
     * @returns {*}
     */
    render() {
        let product = this.getProduct();
        let pagePath = product.productOverview.fields.slug.fields.slug;

        return (
            <Fragment>
                <Meta metadata={this.getMetadata()} pagePath={pagePath}/>
                <Head>
                    {this.renderBVScript()}
                    {this.renderBazaarVoiceDynamicCatalogCollectionScript()}
                </Head>
                <div className="ob-wrapper">
                    {this.renderBody()}
                </div>
            </Fragment>
        )
    }

    getProduct() {
        return this.props.product.fields;
    }

    getMetadata() {
        return this.props.product.fields.metadata.fields
    }

    /**
     * By default, the page will render the modules of the Product.
     * Can be overridden on the child class to do specific rendering.
     * @returns {*|void}
     */
    renderBody() {
        return new Block(this.props.product).renderChildBlocks({ extraAttributes : {entity: this.getProduct(), slug: this.props.slug}});
    }

    getImageUrl(image) {
        let assetId = image.fields.assetId;
        let rendition = image.fields.imageRendition;
        let aspectRatio = rendition?.fields.aspectRatio;
        let transformations = rendition?.fields.transformations;
        let format = image.fields.forcedFormat || 'auto';
        let url = ImageFactory.buildImageUrlByWidth(assetId, aspectRatio, transformations, format);
        return url;
    }

    renderBazaarVoiceDynamicCatalogCollectionScript() {
        let product = this.getProduct();

        let imageUrl = this.getImageUrl(product.productOverview.fields.mainAsset);

        let productVariants = product.productOverview.fields.productVariants;
        let skus = [];
        let eans = [];
        let reviewAndRatingId;
        if (productVariants) {
            for (let i = 0; i < productVariants.length; i++) {
                let productVariant = productVariants[i];
                let sku = productVariant.fields.sku;
                skus.push(sku);
                eans.push("0" + sku);

                if (productVariant.fields.isBazaarVoiceIdentifier) {
                    reviewAndRatingId = productVariant.fields.sku;
                }
            }
        }

        // product
        let dccProduct = {
            "productId": reviewAndRatingId,
            "productName": product.name,
            "productDescription": product.productOverview.fields.description,
            "productImageURL": imageUrl,
            "productPageURL": product.productOverview.fields.slug.fields.slug,
            "brandName": brandName,
            "upcs" : skus,
            "eans": eans
        }

        if(product.productOverview.fields.reviewAndRatingCategory) {
            dccProduct.categoryPath = [
                {
                    "Name" : product.productOverview.fields.reviewAndRatingCategory
                }
            ]
        }

        // Catalog
        let catalogProducts = [];
        catalogProducts.push(dccProduct);


        // Family and Bazaar voice master product values
        let reviewAndRatingFamilyId = product.productOverview.fields.reviewAndRatingFamilyId;

        if (reviewAndRatingFamilyId) {
            let familyJson = [{
                "id": reviewAndRatingFamilyId,
                "expand": false
            }];
            dccProduct.families = familyJson;
            // Master product
            let masterProductId = getBazaarVoiceMasterProductId(reviewAndRatingFamilyId);
            let masterProductName = getBazaarVoiceMasterProductName(reviewAndRatingFamilyId);

            let masterProductJson = {
                "productId": masterProductId,
                "productName": masterProductName,
                "families": [{
                    "id": reviewAndRatingFamilyId,
                    "expand": true
                }]
            }
            catalogProducts.push(masterProductJson)
        }

        let catalogProductsString = JSON.stringify(catalogProducts);

        let render;
        // do not render if we no productID
        if(reviewAndRatingId) {
            render = <Fragment>
                <script dangerouslySetInnerHTML={{
                    __html: `window.bvDCC = {
                    catalogData: {
                        catalogProducts: ${catalogProductsString}
                    };
                    window.bvCallback = function (BV) {
                        BV.pixel.trackEvent("CatalogUpdate", {
                            type: 'Product',
                            locale: window.bvDCC.catalogData.locale,
                            catalogProducts: window.bvDCC.catalogData.catalogProducts
                        });
                    };`,
                }}>
                </script>
            </Fragment> ;
        }

        return render;
    }

    renderBVScript() {
        const bazaarVoiceURL = publicRuntimeConfig.BAZAAR_VOICE_URL;
        return (
            <Fragment>
                <script dangerouslySetInnerHTML={{
                    __html: `(function(w, d, s) {
                                var f = d.getElementsByTagName(s)[0],
                                  j = d.createElement(s);
                                j.async = true;
                                j.type = "text/javascript"
                                j.src = ${bazaarVoiceURL};
                                f.parentNode.insertBefore(j, f);
                              })(window, document, 'script');`,
                    }} key={`bazaarvoice`}>
                </script>
            </Fragment>
        )
    }

}

export function getBazaarVoiceMasterProductId(reviewAndRatingFamilyId) {
    let masterProductId = "master_" + reviewAndRatingFamilyId;
    return masterProductId;
}

export function getBazaarVoiceMasterProductName(reviewAndRatingFamilyId) {
    let masterProductName = "master_product_" + reviewAndRatingFamilyId;
    return masterProductName;
}



Product.propTypes = {
    product: PropTypes.object,
    slug: PropTypes.any,
    url: PropTypes.shape({
        asPath: PropTypes.string,
    })
}

export default Product
