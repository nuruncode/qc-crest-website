import "@babel/polyfill";
import React, {Fragment} from 'react'
import PropTypes from 'prop-types'

import collectionDao from 'Model/dao/CollectionDao';
import Router from 'next/router';
import Head from 'next/head';
import Block from '../helpers/Block';
import Meta from '../components/Meta/Meta';
import getConfig from "next/config";

const {publicRuntimeConfig} = getConfig();

class Collection extends React.Component {

    componentDidMount() {
        Router.beforePopState(({as}) => {
            location.href = as;
        });
    }

    static async getInitialProps({ asPath }) {
        const baseProps = await this.initPage(asPath);
        return {
            ...baseProps
        }
    }

    static async initPage(slug) {
        let collection = undefined;

        if (slug) {
            collection = await collectionDao.getActiveCollectionBySlug(slug);
        }

        return {'collection' : collection, 'slug' : slug};
    }

    constructor(props) {
        super(props);
    }

    /**
     * Rendering of the page. This always render the page using a common layout.
     *
     * Override the render method to do otherwise.
     *
     * @returns {*}
     */
    render() {
        let collection = this.getCollection();
        let pagePath = collection.slug.fields.slug;

        return (
            <Fragment>
                <Meta metadata={this.getMetadata()} pagePath={pagePath}/>
                <Head>
                    {this.renderBVScript()}
                </Head>
                <div className="ob-wrapper">
                    {this.renderBody()}
                </div>
            </Fragment>
        )
    }

    getCollection() {
        return this.props.collection.fields;
    }

    getMetadata() {
        return this.props.collection.fields.metadata.fields
    }

    /**
     * By default, the page will render the modules of the Product.
     * Can be overridden on the child class to do specific rendering.
     * @returns {*|void}
     */
    renderBody() {
        return new Block(this.props.collection).renderChildBlocks({ extraAttributes : {entity: this.getCollection(), slug: this.props.slug} });
    }

    renderBVScript() {
        const bazaarVoiceURL = publicRuntimeConfig.BAZAAR_VOICE_URL;
        return (
            <Fragment>
                <script dangerouslySetInnerHTML={{
                    __html: `(function(w, d, s) {
                                var f = d.getElementsByTagName(s)[0],
                                  j = d.createElement(s);
                                j.async = true;
                                j.type = "text/javascript"
                                j.src = ${bazaarVoiceURL};
                                f.parentNode.insertBefore(j, f);
                              })(window, document, 'script');`,
                }} key={`bazaarvoice`}>
                </script>
            </Fragment>
        )
    }
}

Collection.propTypes = {
    collection: PropTypes.object,
    slug: PropTypes.any,
}

export default Collection
