const path = require('path')

module.exports = {
    components: 'components/**/*.js',
    ignore: ['components/**/*.stories.js', 'components/Form/*.js', 'components/TopMenu/*.js', 'components/Layout/*.js' ],
    webpackConfig: {
        resolve: {
            alias: {
                Component: path.resolve(__dirname, './components'),
                Config: path.resolve(__dirname, './config')
            }
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader'
                }
            ]
        }
    }
};