const path = require("path");

// Export a function. Accept the base config as the only param.
module.exports = async ({ config, mode }) => {
    // `mode` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.
    config.module.rules.push(
      {
        include: path.resolve(__dirname, "../"),
        resolve: {
          ...config.resolve,
          alias: {
            Component: path.resolve(__dirname, './components'),
            Config: path.resolve(__dirname, './config'),
            Page: path.resolve(__dirname, "./pages"),
            Model: path.resolve(__dirname, "./model"),
            Static: path.resolve(__dirname, "./static")
          },
        }
      },
      {
        test: /\.scss$/,
        loaders: [
            'style-loader',
            'css-loader',
            'sass-loader?includePaths[]=' + encodeURIComponent(path.resolve(__dirname, '../app/'))
        ]
      },
      {
          test: /\.css$/,
          loaders: [
              'style-loader',
              'css-loader'
          ]
      },
      {
          test: /\.svg$/,
          loaders: [
              'svg-url-loader',
              'file-loader?name=icons/[name].[ext]'
          ]
      },
      {
          test: /\.(eot|ttf|woff|woff2)$/,
          loader: 'file-loader?name=fonts/[name].[ext]'
      }
  );

  // Return the altered config
  return config;
};