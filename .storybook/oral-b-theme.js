import { create } from '@storybook/theming';

const blue = '#0057b8';
const white = '#ffffff';
const grey_text = '#4c4c4c';
const grey_bg = '#fafafa';
const black = 'black';

export default create({
    base: 'normal',

    colorPrimary: blue,
    colorSecondary: blue,

    // UI
    appBg: grey_bg,
    appContentBg: white,
    appBorderColor: grey_text,
    appBorderRadius: 5,

    // Typography
    fontBase: '"Text Book", sans-serif',
    fontCode: 'Neutraface\ 2',

    // Text colors
    textColor: grey_text,
    textInverseColor: white,

    // Toolbar default and active colors
    barTextColor: grey_text,
    barSelectedColor: black,
    barBg: white,

    // Form colors
    inputBg: white,
    inputBorder: grey_text,
    inputTextColor: grey_text,
    inputBorderRadius: 5,

    brandTitle: 'Crest storybook',
    brandUrl: 'https://crest-pgone-uk-en-dev.azurewebsites.net/en-gb/',
    brandImage: 'https://res.cloudinary.com/pgone/image/upload/w_200,c_limit,q_auto,f_auto/pgone/crest/en-uk/dev/images/fz7bsoocu2kd8oif1ddq',
});