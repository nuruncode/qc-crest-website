import { configure } from '@storybook/react';
import { addParameters } from '@storybook/react';
import oralbTheme from './oral-b-theme';

// Option defaults.
addParameters({
    options: {
        theme: oralbTheme,
    },
});

configure(require.context('../components', true, /\.stories\.js$/), module);