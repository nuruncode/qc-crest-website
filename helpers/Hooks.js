import { useState, useEffect, useLayoutEffect } from 'react';

// https://github.com/streamich/react-use/blob/master/docs/useIsomorphicLayoutEffect.md
export const useIsomorphicLayoutEffect = typeof window !== 'undefined' ? useLayoutEffect : useEffect;

export function useMatchMedia(mediaQueryString, initialValue) {
    const [match, setMatch] = useState(initialValue);

    useEffect(
        () => {
            function handleMatch(event) {
                setMatch(event.matches);
            }

            const matchMedia = window.matchMedia(mediaQueryString);

            setMatch(matchMedia.matches);
            matchMedia.addListener(handleMatch);

            return () => {
                matchMedia.removeListener(handleMatch);
            };
        },
        [mediaQueryString]
    );

    return match;
};
