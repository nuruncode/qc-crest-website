import React, {Fragment} from 'react'
import Component from "./entity/Component";
import { getAnchorIdFromEntity } from './Anchor';
import componentFactory from '../components/ComponentFactory';

class Block extends Component {

  renderBlock(moduleClass, attributes, moduleBody) {
    const moduleDefinition = componentFactory.getModule(moduleClass);
    if (moduleDefinition) {
      const module = React.createElement(moduleDefinition, attributes, moduleBody);
      return module;
    } else {
      console.info("Unsupported module type : " + moduleClass);
    }
  }

  renderBlockFromDocument(extraAttributes, isMobile, isTablet, isTiny, onClickCallback, customEvent, index) {
    const document = this.getDocument();
    if (document && document.fields && document.fields.contentType) {
      let contentType = document.fields.contentType;
      let blockName = document.fields.name;
      let className = document.fields.classNames;

      return this.renderBlock(contentType, {
        name: blockName,
        document: document,
        className: className,
        isTablet: isTablet,
        isMobile: isMobile,
        isTiny: isTiny,
        onClickCallback,
        customEvent,
        key: blockName,
        index,
        ...extraAttributes
      }, null);

    } else {
      console.error("Unsupported block is currently assign to page:" + this.props.page.fields.name);
    }
  }

  /**
   * Render the modules assigned to the page.
   * @returns {*}
   */
  renderChildBlocks(extraAttributes, isMobile, isTablet, isTiny, onClickCallback, customEvent) {
    const doc = this.getDocument();

    return ((doc.fields && doc.fields.blocks) ?
      <Fragment>
        { doc.fields.blocks.map((child, index) => (
            new Block(child).renderBlockFromDocument(extraAttributes, isMobile, isTablet, isTiny, onClickCallback, customEvent, index)
        ))}
      </Fragment>
    :
      null
    );
  }

  getAnchorId() {
    return getAnchorIdFromEntity(this.getDocument());
  }

  getShortName() {
    let shortName = this.getFieldValue('shortName');

    if (shortName == null) {
      shortName = this.getFieldValue('anchorId');
    }

    if (shortName == null) {
      shortName = this.getFieldValue('name');
    }

    return shortName;
  }
}

export default Block