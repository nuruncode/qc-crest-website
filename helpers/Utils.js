// Returns if a value is a string
export function isString (value) {
    return typeof value === 'string' || value instanceof String;
}

export function calculateHeaderHeight() {
    // Not really clean, but we have to do this this way to change 
    // the layout's padding due to the sticky navigation

    const header = document.querySelector('.zone-header');
    const height = header.clientHeight;
    const layout = document.querySelector('.layout:not(.ob-spotlight-layout):not(.scrolling-header)');
    if (!layout) {
        return;
    }
    layout.style.paddingTop = `${height}px`;
}

// Cross browser compatible version of window.scrollTo(options)
export function smoothVerticalScroll(element, where, extraElemHeight = 0 ,time = 300) {
    // The extraElementHeight is the height of any element that isn't accounted by element.getBoundingClientRect().top
    const header = document.querySelector('.zone-header:not(.visible-false)');
    const headerHeight = header?.offsetHeight ? header.offsetHeight : 0;
    const elementTop = element.getBoundingClientRect().top;
    const elementAmount = (elementTop - headerHeight - extraElemHeight) / 100;
    let currentTime = 0;
    while (currentTime <= time) {
        window.setTimeout(smoothVerticalScrollBehaviour, currentTime, elementAmount, where);
        currentTime += time / 100;
    }
}

function smoothVerticalScrollBehaviour(elementAmount, where) {
    if (where === 'center' || where === '') {
        window.scrollBy(0, elementAmount / 2);
    }
    if (where === 'top') {
        window.scrollBy(0, elementAmount);
    }
}

export function focusOnElement (element) {
    if (!element) {
        return;
    }

    element.setAttribute('tabindex', -1);
    element.focus();
    element.addEventListener('blur', event => {
        event.currentTarget.removeAttribute('tabindex');
    })
}

export function toggleScroll(condition, lastScroll) {
    if (condition) {
        document.body.classList.add('noScroll');
        document.body.style.top = `-${lastScroll}px`;
        document.body.style.height = `-${lastScroll}px`;
    } else {
        document.body.classList.remove('noScroll');
        window.scrollTo(0, lastScroll);
    }
}

export function validURL(str) {
    let pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
}