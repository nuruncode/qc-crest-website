import React from 'react';
import Icon from "../components/Icon/Icon";
import Image from "../components/Image/Image";
import LanguageSelection from "../components/LanguageSelection/LanguageSelection"

export default class HierarchyUtils {

  static getLevel(hierarchyDoc) {
    return hierarchyDoc.fields.level;
  }

  static renderHierarchy(hierarchy) {
    const destinations = hierarchy.fields.destinations;
    let title = hierarchy.fields.title;
    return (
        <div>
          {title && this.renderHierarchyTitle(hierarchy, title)}
          {destinations &&
          <ul id={this.getHierarchyId(hierarchy)}>
            {
              destinations.map((d, index) => (
                  <li key={index}>{this.renderDestination(d)}</li>
              ))
            }
          </ul>
          }
        </div>
    )
  }

  static renderDestination(dest, callback = null, icon = null, onClick) {
    const type = dest.fields.contentType;
    switch (type) {
      case 'pageLink':
        return this.renderPageLink(dest, icon, onClick);
      case 'hierarchy':
        dest.fields.level = 'child';
        return callback ? callback() : this.renderHierarchy(dest);
      case 'link':
        return this.renderLink(dest, icon, onClick);
      case 'languageSelectionBlock':
        return <LanguageSelection {...dest}/>
      default:
        return 'Unsupported Type : ' + type;
    }
  }

  static renderPageLink(pageLink, icon = null, onClick) {
    const title = pageLink.fields.title;
    const link = pageLink.fields.pagePath.fields.slug;
    const image = pageLink.fields.image;
    return this.renderLinkAndTitle(link, title, image, icon, onClick);
  }

  static renderLink(link, icon = null, onClick) {
    const title = link.fields.title;
    const url = link.fields.url;
    const image = link.fields.image;
    return this.renderLinkAndTitle(url, title, image, icon, onClick);
  }

  static renderLinkAndTitle(link, title, image, icon = null, onClick) {
    return (
        <a href={link} onClick={onClick}>
          {image && this.renderImage(image)}
          <span>
            { icon && icon.position === 'before' && <Icon {...icon} /> }
            <span  dangerouslySetInnerHTML={{__html: title}}></span>
            { icon && (icon.position === 'after' || icon.position === '') && <Icon {...icon} /> }
          </span>
        </a>
    )
  }

  static renderImage(image) {
    return (
        <Image image={image} />
    )
  }

  static getHierarchyId(hierarchyDoc) {
    let id = hierarchyDoc.fields.name;
    id = id.replace(/ /g, '-');
    return id;
  }

  static renderHierarchyTitle(hierarchy, title) {
    return (
        <div dangerouslySetInnerHTML={{__html: title}}></div>
    );
  }
}