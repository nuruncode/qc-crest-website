import {useMemo} from 'react';

export default function usePercentage(currenValue, maxValue) {
    return useMemo(() => `${Math.ceil(100*currenValue/maxValue)}%`, [currenValue]);
}
