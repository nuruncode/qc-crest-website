import {chunk, flatten, minimum} from './functions';

describe('chunk testing', () => {
    it('should not break given invalid type', () => {
        chunk({});
        chunk({}, {});
        chunk(undefined, undefined);
        chunk(1, 1);
        chunk({}, "bla");
    });

    it('should return array if type is not valid', () => {
        expect(chunk({})).toEqual([]);
        expect(chunk(1)).toEqual([]);
        expect(chunk({}, {})).toEqual([]);
    });

    it('should return groups if more than 1 element', () => {
        expect(chunk([1], 1)).toEqual([[1]]);
        expect(chunk([1, 2], 1)).toEqual([[1], [2]]);
        expect(chunk([1, 2, 3], 2)).toEqual([[1, 2], [3]]);
        expect(chunk([1, 2, 3, 4], 2)).toEqual([[1, 2], [3, 4]]);
        expect(chunk([1, 2, 3, 4, 5], 3)).toEqual([[1, 2, 3], [4, 5]]);
    });
});

describe('flatten testing', () => {
    it('should not break given invalid type', () => {
        flatten({});
        flatten([]);
        flatten(1);
        flatten("str");
        flatten(undefined);
    });

    it('should flatten 1 level deep of an array', () => {
        expect(flatten([])).toEqual([]);
        expect(flatten([1])).toEqual([1]);
        expect(flatten([1, 2])).toEqual([1, 2]);
        expect(flatten([1, [2]])).toEqual([1, 2]);
        expect(flatten([1, [2, 3]])).toEqual([1, 2, 3]);
        expect(flatten([1, [2, 3], 4])).toEqual([1, 2, 3, 4]);

        expect(flatten([1, [2, 3], [4]])).toEqual([1, 2, 3, 4]);
        expect(flatten([1, [2, 3], [4, [5]]])).toEqual([1, 2, 3, 4, [5]]);
    });
});

describe('minimum testing', () => {
    it('should not break given invalid type', () => {
        minimum({});
        minimum([]);
        minimum(1);
        minimum("str");
        minimum(undefined);
    });

    it('should return undefined if no minimum', () => {
        expect(minimum([])).toEqual(undefined);
        expect(minimum(["str", {}, []])).toEqual(undefined);
    });

    it('should return minimum of all numbers', () => {
        expect(minimum([1])).toEqual(1);
        expect(minimum([1, 2])).toEqual(1);
        expect(minimum([1, 2, 0.5])).toEqual(0.5);
        expect(minimum([1, 2, 0.5, -1])).toEqual(-1);
    });
});