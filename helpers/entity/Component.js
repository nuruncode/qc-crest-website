import Document from "./Document";

class Component extends Document {

    constructor(props) {
        super(props);
        this.props = props;
    }
}

export default Component