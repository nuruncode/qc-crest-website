export const DeviceType = Object.freeze({
    All:"all",
    DESKTOP:"desktop",
    MOBILE:"mobile",
    TINY:"tiny",
});