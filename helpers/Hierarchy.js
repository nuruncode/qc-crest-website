import React, {Fragment, Component} from 'react'

class Hierarchy extends Component {

  constructor(hierarchy) {
    super(hierarchy);
    this.hierarchy = hierarchy;
    this.hierarchy.fields.level = 'parent';
  }

  getLevel(hierarchyDoc) {
    return hierarchyDoc.fields.level;
  }

  renderHierarchy(hierarchy) {
    const destinations = hierarchy.fields.destinations;
    let title = hierarchy.fields.title;
    return (
        <Fragment>
          {title && this.renderHierarchyTitle(hierarchy, title)}
          {destinations &&
          <ul id={this.getHierarchyId(hierarchy)}>
            {
              destinations.map((d, index) => (
                  <li key={index}>{this.renderDestination(d)}</li>
              ))
            }
          </ul>
          }
        </Fragment>
    )
  }

  renderDestination(dest) {
    const type = dest.fields.contentType;
    switch (type) {
      case 'pageLink':
        return this.renderPageLink(dest);
      case 'hierarchy':
        dest.fields.level = 'child';
        return this.renderHierarchy(dest);
      case 'link':
        return this.renderLink(dest);
      default:
        return 'Unsupported Type : ' + type;
    }
  }

  render() {
    return (
        <Fragment>
          {this.renderHierarchy(this.hierarchy)}
        </Fragment>
    )
  }

  renderPageLink(pageLink) {
    const title = pageLink.fields.title;
    const link = pageLink.fields.pagePath.fields.slug;
    return this.renderLinkAndTitle(link, title);
  }

  renderLink(link) {
    const title = link.fields.title;
    const url = link.fields.url;
    return this.renderLinkAndTitle(url, title);
  }

  renderLinkAndTitle(link, title) {
    return (
      <a href={link}>{title}</a>
    );
  }

  getHierarchyId(hierarchyDoc) {
    let id = hierarchyDoc.fields.name;
    id = id.replace(/ /g, '-');
    return id;
  }

  renderHierarchyTitle(hierarchy, title) {
    return (
      <div>{title}</div>
    );
  }
}

export default Hierarchy