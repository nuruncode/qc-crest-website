#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

function usage
{
  echo ""
  echo "   usage : ./runServer.sh [-e environment] [-m market]"
  echo ""
  echo "          -e, --environment"
  echo "                 environment used to load .env file (default: develop)"
  echo "          -m, --market"
  echo "                 market used to load .env file (default: en-uk)"
  echo "          -h, --help"
  echo "                 This help"
  echo ""
  echo ""
}

ENVIRONMENT=develop
MARKET=en-uk

while [ "$1" != "" ]
do
  case $1 in
     -e | --environment )
        shift
        ENVIRONMENT=$1
        ;;
     -m | --market )
        shift
        MARKET=$1
        ;;
     -h | --help )
        usage
        exit
        ;;
     * )
        usage
        exit
        ;;
  esac
  shift
done

export environment=$ENVIRONMENT
export MARKET=$MARKET

./yarn run export
