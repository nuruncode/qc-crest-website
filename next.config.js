const webpack = require('webpack');
const path = require('path');
const getRoutes = require('./routes');
const withSass = require('@zeit/next-sass');
const withCSS = require('@zeit/next-css');
const withFonts = require('next-fonts');
const withImages = require('next-images')
const withSourceMaps = require('@zeit/next-source-maps');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const isExport = process.env.NODE_ENV === 'production';
let environment = process.env.environment || 'develop';
const market = process.env.MARKET || 'en-uk';

const dotEnvPath = path.resolve(
  'config/environment/' + market + '/.env.' + environment
);

function GenerateProductsJSON () {
  this.plugin = 'GenerateProductsJSON';
}

GenerateProductsJSON.prototype.apply = function apply (compiler) { 
  const emit = (compilation, callback) => {

    const contentful = require('contentful');

    const client = contentful.createClient({
      space: process.env.CF_SPACE_ID,
      accessToken: process.env.CF_DELIVERY_ACCESS_TOKEN,
      environment: process.env.CF_ENVIRONMENT
    });

    client.getEntries({
      'content_type': 'product',
      'include': 10
    }).then((data) => {
        const products = data && data.items;

        if(Array.isArray(products)) {
          for(let i = 0; i < products.length; i++){
            const product = products[i].fields.productOverview;

            if (!product.fields.slug.fields.disabled) {
              const productPagePath = Buffer.from(product.fields.slug.fields.slug).toString('base64');

              const json = JSON.stringify(product);
  
              compilation.assets["static/products/" + productPagePath + ".json"] = {
                source: () => json,
                size: () => json.length
              }
            }
          }
        }

        callback();
    });
  }

  if (compiler.hooks) {
    compiler.hooks.emit.tapAsync(this.plugin, emit)
  } else {
    compiler.plugin('emit', emit)
  }

}


console.log('Env Path', dotEnvPath);

const dotEnvFile = {
  path: dotEnvPath
};

console.log('Loading .env', dotEnvFile);
require('dotenv').config(dotEnvFile);
console.log('Environment', environment);
console.log('Market', market);
console.log('Is Export?', isExport);

const nextConfig = {
  exportPathMap: getRoutes,
  publicRuntimeConfig: {
    MARKET: process.env.MARKET,
    DL_SITE_ENV: JSON.stringify(process.env.DL_SITE_ENV),
    BAZAAR_VOICE_URL: JSON.stringify(process.env.BAZAAR_VOICE_URL),
    DATA_EV_TAG_PID: JSON.stringify(process.env.DATA_EV_TAG_PID),
    DATA_EV_TAG_OCID: JSON.stringify(process.env.DATA_EV_TAG_OCID),
    SWIFTYPE_READ_API_KEY: process.env.SWIFTYPE_READ_API_KEY,
    SWIFTYPE_ENGINE: process.env.SWIFTYPE_ENGINE,
    BASE_URL: process.env.BASE_URL,
    GCS_URl: process.env.GCS_URl,
    GCS_ACCESS_TOKEN: process.env.GCS_ACCESS_TOKEN,
    GCS_IO_CAMPAIGN_ID: process.env.GCS_IO_CAMPAIGN_ID,
  },
  webpack: (config) => {
    config.resolve = {
      ...config.resolve,
      alias: {
        Component: path.resolve(__dirname, './components'),
        Config: path.resolve(__dirname, './config'),
        Page: path.resolve(__dirname, "./pages"),
        Model: path.resolve(__dirname, "./model"),
        Static: path.resolve(__dirname, "./static")
      },
    };
    config.plugins = config.plugins || [];

    config.plugins = [
      ...config.plugins,
      new GenerateProductsJSON({}),
      new webpack.DefinePlugin({
        'process.env.CF_DELIVERY_ACCESS_TOKEN': JSON.stringify(
          process.env.CF_DELIVERY_ACCESS_TOKEN
        ),
        'process.env.CF_PERSONAL_ACCESS_TOKEN': JSON.stringify(
          process.env.CF_PERSONAL_ACCESS_TOKEN
        ),
        'process.env.CF_SPACE_ID': JSON.stringify(
          process.env.CF_SPACE_ID
        ),
        'process.env.CF_ENVIRONMENT': JSON.stringify(
          process.env.CF_ENVIRONMENT
        ),
        'process.env.GMT_ID': JSON.stringify(
          process.env.GMT_ID
        ),
        'process.env.NEXT_EXPORT_TRAILING_SLASHES': JSON.stringify(
          process.env.NEXT_EXPORT_TRAILING_SLASHES
        ),
        'process.env.SWIFTYPE_READ_API_KEY': process.env.SWIFTYPE_READ_API_KEY,
      }),
    ];

    if (isExport) {
      console.log('Adding TerserPlugin to plugins');
      config.plugins.push(
        new TerserPlugin({
          cache: true,
          parallel: true,
          sourceMap: true
        })
      );
      console.log('Adding OptimizeCssAssetsPlugin to plugins');
      config.plugins.push(
        new OptimizeCssAssetsPlugin({
          cssProcessorOptions: {discardComments: {removeAll: true}},
          canPrint: true
        })
      );
      console.log('Adding BundleAnalyzerPlugin to plugins');
      config.plugins.push(
        new BundleAnalyzerPlugin({
          analyzerMode: 'static',
          openAnalyzer: false,
          generateStatsFile: true,
          statsOptions: { source: false },
        })
      );
    }

    return config
  }
};

module.exports = withImages(withSourceMaps(withCSS(withFonts(withSass(nextConfig)))));
