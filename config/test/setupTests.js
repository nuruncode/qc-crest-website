import { shallow, render, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { configure } from 'enzyme/build';
import Adapter from 'enzyme-adapter-react-16/build';

// React 16 Enzyme adapter
configure({ adapter: new Adapter() });

// Make Enzyme functions available in all test files without importing
global.shallow = shallow;
global.render = render;
global.mount = mount;
global.toJson = toJson;