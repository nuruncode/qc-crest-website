const path = require('path');

const market = process.env.MARKET || 'en-uk';

require('dotenv').config({ path: path.resolve(__dirname + '/../config/environment/' + market + '/.env.management') });
const services = require("../model/Services").create();

function runMigration() {
  console.log("Running Migration ... ");
  services.getContentfulMigrationService().importMigration().then(() => {
    console.log("");
    console.log("Done!");
  })
}

runMigration();