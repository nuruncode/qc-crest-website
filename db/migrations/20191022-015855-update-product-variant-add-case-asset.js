module.exports = (migration, context) => {

    const productVariant = migration.editContentType('productVariant');

    productVariant.createField('caseAsset', {
        name: 'Case Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });
};