module.exports = (migration, context) => {

    const inTheBox = migration.editContentType('inTheBox');

    inTheBox.createField('mobileInTheBoxImage', {
        name: 'In The Box Mobile Image',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage' ] }
        ],
        required: false
    });
};