module.exports = (migration, context) => {

    const contentBlockConfiguration = migration.editContentType('contentBlockConfiguration');

    contentBlockConfiguration.createField('additionalAssetList', {
        name: 'Additional Asset List',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                {linkContentType: ['cloudinaryImage', 'cloudinaryVideo']}
            ],
        },
        required: false
    });
};