module.exports = (migration, context) => {

    const spotlightWaitlistBlock = migration.editContentType('spotlightWaitlistBlock');

    spotlightWaitlistBlock.createField('thankYouBackgroundAsset', {
        name: 'Thank You Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage'] }
        ],
        required: false
    });
};