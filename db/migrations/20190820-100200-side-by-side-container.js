module.exports = (migration, context) => {

  const sideBySide = migration.createContentType('sideBySideContainerBlock', {
    name: 'Block - Side By Side Container Block',
    description: 'Side By Side Container Block',
    displayField: 'name'
  });

  sideBySide.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  sideBySide.createField('classNames', {
    name: 'Class Names',
    type: 'Symbol',
    required: false
  });

  sideBySide.createField('anchorId', {
    name: 'Anchor Id',
    type: 'Symbol',
    required: false
  });

  sideBySide.createField('blocks', {
    name: 'Related blocks',
    type: 'Array',
    items: {
      type: 'Link',
      linkType: "Entry"
    },
    required: false
  });

};
