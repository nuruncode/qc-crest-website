module.exports = (migration, context) => {
    const article = migration.editContentType('article');

    article.deleteField('mainContent')

    article.createField('contents', {
        name: 'Contents',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'richTextBlock' ] }
            ],
        },
        required: false
    });
};