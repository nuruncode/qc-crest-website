module.exports = (migration, context) => {

  const pn = migration.createContentType('pageNavigationBlock', {
    name: 'Block - Page Navigation Block',
    description: 'Page Navigation Block',
    displayField: 'name'
  });

  pn.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  pn.createField('classNames', {
    name: 'Class Names',
    type: 'Symbol',
    required: false
  });

  pn.createField('anchorId', {
    name: 'Anchor Id',
    type: 'Symbol',
    required: false
  });

  pn.createField('blocks', {
    name: 'Blocks',
    type: 'Array',
    items: {
      type: 'Link',
      linkType: "Entry"
    },
    required: false
  });

  const contentBlock = migration.editContentType('contentBlock');



};
