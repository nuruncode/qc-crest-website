module.exports = (migration, context) => {

    const productRecommenderBlock = migration.editContentType('productRecommenderBlock');

    productRecommenderBlock.createField('backgroundAsset', {
        name: 'Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage' ] }
        ],
        required: false
    });

    productRecommenderBlock.createField('mobileBackgroundAsset', {
        name: 'Mobile Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage' ] }
        ],
        required: false
    });
};