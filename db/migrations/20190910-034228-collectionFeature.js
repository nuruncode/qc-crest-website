module.exports = (migration, context) => {

    const collectionFeature = migration.createContentType('collectionFeature', {
        name: 'Collection Feature',
        description: 'Collection Feature',
        displayField: 'name'
    });

    collectionFeature.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    collectionFeature.createField('title', {
        name: 'Title',
        type: 'Symbol',
        required: true
    });

    collectionFeature.createField('mainAsset', {
        name: 'Main Asset',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['cloudinaryImage'] }
        ],
        required: false
    });
};
