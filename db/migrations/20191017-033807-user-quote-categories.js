module.exports = (migration, context) => {
    const userQuoteCategory = migration.createContentType('userQuoteCategory', {
        name: 'User Quote Category',
        description: 'User Quote Category',
        displayField: 'name'
    });

    userQuoteCategory.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    userQuoteCategory.createField('userQuotes', {
        name: 'User Quotes',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                {linkContentType: ['userQuote']}
            ],
        },
        required: false
    });

};