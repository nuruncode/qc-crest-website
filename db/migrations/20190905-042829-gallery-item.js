module.exports = (migration, context) => {
    const galleryItem = migration.createContentType('galleryItem', {
        name: 'Gallery Item',
        description: 'Gallery Item',
        displayField: 'name'
    });

    galleryItem.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    galleryItem.createField('mainAsset', {
        name: 'Gallery Item Media',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage', 'cloudinaryVideo' ] }
        ],
        required: true
    });

    galleryItem.createField('width', {
        name: 'Width',
        type: 'Symbol',
        required: false
    });
};
