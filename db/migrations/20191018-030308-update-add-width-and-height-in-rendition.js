module.exports = (migration, context) => {

    const imageRendition = migration.editContentType('imageRendition');

    imageRendition.createField('width', {
        name: 'Width',
        type: 'Number',
        required: false
    });

    imageRendition.createField('height', {
        name: 'Height',
        type: 'Number',
        required: false
    });
};