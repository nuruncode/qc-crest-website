module.exports = (migration, context) => {
    const articleOverview = migration.editContentType('articleOverview');

    articleOverview.createField('description', {
        name: 'Description',
        type: 'Text',
        required: false
    });
};