module.exports = (migration, context) => {
    const spotlightContentVideo = migration.createContentType('spotlightContentVideo', {
        name: 'Block - Spotlight - Content Video',
        description: 'Content Video block used in the spotlight page',
        displayField: 'name'
    });

    spotlightContentVideo.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    spotlightContentVideo.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    spotlightContentVideo.createField('anchorId', {
        name: 'Anchor Id',
        type: 'Symbol',
        required: false
    });

    spotlightContentVideo.createField('title', {
        name: 'Title',
        type: 'Text',
        required: false
    });

    spotlightContentVideo.createField('titleLevel', {
        name: 'Title Level',
        type: 'Number',
        required: false
    });

    spotlightContentVideo.createField('description', {
        name: 'Description',
        type: 'Text',
        required: false
    });

    spotlightContentVideo.createField('video', {
        name: 'Video',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryVideo']}
        ],
        required: true
    });

    spotlightContentVideo.createField('introText', {
        name: 'Intro Text',
        type: 'Text',
        required: false
    });

    spotlightContentVideo.createField('introLogo', {
        name: 'Intro Logo',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage' ] }
        ],
        required: false
    });

    spotlightContentVideo.createField('nextFrameOnVideoEnd', {
        name: 'Next Frame On Video End',
        type: 'Boolean',
        required: false
    });

    spotlightContentVideo.createField('playButtonColor', {
        name: 'Play Button Color',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "Dark",
                    "White"
                ]
            }
        ],
        required: false
    });
};
