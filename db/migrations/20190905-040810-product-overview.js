module.exports = (migration, context) => {

    const productOverview = migration.createContentType('productOverview', {
        name: 'Product Overview',
        description: 'Product Overview Content ',
        displayField: 'name'
    });

    productOverview.createField('name', {
        name: 'Name',
        type: 'Symbol',
        required: true
    });

    productOverview.createField('slug', {
        name: 'Slug',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['pagePath'] }
        ],
        required: true
    });

    productOverview.createField('pageMapping', {
        name: 'Page Mapping (for Next.js)',
        type: 'Symbol',
        required: true
    });

    productOverview.createField('uniqueId', {
        name: 'Unique ID',
        type: 'Symbol',
        required: true
    });

    productOverview.createField('reviewAndRatingId', {
        name: 'Review and Rating ID',
        type: 'Symbol',
        required: false
    });

    productOverview.createField('mainAsset', {
        name: 'Main Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage', 'cloudinaryVideo' ] }
        ],
        required: true
    });

    productOverview.createField('surtitle', {
        name: 'Surtitle',
        type: 'Text',
        required: true
    });

    productOverview.createField('title', {
        name: 'Title',
        type: 'Text',
        required: false
    });

    productOverview.createField('description', {
        name: 'Description',
        type: 'Text',
        required: true
    });

    productOverview.createField('startingAtPrice', {
        name: 'Starting At Price',
        type: 'Integer',
        required: false
    });

    productOverview.createField('productVariants', {
        name: 'Product Variant',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'productVariant' ] }
            ]
        },
        required: false
    });

    productOverview.createField('collectionFeatures', {
        name: 'Collection Features',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'collectionFeature' ] }
            ]
        },
        required: false
    });

    productOverview.createField('productBenefit', {
        name: 'Product Benefit',
        type: 'Text',
        required: true
    });

    productOverview.createField('comparisonPoints', {
        name: 'Comparison Points',
        type: 'Array',
        items: {
            type: 'Symbol'
        },
        required: false
    });

    productOverview.createField('pressQuote', {
        name: 'Press Quote',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'pressQuote' ] }
        ],
        required: false
    });

    productOverview.createField('needs', {
        name: 'Client Needs',
        type: 'Array',
        items: {
            type: 'Symbol',
            validations: [{
              "in": [
                "Sensitivity protection",
                "Tongue cleaning",
                "Cavity prevention",
                "Teeth whitening",
                "Tartar & plaque removal",
                "Superior gum care"
              ]
            }
          ]
        },
        required: false
    });

    productOverview.createField('buyNowLabel', {
        name: 'Buy Now Label',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'label' ] }
        ],
        required: false
    });
};
