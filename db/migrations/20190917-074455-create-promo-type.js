module.exports = (migration, context) => {

    const promo = migration.createContentType('promo', {
        name: 'Promo',
        description: 'Promotion to buy a product from a retailer',
        displayField: 'name'
    });

    promo.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    promo.createField('title', {
        name: 'Title',
        type: 'Text',
        required: false
    });

    promo.createField('titleLevel', {
        name: 'Title Level',
        type: 'Number',
        required: false
    });

    promo.createField('subtitle', {
        name: 'Subtitle',
        type: 'Text',
        required: false
    });

    promo.createField('description', {
        name: 'Description',
        type: 'Text',
        required: false
    });

    promo.createField('retailerText', {
        name: 'Retailer Text',
        type: 'Symbol',
        required: false
    });

    promo.createField('retailerLogo', {
        name: 'Retailer Logo',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage' ] }
        ],
        required: false
    });

    promo.createField('callToActionLabel', {
        name: 'Call To Action Label',
        type: 'Symbol',
        required: false
    });

    promo.createField('callToActionLink', {
        name: 'Call To Action Link',
        type: 'Symbol',
        required: false
    });

    promo.createField('backgroundAsset', {
        name: 'Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage', 'cloudinaryVideo' ] }
        ],
        required: false
    });

    promo.createField('mobileBackgroundAsset', {
        name: 'Mobile Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage', 'cloudinaryVideo' ] }
        ],
        required: false
    });

};
