module.exports = (migration, context) => {
    const userReviewsBlock = migration.createContentType('userReviewsBlock', {
        name: 'Block - User Reviews Block',
        description: 'Block displaying user reviews',
        displayField: 'name'
    });

    userReviewsBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    userReviewsBlock.createField('title', {
        name: 'Title Level',
        type: 'Text',
        required: false
    });
};