module.exports = (migration, context) => {
    const userQuoteBlock = migration.createContentType('userQuoteBlock', {
        name: 'User Quote Block',
        description: 'User Quote Block',
        displayField: 'name'
    });

    userQuoteBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        required: true
    });

    userQuoteBlock.createField('title', {
        name: 'Title',
        type: 'Text',
        required: false
    });

    userQuoteBlock.createField('classNames', {
        name: 'classNames',
        type: 'Text',
        required: false
    });

    userQuoteBlock.createField('anchorId', {
        name: 'anchorId',
        type: 'Text',
        required: false
    });

    userQuoteBlock.createField('writeAReviewLabel', {
        name: 'Write A Review Label',
        type: 'Text',
        required: false
    });
}