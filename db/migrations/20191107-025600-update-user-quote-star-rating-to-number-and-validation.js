module.exports = (migration, context) => {
    const userQuote = migration.editContentType('userQuote');

    userQuote.deleteField('starRating');

    userQuote.createField('starRating', {
        name: 'Star Rating',
        type: 'Number',
        required: true,
        validations: [
            {
                "in": [
                    3,
                    3.5,
                    4,
                    4.5,
                    5
                ]
            }
        ],
    });
};