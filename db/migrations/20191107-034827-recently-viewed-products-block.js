module.exports = (migration, context) => {

    const recentlyViewedProductsBlock = migration.createContentType('recentlyViewedProductsBlock', {
        name: 'Block - Recently Viewed Products Block',
        description: 'Recently Viewed Products Block',
        displayField: 'name'
    });

    recentlyViewedProductsBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    recentlyViewedProductsBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    recentlyViewedProductsBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    recentlyViewedProductsBlock.createField('title', {
        name: 'Title',
        type: 'Text',
        required: false
    });

    recentlyViewedProductsBlock.createField('titleLevel', {
        name: 'Title Level',
        type: 'Number',
        required: false
    });

    recentlyViewedProductsBlock.createField('seeMoreLabel', {
        name: 'See More Label',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['label'] }
        ],
        required: false
    });

    recentlyViewedProductsBlock.createField('seeLessLabel', {
        name: 'See More Label',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['label'] }
        ],
        required: false
    });

    recentlyViewedProductsBlock.createField('background', {
        name: 'Background',
        type: 'Symbol',
        required: false
    });

};