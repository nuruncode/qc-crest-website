module.exports = (migration, context) => {

    const articleCategoryHeroBlock = migration.createContentType('articleCategoryHeroBlock', {
        name: 'Block - Article Category Hero Block',
        description: 'Article with a list of ctas',
        displayField: 'name'
    });

    articleCategoryHeroBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    articleCategoryHeroBlock.createField('title', {
        name: 'Title',
        type: 'Symbol',
        required: false
    });

    articleCategoryHeroBlock.createField('titleLevel', {
        name: 'Title Level',
        type: 'Number',
        required: false
    });

    articleCategoryHeroBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    articleCategoryHeroBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    articleCategoryHeroBlock.createField('body', {
        name: 'Body',
        type: 'Text',
        required: false
    });

    articleCategoryHeroBlock.createField('ctas', {
        name: 'CTAs',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'link' ] }
            ]
        },
        required: false
    });
}