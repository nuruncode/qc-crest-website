module.exports = (migration, context) => {

    const articleHeroBlock = migration.createContentType('articleHeroBlock', {
        name: 'Block - Article Hero Block',
        description: 'Base article block',
        displayField: 'name'
    });

    articleHeroBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    articleHeroBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    articleHeroBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

}