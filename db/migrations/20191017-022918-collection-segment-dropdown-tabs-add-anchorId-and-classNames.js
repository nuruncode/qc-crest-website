module.exports = (migration, context) => {

    const collectionSegmentTabsDropdownBlock = migration.editContentType('collectionSegmentTabsDropdownBlock');

    collectionSegmentTabsDropdownBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    collectionSegmentTabsDropdownBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });
};
