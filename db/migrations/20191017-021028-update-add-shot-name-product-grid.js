module.exports = (migration, context) => {
    const productGridBlock = migration.editContentType('productGridBlock');

    productGridBlock.createField('shortName', {
        name: 'Short Name',
        type: 'Symbol',
        required: false
    });
};