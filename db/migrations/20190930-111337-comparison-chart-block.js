module.exports = (migration, context) => {
    const comparisonChartBlock = migration.createContentType('comparisonChartBlock', {
        name: 'Block - Comparison Chart Block',
        description: 'Comparison Chart Block',
        displayField: 'name'
    });

    comparisonChartBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    comparisonChartBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    comparisonChartBlock.createField('anchorId', {
        name: 'Anchor Id',
        type: 'Symbol',
        required: false
    });
}
