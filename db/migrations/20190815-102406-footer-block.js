module.exports = (migration, context) => {

  const footer = migration.createContentType('footerBlock', {
    name: 'Block - Footer Block',
    description: 'Footer Block',
    displayField: 'name'
  });

  footer.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  footer.createField('classNames', {
    name: 'Class Names',
    type: 'Symbol',
    required: false
  });

  footer.createField('anchorId', {
    name: 'Anchor Id',
    type: 'Symbol',
    required: false
  });

  footer.createField('hierarchy', {
    name: 'Hierarchy',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      { linkContentType: [ 'hierarchy' ] }
    ],
    required: true
  });

  footer.createField('copyright', {
    name: 'Copyright',
    type: 'Symbol',
    required: false
  });
};
