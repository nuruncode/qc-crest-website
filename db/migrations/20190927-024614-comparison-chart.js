module.exports = (migration, context) => {
    const comparisonChart = migration.createContentType('comparisonChart', {
        name: 'Comparison Chart',
        description: 'Comparison Chart',
        displayField: 'name'
    });

    comparisonChart.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    comparisonChart.createField('title', {
        name: 'Title',
        type: 'Text',
        required: false
    });

}
