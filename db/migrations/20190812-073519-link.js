module.exports = (migration, context) => {

  const link = migration.createContentType('link', {
    name: 'Link',
    description: 'A simple link to a Web destination',
    displayField: 'name'
  });

  link.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  link.createField('title', {
    name: 'Title',
    type: 'Symbol',
    required: false
  });

  link.createField('url', {
    name: 'Destination URL',
    type: 'Symbol',
    required: false
  });

  link.createField('image', {
    name: 'Image',
    type: 'Link',
    linkType: "Entry",
    validations: [
        {linkContentType: ['cloudinaryImage']}
    ],
    required: false
  });

  link.createField('type', {
    name: 'Type',
    type: 'Symbol',
    validations: [
      {
        "in": [
          "social",
          "internal",
          "external"
        ]
      }
    ],
    required: false
  });

  link.createField('onlyShowOn', {
    name: 'Only Show On Device',
    type: 'Symbol',
    validations: [
      {
        "in": [
          "desktop",
          "mobile"
        ]
      }
    ],
    required: false
  });

  link.createField('className', {
    name: 'Class Name',
    type: 'Symbol',
    required: false
  });

};
