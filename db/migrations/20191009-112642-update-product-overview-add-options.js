module.exports = (migration, context) => {

    const productOverview = migration.editContentType('productOverview');

    productOverview.createField('options', {
        name: 'Options',
        type: 'Text',
        required: false
    });
};