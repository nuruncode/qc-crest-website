module.exports = (migration, context) => {

  const announcement = migration.createContentType('announcementBarBlock', {
    name: 'Block - Announcement Bar Block',
    description: 'Announcement Bar Block',
    displayField: 'name'
  });

  announcement.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  announcement.createField('classNames', {
    name: 'Class Names',
    type: 'Symbol',
    required: false
  });

  announcement.createField('anchorId', {
    name: 'Anchor Id',
    type: 'Symbol',
    required: false
  });

  announcement.createField('description', {
    name: 'Description',
    type: 'Text',
    required: true
  });

  announcement.createField('callToActionLabel', {
    name: 'Call To Action Label',
    type: 'Symbol',
    required: false
  });

  announcement.createField('callToActionLink', {
    name: 'Call To Action Link',
    type: 'Symbol',
    required: false
  });

};
