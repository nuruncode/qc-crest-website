module.exports = (migration, context) => {
    const formFieldValidation = migration.editContentType('formFieldValidation');

    formFieldValidation.createField('errorMessage', {
        name: 'Error Message',
        type: 'Symbol',
        required: true
    });
};