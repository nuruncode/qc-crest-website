module.exports = (migration, context) => {

  const region = migration.createContentType('region', {
    name: 'Region',
    description: 'One region of Crest markets',
    displayField: 'name'
  });

  region.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  region.createField('markets', {
    name: 'Markets',
    type: 'Array',
    items: {
      type: 'Link',
      linkType: "Entry",
      validations: [
        { linkContentType: [ 'market' ] }
      ]
    },
    required: true
  });

};
