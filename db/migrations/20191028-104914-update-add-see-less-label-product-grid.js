module.exports = (migration, context) => {
    const productGridBlock = migration.editContentType('productGridBlock');

    productGridBlock.createField('seeLessLabel', {
        name: 'See Less Label',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['label']}
        ],
        required: false
    });
};