module.exports = (migration, context) => {
    const spotlightWaitlistBlock = migration.editContentType('spotlightWaitlistBlock');

    spotlightWaitlistBlock.createField('thankYouTitleLabel', {
        name: 'Thank You Title Label',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['label'] }
        ],
        required: false
    });

    spotlightWaitlistBlock.createField('thankYouDescriptionLabel', {
        name: 'Thank You Description Label',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['label'] }
        ],
        required: false
    });
};