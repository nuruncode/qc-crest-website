module.exports = (migration, context) => {

    const cloudinaryVideo = migration.createContentType('cloudinaryVideo', {
        name: 'Cloudinary Video',
        description: 'Video from the Cloudinary Media Service',
        displayField: 'name'
    });

    cloudinaryVideo.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    cloudinaryVideo.createField('assetId', {
        name: 'Asset Id',
        type: 'Symbol',
        required: true
    });

    cloudinaryVideo.createField('upload', {
        name: 'Upload',
        type: 'Object',
        required: false
    });

    cloudinaryVideo.createField('keyframe', {
        name: 'Keyframe image',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['cloudinaryImage'] }
        ],
        required: false
    });

    cloudinaryVideo.createField('transcript', {
        name: 'Transcript',
        type: 'Symbol',
        required: false
    });

    cloudinaryVideo.createField('autoplay', {
        name: 'Autoplay',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "never",
                    "always",
                    "on-scroll"
                ]
            }
        ],
        required: true
    });

    cloudinaryVideo.createField('muted', {
        name: 'Muted',
        type: 'Boolean',
        required: true
    });

    cloudinaryVideo.createField('loop', {
        name: 'Loop',
        type: 'Boolean',
        required: true
    });

    cloudinaryVideo.createField('controls', {
        name: 'Controls',
        type: 'Boolean',
        required: true
    });

    cloudinaryVideo.createField('videoSettings', {
        name: 'Video Settings',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['videoSettings']}
        ],
        required: false
    });

    cloudinaryVideo.createField('playsInline', {
        name: 'playsinline',
        type: 'Boolean',
        required: false
    });

    cloudinaryVideo.createField('introDurationSecond', {
        name: 'Intro Duration Second',
        type: 'Number',
        required: false
    });
};