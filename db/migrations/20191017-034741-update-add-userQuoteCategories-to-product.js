module.exports = (migration, context) => {

    const product = migration.editContentType('product');

    product.createField('userQuoteCategories', {
        name: 'User Quotes Categories',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                {linkContentType: ['userQuoteCategory']}
            ],
        },
        required: false
    });
};