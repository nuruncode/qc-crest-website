module.exports = (migration, context) => {
    const color = migration.createContentType('color', {
        name: 'Color',
        description: 'Color swatches use for product selection',
        displayField: 'name'
    });

    color.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    color.createField('colorCode', {
        name: 'Color Code',
        type: 'Symbol',
        required: true
    });

    color.createField('colorCodeTop', {
        name: 'Color Code Top',
        type: 'Symbol',
        required: false
    });

    color.createField('colorCodeBottom', {
        name: 'Color Code Bottom',
        type: 'Symbol',
        required: true
    });

    color.createField('gradientStyle', {
        name: 'Gradient Style',
        type: 'Symbol',
        validations: [
          {
            "in": [
              "linear",
              "highlightTop",
              "highlightBottom"
            ]
          }
        ],
        required: false
    });
};
