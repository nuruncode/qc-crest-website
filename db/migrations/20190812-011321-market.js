module.exports = (migration, context) => {

  const market = migration.createContentType('market', {
    name: 'Market',
    description: 'One Oral B market that correspond to one website instance.',
    displayField: 'name'
  });

  market.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  market.createField('nativeLanguageLabel', {
    name: 'Native Language Label',
    type: 'Symbol',
    required: true
  });

  market.createField('link', {
    name: 'Link',
    type: 'Symbol',
    required: true
  });

};
