module.exports = (migration, context) => {
    const spotlightSlider = migration.createContentType('spotlightSlider', {
        name: 'Block - Spotlight Slider',
        description: 'Spotlight Slider',
        displayField: 'name'
    });

    spotlightSlider.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    spotlightSlider.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    spotlightSlider.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    spotlightSlider.createField('blocks', {
        name: 'Related blocks',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry"
        },
        required: false
    });

};
