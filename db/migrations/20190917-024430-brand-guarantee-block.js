module.exports = (migration, context) => {

    const brandGuaranteeBlock = migration.createContentType('brandGuaranteeBlock', {
        name: 'Block - Brand Guarantee Block',
        description: 'Brand Guarantee Block',
        displayField: 'name'
    });

    brandGuaranteeBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    brandGuaranteeBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    brandGuaranteeBlock.createField('anchorId', {
        name: 'Anchor Id',
        type: 'Symbol',
        required: false
    });

    brandGuaranteeBlock.createField('claims', {
        name: 'Product Claims',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'claim' ] }
            ]
        },
        required: false
    });

    brandGuaranteeBlock.createField('callToActionLabel', {
        name: 'Call To Action Label',
        type: 'Symbol',
        required: false
    });

    brandGuaranteeBlock.createField('callToActionLink', {
        name: 'Call To Action Link',
        type: 'Symbol',
        required: false
    });
};
