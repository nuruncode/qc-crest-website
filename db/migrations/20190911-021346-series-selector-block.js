module.exports = (migration, context) => {


  const seriesSelector = migration.createContentType('seriesSelectorBlock', {
    name: 'Block - Series Selector Block',
    description: 'Series Selector Block',
    displayField: 'name'
  });

  seriesSelector.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  seriesSelector.createField('classNames', {
      name: 'Class Names',
      type: 'Symbol',
      required: false
  });

  seriesSelector.createField('anchorId', {
      name: 'Anchor Id',
      type: 'Symbol',
      required: false
  });

  seriesSelector.createField('blockSubtitleLabel', {
    name: 'Block Subtitle Label',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      {
        linkContentType: ['label']
      }
    ],
    required: false
  });

  seriesSelector.createField('handleColorLabel', {
    name: 'Handle colors Label',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      {
        linkContentType: ['label']
      }
    ],
    required: true
  });

  seriesSelector.createField('optionsLabel', {
    name: 'Options Label',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      {
        linkContentType: ['label']
      }
    ],
    required: true
  });

  seriesSelector.createField('compareLabel', {
    name: 'Compare Label',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      {
        linkContentType: ['label']
      }
    ],
    required: true
  });

};
