module.exports = (migration, context) => {
    const spotlightPreOrderBlock = migration.createContentType('spotlightPreOrderBlock', {
        name: 'Block - Spotlight - Pre-order Block',
        description: 'Spotlight Pre-order Block',
        displayField: 'name'
    });

    spotlightPreOrderBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    spotlightPreOrderBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    spotlightPreOrderBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    spotlightPreOrderBlock.createField('title', {
        name: 'Title',
        type: 'Text',
        required: false
    });

    spotlightPreOrderBlock.createField('body', {
        name: 'Body',
        type: 'Text',
        required: false
    });

    spotlightPreOrderBlock.createField('preOrderCallToAction', {
        name: 'Pre-Order Call To Action',
        type: 'Symbol',
        required: true
    });

    spotlightPreOrderBlock.createField('backgroundAsset', {
        name: 'Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage', 'cloudinaryVideo' ] }
        ],
        required: false
    });

    spotlightPreOrderBlock.createField('mobileBackgroundAsset', {
        name: 'Mobile Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage', 'cloudinaryVideo' ] }
        ],
        required: false
    });

    spotlightPreOrderBlock.createField('availableProducts', {
        name: 'Available Product',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: 'Entry',
            validations: [
                { linkContentType: [ 'productOverview' ] }
            ],
        },
        required: false
    });
};