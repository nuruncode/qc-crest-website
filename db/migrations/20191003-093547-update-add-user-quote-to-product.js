module.exports = (migration, context) => {

    const product = migration.editContentType('product');

    product.createField('userQuote', {
        name: 'User Quote',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'userQuote' ] }
        ],
        required: false
    });
}