module.exports = (migration, context) => {

    const collectionTabsBlock = migration.editContentType('collectionSegmentTabsDropdownBlock');

    collectionTabsBlock.createField('tabStyle', {
        name: 'Tab Styles',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "Underlined White",
                    "Blue Button"
                ]
            }
        ],
        required: true
    });
};

