module.exports = (migration, context) => {
    const product = migration.editContentType('product');

    product.createField('productHighlights', {
        name: 'Product Highlights',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'richTextBlock' ] }
            ]
        },
        required: false
    });
};