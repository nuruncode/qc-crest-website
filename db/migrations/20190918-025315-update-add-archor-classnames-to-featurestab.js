module.exports = (migration, context) => {

    const featuresTab = migration.editContentType('featuresTab');

    featuresTab.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    featuresTab.createField('classNames', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    const featuresTabBlock = migration.editContentType('featuresTabBlock');

    featuresTabBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    featuresTabBlock.createField('classNames', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

};