module.exports = (migration, context) => {
    const featuresTabBlock = migration.createContentType('featuresTabBlock', {
        name: 'Features Tab Block',
        description: 'Features Tab Block',
        displayField: 'name'
    });

    featuresTabBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        required: true
    });

    featuresTabBlock.createField('features', {
        name: 'Features',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'feature' ] }
            ]
        },
        required: false
    });
};