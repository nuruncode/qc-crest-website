module.exports = (migration, context) => {
    const series = migration.editContentType('series');

    series.createField('smallAsset', {
        name: 'Small Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage' ] }
        ],
        required: false
    });

    series.createField('endingAtPrice', {
        name: 'Ending At Price',
        type: 'Integer',
        required: true
    });
};