module.exports = (migration, context) => {
    const productOverview = migration.editContentType('productOverview');

    productOverview.createField('reviewAndRatingCategory', {
        name: 'Review and Rating Category',
        type: 'Symbol',
        required: false
    });

    productOverview.createField('reviewAndRatingFamily', {
        name: 'Review and Rating Family',
        type: 'Symbol',
        required: false
    });
};