module.exports = (migration, context) => {


    const formFieldValidation = migration.createContentType('formFieldValidation', {
        name: 'Form Field Validation',
        description: 'Validation applied to a form field',
        displayField: 'name'
    });

    formFieldValidation.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    formFieldValidation.createField('pattern', {
        name: 'Pattern',
        type: 'Symbol',
        required: false
    });

    formFieldValidation.createField('required', {
        name: 'Required',
        type: 'Boolean',
        required: false
    });



    const formField = migration.createContentType('formField', {
        name: 'Form Field',
        description: 'Field included in a form',
        displayField: 'name'
    });

    formField.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    formField.createField('id', {
        name: 'Id',
        type: 'Symbol',
        required: true
    });

    formField.createField('type', {
        name: 'Type',
        type: 'Symbol',
        required: false
    });

    formField.createField('label', {
        name: 'Label',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['label']}
        ],
        required: false
    });

    formField.createField('validation', {
        name: 'Validation',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['formFieldValidation']}
        ],
        required: false
    });

    const spotlightWaitlistBlock = migration.editContentType('spotlightWaitlistBlock');

    spotlightWaitlistBlock.editField('formLabels', {
        name: 'Form Labels',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: 'Entry',
            validations: [
                { linkContentType: [ 'formField' ] }
            ],
        },
        required: false
    });

};