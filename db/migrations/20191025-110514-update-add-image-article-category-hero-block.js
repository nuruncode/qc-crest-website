module.exports = (migration, context) => {

    const articleCategoryHeroBlock = migration.editContentType('articleCategoryHeroBlock');

    articleCategoryHeroBlock.createField('backgroundAsset', {
        name: 'Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });
};