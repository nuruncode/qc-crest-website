module.exports = (migration, context) => {

    const skipNavigationBlock = migration.createContentType('skipNavigationBlock', {
        name: 'Block - Skip Navigation Block',
        description: 'Block useful in the skip navigation for accessibility',
        displayField: 'name'
    });

    skipNavigationBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    skipNavigationBlock.createField('label', {
        name: 'Label',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['label']}
        ],
        required: false
    });

    skipNavigationBlock.createField('targetID', {
        name: 'Target ID',
        type: 'Symbol',
        required: false
    });
}