module.exports = (migration, context) => {
    const faq = migration.createContentType('faq', {
        name: 'FAQ',
        description: 'FAQ',
        displayField: 'name'
    });

    faq.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    faq.createField('question', {
        name: 'Question',
        type: 'Text',
        required: true
    });

    faq.createField('answer', {
        name: 'Answer',
        type: 'Text',
        required: true
    });

    faq.createField('categories', {
        name: 'Categories',
        type: 'Array',
        items: {
            type: 'Symbol'
        },
        required: false
    });

    faq.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });
};
