module.exports = (migration, context) => {
    const inTheBox = migration.createContentType('inTheBox', {
        name: 'In the box',
        description: 'Content in the box',
        displayField: 'name'
    });

    inTheBox.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    inTheBox.createField('title', {
        name: 'Title',
        type: 'Text',
        required: true
    });

    inTheBox.createField('titleLevel', {
        name: 'Title Level',
        type: 'Number',
        required: false
    });

    inTheBox.createField('body', {
        name: 'Body',
        type: 'Text',
        required: true
    });

    inTheBox.createField('items', {
        name: 'Items',
        type: 'Text',
        required: true
    });

    inTheBox.createField('inTheBoxImage', {
        name: 'In The Box Image',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage' ] }
        ],
        required: true
    });

    inTheBox.createField('mobileInTheBoxImage', {
        name: 'In The Box Mobile Image',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage' ] }
        ],
        required: false
    });
};
