module.exports = (migration, context) => {

  const pagePath = migration.createContentType('pagePath', {
    name: 'Page Path',
    description: 'A link to an internal destination',
    displayField: 'slug'
  });

  pagePath.createField('slug', {
    name: 'Slug',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  pagePath.createField('disabled', {
    name: 'Disabled',
    type: 'Boolean',
    required: false
  });

  pagePath.createField('searchable', {
    name: 'Is Searchable (Global search)',
    type: 'Boolean',
    required: false
  });
};
