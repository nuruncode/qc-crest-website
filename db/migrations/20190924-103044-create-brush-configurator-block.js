module.exports = (migration, context) => {

  const brushConfiguratorBlock = migration.createContentType('brushConfiguratorBlock', {
      name: 'Block - Brush Configurator Block',
      description: 'Block that is used to display the right product based on criterion',
      displayField: 'name'
  });

  brushConfiguratorBlock.createField('name', {
      name: 'Name',
      type: 'Symbol',
      validations: [
          {"unique": true}
      ],
      required: true
  });

  brushConfiguratorBlock.createField('classNames', {
      name: 'Class Names',
      type: 'Symbol',
      required: false
  });

  brushConfiguratorBlock.createField('anchorId', {
      name: 'Anchor Id',
      type: 'Symbol',
      required: false
  });

  brushConfiguratorBlock.createField('title', {
      name: 'Block Title',
      type: 'Link',
      linkType: 'Entry',
      validations: [
          {linkContentType: ['label'] }
      ],
      required: true
  });

  brushConfiguratorBlock.createField('crestCareNeedsLabel', {
      name: 'Crest Care Needs Label',
      type: 'Link',
      linkType: 'Entry',
      validations: [
          {linkContentType: ['label'] }
      ],
      required: true
  });

  brushConfiguratorBlock.createField('selectFeaturesLabel', {
      name: 'Select features Label',
      type: 'Link',
      linkType: 'Entry',
      validations: [
          {linkContentType: ['label'] }
      ],
      required: true
  });

  brushConfiguratorBlock.createField('matchLabel', {
      name: 'Match label',
      type: 'Link',
      linkType: 'Entry',
      validations: [
          {linkContentType: ['label'] }
      ],
      required: true
  });

  brushConfiguratorBlock.createField('matchDelimiterLabel', {
      name: 'Match Delimiter label',
      type: 'Link',
      linkType: 'Entry',
      validations: [
          {linkContentType: ['label'] }
      ],
      required: true
  });

  brushConfiguratorBlock.createField('brushSelectionLabel', {
      name: 'Brush Selection label',
      type: 'Link',
      linkType: 'Entry',
      validations: [
          {linkContentType: ['label'] }
      ],
      required: true
  });

  brushConfiguratorBlock.createField('handleColorLabel', {
      name: 'Handle Color label',
      type: 'Link',
      linkType: 'Entry',
      validations: [
          {linkContentType: ['label'] }
      ],
      required: true
  });

  brushConfiguratorBlock.createField('needs', {
    name: "Client Needs List",
    type: "Array",
    items: {
      type: 'Symbol',
      validations: [
        { in: ['Sensitivity protection', 'Tongue cleaning', 'Cavity prevention', 'Teeth whitening', 'Tartar & plaque removal', 'Superior gum care'] }
      ]
    }
  });

  brushConfiguratorBlock.createField('features', {
      name: 'Features List',
      type: 'Array',
      items: {
          type: 'Link',
          linkType: "Entry",
          validations: [
              { linkContentType: [ 'collectionFeature' ] }
          ]
      },
      required: false
  });

    brushConfiguratorBlock.createField('inspiredLabel', {
        name: 'Inspired Label',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['label'] }
        ],
        required: true
    });

};
