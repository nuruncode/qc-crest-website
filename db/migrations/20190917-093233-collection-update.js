module.exports = (migration, context) => {

    const collection = migration.editContentType('collection');

    collection.createField('slug', {
        name: 'Slug',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['pagePath'] }
        ],
        required: true
    });
    
};