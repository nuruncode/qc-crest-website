module.exports = (migration, context) => {

    const userQuote = migration.editContentType('userQuote');

    userQuote.createField('mainAsset', {
        name: 'Main Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage' ] }
        ],
        required: false
    });
}
