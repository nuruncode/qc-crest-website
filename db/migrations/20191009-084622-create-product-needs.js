module.exports = (migration, context) => {

    const clientNeed = migration.createContentType('clientNeed', {
        name: 'Client Need',
        description: 'Need ',
        displayField: 'name'
    });

    clientNeed.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    clientNeed.createField('shortName', {
        name: 'Short Name',
        type: 'Symbol',
        required: false
    });

    const productOverview = migration.editContentType('productOverview');

    productOverview.deleteField('needs');

    productOverview.createField('needs', {
        name: 'Client Needs',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                {linkContentType: ['clientNeed']}
            ],
        },
        required: false
    });
};