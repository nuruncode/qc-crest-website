module.exports = (migration, context) => {

    const product = migration.editContentType('product');

    product.createField('featuresTab', {
        name: 'Features Tab',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['featuresTab'] }
        ],
        required: false
    });
};