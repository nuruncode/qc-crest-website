module.exports = (migration, context) => {

    const collectionTabsBlock = migration.createContentType('collectionTabsBlock', {
        name: 'Collection Tabs Block',
        description: 'Collection Tabs Block',
        displayField: 'name'
    });

    collectionTabsBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    collectionTabsBlock.createField('surtitle', {
        name: 'Surtitle',
        type: 'Text',
        required: false
    });

    collectionTabsBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    collectionTabsBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    collectionTabsBlock.createField('collections', {
        name: 'Device Configurations',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'collection' ] }
            ]
        },
        required: true
    });
};