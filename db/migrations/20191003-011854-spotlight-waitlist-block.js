module.exports = (migration, context) => {
    const spotlightWaitlistBlock = migration.createContentType('spotlightWaitlistBlock', {
        name: 'Block - Spotlight - Waitlist Block',
        description: 'Spotlight Waitlist Block',
        displayField: 'name'
    });

    spotlightWaitlistBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    spotlightWaitlistBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    spotlightWaitlistBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    spotlightWaitlistBlock.createField('title', {
        name: 'Title',
        type: 'Text',
        required: false
    });

    spotlightWaitlistBlock.createField('body', {
        name: 'Body',
        type: 'Text',
        required: false
    });

    spotlightWaitlistBlock.createField('backgroundAsset', {
        name: 'Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage', 'cloudinaryVideo' ] }
        ],
        required: false
    });

    spotlightWaitlistBlock.createField('mobileBackgroundAsset', {
        name: 'Mobile Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage', 'cloudinaryVideo' ] }
        ],
        required: false
    });

    spotlightWaitlistBlock.createField('formLabels', {
        name: 'Form Labels',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: 'Entry',
            validations: [
                { linkContentType: [ 'label' ] }
            ],
        },
        required: false
    });

    spotlightWaitlistBlock.createField('callToActionLabel', {
        name: 'Call To Action Label',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'label' ] }
        ],
        required: false
    });

    spotlightWaitlistBlock.createField('callToActionLink', {
        name: 'Call To Action Link',
        type: 'Symbol',
        required: false
    });

    spotlightWaitlistBlock.createField('legalText', {
        name: 'Legal Text',
        type: 'Text',
        required: false
    });

};