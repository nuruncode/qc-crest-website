module.exports = (migration, context) => {

  const metadata = migration.createContentType('metadata', {
    name: 'Metadata',
    description: 'Collection of pages metadata',
    displayField: 'name'
  });

  metadata.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  metadata.createField('metaTitle', {
    name: 'Meta title',
    type: 'Symbol',
    required: true
  });

  metadata.createField('metaDescription', {
    name: 'Meta description',
    type: 'Text',
    required: true
  });

  metadata.createField('metaKeywords', {
    name: 'Meta keywords',
    type: 'Array',
    items: {
      type: 'Symbol'
    },
    required: false
  })

  metadata.createField('openGraphPageTitle', {
    name: 'Open graph page title',
    type: 'Symbol',
    required: true
  });

  metadata.createField('openGraphDescription', {
    name: 'Open graph page description',
    type: 'Text',
    required: true
  });

  metadata.createField('openGraphImage', {
    name: 'Open graph image',
    type: 'Link',
    linkType: 'Entry',
    validations: [
        {linkContentType: ['cloudinaryImage']}
    ],
    required: false
  });

  metadata.createField('noIndex', {
    name: 'No index',
    type: 'Boolean',
    required: true
  });

  metadata.createField('noFollow', {
    name: 'No follow',
    type: 'Boolean',
    required: true
  });

  metadata.createField('canonicalUrl', {
    name: 'Canonical Url',
    type: 'Symbol',
    required: false
  });
};
