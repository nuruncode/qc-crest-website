module.exports = (migration, context) => {
    const productOverlay = migration.createContentType('productOverlay', {
        name: 'Block - Product Overlay',
        description: 'Product',
        displayField: 'name'
    });

    productOverlay.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    productOverlay.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    productOverlay.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });
};