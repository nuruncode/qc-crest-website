module.exports = (migration, context) => {

    const productGridBlock = migration.editContentType('productGridBlock');

    productGridBlock.deleteField('buyNowButtonText');

    productGridBlock.createField('buyNowButtonText', {
        name: 'Buy Now Button Text',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['label'] }
        ],
        required: false
    });

    const series = migration.editContentType('series');

    series.deleteField('priceRangeLabel');

    series.createField('priceRangeLabel', {
        name: 'Price Range Label',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['label'] }
        ],
        required: false
    });

};