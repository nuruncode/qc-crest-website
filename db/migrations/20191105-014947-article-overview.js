module.exports = (migration, context) => {
    const articleOverview = migration.createContentType('articleOverview', {
        name: 'Article Overview',
        description: 'Article Overview Content Type',
        displayField: 'name'
    });

    articleOverview.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    articleOverview.createField('title', {
        name: 'Title',
        type: 'Symbol',
        required: false
    });

    articleOverview.createField('description', {
        name: 'Description',
        type: 'Symbol',
        required: false
    });

    articleOverview.createField('slug', {
        name: 'Slug',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['pagePath']}
        ],
        required: false
    });

    articleOverview.createField('readTime', {
        name: 'Read Time',
        type: 'Number',
        required: false
    });

};