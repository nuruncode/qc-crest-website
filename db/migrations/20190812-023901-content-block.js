module.exports = (migration, context) => {

  const contentBlock = migration.createContentType('contentBlock', {
    name: 'Block - Content Block',
    description: 'Content Block',
    displayField: 'name'
  });

  contentBlock.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  contentBlock.createField('shortName', {
    name: 'Short Name',
    type: 'Symbol',
    required: false
  });

  contentBlock.createField('classNames', {
    name: 'Class Names',
    type: 'Symbol',
    required: false
  });

  contentBlock.createField('anchorId', {
      name: 'Anchor ID',
      type: 'Symbol',
      required: false
  });

  contentBlock.createField('blueEyebrow', {
    name: 'Blue Eyebrow',
    type: 'Boolean',
    required: false
  });

  contentBlock.createField('surtitle', {
    name: 'Surtitle',
    type: 'Text',
    required: false
  });

  contentBlock.createField('title', {
    name: 'Title',
    type: 'Text',
    required: false
  });

  contentBlock.createField('titleLevel', {
    name: 'Title Level',
    type: 'Number',
    required: false
  });

  contentBlock.createField('titleFontSize', {
    name: 'Title Font Size',
    type: 'Symbol',
    validations: [
        {
            "in": [
                "small",
                "medium",
                "large"
            ]
        }
    ],
  });

  contentBlock.createField('description', {
    name: 'Description',
    type: 'Text',
    required: false
  });

  contentBlock.createField('callToActionLabel', {
    name: 'Call To Action Label',
    type: 'Symbol',
    required: false
  });

  contentBlock.createField('callToActionLink', {
    name: 'Call To Action Link',
    type: 'Symbol',
    required: false
  });

  contentBlock.createField('callToActionIsALink', {
    name: 'Call To Action is a Link',
    type: 'Boolean',
    required: false
  });

  contentBlock.createField('textsImage', {
    name: 'Text As an Image',
    type: 'Link',
    linkType: "Entry",
    validations: [
        { linkContentType: [ 'cloudinaryImage' ] }
    ],
    required: false
  });

  contentBlock.createField('textContainerBackgroundColor', {
    name: 'Text Container Background Color',
    type: 'Text',
    required: false
  });

  contentBlock.createField('backgroundColor', {
    name: 'Background Color',
    type: 'Text',
    required: false
  });

  contentBlock.createField('disclaimer', {
    name: 'Disclaimer',
    type: 'Text',
    required: false
  });

  contentBlock.createField('devicesConfigurations', {
    name: 'Devices Configurations',
    type: 'Array',
    items: {
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'contentBlockConfiguration' ] }
        ]
    },
    required: false
  });

  contentBlock.createField('blocks', {
    name: 'Related blocks',
    type: 'Array',
    items: {
      type: 'Link',
      linkType: "Entry"
    },
    required: false
  });

};
