module.exports = (migration, context) => {

    const collection = migration.editContentType('collection');

    collection.createField('shortTitle', {
        name: 'Short Title',
        type: 'Symbol',
        required: true
    });

};