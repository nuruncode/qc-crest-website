module.exports = (migration, context) => {
    const characterBlock = migration.editContentType('characterBlock');

    characterBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    characterBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });
};