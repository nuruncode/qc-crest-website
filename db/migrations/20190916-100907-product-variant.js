module.exports = (migration, context) => {

    const productVariant = migration.createContentType('productVariant', {
        name: 'Product Variant',
        description: 'Product Variant',
        displayField: 'name'
    });

    productVariant.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    productVariant.createField('sku', {
        name: 'Stock Keeping Unit',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    productVariant.createField('smartlabelUrl', {
        name: 'Smart Label Url',
        type: 'Symbol',
        required: false
    });

    productVariant.createField('color', {
        name: 'Color',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'color' ] }
        ],
        required: false
    });

    productVariant.createField('mainAsset', {
        name: 'Main Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage', 'cloudinaryVideo' ] }
        ],
        required: false
    });

    productVariant.createField('highlightAsset', {
        name: 'Highlights Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage', 'cloudinaryVideo' ] }
        ],
        required: false
    });

    productVariant.createField('seriesAsset', {
        name: 'Series Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage', 'cloudinaryVideo' ] }
        ],
        required: false
    });

};
