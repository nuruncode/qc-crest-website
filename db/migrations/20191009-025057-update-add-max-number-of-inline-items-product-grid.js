module.exports = (migration, context) => {
    const productGridBlock = migration.editContentType('productGridBlock');

    productGridBlock.createField('maxNumberInlineItems', {
        name: 'Maximum Number of Inline Items',
        type: 'Integer',
        required: false
    });
};