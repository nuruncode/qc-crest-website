module.exports = (migration, context) => {
    const faqBlock = migration.createContentType('faqBlock', {
        name: 'Block - FAQ Block',
        description: 'FAQ Block',
        displayField: 'name'
    });

    faqBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    faqBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    faqBlock.createField('anchorId', {
        name: 'Anchor Id',
        type: 'Symbol',
        required: false
    });

    faqBlock.createField('title', {
        name: 'Title',
        type: 'Text',
        required: true
    });

    faqBlock.createField('searchLabel', {
        name: 'Search Label',
        type: 'Text',
        required: false
    });

    faqBlock.createField('popularTagsLabel', {
        name: 'Popular Tags Label',
        type: 'Text',
        required: false
    });
};
