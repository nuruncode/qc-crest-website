module.exports = (migration, context) => {
    const productRecommender = migration.editContentType('productRecommenderBlock');

    productRecommender.createField('switchTextAndProduct', {
        name: 'Switch Text And Product',
        type: 'Boolean',
        required: false
    });
};