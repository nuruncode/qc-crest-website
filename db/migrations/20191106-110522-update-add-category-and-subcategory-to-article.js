module.exports = (migration, context) => {
    const article = migration.editContentType('article');

    article.createField('category', {
        name: 'Category',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['category']}
        ],
        required: false
    });

    article.createField('subcategory', {
        name: 'Subcategory',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['link']}
        ],
        required: false
    });
};