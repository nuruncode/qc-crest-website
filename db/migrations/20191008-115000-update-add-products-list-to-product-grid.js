module.exports = (migration, context) => {

    const productGridBlock = migration.editContentType('productGridBlock');

    productGridBlock.createField('productList', {
        name: 'Product List',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'productOverview' ] }
            ],
        },
        required: false
    });
};