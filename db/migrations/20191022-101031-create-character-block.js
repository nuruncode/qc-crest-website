module.exports = (migration, context) => {

    const characterBlock = migration.createContentType('characterBlock', {
        name: 'Block - Character Block',
        description: 'Little block displaying character logo',
        displayField: 'name'
    });

    characterBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    characterBlock.createField('eyebrow', {
        name: 'Eyebrow',
        type: 'Symbol',
        required: false
    });

    characterBlock.createField('title', {
        name: 'Title Level',
        type: 'Text',
        required: false
    });

    characterBlock.createField('titleLevel', {
        name: 'Title Level',
        type: 'Integer',
        required: false
    });

    characterBlock.createField('description', {
        name: 'Description',
        type: 'Text',
        required: false
    });

    characterBlock.createField('backgroundImage', {
        name: 'Background Image',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });

    characterBlock.createField('mobileBackgroundImage', {
        name: 'Mobile Background Image',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });

    characterBlock.createField('characterLogo', {
        name: 'Character Logo',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });
};