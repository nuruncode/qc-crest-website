module.exports = (migration, context) => {
    const appDownloadBlock = migration.createContentType('appDownloadBlock', {
        name: 'Block - App Download Block',
        description: 'Content block that contains links to download Crest app',
        displayField: 'name'
    });

    appDownloadBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    appDownloadBlock.createField('title', {
        name: 'Title',
        type: 'Symbol',
        required: false
    });

    appDownloadBlock.createField('titleLevel', {
        name: 'Title',
        type: 'Integer',
        required: false
    });

    appDownloadBlock.createField('anchorID', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    appDownloadBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    appDownloadBlock.createField('eyebrow', {
        name: 'Eyebrow',
        type: 'Symbol',
        required: false
    });

    appDownloadBlock.createField('body', {
        name: 'Body',
        type: 'Text',
        required: false
    });

    appDownloadBlock.createField('downloadLinks', {
        name: 'Download Links',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'link' ] }
            ],
        },
        required: false
    });

    appDownloadBlock.createField('backgroundAsset', {
        name: 'Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });

    appDownloadBlock.createField('mobileBackgroundAsset', {
        name: 'Mobile Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });
};