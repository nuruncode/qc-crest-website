module.exports = (migration, context) => {

    const richText = migration.createContentType('richTextBlock', {
      name: 'Block - Rich Text Block',
      description: 'Used to place a single rich text element into a page',
      displayField: 'name'
    });

    richText.createField('name', {
      name: 'Name',
      type: 'Symbol',
      validations: [
        {"unique": true}
      ],
      required: true
    });

    richText.createField('classNames', {
      name: 'Class Names',
      type: 'Symbol',
      required: false
    });

    richText.createField('anchorId', {
      name: 'Anchor Id',
      type: 'Symbol',
      required: false
    });

    richText.createField('text', {
      name: 'Text',
      type: 'RichText',
      required: false
    });
};
