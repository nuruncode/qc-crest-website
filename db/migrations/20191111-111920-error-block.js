module.exports = (migration, context) => {
    const errorBlock = migration.createContentType('errorBlock', {
        name: 'Block - Error Block',
        description: 'Error Page Block',
        displayField: 'name'
    });

    errorBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    errorBlock.createField('eyebrow', {
        name: 'Eyebrow',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['label']}
        ],
        required: false,
    });


    errorBlock.createField('heading', {
        name: 'Heading',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['label']}
        ],
        required: false,
    });

    errorBlock.createField('body', {
        name: 'Body',
        type: 'Text',
        required: false
    });
    

    errorBlock.createField('searchTitle', {
        name: 'Search Title',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['label']}
        ],
        required: false,
    });

    errorBlock.createField('searchPlaceholder', {
        name: 'Search Placeholder',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['label']}
        ],
        required: false,
    });

    errorBlock.createField('backgroundAsset', {
        name: 'Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });

    errorBlock.createField('shortcutsTitle', {
        name: 'Shortcuts title',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['label']}
        ],
        required: false,
    });

    errorBlock.createField('shortcuts', {
        name: 'Shortcuts',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'pageLink' ] }
            ],
        },
        required: false
    });
};