module.exports = (migration, context) => {

    const productHighlightBlock = migration.createContentType('productHighlightBlock', {
        name: 'Block - Product Highlights Block',
        description: 'Highlights of the product',
        displayField: 'name'
    });

    productHighlightBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    productHighlightBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    productHighlightBlock.createField('anchorId', {
        name: 'Anchor Id',
        type: 'Symbol',
        required: false
    });

    productHighlightBlock.createField('highlightsLabel', {
        name: 'Highlights Label',
        type: 'Symbol',
        required: true
    });

    productHighlightBlock.createField('colorLabel', {
        name: 'Color Label',
        type: 'Symbol',
        required: true
    });

    productHighlightBlock.createField('background', {
        name: 'Background',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });

};
