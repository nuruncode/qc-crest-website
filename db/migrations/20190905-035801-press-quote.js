module.exports = (migration, context) => {
    const pressQuote = migration.createContentType('pressQuote', {
        name: 'Press Quote',
        description: 'Product Press Quote',
        displayField: 'name'
    });

    pressQuote.createField('name', {
        name: 'Name',
        type: 'Symbol',
        required: true
    });

    pressQuote.createField('quote', {
        name: 'Quote',
        type: 'Text',
        required: true
    });

    pressQuote.createField('authorName', {
        name: 'Quote Author Name',
        type: 'Symbol'
    });

    pressQuote.createField('pressLogo', {
        name: 'Press Logo',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage' ] }
        ]
    });
};