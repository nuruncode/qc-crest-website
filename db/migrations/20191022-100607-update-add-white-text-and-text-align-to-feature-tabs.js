module.exports = (migration, context) => {
    const featuresTab = migration.editContentType('featuresTab');

    featuresTab.createField('textAlign', {
        name: "Text Align",
        type: "Array",
        items: {
            type: 'Symbol',
            validations: [
                { in: ['Left', 'Center', 'Right'] }
            ]
        },
        required: false
    });

    featuresTab.createField('hasWhiteText', {
        name: "White Text",
        type: "Boolean",
        required: false
    });
};