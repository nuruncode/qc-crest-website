module.exports = (migration, context) => {
    const featuresTab = migration.createContentType('featuresTab', {
        name: 'Features Tab',
        description: 'Features Tab',
        displayField: 'name'
    });

    featuresTab.createField('name', {
        name: 'Name',
        type: 'Symbol',
        required: true
    });

    featuresTab.createField('features', {
        name: 'Features',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'feature' ] }
            ]
        },
        required: false
    });
};