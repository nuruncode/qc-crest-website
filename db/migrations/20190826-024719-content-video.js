module.exports = (migration, context) => {
    const contentVideo = migration.createContentType('contentVideo', {
        name: 'Block - Content Video',
        description: 'Content Video',
        displayField: 'name'
    });

    contentVideo.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    contentVideo.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    contentVideo.createField('anchorId', {
        name: 'Anchor Id',
        type: 'Symbol',
        required: false
    });

    contentVideo.createField('title', {
        name: 'Title',
        type: 'Text',
        required: false
    });

    contentVideo.createField('titleLevel', {
        name: 'Title Level',
        type: 'Number',
        required: false
    });

    contentVideo.createField('description', {
        name: 'Description',
        type: 'Text',
        required: false
    });

    contentVideo.createField('video', {
        name: 'Video',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryVideo']}
        ],
        required: true
    });

    contentVideo.createField('desktopLayout', {
        name: 'Desktop Layout',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "background-image-top-center",
                    "background-image-bottom-center",
                    "background-image-center-left-button-top",
                    "background-image-center-left-button-bottom",
                    "background-image-center-left-button-right",
                    "image-above"
                ]
            }
        ],
        required: false
    });

    contentVideo.createField('mobileLayout', {
        name: 'Desktop Layout',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "background-image-top-center",
                    "background-image-bottom-center",
                    "image-above",
                    "image-below"
                ]
            }
        ],
        required: false
    });

    contentVideo.createField('textContainerMarginTop', {
        name: 'Text Container Margin Top',
        type: 'Symbol',
        required: false
    });

    contentVideo.createField('textContainerMarginBottom', {
        name: 'Text Container Margin Bottom',
        type: 'Symbol',
        required: false
    });

    contentVideo.createField('playIconColor', {
        name: 'Play Icon Color',
        type: 'Symbol',
        required: false
    });

    contentVideo.createField('backgroundColor', {
        name: 'Background Color',
        type: 'Symbol',
        required: false
    });

    contentVideo.createField('devicesConfigurations', {
      name: 'Devices Configurations',
      type: 'Array',
      items: {
          type: 'Link',
          linkType: "Entry",
          validations: [
              { linkContentType: [ 'contentBlockConfiguration' ] }
          ]
      },
      required: false
    });
};
