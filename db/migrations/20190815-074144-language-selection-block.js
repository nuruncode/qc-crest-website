module.exports = (migration, context) => {

  const ls = migration.createContentType('languageSelectionBlock', {
    name: 'Block - Language Selection Block',
    description: 'Link to select the current market. Sends user to different site language/countries.',
    displayField: 'name'
  });

  ls.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  ls.createField('classNames', {
    name: 'Class Names',
    type: 'Symbol',
    required: false
  });

  ls.createField('anchorId', {
    name: 'Anchor Id',
    type: 'Symbol',
    required: false
  });

  ls.createField('currentLanguage', {
    name: 'Current Language',
    type: 'Symbol',
    required: false
  });

  ls.createField('title', {
    name: 'Title',
    type: 'Symbol',
    required: false
  });

  ls.createField('regions', {
    name: 'Regions',
    type: 'Array',
    items: {
      type: 'Link',
      linkType: "Entry",
      validations: [
        { linkContentType: [ 'region' ] }
      ],
    },
    required: false
  });

};
