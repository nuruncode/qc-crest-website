module.exports = (migration, context) => {

    const collectionFeature = migration.editContentType('collectionFeature');

    collectionFeature.createField('description', {
        name: 'Description',
        type: 'Symbol',
        required: false
    });

    collectionFeature.moveField('description').afterField('title');
};