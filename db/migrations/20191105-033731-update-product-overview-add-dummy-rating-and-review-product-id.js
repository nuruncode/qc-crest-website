module.exports = (migration, context) => {
    const productOverview = migration.editContentType('productOverview');

    productOverview.createField('reviewAndRatingFamilyId', {
        name: 'Review and Rating Family Id',
        type: 'Symbol',
        required: false
    });
};