module.exports = (migration, context) => {

  const layout = migration.createContentType('layout', {
    name: 'Layout',
    description: 'A page Layout',
    displayField: 'name'
  });

  layout.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  layout.createField('classNames', {
    name: 'Class Names',
    type: 'Symbol',
    required: false
  });

  layout.createField('blocks', {
    name: 'Related blocks',
    type: 'Array',
    items: {
      type: 'Link',
      linkType: "Entry"
    },
    required: false
  });

  layout.createField('parentLayout', {
    name: 'Parent Layout',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      {
        "linkContentType": [
          "layout"
        ]
      }
    ],
    required: false
  });

};
