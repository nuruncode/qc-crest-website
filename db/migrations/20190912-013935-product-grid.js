module.exports = (migration, context) => {

    const productGridBlock = migration.createContentType('productGridBlock', {
        name: 'Block - Product Grid Block',
        description: 'Product Grid Block',
        displayField: 'name'
    });

    productGridBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    productGridBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    productGridBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    productGridBlock.createField('eyebrow', {
        name: 'Eyebrow',
        type: 'Text',
        required: false
    });

    productGridBlock.createField('title', {
        name: 'Title',
        type: 'Text',
        required: false
    });

    productGridBlock.createField('titleLevel', {
        name: 'Title Level',
        type: 'Number',
        required: false
    });

    productGridBlock.createField('body', {
        name: 'Body',
        type: 'Text',
        required: false
    });

    productGridBlock.createField('buyNowButtonText', {
        name: 'Buy Now Button Text',
        type: 'Symbol',
        required: false
    });

    productGridBlock.createField('seeMoreLabel', {
        name: 'See More Label',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['label'] }
        ],
        required: false
    });

    productGridBlock.createField('radioBackground', {
        name: 'Radio Background',
        type: 'Symbol',
        required: false
    });

    productGridBlock.createField('background', {
        name: 'Background',
        type: 'Symbol',
        required: false
    });

};
