module.exports = (migration, context) => {

    const cloudinaryVideo = migration.editContentType('cloudinaryVideo');

    cloudinaryVideo.createField('preload', {
        name: 'Preload',
        type: 'Boolean',
        required: false
    });
};