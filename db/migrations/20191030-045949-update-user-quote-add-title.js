module.exports = (migration, context) => {
    const userQuote = migration.editContentType('userQuote');

    userQuote.createField('title', {
        name: 'Title',
        type: 'Text',
        required: false
    });

};