module.exports = (migration, context) => {

  const markdownText = migration.createContentType('markdownTextBlock', {
    name: 'Block - Markdown Text',
    description: 'Used to place a single markdown text element into a page',
    displayField: 'name'
  });

  markdownText.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  markdownText.createField('classNames', {
    name: 'Class Names',
    type: 'Symbol',
    required: false
  });

  markdownText.createField('anchorId', {
    name: 'Anchor Id',
    type: 'Symbol',
    required: false
  });

  markdownText.createField('text', {
    name: 'Text',
    type: 'Text',
    required: false
  });

  markdownText.createField('shortName', {
    name: 'Short Name',
    type: 'Symbol',
    required: false
  });
};
