module.exports = (migration, context) => {
    const featuresTab = migration.editContentType('featuresTab');

    featuresTab.createField('textAlign', {
        name: 'Text Align',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "Left",
                    "Center",
                    "Right"
                ]
            }
        ],
    });
};