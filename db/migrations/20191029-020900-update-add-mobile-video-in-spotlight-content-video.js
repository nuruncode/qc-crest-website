module.exports = (migration, context) => {
    const spotlightContentVideo = migration.editContentType('spotlightContentVideo');

    spotlightContentVideo.createField('mobileVideo', {
        name: 'Mobile Video',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryVideo']}
        ],
        required: false
    });
};