module.exports = (migration, context) => {
    const articleHeroBlock = migration.editContentType('articleHeroBlock');

    articleHeroBlock.createField('eyebrow', {
        name: 'Eyebrow',
        type: 'Symbol',
        required: false
    });
};