module.exports = (migration, context) => {

    const brandGuarantee = migration.createContentType('brandGuarantee', {
        name: 'Brand Guarantee',
        description: 'Brand Guarantee',
        displayField: 'name'
    });

    brandGuarantee.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    brandGuarantee.createField('claims', {
        name: 'Product Claims',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'claim' ] }
            ]
        },
        required: false
    });

    brandGuarantee.createField('callToActionLabel', {
        name: 'Call To Action Label',
        type: 'Symbol',
        required: false
    });

    brandGuarantee.createField('callToActionLink', {
        name: 'Call To Action Link',
        type: 'Symbol',
        required: false
    });
};
