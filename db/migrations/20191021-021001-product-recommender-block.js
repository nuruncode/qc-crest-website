module.exports = (migration, context) => {

    const productRecommenderBlock = migration.createContentType('productRecommenderBlock', {
        name: 'Block - Product Recommender Block ',
        description: 'Product Recommender',
        displayField: 'name'
    });

    productRecommenderBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    productRecommenderBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    productRecommenderBlock.createField('eyebrow', {
        name: 'Eyebrow',
        type: 'Symbol',
        required: false
    });

    productRecommenderBlock.createField('headline', {
        name: 'Headline',
        type: 'Text',
        required: false
    });

    productRecommenderBlock.createField('bodyCopy',{
        name: 'Body Copy',
        type: 'Text',
        required: false
    });

    productRecommenderBlock.createField('productList', {
        name: 'Product List',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'productOverview' ] }
            ],
        },
        required: false
    });

    productRecommenderBlock.createField('cta',{
        name: 'Call To Action',
        type: 'Symbol',
        required: false
    });

    productRecommenderBlock.createField('ctaLink',{
        name: 'Call To Action Link',
        type: 'Symbol',
        required: false
    });

    productRecommenderBlock.createField('singleColorBackground',{
        name: 'Single Color Background',
        type: 'Symbol',
        required: false
    });
};