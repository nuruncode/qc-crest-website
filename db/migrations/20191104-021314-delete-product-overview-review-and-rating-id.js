module.exports = (migration, context) => {
    const productOverview = migration.editContentType('productOverview');

    productOverview.deleteField('reviewAndRatingId');
};