module.exports = (migration, context) => {

    const contentBlockConfiguration = migration.editContentType('contentBlockConfiguration');

    contentBlockConfiguration.editField('deviceType', {
        name: 'Device Type',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "all",
                    "desktop",
                    "mobile",
                    "tiny"
                ]
            }
        ],
        required: true
    });
};