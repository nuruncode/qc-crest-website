module.exports = (migration, context) => {

    const showcaseBlock = migration.createContentType('showcaseBlock', {
        name: 'Block - Showcase Block',
        description: 'Simple content block having a title, a description and a product image',
        displayField: 'name'
    });

    showcaseBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    showcaseBlock.createField('title', {
        name: 'Title',
        type: 'Symbol',
        required: false
    });

    showcaseBlock.createField('description', {
        name: 'Description',
        type: 'Text',
        required: false
    });

    showcaseBlock.createField('sideImage', {
        name: 'Side Image',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });
}