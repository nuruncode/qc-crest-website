module.exports = (migration, context) => {
    const brushConfiguratorBlock = migration.editContentType('brushConfiguratorBlock');

    brushConfiguratorBlock.deleteField('needs');

    brushConfiguratorBlock.createField('needs', {
        name: 'Client Needs',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                {linkContentType: ['clientNeed']}
            ],
        },
        required: false
    });
};