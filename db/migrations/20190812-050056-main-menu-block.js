module.exports = (migration, context) => {

  const mainmenu = migration.createContentType('mainMenuBlock', {
    name: 'Block - Main Menu Block',
    description: 'Main Menu Block',
    displayField: 'name'
  });

  mainmenu.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  mainmenu.createField('classNames', {
    name: 'Class Names',
    type: 'Symbol',
    required: false
  });

  mainmenu.createField('anchorId', {
    name: 'Anchor Id',
    type: 'Symbol',
    required: false
  });

  mainmenu.createField('hierarchy', {
    name: 'Hierarchy',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      { linkContentType: [ 'hierarchy' ] }
    ],
    required: true
  });

  mainmenu.createField('searchFieldPlaceholder', {
    name: 'Search Field Placeholder',
    type: 'Symbol',
    required: false
  });

};
