module.exports = (migration, context) => {

    const collectionSegmentTabsDropdownBlock = migration.editContentType('collectionSegmentTabsDropdownBlock');

    collectionSegmentTabsDropdownBlock.deleteField('links');

    collectionSegmentTabsDropdownBlock.createField('links', {
        name: 'Page Links',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'link' ] }
            ]
        },
        required: true
    });
};
