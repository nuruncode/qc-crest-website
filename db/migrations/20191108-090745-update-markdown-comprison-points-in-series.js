module.exports = (migration, context) => {
    const series = migration.editContentType('series');

    series.deleteField('comparisonPoints');

    series.createField('comparisonPoints', {
        name: 'Comparison Points',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'markdownTextBlock' ] }
            ]
        },
        required: false
    });
};