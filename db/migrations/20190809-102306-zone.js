module.exports = (migration, context) => {

  const zone = migration.createContentType('zone', {
    name: 'Zone',
    description: 'A zone on a page.',
    displayField: 'name'
  });

  zone.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  zone.createField('classNames', {
    name: 'Class Names',
    type: 'Symbol',
    required: false
  });

  zone.createField('blocks', {
    name: 'Related blocks',
    type: 'Array',
    items: {
      type: 'Link',
      linkType: "Entry"
    },
    required: false
  });

  zone.createField('role', {
    name: 'Role',
    type: 'Symbol',
    required: false
  });

  zone.createField('code', {
    name: 'Code',
    type: 'Symbol',
    required: false
  });

  zone.createField('backgroundImage', {
      name: 'Background Image',
      type: 'Link',
      linkType: "Entry",
      validations: [
          {linkContentType: ['cloudinaryImage']}
      ],
      required: false
  });
};
