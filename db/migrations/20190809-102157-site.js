module.exports = (migration, context) => {

  const site = migration.createContentType('site', {
    name: 'Site',
    description: 'One instance of the Crest site (one market)',
    displayField: 'name'
  });

  site.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  site.createField('socialAccounts', {
    name: 'Social Accounts',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      { linkContentType: [ 'socialAccounts' ] }
    ],
    required: true
  });

  site.createField('market', {
    name: 'Market',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      { linkContentType: [ 'market' ] }
    ],
    required: true
  });

};
