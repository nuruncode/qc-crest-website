module.exports = (migration, context) => {

  const claim = migration.createContentType('claim', {
    name: 'Claim',
    description: 'Claim',
    displayField: 'name'
  });

  claim.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
        {"unique": true}
    ],
    required: true
  });

  claim.createField('statistic', {
    name: 'Statistic',
    type: 'Text',
    required: true
  });

  claim.createField('title', {
    name: 'Title',
    type: 'Text',
    required: true
  });

};
