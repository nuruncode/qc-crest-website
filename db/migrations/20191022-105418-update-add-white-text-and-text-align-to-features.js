module.exports = (migration, context) => {
    const feature = migration.editContentType('feature');

    feature.createField('textAlign', {
        name: 'Text Align',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "Left",
                    "Center",
                    "Right"
                ]
            }
        ],
        required: false
    });

    feature.createField('hasWhiteText', {
        name: "White Text",
        type: "Boolean",
        required: false
    });
};