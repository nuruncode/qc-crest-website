module.exports = (migration, context) => {
    const errorBlock = migration.editContentType('errorBlock');

    errorBlock.createField('mobileBackgroundAsset', {
        name: 'Mobile background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });
};