module.exports = (migration, context) => {

  const pagelink = migration.createContentType('pageLink', {
    name: 'Page Link',
    description: 'A link to an internal destination',
    displayField: 'name'
  });

  pagelink.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  pagelink.createField('title', {
    name: 'Title',
    type: 'Symbol',
    required: false
  });

  pagelink.createField('pagePath', {
    name: 'Page Path',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      {linkContentType: ['pagePath'] }
    ],
    required: true
  });

  pagelink.createField('image', {
    name: 'Image',
    type: 'Link',
    linkType: "Entry",
    validations: [
        {linkContentType: ['cloudinaryImage']}
    ],
    required: false
  });

  pagelink.createField('onlyShowOn', {
    name: 'Only Show On Device',
    type: 'Symbol',
    validations: [
      {
        "in": [
          "desktop",
          "mobile"
        ]
      }
    ],
    required: false
  });

  pagelink.createField('className', {
    name: 'Class Name',
    type: 'Symbol',
    required: false
  });

};
