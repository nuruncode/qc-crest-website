module.exports = (migration, context) => {

  const videoSettings = migration.createContentType('videoSettings', {
    name: 'Video Settings',
    description: 'Video Settings',
    displayField: 'name'
  });

  videoSettings.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  videoSettings.createField('quality', {
    name: 'Quality (default to auto)',
    type: 'Symbol',
    required: false
  });

  videoSettings.createField('videoFormat', {
    name: 'Video Format (default to auto)',
    type: 'Symbol',
    required: false
  });

  videoSettings.createField('width', {
    name: 'Width',
    type: 'Symbol',
    required: false
  });

  videoSettings.createField('height', {
    name: 'Height',
    type: 'Symbol',
    required: false
  });

  videoSettings.createField('crop', {
    name: 'Crop (default to limit)',
    type: 'Symbol',
    required: false
  });

};