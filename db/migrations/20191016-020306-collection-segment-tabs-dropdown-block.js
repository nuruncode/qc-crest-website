module.exports = (migration, context) => {

    const collectionSegmentTabsDropdownBlock = migration.createContentType('collectionSegmentTabsDropdownBlock', {
        name: 'Collection Segment Tabs Dropdown Block',
        description: 'Collection Segment Tabs Dropdown ',
        displayField: 'name'
    });

    collectionSegmentTabsDropdownBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    collectionSegmentTabsDropdownBlock.createField('title', {
        name: 'Title',
        type: 'Symbol',
        required: false
    });

    collectionSegmentTabsDropdownBlock.createField('surtitle', {
        name: 'Surtitle',
        type: 'Symbol',
        required: false
    });

    collectionSegmentTabsDropdownBlock.createField('links', {
        name: 'Page Links',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'pageLink' ] }
            ]
        },
        required: true
    });

    const pageLink = migration.editContentType('pageLink');

    pageLink.createField('isActivated', {
        name: 'Is Activated',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "Yes",
                    "No"
                ]
            }
        ],
    });
};