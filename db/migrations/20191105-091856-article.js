module.exports = (migration, context) => {
    const article = migration.createContentType('article', {
        name: 'Article',
        description: 'Article Content Type',
        displayField: 'name'
    });

    article.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    article.createField('backgroundAsset', {
        name: 'Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });

    article.createField('mainContent', {
        name: 'Main Content',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['richTextBlock']}
        ],
        required: false
    });

    article.createField('metadata', {
        name: 'Metadata',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['metadata']}
        ],
        required: false
    });

    article.createField('relatedPromotions', {
        name: 'Related Promotions',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'promo' ] }
            ],
        },
        required: false
    });

    article.createField('relatedProducts', {
        name: 'Related Products',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'productOverview' ] }
            ],
        },
        required: false
    });

    article.createField('relatedArticles', {
        name: 'Related Articles',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'articleOverview' ] }
            ],
        },
        required: false
    });
};