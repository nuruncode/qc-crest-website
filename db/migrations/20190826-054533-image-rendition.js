module.exports = (migration, context) => {

  const imageRendition = migration.createContentType('imageRendition', {
    name: 'Image Rendition',
    description: 'Image rendition configuring cloudinary parameters',
    displayField: 'name'
  });

  imageRendition.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  imageRendition.createField('aspectRatio', {
    name: 'Aspect Ratio',
    type: 'Number',
    required: false
  });

  imageRendition.createField('transformations', {
    name: 'Transformations',
    type: 'Symbol',
    required: false
  });

};
