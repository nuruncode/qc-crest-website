module.exports = (migration, context) => {

    const collection = migration.createContentType('collection', {
        name: 'Collection',
        description: 'Collection',
        displayField: 'name'
    });

    collection.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    collection.createField('slug', {
      name: 'Slug',
      type: 'Link',
      linkType: 'Entry',
      validations: [
        {linkContentType: ['pagePath'] }
      ],
      required: true
    });

    collection.createField('pageMapping', {
      name: 'Page Mapping (for Next.js)',
      type: 'Symbol',
      required: true
    });

    collection.createField('metadata', {
      name: 'Metadata',
      type: 'Link',
      linkType: 'Entry',
      validations: [
        {linkContentType: ['metadata'] }
      ],
      required: false
    });

    collection.createField('blocks', {
      name: 'Related blocks',
      type: 'Array',
      items: {
        type: 'Link',
        linkType: 'Entry'
      },
      required: false
    });

    collection.createField('title', {
        name: 'Title',
        type: 'Symbol',
        required: true
    });

    collection.createField('shortTitle', {
        name: 'Short Title',
        type: 'Symbol',
        required: true
    });

    collection.createField('series', {
        name: 'Series',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: 'Entry',
            validations: [
                { linkContentType: [ 'series' ] }
            ]
        },
        required: false
    });

    collection.createField('promo', {
        name: 'Collection Promotion',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['promo'] }
        ],
        required: false
    });

    collection.createField('collectionFeatures', {
        name: 'Collection Features',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: 'Entry',
            validations: [
                { linkContentType: [ 'collectionFeature' ] }
            ]
        },
        required: false
    });

    collection.createField('comparisonChart', {
      name: 'Comparison Chart',
      type: 'Link',
      linkType: 'Entry',
      validations: [
        {linkContentType: ['comparisonChart'] }
      ],
      required: false
    });
};