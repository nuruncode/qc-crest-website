module.exports = (migration, context) => {

  const seriesHighlight = migration.createContentType('seriesHighlight', {
      name: 'Series Highlight',
      description: 'highlight of a series product',
      displayField: 'name'
  });

  seriesHighlight.createField('name', {
      name: 'Name',
      type: 'Symbol',
      validations: [
          {"unique": true}
      ],
      required: true
  });

  seriesHighlight.createField('highlight', {
      name: 'Highlight',
      type: 'Symbol',
      required: true
  });

  seriesHighlight.createField('iconName', {
      name: 'Icon Name',
      type: 'Symbol',
      required: false
  });
};
