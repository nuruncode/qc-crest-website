module.exports = (migration, context) => {

    const articleOverview = migration.editContentType('articleOverview');

    articleOverview.createField('thumbnailImage', {
        name: 'Thumbnail Image',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });
};