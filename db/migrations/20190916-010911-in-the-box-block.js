module.exports = (migration, context) => {

    const inTheBoxBlock = migration.createContentType('inTheBoxBlock', {
        name: 'Block - In The Box Block',
        description: 'Block displaying whats in the box for a product',
        displayField: 'name'
    });

    inTheBoxBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    inTheBoxBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    inTheBoxBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    inTheBoxBlock.createField('title', {
        name: 'Title',
        type: 'Text',
        required: true
    });

    inTheBoxBlock.createField('titleLevel', {
        name: 'Title Level',
        type: 'Number',
        required: true
    });

    inTheBoxBlock.createField('body', {
        name: 'Body',
        type: 'Text',
        required: true
    });

    inTheBoxBlock.createField('items', {
        name: 'Items',
        type: 'Text',
        required: true
    });

    inTheBoxBlock.createField('inTheBoxImage', {
        name: 'In The Box Image',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage' ] }
        ],
        required: true
    });

};
