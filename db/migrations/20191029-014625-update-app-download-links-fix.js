module.exports = (migration, context) => {
    const appDownloadBlock = migration.editContentType('appDownloadBlock');

    appDownloadBlock.deleteField('downloadLinks');

    appDownloadBlock.createField('downloadLinks', {
        name: 'Download Links',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'link' ] }
            ],
        },
        required: false
    });
};