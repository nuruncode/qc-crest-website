module.exports = (migration, context) => {

  const tabbedContent = migration.createContentType('tabbedContentBlock', {
    name: 'Block - Tabbed Content Block',
    description: 'Tabbed Content Block',
    displayField: 'name'
  });

  tabbedContent.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  tabbedContent.createField('classNames', {
    name: 'Class Names',
    type: 'Symbol',
    required: false
  });

  tabbedContent.createField('anchorId', {
    name: 'Anchor Id',
    type: 'Symbol',
    required: false
  });

  tabbedContent.createField('blocks', {
    name: 'Related Blocks',
    type: 'Array',
    items: {
      type: 'Link',
      linkType: "Entry"
    },
    required: false
  });

  tabbedContent.createField('shortName', {
    name: 'Short Name',
    type: 'Symbol',
    required: false
  });

};
