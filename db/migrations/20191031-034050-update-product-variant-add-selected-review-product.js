module.exports = (migration, context) => {
    const productVariant = migration.editContentType('productVariant');

    productVariant.createField('isBazaarVoiceIdentifier', {
        name: 'Is BazaarVoice Identifier ',
        type: 'Boolean',
        required: true
    });
};