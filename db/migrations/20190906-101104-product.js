module.exports = (migration, context) => {

    const product = migration.createContentType('product', {
        name: 'Product',
        description: 'Product',
        displayField: 'name'
    });

    product.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    product.createField('metadata', {
        name: 'Metadata',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['metadata'] }
        ],
        required: false
    });

    product.createField('blocks', {
        name: 'Related blocks',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry"
        },
        required: false
    });

    product.createField('highlights', {
        name: 'Highlights',
        type: 'Text',
        required: true
    });

    product.createField('productOverview', {
        name: 'Product Overview',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'productOverview' ] }
        ],
        required: true
    });

    product.createField('relatedProducts', {
        name: 'Related Products',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'productOverview' ] }
            ]
        },
        required: false
    });

    product.createField('gallery', {
        name: 'Gallery Items',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'galleryItem' ] }
            ]
        },
        required: false
    });

    product.createField('featuredQuote', {
        name: 'Featured Quote',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'userQuote' ] }
        ],
        required: false
    });

    product.createField('inTheBox', {
        name: 'In the Box Content',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'inTheBox' ] }
        ],
        required: false
    });

    product.createField('promo', {
        name: 'Product Promotion',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'promo' ] }
        ],
        required: false
    });

    product.createField('brandGuarantee', {
        name: 'Brand Guarantee',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'brandGuarantee' ] }
        ],
        required: false
    });

    product.createField('featuresTab', {
        name: 'Features Tab',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'featuresTab' ] }
        ],
        required: false
    });

    product.createField('userQuote', {
        name: 'User Quote',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'userQuote' ] }
        ],
        required: false
    });

};
