module.exports = (migration, context) => {
    const showcaseBlock = migration.editContentType('showcaseBlock');

    showcaseBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    showcaseBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });
};