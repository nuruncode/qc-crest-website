module.exports = (migration, context) => {
    const productVariant = migration.editContentType('productVariant');

    productVariant.createField('backgroundsCollection', {
        name: '360 Background Images Collection',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'cloudinaryImage' ] }
            ],
        },
        required: false
    });
};