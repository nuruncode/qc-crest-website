module.exports = (migration, context) => {
    const galleryBlock = migration.createContentType('galleryBlock', {
        name: 'Gallery Block',
        description: 'Gallery Block',
        displayField: 'name'
    });

    galleryBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    galleryBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    galleryBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    galleryBlock.createField('title', {
        name: 'Title',
        type: 'Symbol',
        required: false
    });

    galleryBlock.createField('seeMoreButtonLabel', {
        name: 'See More Button Label',
        type: 'Symbol',
        required: false
    });

    galleryBlock.createField('seeLessButtonLabel', {
        name: 'See Less Button Label',
        type: 'Symbol',
        required: false
    });

};
