module.exports = (migration, context) => {

  const miniNavigation = migration.createContentType('miniNavigationBlock', {
    name: 'Block - Mini Navigation Block',
    description: 'Mini Navigation Block',
    displayField: 'name'
  });

  miniNavigation.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  miniNavigation.createField('classNames', {
    name: 'Class Names',
    type: 'Symbol',
    required: false
  });

  miniNavigation.createField('anchorId', {
    name: 'Anchor Id',
    type: 'Symbol',
    required: false
  });

  miniNavigation.createField('backgroundGrey', {
    name: 'Background Grey',
    type: 'Boolean',
    required: false
  });

  miniNavigation.createField('hierarchy', {
    name: 'Hierarchy',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      { linkContentType: [ 'hierarchy' ] }
    ],
    required: true
  });



  miniNavigation.createField('placeholderTitle', {
    name: 'Placeholder Title',
    type: 'Symbol',
    required: false
  });

};
