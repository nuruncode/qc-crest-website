module.exports = (migration, context) => {

    const productOverview = migration.editContentType('productOverview');

    productOverview.createField('nbOfModes', {
        name: 'Number of modes',
        type: 'Integer',
        required: false
    });

    productOverview.createField('modesDescriptionLabel', {
        name: 'Modes Description Label',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['label'] }
        ],
        required: false
    });
};