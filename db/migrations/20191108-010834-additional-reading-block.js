module.exports = (migration, context) => {

    const additionalReadingBlock = migration.createContentType('additionalReadingBlock', {
        name: 'Block - Additional Reading Block',
        description: 'Block showing ',
        displayField: 'name'
    });

    additionalReadingBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    additionalReadingBlock.createField('title', {
        name: 'Title',
        type: 'Symbol',
        required: false
    });
};