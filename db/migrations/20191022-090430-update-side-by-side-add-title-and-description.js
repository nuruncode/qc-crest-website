module.exports = (migration, context) => {

    const sideBySideContainerBlock = migration.editContentType('sideBySideContainerBlock');

    sideBySideContainerBlock.createField('title', {
        name: 'Title',
        type: 'Text',
        required: false
    });

    sideBySideContainerBlock.createField('titleLevel', {
        name: 'Title Level',
        type: 'Integer',
        required: false
    });

    sideBySideContainerBlock.createField('description', {
        name: 'Description',
        type: 'Text',
        required: false
    });
};