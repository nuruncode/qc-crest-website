module.exports = (migration, context) => {

    const collectionTabsBlock = migration.editContentType('collectionTabsBlock');

    collectionTabsBlock.deleteField('links');

    collectionTabsBlock.createField('links', {
        name: 'Page Links',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'link' ] }
            ]
        },
        required: true
    });
};
