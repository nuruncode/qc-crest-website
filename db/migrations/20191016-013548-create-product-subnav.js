module.exports = (migration, context) => {
    const productSubNav = migration.createContentType('productSubNav', {
        name: 'Block - Product Sub Nav',
        description: 'Product',
        displayField: 'name'
    });

    productSubNav.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    productSubNav.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    productSubNav.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    productSubNav.createField('links', {
        name: 'Links',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                {linkContentType: ['link']}
            ],
        },
        required: true
    });

};