module.exports = (migration, context) => {
    const article = migration.editContentType('article');

    article.createField('blocks', {
        name: 'Related blocks',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry"
        },
        required: false
    });

    const articleOverview = migration.editContentType('articleOverview');

    articleOverview.createField('pageMapping', {
        name: 'Page Mapping (for Next.js)',
        type: 'Symbol',
        required: true
    });
};