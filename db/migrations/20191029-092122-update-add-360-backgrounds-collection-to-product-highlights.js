module.exports = (migration, context) => {
    const productHighlightBlock = migration.editContentType('productHighlightBlock');

    productHighlightBlock.createField('backgroundsCollection', {
        name: '360 Background Images Collection',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'cloudinaryImage' ] }
            ],
        },
        required: false
    });
};