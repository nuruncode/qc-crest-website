module.exports = (migration, context) => {

    const contentBlockConfiguration = migration.createContentType('contentBlockConfiguration', {
        name: 'Content Block Configuration',
        description: 'Content block configuration by device',
        displayField: 'name'
    });

    contentBlockConfiguration.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    contentBlockConfiguration.createField('deviceType', {
        name: 'Device Type',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "all",
                    "desktop",
                    "mobile"
                ]
            }
        ],
        required: true
    });

    contentBlockConfiguration.createField('titleWidth', {
        name: "Title Width",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('descriptionWidth', {
        name: "Description Width",
        type: "Symbol",
        required: false
    });
    
    contentBlockConfiguration.createField('textColor', {
        name: 'Text Color',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "Dark",
                    "White"
                ]
            }
        ],
      });

    contentBlockConfiguration.createField('textContainerWidth', {
        name: "Text Container Width",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('textFadeIn', {
        name: 'Text Fade In',
        type: 'Boolean',
        required: false
    });

    contentBlockConfiguration.createField('textShowAnimation', {
        name: 'Text Show Animation',
        type: 'Symbol',
        validations: [
            {
                "in": [
                  "bottom-transition",
                  "top-transition",
                  "left-transition",
                  "right-transition",
                  "scale-transition"
                ]
            }
        ],
    });

    contentBlockConfiguration.createField('textAlign', {
        name: 'Text Align',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "left",
                    "right",
                    "center"
                ]
            }
        ],
        required: false
    });

    contentBlockConfiguration.createField('textContainerHorizontalAlignment', {
        name: 'Text Container Horizontal Alignment',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "left",
                    "right",
                    "center"
                ]
            }
        ],
        required: false
    });

    contentBlockConfiguration.createField('textContainerVerticalAlignment', {
        name: 'Text Container Vertical Alignment',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "top",
                    "center",
                    "bottom"
                ]
            }
        ],
        required: false
    });

    contentBlockConfiguration.createField('textContainerOffset', {
        name: "Text Container Offset",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('textContainerMarginLeft', {
        name: "Text Container Margin Left",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('textContainerMarginRight', {
        name: "Text Container Margin Right",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('textParallaxConfiguration', {
        name: 'Text Parallax Configuration',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'parallaxConfiguration' ] }
        ],
        required: false
    });

    contentBlockConfiguration.createField('mainAsset', {
        name: 'Main Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });

    contentBlockConfiguration.createField('mainAssetScale', {
        name: "Main Asset Scale",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('mainAssetHorizontalAlignment', {
        name: 'Main Asset Horizontal Alignment',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "left",
                    "right",
                    "center"
                ]
            }
        ],
        required: false
    });

    contentBlockConfiguration.createField('mainAssetVerticalAlignment', {
        name: 'Main Asset Vertical Alignment',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "top",
                    "center",
                    "bottom"
                ]
            }
        ],
        required: false
    });

    contentBlockConfiguration.createField('mainAssetMaxWidth', {
        name: "Main Asset Max Width",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('mainAssetMaxHeight', {
        name: "Main Asset Max Height",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('mainAssetHeight', {
        name: "Main Asset Height",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('mainAssetMarginTop', {
        name: "Main Asset Margin Top",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('mainAssetMarginRight', {
        name: "Main Asset Margin Right",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('mainAssetOffsetBottom', {
        name: "Main Asset Offset Bottom",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('mainAssetOffsetLeft', {
        name: "Main Asset Offset Left",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('assetParallaxConfiguration', {
        name: 'Asset Parallax Configuration',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'parallaxConfiguration' ] }
        ],
        required: false
    });

    contentBlockConfiguration.createField('backgroundAsset', {
        name: 'BackGround Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });

    contentBlockConfiguration.createField('sectionOffsetTop', {
        name: "Section Offset Top",
        type: "Symbol",
        required: false
    });

    contentBlockConfiguration.createField('secondaryAsset', {
        name: 'Secondary Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });
};
