module.exports = (migration, context) => {

  const parallaxConfiguration = migration.createContentType('parallaxConfiguration', {
      name: 'Parallax Configuration',
      description: 'Parallax configuration use to have smooth animation',
      displayField: 'name'
  });

  parallaxConfiguration.createField('name', {
      name: 'Name',
      type: 'Symbol',
      validations: [
          {"unique": true}
      ],
      required: true
  });

  parallaxConfiguration.createField('xSrc', {
      name: 'X Source',
      type: 'Symbol',
      required: false
  });

  parallaxConfiguration.createField('xDest', {
      name: 'X Destination',
      type: 'Symbol',
      required: false
  });

  parallaxConfiguration.createField('ySrc', {
      name: 'Y Source',
      type: 'Symbol',
      required: false
  });

  parallaxConfiguration.createField('yDest', {
      name: 'Y Destination',
      type: 'Symbol',
      required: false
  });

};
