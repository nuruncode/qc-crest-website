module.exports = (migration, context) => {

  const cloudinaryImage = migration.createContentType('cloudinaryImage', {
    name: 'Cloudinary Image',
    description: 'Image from the Cloudinary Media Service',
    displayField: 'name'
  });

  cloudinaryImage.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  cloudinaryImage.createField('assetId', {
    name: 'Asset Id',
    type: 'Symbol',
    required: true
  });

  cloudinaryImage.createField('upload', {
    name: 'Upload',
    type: 'Object',
    required: false
  });

  cloudinaryImage.createField('alternateText', {
    name: 'Alternate Text',
    type: 'Symbol',
    required: false
  });

  cloudinaryImage.createField('classNames', {
    name: 'Class Names',
    type: 'Symbol',
    required: false
  });

  cloudinaryImage.createField('forcedFormat', {
    name: 'Forced File Format',
    type: 'Symbol',
    required: false
  });

  cloudinaryImage.createField('imageRendition', {
    name: 'Image Rendition',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      {linkContentType: ['imageRendition'] }
    ],
    required: true
  });

};
