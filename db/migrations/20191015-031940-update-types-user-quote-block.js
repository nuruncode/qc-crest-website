module.exports = (migration, context) => {

    const userQuoteBlock = migration.editContentType('userQuoteBlock');

    userQuoteBlock.deleteField('classNames');
    userQuoteBlock.deleteField('anchorId');
    userQuoteBlock.deleteField('writeAReviewLabel');

    userQuoteBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    userQuoteBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    userQuoteBlock.createField('writeAReviewLabel', {
        name: 'Write A Review Label',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'label' ] }
        ],
        required: false
    });
};