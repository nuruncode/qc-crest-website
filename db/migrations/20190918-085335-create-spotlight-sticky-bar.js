module.exports = (migration, context) => {

    const spotlightStickyBarBlock = migration.createContentType('spotlightStickyBarBlock', {
        name: 'Block - Spotlight - Sticky Bar',
        description: 'Sticky Bar to allow to skip presentation',
        displayField: 'name'
    });

    spotlightStickyBarBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    spotlightStickyBarBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    spotlightStickyBarBlock.createField('anchorId', {
        name: 'Anchor Id',
        type: 'Symbol',
        required: false
    });

    spotlightStickyBarBlock.createField('title', {
        name: 'Title',
        type: 'Symbol',
        required: false
    });

    spotlightStickyBarBlock.createField('callToActionLabel', {
        name: 'Call To Action Label',
        type: 'Symbol',
        required: false
    });

    spotlightStickyBarBlock.createField('callToActionLink', {
        name: 'Call To Action Link',
        type: 'Symbol',
        required: false
    });

    spotlightStickyBarBlock.createField('scrollTo', {
        name: 'Scroll To Slide',
        type: 'Symbol',
        required: false
    });

    spotlightStickyBarBlock.createField('logo', {
        name: 'Logo',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: false
    });

    
    spotlightStickyBarBlock.createField('logoLink', {
        name: 'Logo Link',
        type: 'Symbol',
        required: false
    });

};
