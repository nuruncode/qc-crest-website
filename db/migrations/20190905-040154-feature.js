module.exports = (migration, context) => {
    const feature = migration.createContentType('feature', {
        name: 'Feature',
        description: 'Feature',
        displayField: 'name'
    });

    feature.createField('name', {
        name: 'Name',
        type: 'Symbol',
        required: true
    });

    feature.createField('shortTitle', {
        name: 'Short Title',
        type: 'Text',
        required: true
    });

    feature.createField('title', {
        name: 'Title',
        type: 'Text',
        required: true
    });

    feature.createField('subtitle', {
        name: 'Subtitle',
        type: 'Text',
        required: false
    });

    feature.createField('description', {
        name: 'Description',
        type: 'Text',
        required: false
    });

    feature.createField('backgroundAsset', {
        name: 'Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage', 'cloudinaryVideo' ] }
        ],
        required: true
    });

    feature.createField('mobileBackgroundAsset', {
        name: 'Mobile Background Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage', 'cloudinaryVideo' ] }
        ],
        required: false
    });

    feature.createField('classNames', {
        name: 'Class Names',
        type: 'Text',
        required: false
    });
};
