module.exports = (migration, context) => {

  const socialAccounts = migration.createContentType('socialAccounts', {
    name: 'Social Accounts',
    description: 'Collection of social accounts',
    displayField: 'name'
  });

  socialAccounts.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  socialAccounts.createField('facebookUser', {
    name: 'Facebook User',
    type: 'Symbol',
    required: false
  });

  socialAccounts.createField('facebookUrl', {
    name: 'Facebook Page URL',
    type: 'Symbol',
    required: false
  });

  socialAccounts.createField('youtubeUrl', {
    name: 'Youtube Page URL',
    type: 'Symbol',
    required: false
  });

  socialAccounts.createField('twitterUrl', {
    name: 'Twitter Page URL',
    type: 'Symbol',
    required: false
  });

  socialAccounts.createField('twitterUser', {
    name: 'Twitter User',
    type: 'Symbol',
    required: false
  });

  socialAccounts.createField('instagramUser', {
    name: 'Instagram User',
    type: 'Symbol',
    required: false
  });

  socialAccounts.createField('instagramUrl', {
    name: 'Instagram Profile URL',
    type: 'Symbol',
    required: false
  });
};