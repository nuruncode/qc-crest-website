module.exports = (migration, context) => {
    const spotlightStickyBarBlock = migration.editContentType('spotlightStickyBarBlock');

    spotlightStickyBarBlock.createField('scrollTo', {
        name: "Scroll To Slide",
        type: "Symbol",
        required: false
    });
};