module.exports = (migration, context) => {
    const userQuote = migration.createContentType('userQuote', {
        name: 'User Quote',
        description: 'User Quote',
        displayField: 'name'
    });

    userQuote.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    userQuote.createField('authorName', {
        name: 'Author Name',
        type: 'Symbol',
        required: true
    });

    userQuote.createField('starRating', {
        name: 'Star Rating',
        type: 'Integer',
        required: true
    });

    userQuote.createField('quote', {
        name: 'Quote',
        type: 'Text',
        required: true
    });

    userQuote.createField('mainAsset', {
        name: 'Main Asset',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['cloudinaryImage']}
        ],
        required: true
    });
};
