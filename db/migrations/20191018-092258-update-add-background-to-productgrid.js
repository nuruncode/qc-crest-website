module.exports = (migration, context) => {

    const productGridBlock = migration.editContentType('productGridBlock');

    productGridBlock.createField('background', {
        name: 'Background',
        type: 'Symbol',
        required: false
    });
};