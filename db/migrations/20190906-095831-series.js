module.exports = (migration, context) => {

    const series = migration.createContentType('series', {
        name: 'Series',
        description: 'Series',
        displayField: 'name'
    });

    //internal name
    series.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    series.createField('title', {
        name: 'Title',
        type: 'Symbol',
        required: true
    });

    series.createField('shortTitle', {
        name: 'Short Title',
        type: 'Symbol',
        required: true
    });

    series.createField('description', {
        name: 'Description',
        type: 'Text',
        required: true
    });

    series.createField('options', {
        name: 'Options',
        type: 'Text',
        required: false
    });

    series.createField('products', {
        name: 'Products',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'productOverview' ] }
            ]
        },
        required: false
    });

    series.createField('featuredProduct', {
        name: 'Featured Product',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['productOverview'] }
        ],
        required: false
    });

    series.createField('comparisonPoints', {
        name: 'Comparison Points',
        type: 'Array',
        items: {
            type: 'Symbol'
        },
        required: false
    });

    series.createField('promo', {
        name: 'Serie Promotion',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['promo'] }
        ],
        required: false
    });

    series.createField('highlights', {
        name: 'Series Highlights',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'seriesHighlight' ] }
            ]
        },
        required: false
    });

    series.createField('backgroundAsset', {
        name: 'Background Asset',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['cloudinaryImage', 'cloudinaryVideo'] }
        ],
        required: false
    });

    series.createField('backgroundColor', {
        name: 'Background Color',
        type: 'Symbol',
        required: false
    });

    series.createField('ctaLabel', {
        name: 'Call to Action Label',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['label'] }
        ],
        required: false
    });

    series.createField('ctaLink', {
        name: 'Call to Action Link',
        type: 'Symbol',
        required: false
    });

    series.createField('assetHeight', {
        name: 'Asset Height',
        type: 'Symbol',
        required: false
    });

    series.createField('startingAtPrice', {
        name: 'Starting At Price',
        type: 'Integer',
        required: true
    });

    series.createField('priceRangeLabel', {
        name: 'Price Range Label',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['label'] }
        ],
        required: false
    });

    series.createField('experienceLinkText', {
        name: 'Experience Link Text',
        type: 'Link',
        linkType: 'Entry',
        validations: [
            {linkContentType: ['label'] }
        ],
        required: false
    });

    series.createField('experienceLink', {
        name: 'Experience Link',
        type: 'Symbol',
        required: false
    });

};
