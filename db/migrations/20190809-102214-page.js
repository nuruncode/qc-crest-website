module.exports = (migration, context) => {

  const page = migration.createContentType('page', {
    name: 'Page',
    description: 'Web Page',
    displayField: 'name'
  });

  page.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  page.createField('code', {
    name: 'Code',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  page.createField('metadata', {
    name: 'Metadata',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      {linkContentType: ['metadata'] }
    ],
    required: false
  });

  page.createField('blocks', {
    name: 'Related blocks',
    type: 'Array',
    items: {
      type: 'Link',
      linkType: "Entry"
    },
    required: false
  });

  page.createField('slug', {
    name: 'Slug',
    type: 'Link',
    linkType: 'Entry',
    validations: [
      {linkContentType: ['pagePath'] }
    ],
    required: true
  });

  page.createField('pageMapping', {
    name: 'Page Mapping (for Next.js)',
    type: 'Symbol',
    required: true
  });

};
