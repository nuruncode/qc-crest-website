module.exports = (migration, context) => {
    const article = migration.editContentType('article');

    article.createField('articleOverview', {
        name: 'Article Overview',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['articleOverview']}
        ],
        required: false
    });
};