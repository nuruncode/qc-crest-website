module.exports = (migration, context) => {

    const collectionTabsBlock = migration.editContentType('collectionSegmentTabsDropdownBlock');

    collectionTabsBlock.createField('links', {
        name: 'Page Links',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'pageLink' ] }
            ]
        },
        required: true
    });
};
