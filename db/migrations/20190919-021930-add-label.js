module.exports = (migration, context) => {

    const label = migration.createContentType('label', {
        name: 'Label',
        description: 'Label used by the front end',
        displayField: 'text'
    });

    label.createField('text', {
        name: 'Text',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

};