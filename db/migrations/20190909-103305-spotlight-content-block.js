module.exports = (migration, context) => {

    const spotlightContentBlock = migration.createContentType('spotlightContentBlock', {
        name: 'Block - Spotlight - Content Block',
        description: 'Content Block for the spotlight page',
        displayField: 'name'
    });

    spotlightContentBlock.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    spotlightContentBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });

    spotlightContentBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    spotlightContentBlock.createField('title', {
        name: 'Title',
        type: 'Text',
        required: false
    });

    spotlightContentBlock.createField('titleLevel', {
        name: 'Title Level',
        type: 'Number',
        required: false
    });

    spotlightContentBlock.createField('description', {
        name: 'Description',
        type: 'Symbol',
        required: false
    });

    spotlightContentBlock.createField('textContent', {
        name: 'Text Content',
        type: 'Text',
        required: false
    });

    spotlightContentBlock.createField('textImage', {
        name: 'Text as an Image ',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage' ] }
        ],
        required: false
    });

    spotlightContentBlock.createField('textImageMobile', {
        name: 'Text as an Image - Mobile',
        type: 'Link',
        linkType: "Entry",
        validations: [
            { linkContentType: [ 'cloudinaryImage' ] }
        ],
        required: false
    });

    spotlightContentBlock.createField('devicesConfigurations', {
        name: 'Device Configurations',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'contentBlockConfiguration' ] }
            ]
        },
        required: false
    });

    spotlightContentBlock.createField('refreshImageOnFrameChange', {
        name: 'Refresh Image On Frame Change',
        type: 'Boolean',
        required: false
    });


    spotlightContentBlock.createField('textImagePosition', {
        name: 'Text Image Position',
        type: 'Symbol',
        validations: [
            {
                "in": [
                    "top",
                    "bottom"
                ]
            }
        ],
        required: false
    });
};
