module.exports = (migration, context) => {
    const additionalReadingBlock = migration.editContentType('additionalReadingBlock');

    additionalReadingBlock.createField('anchorId', {
        name: 'Anchor ID',
        type: 'Symbol',
        required: false
    });

    additionalReadingBlock.createField('classNames', {
        name: 'Class Names',
        type: 'Symbol',
        required: false
    });
};