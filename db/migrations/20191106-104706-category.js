module.exports = (migration, context) => {
    const category = migration.createContentType('category', {
        name: 'Category',
        description: 'Category Content Type',
        displayField: 'name'
    });

    category.createField('name', {
        name: 'Name',
        type: 'Symbol',
        validations: [
            {"unique": true}
        ],
        required: true
    });

    category.createField('title', {
        name: 'Title',
        type: 'Symbol',
        required: false
    });

    category.createField('description', {
        name: 'Title',
        type: 'Text',
        required: false
    });

    category.createField('slug', {
        name: 'Slug',
        type: 'Link',
        linkType: "Entry",
        validations: [
            {linkContentType: ['pagePath']}
        ],
        required: false
    });

    category.createField('subcategories', {
        name: 'Subcategories',
        type: 'Array',
        items: {
            type: 'Link',
            linkType: "Entry",
            validations: [
                { linkContentType: [ 'link' ] }
            ],
        },
        required: false
    });
};