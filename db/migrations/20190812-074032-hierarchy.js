module.exports = (migration, context) => {

  const hierarchy = migration.createContentType('hierarchy', {
    name: 'Hierarchy',
    description: 'Collection of hierarchies',
    displayField: 'name'
  });

  hierarchy.createField('name', {
    name: 'Name',
    type: 'Symbol',
    validations: [
      {"unique": true}
    ],
    required: true
  });

  hierarchy.createField('destinations', {
    name: 'Destination',
    type: 'Array',
    items: {
      type: 'Link',
      linkType: "Entry",
      validations: [
          { linkContentType: [ 'pagePath', 'link', 'hierarchy' ] }
      ]
    },
    required: false
  });

  hierarchy.createField('onlyShowOn', {
    name: 'Only Show On Device',
    type: 'Symbol',
    validations: [
      {
        "in": [
          "desktop",
          "mobile"
        ]
      }
    ],
    required: false
  });

  hierarchy.createField('title', {
    name: 'Title',
    type: 'Symbol',
    required: false
  });

  hierarchy.createField('type', {
    name: 'Type (product, article, etc)',
    type: 'Symbol',
    required: false
  });

  hierarchy.createField('mainAsset', {
    name: 'Main Asset',
    type: 'Link',
    linkType: 'Entry',
    validations: [
        {linkContentType: ['cloudinaryImage']}
    ],
    required: false
  });

};
