const fs = require('fs');
const path = require('path');
const moment = require('moment');
const migrationsOrder = require('./migrations/migrationsOrder.json');

function addBrokenLines (text) {
    return text.replace(/,/g, ',\n')
}

const newMigrationContent = `module.exports = (migration, context) => {
    
};`;

// eslint-disable-next-line prefer-destructuring
console.log(process.argv);
const firstArgument = process.argv[2];
console.log(firstArgument);

if (!firstArgument) {
    throw Error('You must specify the name of the file as an argument');
}

const migrationsDir = path.resolve(__dirname, './migrations');
const newMigrationFileName = generateFileName(firstArgument);
const updatedOrder = JSON.stringify([].concat(migrationsOrder, newMigrationFileName));

// Create file with new migration
fs.writeFile(path.resolve(__dirname, migrationsDir, `./${newMigrationFileName}`), newMigrationContent, (err) => {
    if (err) throw err;
    console.info(`Migration template as been added to ${migrationsDir}/${newMigrationFileName}`);
});

// Write migration name to the migration order json
fs.writeFile(path.resolve(__dirname, migrationsDir, './migrationsOrder.json'), addBrokenLines(updatedOrder), (err) => {
    if (err) throw err;
    console.info(`Migration ${newMigrationFileName} has been added to migrationsOrder`);
});

function generateFileName(fileName) {
    const prefix = moment().format('YYYYMMDD-hhmmss');
    return `${prefix}-${fileName}.js`;
}