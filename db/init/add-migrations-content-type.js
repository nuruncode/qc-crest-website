module.exports = (migration, context) => {
  const ct = migration.createContentType('_migrations', {
    name: '_migrations',
    description: 'Collection of migration files executed on this space',
    displayField: 'name'
  });

  ct.createField('name', {
    name: 'Name',
    type: 'Symbol',
    required: true
  });

  ct.createField('type', {
    name: 'Type',
    type: 'Symbol',
    required: true
  });
};