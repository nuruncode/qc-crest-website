import BaseDao from './BaseDao';
import ArticleOverviewDao from "./ArticleOverviewDao";

const CONTENT_TYPE = "article";

class ArticleDao extends BaseDao {

    constructor() {
        super();
    }

    async getActiveArticleBySlug(slug) {
        let requestedArticle = null;

        // Find ArticleOverview First to retreive it by id afterward
        // content delivery api doesn t support 2 level+ comparison (ie fields.slug.fields.slug: slug)
        const articleOverview = await ArticleOverviewDao.getActiveArticleOverviewBySlug(slug);

        if (articleOverview != null) {
            const pageQuery = {
                'content_type': CONTENT_TYPE,
                'fields.articleOverview.sys.id': articleOverview.sys.id,
                'include': 10
            };

            const entries = await this.getEntriesNoSys(pageQuery);

            if(entries && entries.length > 1) {
                console.error('there is more than one entry assign to the path' + slug);
            }

            entries.forEach(page => {
                this.computeLayout(page);
            });

            requestedArticle = (entries && entries.length == 1) ? entries[0] : undefined;
        }

        return requestedArticle;
    }

}

export default new ArticleDao();
