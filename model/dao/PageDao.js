import BaseDao from './BaseDao';
import PagePathDao from './PagePathDao';

const CONTENT_TYPE = "page";

class PageDao extends BaseDao {

  constructor() {
    super();
  }

  async getActivePageBySlug(slug) {
    let requestedPage = null;

    // Find PagePath First to retreive it by id id afterward
    // content delivery api doesn t support 2 level+ comparison (ie fields.slug.fields.slug: slug)
    const pagePath = await PagePathDao.getActivePagePathBySlug(slug);

    if (pagePath != null) {
      const pageQuery = {
        'content_type': CONTENT_TYPE,
        'fields.slug.sys.id': pagePath.sys.id,
        'include': 10
      };

      const entries = await this.getEntriesNoSys(pageQuery);

      if(entries && entries.length > 1) {
        console.error('there is more than one entry assign to the path' + slug);
      }

      entries.forEach(page => {
        this.computeLayout(page);
      });

      requestedPage = (entries && entries.length == 1) ? entries[0] : undefined;
    }

    return requestedPage;
  }

  async getPageByCode(code) {
    const pageQuery = {
      'content_type': CONTENT_TYPE,
      'fields.code': code,
      'include': 10
    };

    const entries = await this.getEntriesNoSys(pageQuery);
    return (entries && entries.length == 1) ? entries[0] : undefined;
  }

  async getAllPages() {
    try {
      let pages = [];
      let hasContentType = await this.hasContentType(CONTENT_TYPE);
      if (hasContentType) {
        pages = await this.getEntriesNoSys({
          'content_type': CONTENT_TYPE
        });
      }
      return pages;
    } catch (e) {
      console.error(e);
    }
  }
}

export default new PageDao();
