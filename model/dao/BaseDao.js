import contentfulClient from "../ContentfulClient";
import OrderedMap from '../OrderedMap';

class BaseDao {

  constructor() {
    this.contentfulClient = contentfulClient;
  }

  getContentfulClient() {
    return this.contentfulClient;
  }

  async hasContentType(type) {
    const client = await this.contentfulClient.getClient();
    const contentTypes = await client.getContentTypes();

    return contentTypes.items.some(contentType => contentType.sys.id == type);
  }

  async getEntriesNoSys(query) {
    const client = await this.contentfulClient.getClient();

    return await client.getEntries(query)
      .then(data => {
        let items = data.items;
        items && items.forEach(item => {
          this.filter(item, this.removeSysFromDocument, data);
        });
        return items;
      })
      .catch(error => {
        console.error(error);
      });
  }

  async getEntries(query) {
    const client = await this.contentfulClient.getClient();

    return await client.getEntries(query)
      .then(data => {
        let items = data.items;
        return items;
      })
      .catch(error => {
        console.error(error);
      });
  }

  removeSysFromDocument(document, parentDocument) {
    if (document && document.sys) {
      const sys = document.sys;

      let contentType;

      if(sys.contentType) {
        contentType = sys.contentType.sys.id
      } else if(sys.type === 'Asset') {
        contentType = sys.type
      } else {
        console.error("Document is abnormal");
        console.error(document);
        console.error("On:");
        console.error(parentDocument);
      }

      document.fields.contentType = contentType;

      // Special case for images
      if (contentType == 'staticImage') {
        const rev = sys.revision;
        document.fields.revision = rev;
      }

      delete document.sys;
    }
  }

  filter(document, filterMethod, data) {
    filterMethod(document, data);
    if (document && document.fields) {
      for (let field in document.fields) {
        const fieldValue = document.fields[field];
        if (fieldValue instanceof Array) {
          fieldValue.forEach(item => {
            this.filter(item, filterMethod, fieldValue);
          });
        } else {
          this.filter(fieldValue, filterMethod, document);
        }
      }
    }
  }

  /**
   * We need to support inheritance of layouts. A zone can be overriden from parent to child.
   * @param page
   */
  computeLayout(entity) {
    const blocks = entity.fields.blocks;

    if (blocks != null && blocks.length == 1) {
      blocks.forEach(rootBlock => {
        const isLayout = rootBlock.fields.contentType == 'layout';
        if (isLayout) {
          // For each zone child of the layout, starting by parent.
          const layouts = [];
          this.convertToLayoutList(rootBlock, layouts);
          layouts.reverse();

          const finalChilds = new OrderedMap();

          layouts.forEach(layout => {
            const blocks = layout.fields.blocks;
            blocks.forEach(childBlock => {
              let code = childBlock.fields.code;
              if (!code) {
                code = childBlock.fields.name;
              }
              finalChilds.push(code, childBlock);
            });
          });

          rootBlock.fields.blocks = finalChilds.values();
        }
      });
    }

    return entity;
  }

  convertToLayoutList(layout, layoutList) {
    layoutList.push(layout);
    const parent  = layout.fields.parentLayout;
    if (parent) {
      this.convertToLayoutList(parent, layoutList);
    }
  }
}

export default BaseDao;