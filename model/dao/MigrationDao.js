import BaseDao from './BaseDao';

const CONTENT_TYPE = "_migrations";

class MigrationDao extends BaseDao {

  constructor() {
    super();
  }

  async getMigrations(type) {
    const query = {
      'content_type': CONTENT_TYPE,
      'fields.type': type,
      'order': 'sys.updatedAt',
      'limit': '1000'
    };

    return this.getEntries(query);
  }
}

module.exports = new MigrationDao();
