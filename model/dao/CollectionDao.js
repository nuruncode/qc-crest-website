import BaseDao from './BaseDao';
import PagePathDao from './PagePathDao';

const CONTENT_TYPE = "collection";

class CollectionDao extends BaseDao {

  constructor() {
    super();
  }

  async getActiveCollectionBySlug(slug) {
    let requestedCollection = null;

    // Find PagePath First to retreive it by id afterward
    // content delivery api doesn t support 2 level+ comparison (ie fields.slug.fields.slug: slug)
    const pagePath = await PagePathDao.getActivePagePathBySlug(slug);

    if (pagePath != null) {
      const pageQuery = {
        'content_type': CONTENT_TYPE,
        'fields.slug.sys.id': pagePath.sys.id,
        'include': 10
      };


      const entries = await this.getEntriesNoSys(pageQuery);
      if(entries && entries.length > 1) {
        console.error('there is more than one entry assign to the path' + slug);
      }
      
      entries.forEach(page => {
        this.computeLayout(page);
      });

      requestedCollection = (entries && entries.length == 1) ? entries[0] : undefined;
    }

    return requestedCollection;
  }
}

export default new CollectionDao();
