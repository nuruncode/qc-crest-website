import BaseDao from './BaseDao';

const CONTENT_TYPE = "staticImage";

class StaticImageDao extends BaseDao {

  constructor() {
    super();
  }

  async getStaticImageByName(name) {
    const query = {
      'content_type': CONTENT_TYPE,
      'fields.name': name
    };

    const entries = await this.getEntries(query);
    return (entries && entries.length == 1) ? entries[0] : undefined;
  }
}

export default new StaticImageDao();