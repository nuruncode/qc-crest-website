import BaseDao from './BaseDao';

class BlockDao extends BaseDao {

  constructor() {
    super();
  }

  static create() {
    return new BlockDao();
  }

  async getBlockByTypeAndName(contentType, name) {
    const query = {
      'content_type': contentType,
      'fields.name': name
    };

    return this.getEntries(query);
  }

}

export default new BlockDao();