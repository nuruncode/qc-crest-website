import BaseDao from './BaseDao';
import ProductOverviewDao from './ProductOverviewDao';

const CONTENT_TYPE = "product";

class ProductDao extends BaseDao {

  constructor() {
    super();
  }

  async getActiveProductBySlug(slug) {
    let requestedProduct = null;

    // Find ProductOverview First to retreive it by id afterward
    // content delivery api doesn t support 2 level+ comparison (ie fields.slug.fields.slug: slug)
    const productOverview = await ProductOverviewDao.getActiveProductOverviewBySlug(slug);

    if (productOverview != null) {
      const pageQuery = {
        'content_type': CONTENT_TYPE,
        'fields.productOverview.sys.id': productOverview.sys.id,
        'include': 10
      };

      const entries = await this.getEntriesNoSys(pageQuery);

      if(entries && entries.length > 1) {
        console.error('there is more than one entry assign to the path' + slug);
      }

      entries.forEach(page => {
        this.computeLayout(page);
      });

      requestedProduct = (entries && entries.length == 1) ? entries[0] : undefined;
    }

    return requestedProduct;
  }

}

export default new ProductDao();
