import BaseDao from './BaseDao';

const CONTENT_TYPE = "hierarchy";

class HierarchyDao extends BaseDao {

  constructor() {
    super();
  }

  async getHierarchy(name) {

    const pageQuery = {
      'content_type': CONTENT_TYPE,
      'fields.name': name,
      'include': 10
    };

    const entries = await this.getEntriesNoSys(pageQuery);
    return (entries && entries.length == 1) ? entries[0] : undefined;
  }
}

export default new HierarchyDao();


