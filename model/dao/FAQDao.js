import BaseDao from './BaseDao';

const CONTENT_TYPE = "faq";

class FAQDao extends BaseDao {

  constructor() {
    super();
  }

  async getAllFAQs() {
    const query = {
      'content_type': CONTENT_TYPE
    };

    return this.getEntries(query);
  }
}

export default new FAQDao();


