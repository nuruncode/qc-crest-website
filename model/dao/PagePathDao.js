import BaseDao from './BaseDao';

const CONTENT_TYPE = "pagePath";

class PagePathDao extends BaseDao {

  constructor() {
    super();
  }

  static create() {
    return new PagePathDao();
  }

  async getPagePathBySlug(slug) {

    const pageQuery = {
      'content_type': CONTENT_TYPE,
      'fields.slug': slug,
      'include': 10
    };

    const entries = await this.getEntries(pageQuery);

    return (entries && entries.length == 1) ? entries[0] : undefined;
  }

  async getActivePagePathBySlug(slug) {
    const pageQuery = {
      'content_type': CONTENT_TYPE,
      'fields.slug': slug,
      'fields.disabled[ne]': true,
      'include': 10
    };

    const entries = await this.getEntries(pageQuery);
    return (entries && entries.length == 1) ? entries[0] : undefined;
  }

}

export default new PagePathDao();
