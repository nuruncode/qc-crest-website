const contentfulManagement = require('contentful-management');

class ContentfulManagementClient {

  constructor() {
    this.personalAccessToken = process.env.CF_PERSONAL_ACCESS_TOKEN;
    this.spaceId = process.env.CF_SPACE_ID;
    this.environmentId = process.env.CF_ENVIRONMENT;

    if (this.personalAccessToken !== "") {
      this.contentfulManagementClient = contentfulManagement.createClient({
        accessToken: this.personalAccessToken
      });
    }
  }

  getPersonalAccessToken() {
    return this.personalAccessToken;
  }

  getSpaceId() {
    return this.spaceId;
  }

  getEnvironmentId() {
    return this.environmentId;
  }

  async getEnvironment() {
    if (this.contentfulManagementClient) {
      return this.contentfulManagementClient.getSpace(this.getSpaceId()).then((space) => {
        return space.getEnvironment(this.getEnvironmentId() || 'master');
      });
    }
  }

}

module.exports = new ContentfulManagementClient();