class OrderedMap {

  constructor() {
    this.k = [];
    this.v = {};
  }

  push(key, value) {
    if (!this.v[key]) {
      this.k.push(key);
    }
    this.v[key] = value;
  }

  length() {
    return this.k.length;
  }

  keys() {
    return this.k;
  }

  values() {
    let vArray = [];

    this.k.forEach(key => {vArray.push(this.v[key])});

    return vArray;
  }

}

export default OrderedMap;