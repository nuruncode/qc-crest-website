const localeService = require("./service/locale/LocaleService");
const contentfulMigrationService = require("./service/contentful/migration/ContentfulMigrationService");
const contentfulClient = require("./ContentfulClient");
const contentTypeMigrationService = require("./service/contentful/migration/cmd/ContentTypeMigrationService");
const extensionMigrationService = require("./service/contentful/migration/cmd/ExtensionsMigrationService");
const migrationSetupService = require("./service/contentful/migration/MigrationSetupService");

class Services {

  static create() {
    return new Services();
  }

  constructor() {
    this.localeService = localeService;
    this.contentfulMigrationService = contentfulMigrationService;
    this.contentfulClient = contentfulClient;
    this.contentTypeMigrationService = contentTypeMigrationService;
    this.extensionMigrationService = extensionMigrationService;
    this.migrationSetupService = migrationSetupService;
  }

  getMigrationSetupService() {
    return this.migrationSetupService;
  }

  getExtensionMigrationService() {
    return this.extensionMigrationService;
  }

  getContentTypeMigrationService() {
    return this.contentTypeMigrationService;
  }

  getLocaleService() {
    return this.localeService;
  }

  getContentfulMigrationService() {
    return this.contentfulMigrationService;
  }

  getContentfulClient() {
    return this.contentfulClient;
  }
}

module.exports = Services;