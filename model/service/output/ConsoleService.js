const chalk = require('chalk');

export let CREATED = "Created";
export let DELETED = "Deleted";
export let ALREADY_EXECUTED = "Already executed";
export let EXECUTING = "Executing ...";

class ConsoleService {

  constructor() {
    this.log = console.log;
  }

  printLog(type, source) {
    this.printMigrationlog(type, source, ALREADY_EXECUTED);
  }

  printCreationLog(type, source) {
    this.printMigrationlog(type, source, EXECUTING);
  }

  printMigrationlog(type, source, status) {
    this.log(chalk`{white.bold ${type.toUpperCase()}}: {underline ${source}} : {green ${status}}`);
  }

  printMigrationRedlog(type, source, status) {
    this.log(chalk`{white.bold ${type.toUpperCase()}}: {underline ${source}} : {red ${status}}`);
  }

}

module.exports = new ConsoleService();