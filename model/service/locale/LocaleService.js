class LocaleService {

  async getDefaultLanguageCode(env) {
    const locales = await env.getLocales();
    const defaultLocale = locales.items.find(({default: def}) => def === true);
    return defaultLocale.code;
  }

}

module.exports = new LocaleService();