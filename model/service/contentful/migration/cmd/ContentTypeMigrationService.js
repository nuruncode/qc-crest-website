const { runMigration } = require('contentful-migration/built/bin/cli');
const path = require('path');
const contentfulManagementClient = require('../../../../ContentfulManagementClient');
const localeService = require('../../../locale/LocaleService');

const migrationFolderRelativePath = "../../../../../db/";
const CONTENT_TYPE = "_migrations";

class ContentTypeMigrationService {

  async executeMigrationFile(fileName, type) {
    try {

      const filePath = path.join(__dirname, migrationFolderRelativePath, "./" + type + "/", fileName);

      const options = {
        filePath: filePath,
        spaceId: contentfulManagementClient.getSpaceId(),
        accessToken: contentfulManagementClient.getPersonalAccessToken(),
        environmentId: contentfulManagementClient.getEnvironmentId(),
        yes: true
      };

      await runMigration(options);
      await this.createEntry(fileName, type);
    } catch (e) {
      console.error(e);
    }
  }

  async createEntry(name, type) {
    try {
      let newMigration = await contentfulManagementClient.getEnvironment().then(async env => {
        const languageCode = await localeService.getDefaultLanguageCode(env);
        return await env.createEntry(CONTENT_TYPE, {
          fields: {
            name: {
              [languageCode]: name
            },
            type: {
              [languageCode]: type
            }
          }
        });
      });

      newMigration.publish();
    } catch (e) {
      console.error(e)
    }
  }
}

module.exports = new ContentTypeMigrationService();