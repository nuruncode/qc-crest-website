import {createExtensionHandler} from 'contentful-cli/dist/cmds/extension_cmds/create';
import fetch from 'node-fetch';
import url from 'url';

const contentfulManagementClient = require('../../../../ContentfulManagementClient');
import localeService from '../../../locale/LocaleService';

const CONTENT_TYPE = "_migrations";

class ContentTypeMigrationService {

  async createExtension(extensionRepoUrl, type) {
    try {
      let definitionResponse = await fetch(extensionRepoUrl);
      let definitionData = await definitionResponse.json();
      let extensionUrl = url.resolve(extensionRepoUrl, definitionData.srcdoc);

      const extensionOptions = {
        context:{
          activeSpaceId: contentfulManagementClient.getSpaceId(),
          managementToken: contentfulManagementClient.getPersonalAccessToken(),
          activeEnvironmentId: contentfulManagementClient.getEnvironmentId(),
        },
        name: definitionData.name,
        id: definitionData.id,
        fieldTypes: definitionData.fieldTypes,
        src: extensionUrl,
        sidebar: definitionData.sidebar,
        parameters: definitionData.parameters
      };


      await createExtensionHandler(extensionOptions);
      await this.createEntry(extensionRepoUrl, type);
    } catch (e) {
      console.error(e)
    }
  }

  async createEntry(name, type) {
    try {
      let newMigration = await contentfulManagementClient.getEnvironment().then(async env => {
        const languageCode = await localeService.getDefaultLanguageCode(env);
        return await env.createEntry(CONTENT_TYPE, {
          fields: {
            name: {
              [languageCode]: name
            },
            type: {
              [languageCode]: type
            }
          }
        });
      });

      newMigration.publish();
    } catch (e) {
      console.error(e)
    }
  }
}

module.exports = new ContentTypeMigrationService();
