import contentTypeMigrationService from "./cmd/ContentTypeMigrationService";
import migrationDao from "../../../dao/MigrationDao";
import consoleService from '../../output/ConsoleService';

const initScriptFileName = "add-migrations-content-type.js";
const initType = "init";

class MigrationSetupService {

  async setupMigration() {
    console.log("");
    console.log("Setting up migration ...");

    if (await this.hasBeenInitiated()) {
      consoleService.printLog(initType, initScriptFileName);
    } else {
      consoleService.printCreationLog(initType, initScriptFileName);
      await contentTypeMigrationService.executeMigrationFile(initScriptFileName, initType);
    }
  }

  async hasBeenInitiated() {
    const ranMigrations = await migrationDao.getMigrations(initType);
    let matching = [];

    if (ranMigrations != null && ranMigrations.length > 0) {
      matching = ranMigrations.filter(migration => migration.fields.name == initScriptFileName);
    }
    return matching.length > 0;
  }

}

module.exports = new MigrationSetupService();