const contentTypeMigrationService = require("./cmd/ContentTypeMigrationService");
const migrationDao = require("../../../dao/MigrationDao");
const consoleService = require('../../output/ConsoleService');
const migrationsOrder = require("../../../../db/migrations/migrationsOrder.json");

const initType = "migrations";

class MigrationContentTypeService {

  async runMigration() {
    console.log("");
    console.log("Starting content types migration ...");

    const ranMigrations = await migrationDao.getMigrations(initType);

    for (const fileName of migrationsOrder) {
      let matching = ranMigrations.filter(migration => migration.fields.name == fileName);

      if (matching == 0) {
        consoleService.printCreationLog(initType, fileName);
        await contentTypeMigrationService.executeMigrationFile(fileName, initType);
      } else {
        consoleService.printLog(initType, fileName);
      }
    }
  }
}

module.exports = new MigrationContentTypeService();
