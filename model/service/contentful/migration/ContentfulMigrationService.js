const migrationSetupService = require("./MigrationSetupService");
const migrationExtensionService = require("./MigrationExtensionService");
const migrationContentTypeService = require("./MigrationContentTypeService");

class ContentfulMigrationService {

  async importMigration() {
    await migrationSetupService.setupMigration();
    await migrationExtensionService.extensionMigration();
    await migrationContentTypeService.runMigration();
  }
}

module.exports = new ContentfulMigrationService();