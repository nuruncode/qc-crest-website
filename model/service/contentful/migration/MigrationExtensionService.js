const extensionMigrationService = require("./cmd/ExtensionsMigrationService");
const migrationDao = require("../../../dao/MigrationDao");
const consoleService = require('../../output/ConsoleService');
const extensionsUrls = require("../../../../db/extensions/extensionsUrls.json");

const initType = "extensions";

class MigrationExtensionService {

  static create() {
    return new MigrationExtensionService();
  }

  async extensionMigration() {
    console.log("");
    console.log("Starting extension migration ...");

    const ranMigrations = await migrationDao.getMigrations(initType);

    for (const extensionRepoUrl of extensionsUrls) {
      if(await this.hasBeenExecuted(ranMigrations, extensionRepoUrl)) {
        consoleService.printLog(initType, extensionRepoUrl);
      } else {
        consoleService.printCreationLog(initType, extensionRepoUrl);
        await extensionMigrationService.createExtension(extensionRepoUrl, initType);
      }
    }
  }

  async hasBeenExecuted(ranMigrations, extenstionUrl) {
    let matching = [];

    if (ranMigrations != null && ranMigrations.length > 0) {
      matching = ranMigrations.filter(migration => migration.fields.name == extenstionUrl);
    }
    return matching.length > 0;
  }

}

module.exports = new MigrationExtensionService();