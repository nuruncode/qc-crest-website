import 'isomorphic-fetch';
import getConfig from 'next/config'

const {publicRuntimeConfig} = getConfig();


class SearchService {

  async searchDocument(query) {
    this.swiftypeApiKey = publicRuntimeConfig.SWIFTYPE_READ_API_KEY;
    this.swiftypeEngine = publicRuntimeConfig.SWIFTYPE_ENGINE;
    this.swiftypeBaseUrl = "https://api.swiftype.com/api/v1";
    this.swiftypePath = '/public/engines/search';

    const params = {'engine_key': this.swiftypeApiKey,'q': query};

    const encodedParam = this.encodeQueryData(params);

    let payload = { method: 'GET'};
    let url = this.swiftypeBaseUrl + this.swiftypePath;

    let searchResults = [];


    const response = await fetch(url + "?" + encodedParam, payload);
    const data = await response.json();

    // group so we can iterate on both
    if(data.records.faq) {
      data.records.faq.forEach(function(faqEntry) {
        searchResults.push(faqEntry);
      });
    }
    if(data.records.page) {
      data.records.page.forEach(function(pageEntry) {
        searchResults.push(pageEntry);
      });
    }

    // sort base on score
    searchResults.sort(this.compare);

    return searchResults;
  }

  encodeQueryData(data) {
    const ret = [];
    for (let d in data)
      ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
    return ret.join('&');
  }

  compare(a,b) {
    if (a._score > b._score)
      return -1;
    if (a._score < b._score)
      return 1;
    return 0;
  }
}

export default new SearchService();