const swiftypeApi = require('swiftype');

class SwiftypeClient {

  constructor() {
    this.swiftypeApiKey = process.env.SWIFTYPE_API_KEY;

    this.client = new swiftypeApi({
      apiKey: this.swiftypeApiKey
    });
  }

  getClient() {
    return this.client;
  }

}

export default new SwiftypeClient();