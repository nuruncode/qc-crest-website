import SwiftypeClient from './SwiftypeClient';
const fetch = require('node-fetch');
const htmlToText = require('html-to-text');
const {Headers} = require('node-fetch');

import faqDao from './../../dao/FAQDao';
import pageDao from './../../dao/PageDao';
import ConsoleService from './../output/ConsoleService';

export let CREATED = "Created";
export let DELETED = "Deleted";
export let ALREADY_EXECUTED = "Already executed";

const FAQ_DOCUMENT_TYPE = 'faq';
const PAGE_DOCUMENT_TYPE = 'page';

class DocumentService {

  async indexAllDocuments() {
    console.log("");
    console.log("indexing all documents");
    console.log("----------------------------------");
    await this.indexFaqs();
    await this.indexPages();
  }

  async setupDocumentType(documentType) {
    let that = this;

    await new Promise(resolve => {
      SwiftypeClient.getClient().documentTypes.get({
          engine: process.env.SWIFTYPE_ENGINE.toLowerCase(),
          documentType: documentType
        }, async function (res, err) {
          if (res && res.error) {
            console.error(res.error);
            await that.createDocumentType(documentType);
          } else {
            ConsoleService.printMigrationlog("document type", documentType, ALREADY_EXECUTED);
          }
          resolve();
        }
      );
    });
  }

  async createDocumentType(documentType) {
    await new Promise(resolve => {
      SwiftypeClient.getClient().documentTypes.create({
          engine: process.env.SWIFTYPE_ENGINE.toLowerCase(),
          document_type: {name: documentType}
        }, async function (err, res) {
          if (err) {
            console.error(err.error);
          } else {
            ConsoleService.printMigrationlog("document type", documentType, CREATED);
          }
          resolve();
        }
      );
    });
  }

  async deleteDocumentType(documentType) {
    await new Promise(resolve => {
      SwiftypeClient.getClient().documentTypes.destroy({
        engine: process.env.SWIFTYPE_ENGINE.toLowerCase(),
        documentType: documentType
      }, async function(err, res) {
        if (err && err.error) {
          ConsoleService.printMigrationRedlog("document type", documentType, "non existant");
        } else {
          ConsoleService.printMigrationRedlog("document type", documentType, "deleted");
        }

        resolve();
      });
    });
  }

  async createDocument(documentType, document) {
    const id = document.sys.id;
    const fields = document.fields;

    fields.type = documentType;
    let formattedFields = [];
    for (var field in fields) {
      formattedFields.push(this.formatFields(field, fields[field], (field == 'title' && field == 'description') ? "text" : null));
    }

    const payload = {
      engine: process.env.SWIFTYPE_ENGINE.toLowerCase(),
      documentType: documentType,
      document: {
        external_id: id,
        fields: formattedFields
      }
    };

    await new Promise(resolve => {
      SwiftypeClient.getClient().documents.create(payload, async function (err, res) {
          if (err) {
            console.error(err.error);
          } else {
            ConsoleService.printMigrationlog("document", documentType + " - " + fields.name, CREATED);
          }
          resolve();
        }
      );
    });
  }

  formatFields(key, value, fieldType) {
    let formattedField = {};
    formattedField.name = key;
    formattedField.value = value;
    formattedField.type = (fieldType) ? fieldType : this.getFieldType(value);

    return formattedField;
  }

  getFieldType(value) {
    let type = 'text';

    if(typeof value == 'object') {
      type = 'enum';
    }

    return type;
  }

  async indexFaqs() {
    const that = this;
    await this.setupDocumentType(FAQ_DOCUMENT_TYPE);

    let faqPage = await pageDao.getPage('faq');
    let pageUrl = process.env.BASE_URL + faqPage.fields.slug;

    const faqList = await faqDao.getAllFAQs();
    faqList.forEach(async function(faq) {
      let faqInstance = faq;

      let categoryQueryString = "";
      if(faq.fields.categories && faq.fields.categories.length > 0) {
        categoryQueryString = "&category=" + faq.fields.categories[0];
      }

      faqInstance.fields.url = pageUrl + "#element=" + faq.fields.anchorLink + categoryQueryString;
      faqInstance.fields.title = encodeURIComponent(faq.fields.question);
      faqInstance.fields.name = faq.fields.question;
      faqInstance.fields.description = encodeURIComponent(faq.fields.answer);

      await that.createDocument(FAQ_DOCUMENT_TYPE, faq)
    });
  }

  async indexPages() {
    const that = this;
    await this.setupDocumentType(PAGE_DOCUMENT_TYPE);
    const pageList = await pageDao.getEnabledAndSearchablePages();
    const username = process.env.SITE_USERNAME;
    const password = process.env.SITE_PASSWORD;

    let headers = new Headers();
    if (username && password) {
      headers.set('authorization', 'Basic ' + Buffer.from(username + ":" + password).toString('base64'));
    }

    pageList.forEach(async function(page) {
      let pageInstance = {};
      pageInstance.sys = page.sys;
      pageInstance.fields = {};
      let metadata = page.fields.metadata.fields;
      let openGraphImage = metadata.openGraphImage;

      let pageUrl = process.env.BASE_URL + page.fields.slug;
      let imageUrl = process.env.BASE_URL + "/static/images/" + openGraphImage.fields.filename +
        "." + openGraphImage.fields.extension + "?revision=" + openGraphImage.sys.revision;

      pageInstance.fields.url = pageUrl;
      pageInstance.fields.imageUrl = imageUrl;
      pageInstance.fields.title = encodeURIComponent(metadata.metaTitle);
      pageInstance.fields.name = metadata.metaTitle;
      pageInstance.fields.description = encodeURIComponent(metadata.metaDescription);

      await fetch(pageUrl, { method: 'GET', headers: headers})
        .then(res => res.text())
        .then(async function(body) {
          const text = htmlToText.fromString(body, { baseElement : ['main'], ignoreHref: true, ignoreImage: true});
          pageInstance.fields.text = text;
          await that.createDocument(PAGE_DOCUMENT_TYPE, pageInstance);
        });
    });
  }
}

module.exports = new DocumentService();