const path = require('path');

let environment = process.env.environment || 'develop';
const market = process.env.MARKET || 'en-uk';

const dotEnvPath = path.resolve(
  'config/environment/' + market + '/.env.' + environment
);

const dotEnvFile = {
  path: dotEnvPath
};

require('dotenv').config(dotEnvFile);


console.log("----------------------------------");
console.log("| Clean up");
console.log("----------------------------------");
console.log("| Configurations");
console.log("----------------------------------");
console.log("| config file: " + dotEnvFile.path);
console.log("| engine :" + process.env.SWIFTYPE_ENGINE);
console.log("| api key :" + process.env.SWIFTYPE_API_KEY);
console.log("----------------------------------");
console.log("");

const indexationService = require('./IndexationSevice');

function runDocumentsCleanup() {

  indexationService.runCleanup().catch(e => {
    console.error(e)
  });

}

runDocumentsCleanup();

