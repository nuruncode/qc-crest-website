import SwiftypeClient from './SwiftypeClient';
import ConsoleService from './../output/ConsoleService';
import DocumentService from './../swiftype/DocumentService';

class EngineService {

  async setupEngine() {
    const that = this;

    await new Promise(resolve => {
      SwiftypeClient.getClient().engines.get({
        engine: process.env.SWIFTYPE_ENGINE.toLowerCase()
      }, async function(err, res) {
        if(err) {
          await that.createEngine();
          resolve();
        } else {
          ConsoleService.printMigrationRedlog("engine", process.env.SWIFTYPE_ENGINE.toLowerCase(), "already exist");
          resolve();
        }
      });
    });
  }

  async createEngine() {
    await new Promise(resolve => {
      SwiftypeClient.getClient().engines.create({
        engine: {name: process.env.SWIFTYPE_ENGINE}
      }, async function(err, res) {
        if (err) {
          console.error(err.error);
        } else {
          ConsoleService.printMigrationlog("engine", process.env.SWIFTYPE_ENGINE.toLowerCase(), "created");
        }

        resolve();
      });
    });
  }

  async deleteEngine() {
    await new Promise(resolve => {
      SwiftypeClient.getClient().engines.destroy({
        engine: process.env.SWIFTYPE_ENGINE.toLowerCase()
      }, async function(err, res) {
        if (err) {
          console.error(err.error);
        } else {
          ConsoleService.printMigrationRedlog("engine", process.env.SWIFTYPE_ENGINE.toLowerCase(), "deleted");
        }

        resolve();
      });
    });
  }
}

module.exports = new EngineService();