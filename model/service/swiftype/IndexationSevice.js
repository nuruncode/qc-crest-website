
const engineService = require('./EngineService');
const documentService = require('./DocumentService');


class IndexationService {

  async runIndexation() {
    await engineService.setupEngine();
    await documentService.indexAllDocuments();
  }

  async runCleanup() {
    await documentService.deleteDocumentType("page");
    await documentService.deleteDocumentType("faq");
  }

}

module.exports = new IndexationService();