
const LOCAL_STORAGE_KEYS = {
    RECENTLY_VIEWED_PRODUCTS: 'RECENTLY_VIEWED_PRODUCTS',
}

export { LOCAL_STORAGE_KEYS };