
class VideoFactory {

    constructor() {
        this.CLOUDINARY_BASEURL = 'https://res.cloudinary.com/pgone/video/upload/';
    }

    buildVideoUrl(videoId) {
        return this.CLOUDINARY_BASEURL + videoId;
    }
}

export default new VideoFactory();