import TransformationParameters from "./TransformationParameters";

class ImageFactory {

  constructor() {
    this.CLOUDINARY_BASEURL = 'https://res.cloudinary.com/pgone/image/upload';
  }

  buildImageUrl(imageId) {
    return this.buildImageUrl(imageId, null);
  }

  buildImageUrlByWidth(imageUrl, width = 'auto', transformations = 'c_limit', format = 'auto') {
    let url = `${this.CLOUDINARY_BASEURL}/w_${width}${transformations && ','
    + transformations},q_auto,f_${format}/${imageUrl}`;
    return url;
  }

  buildImageUrlByHeight(imageUrl, height = 'auto', transformations = 'c_limit', format = 'auto') {
    let url = `${this.CLOUDINARY_BASEURL}/h_${height}${transformations && ','
    + transformations},q_auto,f_${format}/${imageUrl}`;
    return url;
  }

  buildImageUrl(imageId, extension, configuration) {
    let imageUrl = this.CLOUDINARY_BASEURL;
    let parameters = (configuration !== undefined) ? this.buildParameters(
        configuration) : '';

    if (parameters.length > 0) {
      imageUrl += "/" + parameters;
    }

    if(extension) {
      imageUrl += "/" + imageId + '.' + extension;
    } else {
      imageUrl += "/" + imageId;
    }


    return imageUrl;
  }

  buildParameters(configuration) {
    let parameters = "";

    parameters = this.addWidthParameter(configuration.width, parameters);
    parameters = this.addQualityParameter(configuration.quality, parameters);

    return parameters;
  }

  addWidthParameter(width, parameters) {
    if (width !== undefined) {
      // add crop to limit when width is specified
      parameters = this.addCropLimitParameter(parameters);
      parameters = this.addParameter(parameters,
          TransformationParameters.WIDTH + TransformationParameters.SEPARATOR
          + width.trim());
    }
    return parameters;
  }

  addCropLimitParameter(parameters) {
    parameters = this.addParameter(parameters,
        TransformationParameters.CROP + TransformationParameters.SEPARATOR
        + "limit");
    return parameters;
  }

  addQualityParameter(quality, parameters) {
    if (quality !== undefined) {
      parameters = this.addParameter(parameters,
          TransformationParameters.QUALITY + TransformationParameters.SEPARATOR
          + quality.trim());
    }
    return parameters
  }

  addParameter(parameters, parameter) {
    parameters = this.addCommaSeparator(parameters);
    parameters += parameter;
    return parameters;
  }

  addCommaSeparator(parameters) {
    if (parameters.length > 0) {
      parameters += ",";
    }
    return parameters;
  }
}

export default new ImageFactory();