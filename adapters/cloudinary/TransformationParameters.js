const TransformationParameters = {
  WIDTH: "w",
  CROP: "c",
  QUALITY: "q",
  SEPARATOR: "_"
};

export default TransformationParameters