module.exports = {
    presets: [
        [
            "next/babel",
            {"transform-runtime": { "useESModules": false } }
        ],
    ],
    plugins: [
        ["@babel/plugin-proposal-optional-chaining", { "loose": true } , "react-require"]
    ]
};